# make recursively

MINDEPTH := 2
MAXDEPTH := 2
MAKE := $(MAKE) --print-directory

TARGETS := $(shell for targets in `find . -mindepth $(MINDEPTH) -maxdepth $(MAXDEPTH) -name Makefile`; do echo `dirname $$targets`; done)
CLEANS := $(TARGETS:%=%/clean)

.PHONY: all clean $(TARGETS) $(CLEANS)

all: $(TARGETS)

$(TARGETS):
	$(MAKE) -C $@

clean: $(CLEANS)

$(CLEANS):
	- $(MAKE) -C $(@D) clean

sinclude makefile.option

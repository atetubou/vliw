# 即値の種類
16 bit即値 (set系命令のため)
15 bit unsigned 即値 (絶対アドレスジャンプ指定.)
15 bit即値
 6 bit即値 (比較 + 分岐の即値)


# 用語
"2命令"同時発行のVLIWなので，以下では命令という用語を以下のように使う.
源命令: 32bitで指定される命令.
命令: 源命令2つをセットにしたもの.

# 形式
I    opcode(5)  rs(6)  rt(6)  iv(15)
R    opcode(5)  rs(6)  rt(6)  rd(6)   funct(9)
B    opcode(5)  rs(6)  rt(6)  addr(15)
C    opcode(5)  rs(6)  iv(6)  addr(15)
S    opcode(5)  rs(6)  iv(16) 


# レジスタ
汎用レジスタ, 浮動小数点レジスタそれぞれ64個の合計128個．
コアから見て特殊なレジスタは存在せず，全てのレジスタは対等である．

全てのレジスタの読み出しはrs,rtに対応する部分からのみ行う。
rdから読み出す事はないので、もし誤植を見つけたらいってください。

また、書き込みはrsまたはrdに対応する部分にのみ行います。
これもそれ以外の場所に書き込んでたら、誤植なのでいってください。


# 左右の片方にしか入れられない命令
左のみ、メモリ操作命令、入出力命令
右のみ、分岐命令

# レイテンシ（取り敢えず、以下のようにしてもらえるとうれしいです）
recv 1
ALU 1
FPU 2
load 3
send,store 0

# 命令メモリについて
1命令は32 bit * 2 = 64 bit．
15 bitの絶対アドレス指定としているが，
2^15個の命令をおさめるには，2^15 * 8 byte = 256 KB 必要．
実際にはこのサイズをBlock RAMにのせるのは難しいかもしれない．

仮に2^14命令に収まったとすると，絶対アドレスの指定を14bitに減らすことができる．
すると, 源命令で1bit余らせることができる．（他の即値命令も15bitから1bit切り詰める必要あり）
すなわち各命令で2bit自由に使えるので，これを用いればその命令がストールすべきクロック数を埋め込むことができる．


# ISA

Type, OpCode(5bit), Funct      , Mneomonic, Assembly                    , Meanings                      , Note
-   , 00000       , ---------  , nop      , nop                         , nop                           ,   
I   , 00001       , ---------  , loadi    , loadi   $rs,$rt,iv          , rs = M[rt + iv];              , load, storeは全てワードアドレシングであることに注意。
I   , 00010       , ---------  , storei   , storei  $rs,$rt,iv          , M[rt + iv] = rs;              , 
I   , 00011       , ---------  , floadi   , floadi  $fs,$ft,iv          , fs = M[rt + iv];              , 
I   , 00100       , ---------  , fstorei  , fstorei $fs,$ft,iv          , M[rt + iv] = fs;              ,

I   , 01111       , ---------  , caldi    , caldi   $rs,$rt,iv          , rs = cache[rt + iv];          , load, storeは全てワードアドレシングであることに注意。
I   , 00110       , ---------  , casti    , casti   $rs,$rt,iv          , cache[rt + iv] = rs;          , 
I   , 10110       , ---------  , cafldi   , cafldi  $fs,$ft,iv          , fs = cache[rt + iv];          , 
I   , 10111       , ---------  , cafsti   , cafsti  $fs,$ft,iv          , cache[rt + iv] = fs;          ,

I   , 00101       , ---------  , addi     , addi $rs,$rt,iv             , rs = iv + rt;                 , 即値は左オペランドにくる。(subiとのバランスの観点から）
#I   , 00110       , ---------  , subi     , subi $rs,$rt,iv             , rs = iv - rt;                 , iv = 0とすることで、negの代わりになる。
I   , 00111       , ---------  , shll     , shll $rs,$rt,iv             , rs = rt << iv;                ,
I   , 10101       , ---------  , shra     , shra $rs,$rt,iv             , rs = rt >> iv;                , 
R   , 01000       , ---------  , caldr    , $rd = cache[$rs + $rt]      , rd = cache[rs + rt];              , 
R   , 01010       , ---------  , cafldr   ,                             , fd = cache[rs + rt];              , 
R   , 01011       , ---------  , add      ,                             , rd = rs + rt;                 ,
R   , 01100       , ---------  , sub      ,                             , rd = rs - rt;                 ,
R   , 01101       , 000---NAF  , fadd     ,                             , fd = fs + ft;                 ,
R   , 01101       , 001---NA0  , fmul     ,                             , fd = fs * ft;                 , 
R   , 01101       , 010---NA0  , finv     ,                             , fd =    1/ft;                 , 0除算は未定義（シミュレータはエラーを出すとよい）
R   , 01101       , 011---N0F  , fsqrt    ,                             , fd = sqrt(ft);                , 負の値が入力されたときは未定義。（シミュレータはエラーを出すとよい）
R   , 01101       , 100---NA0  , fmv      ,                             , fd =      ft;                 ,
R   , 10000       , ---------  , recv     ,                             , rd := getchar();              , 入力8bitを代入。
R   , 10001       , ---------  , send     ,                             , putchar(rs & 0xff);           , 下位8bitのみを出力。
R   , 01001       , ---------  , loadinst ,                             , loadinst                      , 命令ロード
R   , 01110       , ---------  , writeinst,                             , wirteinst        　　　　　　 , 命令書き込み 　
S   , 10010       , ---------  , setlo    ,                             , rs = iv;                      , 16bit符号付き即値を、符号拡張して代入する。
S   , 10010       , ---------  , setlabel ,                             , ラベル代入                     , BIOSがsetlabelかsetloか判断するために導入されている。動作はsetloに同じ。
B   , 11000       , ---------  , beq      ,                             , if (rs == rt) goto addr;      , 
B   , 11001       , ---------  , blt      ,                             , if (rs <  rt) goto addr;      , 
B   , 11010       , ---------  , fbeq     ,                             , if (fs == ft) goto addr;      , 
B   , 11011       , ---------  , fblt     ,                             , if (fs <  ft) goto addr;      , 
B   , 11100       , ---------  , jmp      ,                             , goto addr;                    , 15bit絶対アドレスへジャンプ
B   , 11101       , ---------  , jr       ,                             , goto *L[rs];                  , レジスタのさす番地へジャンプ
C   , 11110       , ---------  , beqi     ,                             , if (rs == iv) goto addr;      , 6bit符号付き即値と比較。15bit符号なしアドレスにジャンプ。
C   , 11111       , ---------  , blti     ,                             , if (rs <  iv) goto addr;      ,
#R   , 00101       , -------11  , pstorei  , P[$rs + iv] = $rd          , プログラム領域にstore          ,
#S   , 10011       , ---------  , fsethi   ,                            ,                               , $fdの上位16bitにivを代入. 下位ビットは0を代入すること！
#S   , 10100       , ---------  , fsetlo   ,                            ,                               , $fdの下位16bitにivを代入.
#R   , 01110       , ---------  , itof     ,                             , fd = itof(rs);                , ビット列を転送するだけ。キャストはしない。
#R   , 01111       , ---------  , ftoi     ,                             , rd = ftoi(fs);                , ビット列を転送するだけ。キャストはしない。


#define OPNBLTI        (0b10111)


# FPUの仕様
FPUモジュールのインターフェースは以下の通り。
* ファンクト3bit (Neg bit, Abs bit, FPU bit）をこの順で受け取る。
* 32bitの左オペランドfs, 右オペランドftを受け取る。
* 演算の種類 2bitを受け取る。 (00ならFAdd, 01ならFMul, 10ならFInv, 11ならFSqrt)

FPU bit = 1のとき、演算の前にftの符号を反転する。

Abs bit = 1のとき、出力の符号はNeg bitと等しいとする。

Abs bit = 0のとき、出力の符号は, 演算の結果の符号 xor Neg bitとする。

つまり、FPU bit = 1であればftを -ftに変換する。
Abs, Neg bitについては、例えば、加算のとき、
Abs bit = 1, Neg bit = 1 のときは, -|fs + ft|を返す。
Abs bit = 0, Neg bit = 1 のときは, -(fs + ft)を返す。
Abs bit = 1, Neg bit = 0 のときは,  |fs + ft|を返す。
Abs bit = 0, Neg bit = 0 のときは,   fs + ft を返す。

ただし、FMul, FInvのときのFPU bitは必ず0であるとしてよい。（無視してよい）
FSqrtのとき、Abs bitは必ず0であるとしてよい。（無視してよい）


# アセンブラの文法について
命令の凡例とかを書いてほしい
fib.sを参照
基本的にmeanings と形式にあうように書いておけばよい。

FPUはもう少し複雑で、
例えば fadd した結果をneg してabsとする場合はみたいな形で後ろにオプションfpu,neg,absをくっつければよい。順番は可変。
fadd $r1,$r2,$r3, neg abs


branch のnot版を追加
それぞれの分岐命令のbのあとにnを入れるだけ
beq  -> bneq
blt  -> bnlt
beqi -> bneqi
blti -> bnlt
fbeq -> fbneq
fblt -> fbnlt


フォワーディング

右スロットで発行されたALU命令は左スロットのオペランドrtにフォワーディングされる。
その時に限ってレイテンシ0で発行可能
フォワーディングの対称はshll,shra,addi,add,sub,setlo,setlabelというALU命令のみ

例
nop \ shll r3 r3 1 \ 0   
loadi r3 r3 1 \ nop \ 0  #　右スロットの命令がフォワーディングされることによって、loadiがそくざに発行可能になる
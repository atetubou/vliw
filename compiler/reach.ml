(* 到達定義解析 *)

open Block

module DS =
  DestructiveSet.Make
    (struct
       (* 定義する変数, s_id *)
       type t = Id.t * Id.t
       let compare = compare
     end)

type t = {
  reach    : DS.t M.t;
  gen      : DS.t M.t;
  mask     : DS.t M.t;
  transfer : DS.t -> stmt -> unit;

  (* var2defs : 変数 -> その変数を定義する文集合(リスト)
   * ほとんどSSA形式であって, ある変数を定義する文は多くないのでリストで保持する. *)
  var2defs : stmt list M.t;
}

(* 文単位の伝播関数. 破壊的. *)
let transfer t set stmt = t.transfer set stmt

(* ブロック単位の伝播関数 *)
let transfer_of_block t set b = DS.inter set (M.find b t.mask); DS.union set (M.find b t.gen)

let reach_of b t = DS.copy (M.find b t.reach)


(* setから変数xを定義する文のリストを返す. *)
let def_of t x set =
  if not (M.mem x t.var2defs) then []
  else
    List.filter
      (fun stmt -> DS.mem (x, stmt.s_id) set)
      (M.find x t.var2defs)

(* 集合DS.tをリストに変換 *)
let to_list = DS.to_list
let remove = DS.remove
let add = DS.add

let create cfg =
  Time.start "Reaching definitions analysis";
  let direction = DataFlow.Forward in
  (* 扱う全体集合 = { (定義する変数, 定義文) } を計算 *)
  let universe =
    G.fold_stmt
      (fun stmt -> List.fold_right (fun (x, _) -> DS.S.add (x, stmt.s_id)) stmt.def)
      cfg DS.S.empty
  in
  let creator = DS.creator_of universe in
  let var2defs =
    G.fold_stmt
      (fun stmt acc ->
         List.fold_left
           (fun acc (x, _) ->
              let ls = if M.mem x acc then M.find x acc else [] in
                M.add x (stmt :: ls) acc)
           acc stmt.def)
      cfg M.empty
  in
  (* 文単位の伝播関数を計算する.
   * 到達する定義文のデータフローは,
   * (1) 定義文の定義する変数が, この文で定義されていれば消滅する
   * (2) この文は生成される *)
  let transfer set stmt =
    List.iter (fun (x, _) -> List.iter (fun stmt -> DS.remove (x, stmt.s_id) set) (M.find x var2defs)) stmt.def;
    List.iter (fun (x, _) -> DS.add (x, stmt.s_id) set) stmt.def
  in
  let (gen, mask) = DataFlow.transfer_of_block ~direction ~transfer ~creator cfg in
  (* ブロックの先頭に到達する定義 *)
  let reach = G.fold (fun b _ -> M.add b (creator false)) cfg M.empty in
  let update b pred =
    let this = creator false in
      List.iter
        (fun b' ->
           let temp = DS.copy (M.find b' reach) in
             DS.union temp (M.find b' gen);
             DS.inter temp (M.find b' mask);
             DS.union this temp)
        pred;
      let old = M.find b reach in
      let updated = not (DS.equal this old) in
        DS.move old this;
        updated
  in
    DataFlow.solution_of_block ~direction ~update cfg;
    Time.stop "Reaching definitions analysis";
    {
      reach    = reach;
      gen      = gen;
      mask     = mask;
      transfer = transfer;
      var2defs = var2defs;
    }

(* ステートメント単位の到達定義解析と定義使用解析を計算する
 * useは全域関数とは限らないことに注意. (その文が使われない場合.)*)
let create_for_each_statement cfg =
  let b_reach = create cfg in
  Time.start "reach and use for each statement";
  let reach =
    G.fold
      (fun b stmts acc ->
         let set = reach_of b b_reach in
           List.fold_left
             (fun acc stmt ->
                let this =
                  List.fold_left
                    (fun acc x ->
                       M.add x (def_of b_reach x set) acc)
                    M.empty stmt.use
                in
                  transfer b_reach set stmt;
                  M.add stmt.s_id this acc)
             acc stmts)
      cfg M.empty
  in
  let id2stmt =
    G.fold_stmt
      (fun stmt acc ->
         M.add stmt.s_id stmt acc)
      cfg M.empty
  in
  let use =
    M.fold
      (fun s_id env acc ->
         M.fold
           (fun x ls acc ->
              List.fold_left
                (fun acc stmt ->
                   M.add_ls stmt.s_id (M.find s_id id2stmt) acc)
                acc ls)
           env acc)
      reach M.empty
  in
    Time.stop "reach and use for each statement";
    (reach, use)



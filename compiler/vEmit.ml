(* Vliw.programを出力する *)

open Vliw

(* VLIWの命令の区切り *)
let delimiter = " \\ "

let string_of_half_stmt = function
  | { exp = Nop                               } -> Printf.sprintf "nop"
  | { exp = Set (i)            ; def = [x, _] } -> Printf.sprintf "setlo %s %d" x i
  | { exp = SetLabel (l)       ; def = [x, _] } -> Printf.sprintf "setlabel %s %s" x l
  | { exp = Mov (y)            ; def = [x, _] } -> Printf.sprintf "addi %s %s 0" x y
  | { exp = Add (V (y), z)     ; def = [x, _] } -> Printf.sprintf "add %s %s %s" y z x
  | { exp = Add (C (i), z)     ; def = [x, _] } -> Printf.sprintf "addi %s %s %d" x z i
  | { exp = Sub (V (y), z)     ; def = [x, _] } -> Printf.sprintf "sub %s %s %s" y z x
  | { exp = Sub (C (i), z)     ; def = [x, _] } -> Printf.sprintf "subi %s %s %d" x z i
  | { exp = ShiftL (y, i)      ; def = [x, _] } -> Printf.sprintf "shll %s %s %d" x y i
  | { exp = ShiftR (y, i)      ; def = [x, _] } -> Printf.sprintf "shra %s %s %d" x y i
  | { exp = Load (y, i)        ; def = [x, _] } -> Printf.sprintf "loadi %s %s %d" x y i
  | { exp = FLoad (y, i)       ; def = [x, _] } -> Printf.sprintf "floadi %s %s %d" x y i
  | { exp = Store (x, y, i)                   } -> Printf.sprintf "storei %s %s %d" x y i
  | { exp = FStore (x, y, i)                  } -> Printf.sprintf "fstorei %s %s %d" x y i
  | { exp = LoadC (y, V (z))   ; def = [x, _] } -> Printf.sprintf "caldr %s %s %s" y z x
  | { exp = FLoadC (y, V (z))  ; def = [x, _] } -> Printf.sprintf "cafldr %s %s %s" y z x
  | { exp = LoadC (y, C (i))   ; def = [x, _] } -> Printf.sprintf "caldi %s %s %d" x y i
  | { exp = FLoadC (y, C (i))  ; def = [x, _] } -> Printf.sprintf "cafldi %s %s %d" x y i
  | { exp = StoreC (x, y, i)                  } -> Printf.sprintf "casti %s %s %d" x y i
  | { exp = FStoreC (x, y, i)                 } -> Printf.sprintf "cafsti %s %s %d" x y i
  | { exp = FMov (y, funct)    ; def = [x, _] } -> Printf.sprintf "fmv 0 %s %s %s" y x (string_of_funct funct)
  | { exp = FAdd (y, z, funct) ; def = [x, _] } -> Printf.sprintf "fadd %s %s %s %s" y z x (string_of_funct funct)
  | { exp = FMul (y, z, funct) ; def = [x, _] } -> Printf.sprintf "fmul %s %s %s %s" y z x (string_of_funct funct)
  | { exp = FInv (y, funct)    ; def = [x, _] } -> Printf.sprintf "finv 0 %s %s %s" y x (string_of_funct funct)
  | { exp = FSqrt (y, funct)   ; def = [x, _] } -> Printf.sprintf "fsqrt 0 %s %s %s" y x (string_of_funct funct)
  | { exp = Send (x)                          } -> Printf.sprintf "send %s 0 0" x
  | { exp = Recv               ; def = [x, _] } -> Printf.sprintf "recv 0 0 %s" x
  | { exp = Call (l, _)                       } -> Printf.sprintf "jmp 0 0 %s" l
  | { exp = JumpTo (x)                        } -> Printf.sprintf "jr %s 0 0" x
(*  | _                                           -> assert false (* 分岐などはここで出力しない *)*)
  | stmt -> Format.eprintf "%s is not consistent!@." (string_of_half_stmt stmt); assert false

(* ジャンプ先が必要な命令のstring *)
let string_of_branch v = function
  | { exp = IfEq  (x, V (y)) } -> Printf.sprintf "beq %s %s %s" x y v
  | { exp = IfEq  (x, C (i)) } -> Printf.sprintf "beqi %s %d %s" x i v
  | { exp = IfLt  (x, V (y)) } -> Printf.sprintf "blt %s %s %s" x y v
  | { exp = IfLt  (x, C (i)) } -> Printf.sprintf "blti %s %d %s" x i v
  | { exp = IfFEq (x, y) } -> Printf.sprintf "fbeq %s %s %s" x y v
  | { exp = IfFLt (x, y) } -> Printf.sprintf "fblt %s %s %s" x y v
  | { exp = Jmp } -> Printf.sprintf "jmp 0 0 %s" v
  | _ -> assert false


let emit_label oc label = Printf.fprintf oc "%s:\n" label

let emit_double oc s1 s2 stall =
  Printf.fprintf oc
    "\t%30s %s %30s %s %d\n"
    s1
    delimiter
    s2
    delimiter
    stall

let rec emit_stmt oc = function
  | { left = { exp = Entry } } -> ()
  | { left = { exp = Call (l, _) } } as stmt ->
      let ret = Id.genid "call" in
        emit_double oc
          (string_of_half_stmt (gen_half_stmt [Asm.link, Type.Int] (SetLabel (ret))))
          (string_of_branch l (gen_half_stmt [] Jmp))
          stmt.stall;
        emit_stmt oc { (gen_stmt [] Nop) with stall = 1 };
        emit_label oc ret
  | stmt ->
        emit_double oc
          (string_of_half_stmt stmt.left)
          (string_of_half_stmt stmt.right)
          stmt.stall

let emit_branch oc v stmt =
  emit_double oc
    (string_of_half_stmt stmt.left)
    (string_of_branch v stmt.right)
    stmt.stall

(* 遅延スロットをnopで埋めたjmpを出力 *)
let emit_jmp oc v =
  emit_double oc
    (string_of_half_stmt (gen_half_stmt [] Nop))
    (string_of_branch v (gen_half_stmt [] Jmp))
    0;
  emit_stmt oc { (gen_stmt [] Nop) with stall = 1 }




let g oc prog l = (* 関数lのemit *)
  let { cfg; v_entry } = M.find l prog.entries in
  let rec dfs vis v =
    if S.mem v vis then vis
    else
      let vis = S.add v vis in
      emit_label oc v;
      let (stmts, branch, delay) = partition_block (G.find v cfg) in
      List.iter (emit_stmt oc) stmts;
      let vis =
        (match branch.right with
           | { exp = (IfEq _ | IfLt _ | IfFEq _ | IfFLt _) } ->
               let b_then = G.succ_of v Then cfg in
               let b_else = G.succ_of v Else cfg in
                 emit_branch oc b_then branch;
                 List.iter (emit_stmt oc) delay;
                 if S.mem b_else vis then
                   (emit_jmp oc b_else;
                    vis)
                 else
                   dfs vis b_else
           | { exp = Jmp } ->
               let w = G.succ_of v Other cfg in
                 emit_branch oc w branch;
                 List.iter (emit_stmt oc) delay;
                 vis
                 (* 
                 if S.mem w vis then
                   (emit_branch oc w branch;
                    List.iter (emit_stmt oc) delay;
                    vis)
                 else
                   (List.iter (emit_stmt oc) delay;
                   dfs vis w)*)
           | _ ->
               emit_stmt oc branch;
               List.iter (emit_stmt oc) delay;
               vis)
      in
        (* 残りの後続ブロックを出力 *)
        List.fold_left dfs vis (G.succ v cfg)
  in
    emit_label oc l;
    ignore (dfs S.empty v_entry)

(* メイン関数funcに対して, Haltのためのブロックを作り, Returnを変える*)
let add_halt func =
  let cfg = func.cfg in
  let halt = Id.genid "halt" in
  let cfg = G.add_vertex halt (VListScheduling.fill_delay_slot [gen_stmt [] Jmp]) cfg in
  let cfg = G.add_edge halt halt Other cfg in
  let cfg =
    G.fold
      (fun b stmts cfg ->
         if List.exists (function { right = { exp = Return } } -> true | _ -> false) stmts then
           G.add_edge b halt Other cfg
         else
           cfg)
      cfg cfg
  in
  let cfg =
    G.map_stmt
      (map_half
         (function
            | { exp = Return } -> gen_half_stmt [] Jmp
            | half -> half))
      cfg
  in
    { func with cfg = cfg }

let f oc prog =
  Format.eprintf "# Emit assembly of VLIW@.";
  (* Returnを処理する *)
  let prog = { prog with entries = M.add Id.main_label (add_halt (M.find Id.main_label prog.entries)) prog.entries } in
  let prog = map_cfg (fun _ -> G.map_stmt (map_half (function { exp = Return } -> gen_half_stmt [] (JumpTo (Asm.link)) | stmt -> stmt))) prog in

  (* 埋まらなかった遅延スロットを埋めておく *)
  let prog = map_cfg (fun _ -> G.map (fun _ -> VListScheduling.fill_delay_slot)) prog in
    
  let prog = VListScheduling.validate_stall prog in
  check_consistency prog;

  (* 始めにmainから出力する. *)
  let ls = M.to_keys (M.remove Id.main_label prog.entries) in
  let ls = Id.main_label :: ls in
    List.iter (g oc prog) ls;

    Format.eprintf "Compiling has been successfully done.@."


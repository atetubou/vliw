(* VLIWのための生存解析 *)

open Vliw

type t = S.t ref M.t

(* 文単位の伝播関数. 副作用なし. *)
let transfer ?(is_target = (fun x -> true)) stmt set =
  List.fold_left
    (fun acc x -> if is_target x then S.add x acc else acc)
    (List.fold_left
       (fun acc (x, _) -> S.remove x acc)
       set (def_of stmt))
    (use_of stmt)

(* block -> S.tに変換する. *)
let to_map t = M.map (fun ss -> !ss) t

(* 出口生存な変数の集合. *)
let live_of b t = !(M.find b t)

(* stmtsの出口生存な集合setを受け取って, stmtsの入口生存な集合を計算する. *)
let live_in_of stmts set = List.fold_right transfer stmts set

(* ブロックbの出口生存なものにxを追加 *)
let add b x t =
  let this = M.find b t in
    this := S.add x !this

(* ブロックbの出口生存の情報をb'へコピーする *)
let copy_to b b' t = M.add b' (ref !(M.find b t)) t

let create ?(is_target = (fun x -> true)) cfg =
  Time.start "Liveness analysis";
  let direction = VDataFlow.Backward in
  (* block -> そのブロックで定義される変数の集合 *)
  let def =
    G.fold
      (fun b stmts acc ->
         M.add b
           (List.fold_left
              (fun def stmt ->
                 List.fold_right (fun (x, _) -> S.add x) (def_of stmt) def)
              S.empty stmts)
           acc)
      cfg M.empty
  in
  (* block -> そのブロック内において、先行命令によって定義されない変数の集合 *)
  let use =
    G.fold
      (fun b stmts acc ->
         M.add b
           (fst
              (List.fold_left
                (fun (use, env) stmt ->
                   let use = S.union (S.diff (S.of_list (use_of stmt)) env) use in
                   let env = List.fold_right (fun (x, _) -> S.add x) (def_of stmt) env in
                     (use, env))
                (S.empty, S.empty) stmts))
           acc)
      cfg M.empty
  in
  (* is_targetがtrueなる変数のみ取り出す. *)
  let def = M.map (S.filter is_target) def in
  let use = M.map (S.filter is_target) use in
  let live =
    let live = G.fold (fun b _ acc -> M.add b (ref S.empty) acc) cfg M.empty in
    let update b succ =
      let this =
        List.fold_left
          (fun acc b' -> S.union acc (S.union (S.diff !(M.find b' live) (M.find b' def)) (M.find b' use)))
          S.empty succ
      in
      let old = M.find b live in
      let updated = not (S.equal this !old) in
        old := this;
        updated
    in
      VDataFlow.solution_of_block ~direction ~update cfg;
      live
  in
    Time.stop "Liveness analysis";
    (* プログラムの入口で生存する変数集合は空か *)
    G.iter
      (fun b _ ->
         if G.in_degree_of b cfg = 0 then
           let set = S.union (S.diff !(M.find b live) (M.find b def)) (M.find b use) in
             if not (S.is_empty set) then
               (Format.eprintf "WARNING: Live at entry: %s@." (String.concat ", " (S.to_list set))))
      cfg;
    live




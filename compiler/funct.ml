(* データフロー解析を用いて浮動小数点のfunctを利用する. *)

open Block

(* リスト表現ではなく，わかりやすいレコードで表現する *)
type fpu_funct = { fpu : bool; neg : bool; abs : bool }

(* リスト表現からレコード表現への変換 *)
let of_funct funct = { fpu = List.mem FPUBit funct; neg = List.mem NegBit funct; abs = List.mem AbsBit funct }

(* of_functの逆変換 *)
let to_funct f =
  (if f.fpu then [FPUBit] else []) @
  (if f.neg then [NegBit] else []) @
  (if f.abs then [AbsBit] else [])

(* 式からfunctを取り出す *)
let extract_funct = function
  | FMov (_, funct) | FAdd (_, _, funct) | FMul (_, _, funct) | FInv (_, funct) | FSqrt (_, funct) -> of_funct funct
  | _ -> assert false


(* fのneg, absをf'と組み合わせる *)
let combine_funct f f' =
  let () = assert (not f.fpu) in
  let res =
    if f.abs then { f with fpu = f'.fpu }
    else
      { f' with neg = f.neg <> f'.neg }
  in
    to_funct res

(* 利用可能式 a <- exp' を用いてexpを書き直す.
 * expはaを使用することが保証されている. *)
let combine exp a exp' =
  (match exp' with
     | FMov _ | FAdd _ | FMul _ | FInv _ | FSqrt _ -> ()
     | _ ->
         Format.eprintf "unexpected %s@." (string_of_stmt (gen_stmt [a, Type.Float] exp')))
    ;
  let f = extract_funct exp in
  let f' = extract_funct exp' in
    match (exp, exp') with
      | FMov (a, _), FMov (x, _) -> FMov (x, combine_funct f f')
      | FMov (a, _), FAdd (x, y, _) -> FAdd (x, y, combine_funct f f')
      | FMov (a, _), FMul (x, y, _) -> FMul (x, y, combine_funct f f')
      | FMov (a, _), FInv (x, _) -> FInv (x, combine_funct f f')
      | FMov (a, _), FSqrt (x, _) -> FSqrt (x, combine_funct f f')

      | FAdd (a', b, _), FMov (x, _) when a = a' && not (f'.abs) ->
          (* FMovのFPU bitは常にfalseなはず. (Neg bitで代用できるから) *)
          assert (not (f'.fpu));
          let f =
            if f.fpu = f'.neg then
              (* add *)
              (if f.abs then { f with fpu = false }
               else { f with fpu = false; neg = f.fpu <> f.neg })
            else
              (* sub *)
              (if f.abs then { f with fpu = true }
               else { f with fpu = true; neg = f'.neg <> f.neg })
          in
            FAdd (x, b, to_funct f)
      | FAdd (b, a', _), FMov (x, _) when a = a' && not (f'.abs) ->
          assert (not (f'.fpu));
          let f = { f with fpu = f.fpu <> f'.neg } in
            FAdd (b, x, to_funct f)

      | FMul (a', b, _), FMov (x, _) when a = a' && not (f'.abs) ->
          assert (not (f.fpu));
          assert (not (f'.fpu));
          let f =
            if f.abs then f
            else { f with neg = f.neg <> f'.neg }
          in
            FMul (x, b, to_funct f)
      | FMul (b, a', _), FMov (x, _) when a = a' && not (f'.abs) ->
          assert (not (f.fpu));
          assert (not (f'.fpu));
          let f =
            if f.abs then f
            else { f with neg = f.neg <> f'.neg }
          in
            FMul (b, x, to_funct f)

      | FInv (a, _), FMov (x, _) when not (f'.abs) ->
          assert (not (f.fpu));
          assert (not (f'.fpu));
          let f =
            if f.abs then f
            else { f with neg = f.neg <> f'.neg }
          in
            FInv (x, to_funct f)

      | FSqrt (a, _), FMov (x, _) when not (f'.abs) ->
          (* Sqrtはabs bitに意味がないので常にfalseなはず. *)
          assert (not (f.abs));
          assert (not (f'.fpu));
          let f = { f with fpu = f.fpu <> f'.neg } in
            FSqrt (x, to_funct f)

      (* 逆数の逆数は恒等変換 *)
      | FInv (a, _), FInv (x, _) ->
          let f =
            if f.abs then f
            else { f' with neg = f.neg <> f'.neg }
          in
            FMov (x, to_funct f)

      | _ -> exp

  
let is_target = function
  | { exp = FMov _ | FAdd _ | FMul _ | FInv _ | FSqrt _ } -> true
  | _ -> false

let g cfg =
  let avail = Avail.create ~is_target cfg in
    G.map
      (fun b stmts ->
         let set = Avail.avail_of b avail in
           List.map
             (fun stmt ->
                let stmt' =
                  if is_target stmt then
                    let exp =
                      List.fold_left
                        (fun exp x ->
                           match Avail.def_of avail x set with
                             | Some (exp') -> combine exp x exp'
                             | _ -> exp)
                        stmt.exp stmt.use
                    in
                      { (gen_stmt stmt.def exp) with s_id = stmt.s_id }
                  else stmt
                in
                  Avail.transfer avail set stmt;
                  stmt')
             stmts)
      cfg

let f prog = map_cfg (fun l cfg -> g cfg) prog



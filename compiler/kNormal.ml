(* give names to intermediate values (K-normalization) *)

type t = (* K正規化後の式 (caml2html: knormal_t) *)
  | Unit
  | Int of int
  | Float of float
  | Add of Id.t * Id.t
  | Sub of Id.t * Id.t
  | ShiftL of Id.t * int
  | ShiftR of Id.t * int
  | FNeg of Id.t
  | FAbs of Id.t
  | FAdd of Id.t * Id.t
  | FSub of Id.t * Id.t
  | FMul of Id.t * Id.t
  | FInv of Id.t
  | IfEq of Id.t * Id.t * t * t (* 比較はlist or int or boolのみ *)
  | IfLE of Id.t * Id.t * t * t
  | Let of (Id.t * Type.t) * t * t
  | Var of Id.t
  | LetRec of fundef * t
  | App of Id.t * Id.t list
  | Tuple of Id.t list
  | LetTuple of (Id.t * Type.t) list * Id.t * t
  | Get of Id.t * Id.t
  | Put of Id.t * Id.t * Id.t
  | ExtArray of Id.t
  | ExtFunApp of Id.t * Id.t list
and fundef = { name : Id.t * Type.t; args : (Id.t * Type.t) list; body : t }

(* 関数がエスケープするか？ *)
let free_occur_of_function x e =
  let rec check = function
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2)
    | Let (_, e1, e2) | LetRec ({ body = e1 }, e2) -> check e1 || check e2
    | LetTuple (_, _, e) -> check e
    | Var y | Put (_, _, y) -> x = y
    | App (_, ys) | ExtFunApp (_, ys) | Tuple ys -> List.exists (fun y -> x = y) ys
    | _ -> false
  in
    check e

let rec fv = function (* 式に出現する（自由な）変数 (caml2html: knormal_fv) *)
  | Unit | Int(_) | Float(_) | ExtArray(_) -> S.empty
  | FNeg(x) | FAbs(x) | FInv(x) | ShiftL(x, _) | ShiftR(x, _) -> S.singleton x
  | Add(x, y) | Sub(x, y) | FAdd(x, y) | FSub(x, y) | FMul(x, y) | Get(x, y) -> S.of_list [x; y]
  | IfEq(x, y, e1, e2) | IfLE(x, y, e1, e2) -> S.add x (S.add y (S.union (fv e1) (fv e2)))
  | Let((x, t), e1, e2) -> S.union (fv e1) (S.remove x (fv e2))
  | Var(x) -> S.singleton x
  | LetRec({ name = (x, t); args = yts; body = e1 }, e2) ->
      let zs = S.diff (fv e1) (S.of_list (List.map fst yts)) in
      S.diff (S.union zs (fv e2)) (S.singleton x)
  | App(x, ys) -> S.of_list (x :: ys)
  | Tuple(xs) | ExtFunApp(_, xs) -> S.of_list xs
  | Put(x, y, z) -> S.of_list [x; y; z]
  | LetTuple(xs, y, e) -> S.add y (S.diff (fv e) (S.of_list (List.map fst xs)))

let rec size = function
  | IfEq(_, _, e1, e2) | IfLE(_, _, e1, e2)
  | Let(_, e1, e2) | LetRec({ body = e1 }, e2) -> 1 + size e1 + size e2
  | LetTuple(_, _, e) -> 1 + size e
  | _ -> 1


let insert_let (e, t) k = (* letを挿入する補助関数 (caml2html: knormal_insert) *)
  match e with
  | Var(x) -> k x
  | _ ->
      let x = Id.gentmp t in
      let e', t' = k x in
      Let((x, t), e, e'), t'

let rec g env = function (* K正規化ルーチン本体 (caml2html: knormal_g) *)
  | Syntax.PosHint (_, e) -> g env e
  | Syntax.Unit -> Unit, Type.Unit
  | Syntax.Bool(b) -> Int(if b then 1 else 0), Type.Int (* 論理値true, falseを整数1, 0に変換 (caml2html: knormal_bool) *)
  | Syntax.Int(i) -> Int(i), Type.Int
  | Syntax.Float(d) -> Float(d), Type.Float
  | Syntax.Not(e) -> g env (Syntax.If(e, Syntax.Bool(false), Syntax.Bool(true)))
  | Syntax.Add(e1, e2) -> (* 足し算のK正規化 (caml2html: knormal_add) *)
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> Add(x, y), Type.Int))
  | Syntax.Sub(e1, e2) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> Sub(x, y), Type.Int))
  | Syntax.ShiftL(e1, i) ->
      insert_let (g env e1) (fun x -> ShiftL(x, i), Type.Int)
  | Syntax.ShiftR(e1, i) ->
      insert_let (g env e1) (fun x -> ShiftR(x, i), Type.Int)
  | Syntax.FNeg(e) ->
      insert_let (g env e)
	(fun x -> FNeg(x), Type.Float)
  | Syntax.FAdd(e1, e2) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FAdd(x, y), Type.Float))
  | Syntax.FSub(e1, e2) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FSub(x, y), Type.Float))
  | Syntax.FMul(e1, e2) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FMul(x, y), Type.Float))
  | Syntax.FDiv(e1, e2) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> insert_let (FInv y, Type.Float)
                 (fun z -> FMul(x, z), Type.Float)))
  | Syntax.Eq _ | Syntax.LE _ as cmp ->
      g env (Syntax.If(cmp, Syntax.Bool(true), Syntax.Bool(false)))
  | Syntax.If(Syntax.Not(e1), e2, e3) -> g env (Syntax.If(e1, e3, e2)) (* notによる分岐を変換 (caml2html: knormal_not) *)
  | Syntax.If(Syntax.Eq(e1, e2), e3, e4) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y ->
	      let e3', t3 = g env e3 in
	      let e4', t4 = g env e4 in
	      IfEq(x, y, e3', e4'), t3))
  | Syntax.If(Syntax.LE(e1, e2), e3, e4) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y ->
	      let e3', t3 = g env e3 in
	      let e4', t4 = g env e4 in
	      IfLE(x, y, e3', e4'), t3))
  | Syntax.If(e1, e2, e3) -> g env (Syntax.If(Syntax.Eq(e1, Syntax.Bool(false)), e3, e2)) (* 比較のない分岐を変換 (caml2html: knormal_if) *)
  | Syntax.Let((x, t), e1, e2) ->
      let e1', t1 = g env e1 in
      let e2', t2 = g (M.add x t env) e2 in
      Let((x, t), e1', e2'), t2
  | Syntax.Var(x) when M.mem x env -> Var(x), M.find x env
  | Syntax.Var(x) -> (* 外部配列の参照 (caml2html: knormal_extarray) *)
      (match M.find x !Typing.extenv with
      | Type.Array(_) as t -> ExtArray x, t
      | _ -> failwith (Printf.sprintf "external variable %s does not have an array type" x))
  | Syntax.LetRec({ Syntax.name = (x, t); Syntax.args = yts; Syntax.body = e1 }, e2) ->
      let env' = M.add x t env in
      let e2', t2 = g env' e2 in
      let e1', t1 = g (M.add_list yts env') e1 in
      LetRec({ name = (x, t); args = yts; body = e1' }, e2'), t2
  | Syntax.App(e1, e2s) ->
      (match Syntax.delete_hint e1 with
         | Syntax.Var(f) when not (M.mem f env) -> (* 外部関数の呼び出し (caml2html: knormal_extfunapp) *)
             (match (f, e2s) with
                | ("abs_float", [e]) | ("fabs", [e]) -> insert_let (g env e) (fun x -> FAbs(x), Type.Float)
                | _ ->
             (match M.find f !Typing.extenv with
             | Type.Fun(_, t) ->
             let rec bind xs = function (* "xs" are identifiers for the arguments *)
               | [] -> ExtFunApp(f, xs), t
               | e2 :: e2s ->
           	insert_let (g env e2)
           	  (fun x -> bind (xs @ [x]) e2s) in
             bind [] e2s (* left-to-right evaluation *)
             | _ -> assert false))
         | _ ->
             (match g env e1 with
             | _, Type.Fun(_, t) as g_e1 ->
       	  insert_let g_e1
       	    (fun f ->
       	      let rec bind xs = function (* "xs" are identifiers for the arguments *)
       		| [] -> App(f, xs), t
       		| e2 :: e2s ->
       		    insert_let (g env e2)
       		      (fun x -> bind (xs @ [x]) e2s) in
       	      bind [] e2s) (* left-to-right evaluation *)
             | _ -> assert false))
  | Syntax.Tuple(es) ->
      let rec bind xs ts = function (* "xs" and "ts" are identifiers and types for the elements *)
	| [] -> Tuple(xs), Type.Tuple(ts)
	| e :: es ->
	    let _, t as g_e = g env e in
	    insert_let g_e
	      (fun x -> bind (xs @ [x]) (ts @ [t]) es) in
      bind [] [] es
  | Syntax.LetTuple(xts, e1, e2) ->
      insert_let (g env e1)
	(fun y ->
	  let e2', t2 = g (M.add_list xts env) e2 in
	  LetTuple(xts, y, e2'), t2)
  | Syntax.Array(e1, e2) ->
      insert_let (g env e1)
	(fun x ->
	  let _, t2 as g_e2 = g env e2 in
	  insert_let g_e2
	    (fun y ->
	      let l =
		match t2 with
		| Type.Float -> "create_float_array"
		| _ -> "create_array" in
	      ExtFunApp(l, [x; y]), Type.Array(t2)))
  | Syntax.ArrayInit(e1, e2) ->
      insert_let (g env e1)
        (fun x ->
          match g env e2 with
            | _, Type.Fun([Type.Int], t) as g_e2 ->
                insert_let g_e2
                  (fun y ->
                  let f = Id.genid "INIT" in
                  let a = Id.gentmp (Type.Array t) in
                  let i = Id.gentmp Type.Int in
                  let n = Id.gentmp Type.Int in
                  let t' = Type.Fun([Type.Array t; Type.Int; Type.Int], Type.Array t) in
                    LetRec ({ name = (f, t');
                              args = [(a, Type.Array t); (i, Type.Int); (n, Type.Int)];
                              body =
                              IfLE (n, i, Var(a),
                                     fst (
                                     insert_let (App (y, [i]), t)
                                     (fun z ->
                                       insert_let (Put (a, i, z), Type.Unit)
                                       (fun _ ->
                                         insert_let (Int 1, Type.Int)
                                         (fun one ->
                                           insert_let (Add (i, one), Type.Int)
                                           (fun i' ->
                                               App (f, [a; i'; n]), Type.Array t))))))
                            },
                             fst (
                               insert_let (Int 0, Type.Int)
                               (fun zero ->
                                 insert_let (ExtFunApp ("create_array", [x;x]), Type.Array t) (* Note: float array may be created by 'create_array', but this works. *)
                                 (fun ff ->
                                   App (f, [ff; zero; x]), Type.Array t))))
                    , Type.Array t)
            | _ -> assert false)
  | Syntax.ArrayAllocate (e, typ) ->
      insert_let (g env e)
        (fun x ->
           ExtFunApp ("allocate", [x]), Type.Array (typ))
  | Syntax.Get(e1, e2) ->
      (match g env e1 with
      |	_, Type.Array(t) as g_e1 ->
	  insert_let g_e1
	    (fun x -> insert_let (g env e2)
		(fun y -> Get(x, y), t))
      | _ -> assert false)
  | Syntax.Put(e1, e2, e3) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> insert_let (g env e3)
		(fun z -> Put(x, y, z), Type.Unit)))
  | Syntax.Nil t -> Int (-1), t
  | Syntax.Cons (e1, e2) ->
      let (_, t) as g_e1 = g env e1 in
        insert_let g_e1
          (fun x -> insert_let (g env e2)
            (fun y -> Tuple [x; y], Type.List t))
  | Syntax.Head e ->
      (match g env e with
        | _, Type.List t as g_e ->
            insert_let g_e
              (fun x ->
                let y = Id.gentmp t in
                  LetTuple ([(y, t); (Id.gentmp t, Type.List t)], x, Var y), t)
        | _ -> assert false)
  | Syntax.Tail e ->
      (match g env e with
        | _, Type.List t as g_e ->
            insert_let g_e
              (fun x ->
                let y = Id.gentmp t in
                  LetTuple ([(Id.gentmp t, t); (y, Type.List t)], x, Var y), Type.List t)
        | _ -> assert false)
  | Syntax.Ref e ->
      let (_, t) as g_e = g env e in
        insert_let g_e
          (fun x ->
            (insert_let (Int 1, Type.Int)
              (fun y -> ExtFunApp (Type.create_array_of t, [y; x]), Type.Array t)))
  | Syntax.Deref e ->
      (match g env e with
        | _, Type.Array t as g_e ->
            insert_let g_e
              (fun x ->
                (insert_let (Int 0, Type.Int)
                  (fun y -> Get (x, y), t)))
        | _ -> assert false)
  | Syntax.Subst (e1, e2) ->
      let (_, t) as g_e2 = g env e2 in
        insert_let (g env e1)
          (fun x ->
            (insert_let g_e2
              (fun y ->
                (insert_let (Int 0, Type.Int)
                  (fun z -> Put (x, z, y), Type.Unit)))))

let f e = fst (g M.empty e)


let merge x y = (fst x @ fst y, snd x @ snd y)

let rec constants = function
  | Int n -> ([n], [])
  | Float f -> ([], [f])
  | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) | LetRec ({body = e1}, e2) -> merge (constants e1) (constants e2)
  | LetTuple (_, _, e) -> constants e
  | _ -> ([], [])
(* for debug output *)
let string_of tree =
  let rec sub tree depth =
    Common.indent depth ^ (
      match tree with
        | Unit -> "Unit\n"
        | Int n -> "Int("^string_of_int n^")\n"
        | Float f -> "Float("^string_of_float f^")\n"
        | Add (s, t) -> "Add " ^ s ^ " " ^ t ^ "\n"
        | Sub (s, t) -> "Sub " ^ s ^ " " ^ t ^ "\n"
        | ShiftL (s, i) -> "ShiftL " ^ s ^ " " ^ string_of_int i ^ "\n"
        | ShiftR (s, i) -> "ShiftR " ^ s ^ " " ^ string_of_int i ^ "\n"
        | FNeg s -> "FNeg " ^ s ^ "\n"
        | FAbs s -> "FAbs " ^ s ^ "\n"
        | FAdd (s, t) -> "FAdd " ^ s ^ " " ^ t ^ "\n"
        | FSub (s, t) -> "FSub " ^ s ^ " " ^ t ^ "\n"
        | FMul (s, t) -> "FMul " ^ s ^ " " ^ t ^ "\n"
        | FInv s -> "FInv " ^ s ^ "\n"
        | IfEq (s, t, e1, e2) ->
            "If " ^ s ^ " = " ^ t ^ " then\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub e2 (depth+1)
        | IfLE (s, t, e1, e2) ->
            "If " ^ s ^ " <= " ^ t ^ " then\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub e2 (depth+1)
        | Let ((s, t), e1, e2) ->
            "Let " ^ s ^ " : " ^ Type.string_of t ^ " =\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "in\n" ^
            sub e2 (depth(*+1*))
        | Var x -> x ^ "\n"
        | LetRec ({ name = (x, t); args = ls; body = e1 }, e2) ->
            "LetRec " ^ (String.concat " " (x :: List.map fst ls)) ^ " : " ^ Type.string_of t ^" =\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "in\n" ^
            sub e2 (depth(*+1*))
        | App (s, ls) ->
            "App " ^ s ^ " to (" ^ (String.concat ", " ls) ^ ")\n"
        | Tuple ls ->
            "Tuple (" ^ (String.concat ", " ls) ^ ")\n"
        | LetTuple (ls, s, e) ->
            "LetTuple (" ^ (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) ls)) ^ ") = " ^ s ^ " in\n" ^
            sub e (depth(*+1*))
        | Get (s, t) -> "Get " ^ s ^ ".(" ^ t ^ ")\n"
        | Put (s, t, u) -> "Put " ^ s ^ ".(" ^ t ^ ") = " ^ u ^ "\n"
        | ExtArray s -> "ExtArray " ^ s ^ "\n"
        | ExtFunApp (s, ls) -> "ExtFunApp " ^ s ^ " (" ^ (String.concat ", " ls) ^ ")\n"
    )
  in
    sub tree 0

let output path tree =
  let str = string_of tree in
  let out = open_out path in
    output_string out str;
    close_out out;
    tree


open KNormal
(* 逆ループアンローリング. 
 * let () = M(0) in
 * let () = M(1) in
 * ...
 * let () = M(n-1) in
 *   M(n)
 * の形の関数を、ループ構造に変換する。 *)

let minimum_target = 4 (* 長さ4以上のループにしかRollingしない *)

exception Giveup (* 逆ループアンローリング変換を諦める *)

let rec split_unit_lets = function (* Type.Unitをつなげる *)
  | Let ((_, Type.Unit), e1, e2) -> e1 :: split_unit_lets e2
  | e -> [e]

let replace p e = (* eの部分式e'でありp e'なる現れを"穴"にした式を返す *)
  let rec f = function
    | e1 when p e1 -> (fun e -> e) (* p e1ならば穴を返す *)
    | IfEq (x, y, e1, e2) -> (fun e -> IfEq (x, y, f e1 e, f e2 e))
    | IfLE (x, y, e1, e2) -> (fun e -> IfLE (x, y, f e1 e, f e2 e))
    | Let ((x, t), e1, e2) -> (fun e -> Let ((x, t), f e1 e, f e2 e))
    | LetRec ({ body = e1 } as fundef, e2) ->
        (fun e -> LetRec ({ fundef with body = f e1 e }, f e2 e))
    | LetTuple (xts, y, e1) -> (fun e -> LetTuple (xts, y, f e1 e))
    | e1 -> (fun e -> e1)
  in
    f e

let check_identity e1 e2 = (* 内部で定義されるテンポラリ変数を除いて同一の式であるか *)
  let is_equal x1 x2 env = x1 = x2 || (M.mem x1 env && M.find x1 env = x2) in (* x1は複数のx2と等しくならないことを仮定。 *)
  let identify_with x1 x2 env = M.add x1 x2 env in
  let rec f env = function
    | (Add (x1, y1), Add (x2, y2))
    | (Sub (x1, y1), Sub (x2, y2))
    | (FAdd (x1, y1), FAdd (x2, y2))
    | (FSub (x1, y1), FSub (x2, y2))
    | (FMul (x1, y1), FMul (x2, y2))
    | (Get (x1, y1), Get (x2, y2))
      ->
        is_equal x1 x2 env &&
        is_equal y1 y2 env
    | (ShiftL (x1, i1), ShiftL (x2, i2))
    | (ShiftR (x1, i1), ShiftR (x2, i2))
      ->
        is_equal x1 x2 env &&
        i1 = i2
    | (FNeg (x1), FNeg (x2))
    | (FAbs (x1), FAbs (x2))
    | (FInv (x1), FInv (x2))
      ->
        is_equal x1 x2 env
    | (IfEq (x1, y1, d1, e1), IfEq (x2, y2, d2, e2))
    | (IfLE (x1, y1, d1, e1), IfLE (x2, y2, d2, e2))
      ->
        is_equal x1 x2 env &&
        is_equal y1 y2 env &&
        f env (d1, d2) &&
        f env (e1, e2)
    | (Let ((x1, t1), d1, e1), Let ((x2, t2), d2, e2))
      ->
        t1 = t2 &&
        f env (d1, d2) &&
        (let env = identify_with x1 x2 env in
           f env (e1, e2))
    | (Var (x1), Var (x2)) ->
        is_equal x1 x2 env
    | (LetRec _, LetRec _) -> false (* [XXX] 関数定義が内部にある場合は無視する *)
    | (App (x1, ys1), App (x2, ys2))
    | (ExtFunApp (x1, ys1), ExtFunApp (x2, ys2)) ->
        x1 = x2 && (* 関数名は完全に同じでないといけない. (内部で定義されないため) *)
        List.for_all2 (fun y1 y2 -> is_equal y1 y2 env) ys1 ys2
    | (Tuple (xs1), Tuple (xs2)) ->
        List.for_all2 (fun x1 x2 -> is_equal x1 x2 env) xs1 xs2
    | (Put (x1, y1, z1), Put (x2, y2, z2)) ->
        is_equal x1 x2 env &&
        is_equal y1 y2 env &&
        is_equal z1 z2 env
    | (LetTuple (xts1, y1, e1), LetTuple (xts2, y2, e2)) ->
        List.for_all2 (fun (x1, t1) (x2, t2) -> t1 = t2 && is_equal x1 x2 env) xts1 xts2 &&
        is_equal y1 y2 env &&
        (let env = List.fold_left2 (fun env (x1, _) (x2, _) -> identify_with x1 x2 env) env xts1 xts2 in
           f env (e1, e2))
    | (ExtArray (x1), ExtArray (x2)) -> x1 = x2
    | (e1, e2) when e1 = e2 -> true (* 構造として同一であればよい *)
    | _ -> false
  in
    if f M.empty (e1, e2) then () else raise Giveup 


let unify pat1 pat2 = (* pat1とpat2に同時に適合するようなパターンを返す. 正しいかはあとで確認するので適当なものを作ればよい. *)
  let ind = Id.gentmp Type.Int in
  let e1 = pat1 (Var (ind)) in
  let e2 = pat2 (Var (ind)) in
  let predicate = (function Var (x) when x = ind -> true | _ -> false) in
  let rec f = function (* e1, e2ともにVar (ind)がある場所に"穴"を持つ式を返す *)
    | (Var (x1), e2) when x1 = ind -> replace predicate e2 (* 片方だけがindであれば他方を使う *)
    | (e1, Var (x2)) when x2 = ind -> replace predicate e1
    | (IfEq (x1, y1, d1, e1), IfEq (x2, y2, d2, e2)) -> (fun e -> IfEq (x1, y1, f (d1, d2) e, f (e1, e2) e)) (* x1, y1は同じかは今は調べないでよい *)
    | (IfLE (x1, y1, d1, e1), IfLE (x2, y2, d2, e2)) -> (fun e -> IfLE (x1, y1, f (d1, d2) e, f (e1, e2) e))
    | (Let ((x1, t1), d1, e1), Let ((x2, t2), d2, e2)) -> (fun e -> Let ((x1, t1), f (d1, d2) e, f (e1, e2) e))
    | (LetTuple (xts1, y1, e1), LetTuple (xts2, y2, e2)) -> (fun e -> LetTuple (xts1, y1, f (e1, e2) e))
    | (e1, e2) -> replace predicate e1 (* それ以外の場合は、Var (ind)を穴に戻して終了。 [XXX] 関数が内部にある場合は無視していることに注意。 *)
  in
    f (e1, e2)

let rec g env = function
  | IfEq (x, y, e1, e2) -> IfEq (x, y, g env e1, g env e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, g env e1, g env e2)
  | Let ((x, t), e1, e2) ->  Let ((x, t), g env e1, g env e2)
  | LetRec ({ name = (x, t); args = yts; body = e1 } as fundef, e2) ->
      begin
        try
          if free_occur_of_function x e1 || free_occur_of_function x e2 then raise Giveup;
          let lets = split_unit_lets e1 in
          let len = List.length lets in
            if len < minimum_target then raise Giveup; (* 短い場合は無視する *)
            let (_, matchings) =
              List.fold_left
                (fun (i, acc) e -> (* Int (i)の現れを穴で置換 *)
                   (i+1, (i, replace (function Int (j) when i = j -> true | _ -> false) e) :: acc))
                (0, []) lets
            in
           let pattern =
              List.fold_left
                (fun acc (_, pat) -> unify acc pat)
                (snd (List.hd matchings)) matchings
            in
              List.iter (fun (i, pat) -> check_identity (pattern (Int (i))) (pat (Int (i)))) matchings; (* ちゃんと適合しているか確認する *)
              Format.eprintf "'%s' is rolled@." x;
              let var = Id.genid "IND" in
              let e = pattern (Var (var)) in (* パターンに帰納変数を当てはめる *)
              let temp_one  = Id.gentmp Type.Int in (* ループの実行をするコードを追加 *)
              let temp_end  = Id.gentmp Type.Int in
              let temp_unit = Id.gentmp Type.Unit in
              let temp_next = Id.gentmp Type.Int in
              let e = Let ((temp_one, Type.Int), Int (1),
                      Let ((temp_end, Type.Int), Int (len),
                        IfEq (var, temp_end,
                              Unit,
                              Let ((temp_unit, Type.Unit), e,
                              Let ((temp_next, Type.Int), Add(var, temp_one),
                                App (x, temp_next :: List.map fst yts))))))
              in
              let yts = (var, Type.Int) :: yts in (* 引数に帰納変数を追加 *)
              let env = S.add x env in (* xの関数呼び出しで帰納変数を追加する *)
                LetRec ({ name = (x, t); args = yts; body = e }, g env e2)
        with Giveup -> LetRec ({ fundef with body = g env e1 }, g env e2)
      end
  | LetTuple (xts, y, e) -> LetTuple (xts, y, g env e)
  | App (x, ys) when S.mem x env ->
      let zero = Id.gentmp Type.Int in
        Let ((zero, Type.Int), Int (0), App (x, zero :: ys))
  | e -> e

let rec f e = g S.empty e

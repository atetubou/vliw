(* レジスタ彩色を行う.
 * プログラムがstrictであることを仮定する.
 * (そうでないと変数の型がdefから決定できない.)
 *
 * 事前条件: $stackに関する操作が存在しないこと.
 *)

open Block

(* 関数呼び出しの前後で生きている変数を退避する *)
let save_and_restore global_variables l func =
  let liveness = Liveness.create func.cfg in
  let cfg = func.cfg in
  let max_stack_size = ref func.stack_size in
  let cfg =
    G.map
      (fun b stmts ->
         snd
           (List.fold_right
              (fun stmt (live, stmts) ->
                 let stmts =
                   match stmt with
                     | { exp = Call _ } ->
                         let set = live in
                         (* グローバル変数とCallが定義する変数は退避しない *)
                         let set = List.fold_left (fun acc (x, _) -> S.remove x acc) set (stmt.def @ global_variables) in
                         (* [XXX] リンクレジスタはともかく退避する *)
                         let set = if l = Id.main_label then set else (assert (not (S.mem Asm.link set)); S.add Asm.link set) in
                         let (stores, loads, new_stack_size) =
                           S.fold
                             (fun x (stores, loads, i) ->
                                (gen_store (Asm.reg_type_of x) x (Coloring.stack_pos_of l i) :: stores,
                                 gen_load  (Asm.reg_type_of x) x (Coloring.stack_pos_of l i) :: loads,
                                 i + 1))
                             set ([], [], func.stack_size)
                         in
                           max_stack_size := max !max_stack_size new_stack_size;

                           stores @
                           [stmt] @
                           loads @
                           stmts
                     | _ -> stmt :: stmts
                 in
                   (Liveness.transfer stmt live, stmts))
              stmts (Liveness.live_of b liveness, [])))
      cfg
  in

  (* 新たなスタックのサイズ *)
  let stack_size = !max_stack_size in
  (* スタックサイズが確定したので, スタックポインタの設定を行う. *)
  let cfg =
    G.map_stmt_list
      (function
         (* ダミーのスタックポインタの設定の文の場合. 実際のサイズを設定する. *)
         | { exp = Set (0); def = [x, _] } as stmt when x = Asm.stack ->
             (* スタックポインタを変更する文はまだトップレベルにしかないはず. *)
             assert (l = Id.main_label);
             [{ stmt with exp = Set (Global.size () + stack_size) }]
         (* メイン関数以外の場合は、関数の入口/出口でスタックを伸張/縮小する. *)
         | { exp = Entry } as stmt when l <> Id.main_label && stack_size <> 0 ->
             [stmt; gen_stmt [Asm.stack, Type.Int] (Add (C ( stack_size), Asm.stack))]
         | { exp = Return } as stmt when l <> Id.main_label && stack_size <> 0 ->
             [gen_stmt [Asm.stack, Type.Int] (Add (C (-stack_size), Asm.stack)); stmt]
         | stmt -> [stmt])
      cfg
  in
  let func =
    { func with
          cfg = cfg;
          stack_size = stack_size }
  in
    Format.eprintf "stack size of %s is %d@." l stack_size;
    func



(* 関数funcのレジスタ割り当てを行う *)
let g global_variables l func =
  Format.eprintf "# Register Allocation of %s@." l;
  (* メイン関数の場合は, ダミーのスタックポインタを直前で設定する.
   * (メイン関数をstrictにするために必要な処理.)
   * $stackの生存区間を短くするために, プログラムの先頭ではなくCallの直前で行う.
   * Note: メイン関数以外では, $stackをグローバル変数に加えることで, 入口/出口生存となっている. *)
  let func =
    if l = Id.main_label then
      { func with
            cfg =
              G.map_stmt_list
                (function
                   | { exp = Call _ } as stmt ->
                       (* フレームサイズが決定できないので, 適当に定める. *)
                       [gen_stmt [Asm.stack, Type.Int] (Set (0)); stmt]
                   | stmt -> [stmt])
                func.cfg }
    else
      func
  in

  let func = Coloring.f l func in
  let func = save_and_restore global_variables l func in

  let () =
    let used_regs = gather_variables_of func in
    let used_regs = S.union (S.of_list (List.map fst (func.args @ func.rets))) used_regs in
    (* 何故か彩色されなかったもの *)
    S.iter
      (fun x ->
         if not (Asm.is_reg x) then (
           Format.eprintf "WARNING: %s is not register, but not colored@." x;
           G.fold_stmt
             (fun stmt () ->
                List.iter (fun (y, t) -> if x = y then Format.eprintf "%s is type of %s@." x (Type.string_of t)) stmt.def)
             func.cfg ()
         )) used_regs;
    let unused = S.diff (S.of_list (Asm.regs @ Asm.fregs)) used_regs in
      Format.eprintf "unused registers: %d (regs: %d)@." (S.cardinal unused) (S.cardinal (S.filter (fun x -> Asm.reg_type_of x <> Type.Float) unused))
  in
    func


(* 関数呼び出し規約をargs, rets(レジスタと型の組のリスト)に決定する.
 * 関数の入口で今の引数(func.args)にargsから代入する式を追加.
 * func.argsのかわりにargsを入口生存とする. *)
let specify_calling_convention func args rets =
  (* 関数呼び出し規約はレジスタに関する規約なはず. *)
  assert (List.for_all (fun (x, _) -> Asm.is_reg x) (args @ rets));
  let cfg = func.cfg in
  let env =
    List.fold_left2
      (fun acc (x, t) (y, t) ->
         (* レジスタの場合は変更されないはず. *)
         if Asm.is_reg x then assert (x = y);
         M.add x y acc)
      M.empty (func.args @ func.rets) (args @ rets)
  in
  let replace x = if M.mem x env then M.find x env else x in
  (* 入口にfunc.argsに新しい引数argsから代入する文を追加.
   * 同時に入口生存なものをfunc.argsからargsに変更する. 
   * 出口に関しても同様. *)
  let cfg =
    G.map_stmt_list
      (function
         | { exp = Entry; def = def } as stmt ->
             { stmt with def = List.map (fun (x, t) -> (replace x, t)) def } ::
             move_list func.args (List.map fst args)
         | { exp = Return; use = use } as stmt ->
             move_list rets (List.map fst func.rets) @
             [{ stmt with use = List.map replace use }]
         | stmt -> [stmt])
      cfg
  in
    { func with
          cfg = cfg;
          args = args;
          rets = rets; }

(* CallをレジスタへのMove + Call + レジスタからのMoveに分解 *)
let take_call_apart prog =
  let prog =
    map_func
      (fun l func ->
         let take ls =
           (* レジスタ以外の変数があれば，レジスタの0番から割り当てていく *)
           let (_, _, ls) =
             List.fold_right
               (fun (x, t) (regs, fregs, acc) ->
                  match (regs, fregs, t = Type.Float) with
                    | _ when Asm.is_reg x ->
                        (regs, fregs, (x, t) :: acc)
                    | (regs, x :: fregs, true) ->
                        (regs, fregs, (x, Type.Float) :: acc)
                    | (x :: regs, fregs, false) ->
                        (regs, fregs, (x, Type.Int) :: acc)
                    (* レジスタが足りないとき *)
                    | _ -> assert false)
               ls (Asm.regs, Asm.fregs, [])
           in
             ls
         in
           specify_calling_convention
             func
             (take func.args)
             (take func.rets))
      prog
  in
  let prog =
    map_cfg
      (fun l cfg ->
         G.map_stmt_list
           (function
              | { exp = Call (r, xs); use = use; def = def } as stmt ->
                  let func = M.find r prog.entries in
                  (* 引数を設定する. *)
                  let init_stmt = move_list func.args xs in
                  (* 結果を取り出す. *)
                  let fin_stmt = move_list def (List.map fst func.rets) in
                    init_stmt @
                    [{ stmt with
                           exp = Call (r, List.map fst func.args);
                           use = List.map fst func.args;
                           def = func.rets }] @
                    fin_stmt
              | stmt -> [stmt])
           cfg)
      prog
  in
    prog


let f prog =
  let prog = take_call_apart prog in
  let prog = add_global_variable ~is_const:true (Asm.stack, Type.Int) prog in
  Format.eprintf "An approximate program size is %d@." (M.fold (fun _ { cfg = cfg } i -> G.fold (fun _ stmts i -> i + List.length stmts) cfg 0 + i) prog.entries 0);
  let prog = map_func (g prog.global_variables) prog in
  let prog = Block.simplify prog in
    prog



(* ブロックのプログラム上での順番を管理する.
 * Else節は常に後続ブロックになるようにしておく. *)

open Vliw


type t = {
  (* プログラム全体のCFG *)
  all_cfg : G.t;
  (* 開始ブロック *)
  start_block : Id.t;
  (* ブロックb -> 最終的に出力されるときに, bの前にくるブロック *)
  prev : Id.t M.t;
  succ : Id.t M.t;
}

(* ブロックを繋げることによって, 無駄なJmpを削除する.
 * 分岐のElse節は必ず後ろにくるようにする. *)
let concatenate ({ all_cfg = cfg } as seq) =
  (* くっつけるべき辺のリスト *)
  let edges =
    G.fold_edge
      (fun b b' e_tag acc ->
         (* Elseの辺か, Jmpであればaccに追加 *)
         if e_tag = Else ||
            (match partition_block (G.find b cfg) with
               | (_, { right = { exp = Jmp } }, _) -> true
               | _ -> false)
         then
           (-VProfile.count_of_edge b b', b, b') :: acc
         else
           acc)
      cfg []
  in
  let edges = List.sort compare edges in
  (* edgesから繋げられるだけ繋げる *)
  let (_, succ) =
    List.fold_left
      (fun (prev, succ) (_, b, b') ->
         let rec parent_of b =
           if M.mem b prev then
             parent_of (M.find b prev)
           else
             b
         in
           (*
           Format.eprintf "%s -> %s: %s %s (%s, %s)@."
             b b'
             (string_of_bool (M.mem b succ))
             (string_of_bool (M.mem b' prev))
             ((parent_of b))
             ((parent_of b'));
            *)
         if not (M.mem b succ) && not (M.mem b' prev) && parent_of b <> parent_of b' then
           (M.add b' b prev,
            M.add b b' succ)
         else
           (prev, succ))
      (seq.prev, seq.succ) edges
  in
  let (cfg, succ) =
    G.fold
      (fun b stmts (cfg, succ) ->
         match partition_block stmts with
           | (_, { right = { exp = Jmp } }, _) when M.mem b succ ->
               (* すぐ後ろのブロックにジャンプする場合はJmp命令を削除してよい. *)
               (G.update_block b
                  (List.map
                     (filter_half
                        (function { exp = Jmp } -> false | _ -> true))
                     stmts)
                  cfg,
                succ)
           | (_, { right = { exp = IfEq _ | IfLt _ | IfFEq _ | IfFLt _ } }, _) when not (M.mem b succ) ->
               (* ifがあるのに後続ブロックが決まっていないということは, Else節が後続ブロックになっていない. *)
               (* 仕方がないのでJmpだけのブロックを追加しておく. *)
               let b_else = G.succ_of b Else cfg in
               let (b', cfg) = G.add_block [gen_stmt [] Jmp] cfg in
               let succ = M.add b b' succ in
               (* bとb_elseの間にb'を挟む *)
               let cfg = G.remove_edge b b_else cfg in
               let cfg = G.add_edge b b' Else cfg in
               let cfg = G.add_edge b' b_else Other cfg in
                 (* 統計情報を更新 *)
                 VProfile.add_prob b b' (VProfile.prob_of b b_else);
                 VProfile.add_count b' (VProfile.count_of_edge b b_else);
                 VProfile.add_prob b' b_else 1.0;
                 (cfg, succ)
           | _ -> (cfg, succ))
      cfg (cfg, succ)
  in
  let prev =
    M.fold
      (fun b b' acc ->
         M.add b' b acc)
      succ M.empty
  in
    { seq with all_cfg = cfg; prev; succ }

  

(* メイン関数funcに対して, Haltのためのブロックを作り, Returnを変える.
 * プログラムの終了時には無限ループをさせることにする. *)
let add_halt func =
  let cfg = func.cfg in
  let halt = Id.genid "halt" in
  let cfg = G.add_vertex halt [gen_stmt [] Jmp] cfg in
  let cfg = G.add_edge halt halt Other cfg in
    VProfile.add_count halt 1;
    VProfile.add_prob halt halt 1.0;
  let cfg =
    G.fold
      (fun b stmts cfg ->
         if List.exists (function { right = { exp = Return } } -> true | _ -> false) stmts then
           (VProfile.add_prob b halt 1.0;
            G.add_edge b halt Other cfg)
         else
           cfg)
      cfg cfg
  in
  let cfg =
    G.map_stmt
      (map_half
         (function
            | { exp = Return } -> gen_half_stmt [] Jmp
            | half -> half))
      cfg
  in
    { func with cfg = cfg }


let build { entries } =
  (* 関数名 -> その関数の出口ブロックのリスト *)
  let exits =
    let acc =
      M.mapi
        (fun l { cfg } ->
           G.fold
             (fun b stmts acc ->
                if List.exists
                     (function { right = { exp = Return } } -> true | _ -> false)
                     stmts
                then
                  b :: acc
                else
                  acc)
             cfg [])
        entries
    in
      ref acc
  in
  (* Returnを処理する.
   * 関数内のReturnはJumpToへ, メイン関数のReturnはhaltへ. *)
  let entries =
    M.mapi
      (fun l func ->
         if l = Id.main_label then
           add_halt func
         else
           { func with
                 cfg =
                   G.map_stmt
                     (map_half
                        (function
                           | { exp = Return } -> gen_half_stmt [] (JumpTo (Asm.link))
                           | stmt -> stmt))
                     func.cfg })
      entries
  in
  (* main以外のReturnをJumpToに変更する. *)
  let entries =
    M.mapi
      (fun l func ->
         if l = Id.main_label then
           func
         else
           { func with
                 cfg =
                   G.map_stmt
                     (function
                        | { right = { exp = Return } } as stmt -> { stmt with right = gen_half_stmt [] (JumpTo (Asm.link)) }
                        | stmt -> stmt)
                     func.cfg })
      entries
  in
  (* 各関数のCFGをひとつにまとめる *)
  let cfg =
    M.fold
      (fun l func cfg ->
         let cfg = G.fold G.add_vertex func.cfg cfg in
         let cfg = G.fold_edge G.add_edge func.cfg cfg in
           cfg)
      entries G.empty
  in
  (* ブロックbに含まれているCallを分解する. *)
  let rec split_call b cfg =
    let stmts = G.find b cfg in
      match Common.span (function { left = { exp = Call _ } } -> false | _ -> true) stmts with
        | (stmts, { left = { exp = Call (l, _) } } :: stmts') ->
            let (b', cfg) = G.add_block stmts' cfg in
            VProfile.add_count b' (VProfile.count_of b);
            let stmts = stmts @ [gen_stmt [Asm.link, Type.Int] (SetLabel (b'))] @ [gen_stmt [] Jmp] in
            let cfg = G.update_block b stmts cfg in
            (* bの後続ブロックをb'に付け替え *)
            let cfg =
              G.expand
                (fun b'' e_tag cfg ->
                   VProfile.add_prob b' b'' (VProfile.prob_of b b'');
                   G.add_edge b' b'' e_tag (G.remove_edge b b'' cfg))
                b cfg cfg
            in
            let func_entry = (M.find l entries).v_entry in
            let cfg =
              VProfile.add_prob b func_entry 1.0;
              G.add_edge b func_entry Other cfg
            in
            (* exitsのブロックにbが含まれていた場合はb'に変える *)
            exits :=
              M.map
                (List.map
                   (fun b'' ->
                      if b'' = b then
                        b'
                      else
                        b''))
                !exits;
            (* lの出口からb'へ戻る辺を追加 *)
            let cfg =
              List.fold_right
                (fun b'' ->
                   let prob =
                     float_of_int (VProfile.count_of b'' * VProfile.count_of b) /.
                     ((float_of_int (VProfile.count_of func_entry)) ** 2.0)
                   in
                     VProfile.add_prob b'' b' prob;
                     G.add_edge b'' b' Other)
                (M.find l !exits) cfg
            in
              split_call b' cfg
        | _ -> cfg
  in
  (* 関数呼び出しをCFG上で実現する. *)
  let cfg =
    G.fold
      (fun b _ cfg ->
         split_call b cfg)
      cfg cfg
  in
    concatenate
      {
        all_cfg = cfg;
        start_block = (M.find Id.main_label entries).v_entry;
        prev = M.empty;
        succ = M.empty;
      }




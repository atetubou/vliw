open Block
open ListScheduling

module PQ = Set.Make (struct type t = int * G.vertex let compare = compare end)

type data = {
  cfg : G.t;
  liveness : Liveness.t;
  b2order : int G.M.t;
  worklist : PQ.t;
  b2totalclk : int G.M.t; (* そのブロックを実行するのにかかる総クロック数 *)
}

let add b data = { data with worklist = PQ.add (G.M.find b data.b2order, b) data.worklist }
let pop data =
  let (_, b) as elt = PQ.min_elt data.worklist in
    (b, { data with worklist = PQ.remove elt data.worklist })
      
let build cfg =
  let (cfg, b2totalclk) =
    G.fold
      (fun b stmts (cfg, b2totalclk) ->
         let (_, totalclk, stmts) = ListScheduling.scheduling stmts in
         let cfg = G.update_block b stmts cfg in
         let b2totalclk = G.M.add b totalclk b2totalclk in
           (cfg, b2totalclk))
      cfg (cfg, G.M.empty)
  in
  let liveness = Liveness.create cfg in
  let b2order =
    let order = List.rev (snd (G.topological_sort cfg)) in (* 逆トポロジカル順 *)
    let (_, acc) =
      List.fold_left
        (fun (i, acc) b -> (i+1, G.M.add b i acc))
        (0, G.M.empty) order
    in
      acc
  in
  let data =
    {
      cfg = cfg;
      liveness = liveness;
      b2order = b2order;
      worklist = PQ.empty;
      b2totalclk = b2totalclk;
    }
  in
  let data = G.fold (fun b _ -> add b) data.cfg data in (* 本当はworklistには出口ブロックだけ入れておけばよい。 *)
    data

let is_ok = function (* 投機的に分岐命令を超えて移動できる命令か *)
  | Set _ | SetLabel _ | FSet _ | Mov _ | FMov _
  | Add _ | Sub _ | ShiftL _ | ShiftR _
  | FAdd _ | FMul _ | FInv _ | FSqrt _
  | Load _ | FLoad _ (* Storeなど副作用があるものは不可能 *)
  | ItoF _ | FtoI _
    -> true
  | _ -> false (* Call, Send, Recvや、分岐は"副作用"があるので不可能 *)

exception Giveup

let move_to b totalclk data ready = (* bにreadyから投機的に命令を移動してみる. ただしtotalclkから変化してはならない. *)
  let rec loop data ready =
    match ready with
      | (b', stmt) :: ready when
          b <> b' &&
          is_ok stmt.exp (* そもそも分岐を超えて移動させてもよいか *)
        ->
          let (branch, stmts) = match List.rev (G.find b data.cfg) with branch :: stmts -> (branch, List.rev stmts) | _ -> assert false in
          let live = Liveness.live_of b data.liveness in
          let live = List.fold_right S.add branch.use live in (* 分岐に入るlive変数集合 *)
            (try
               (* stmtが何かを定義する場合、生きている変数を定義してはならない *)
               if List.exists (fun (x, _) -> S.mem x live) stmt.def then raise Giveup;
               let stmts = stmts @ [{ stmt with s_id = Block.gensid () }; branch] in (* stmtを分岐の前に移動する。コピーすることになるかもしれないので、s_idは変更しておく。 *)
               let (_, totalclk', stmts) = ListScheduling.scheduling stmts in
               let () = if totalclk < totalclk' then raise Giveup in (* クロックが増えたら移動しない *)
               let data = { data with cfg = G.update_block b stmts data.cfg } in (* stmtをbに追加して、スケジューリングしたものを追加する *)
                 if G.pred b' data.cfg = [b] then (* 先行ブロックがbのみの場合、stmtを直接b'からbへ移せる *)
                   let stmts' = G.find b' data.cfg in
                   let stmts' = List.filter (fun { s_id = s_id } -> s_id <> stmt.s_id) stmts' in
                   let data = { data with cfg = G.update_block b' stmts' data.cfg } in (* stmtをb'から消去する. (TODO: b以外に先行命令がないから削除するだけでよい *)
                     (* livenessの更新. stmtが何かを定義する場合は、出口生存となる. *)
                     List.iter (fun (x, _) -> Liveness.add b x data.liveness) stmt.def;
                   let data = List.fold_right add (G.pred b data.cfg) data in (* ブロックに新たな命令が追加されたので、先行ブロックもさらにスケジューリングできるかも. XXX: これは更新があったとき一度だけ行えばよい *)
                     loop data ready
                 else
                   let cfg = data.cfg in
                   let stmts' = G.find b' cfg in
                   let () = if List.length stmts' > 5 then raise Giveup in (* コピーするとかなり増える場合はあきらめる *)
                   let stmts' = List.filter (fun { s_id = s_id } -> s_id <> stmt.s_id) stmts' in
                   let stmts' = List.map (fun stmt -> { stmt with s_id = Block.gensid () }) stmts' in
                   let (b'', cfg) = G.add_block stmts' cfg in (* 新たにコピーする *)
                   let cfg = G.add_edge b b'' (G.tag_of_edge b b' cfg) (G.remove_edge b b' cfg) in (* 辺の付け替え *)
                   let alter c = if c = b' then b'' else c in (* 頂点の変更を行う *)
                   let cfg = List.fold_left (fun cfg c-> G.add_edge b'' (alter c) (G.tag_of_edge b' c cfg) cfg) cfg (G.succ b' cfg) in (* 後続辺のコピー *)
                   let data = { data with cfg = cfg } in
  
                   let ready = List.map (fun (c, stmt) -> (alter c, stmt)) ready in (* ready内のb'もb''に付け替える *)
                   let data = { data with b2order = G.M.add b'' (G.M.find b' data.b2order) data.b2order } in (* とりあえずb''と同じ順番（優先度）ということにしておく *)
                   let data = { data with liveness = Liveness.copy_to b' b'' data.liveness } in (* 生存情報はまったく同じでよい。 *)
                     (* livenessの更新. stmtが何かを定義する場合は、出口生存となる. *)
                     List.iter (fun (x, _) -> Liveness.add b x data.liveness) stmt.def;
                   let data = List.fold_right add (G.pred b data.cfg) data in (* ブロックに新たな命令が追加されたので、先行ブロックもさらにスケジューリングできるかも. XXX: これは更新があったとき一度だけ行えばよい *)
                     loop data ready
  
  
             with Giveup -> loop data ready)
      | (b', stmt) :: ready -> loop data ready
      | [] -> data
  in
    loop data ready

let rec g data =
  if PQ.is_empty data.worklist then data
  else
    let (b, data) = pop data in
    let stmts = G.find b data.cfg in
    let (stall, totalclk, stmts) = ListScheduling.scheduling stmts in
    let data = { data with cfg = G.update_block b stmts data.cfg } in
      if stall = 0 then (* スケジューリングにストールなしで成功した *)
        g data
      else (* スケジューリングしてストールが発生する場合は、投機的に移動する *)
        let ready = ListScheduling.ready_of_succ b data.cfg in
(*          Format.eprintf "b%d ready: %d@." b (List.length ready);*)
          g (move_to b totalclk data ready)


(*
let rec move_from b totalclk data = (* bの先行ブロックへ命令を移動させてみる *)
  let try_moving data b stmt = (* bにstmtを移動してみる. 失敗したら例外Giveupを発生させる *)
    let (branch, stmts) = match List.rev (G.find b data.cfg) with branch :: stmts -> (branch, List.rev stmts) | _ -> assert false in
    let live = G.M.find b data.liveness in
    let live = List.fold_right S.add branch.use live in (* 分岐に入るlive変数集合 *)
    let () = match stmt.def with Some (x, _) when S.mem x live -> raise Giveup | _ -> () in (* stmtが何かを定義する場合、生きている変数を定義してはならない *)
    let stmts = stmts @ [{ stmt with s_id = Block.genid () }; branch] in (* stmtを分岐の前にコピーする. *)
    let (_, totalclk', stmts) = ListScheduling.scheduling stmts in
    let totalclk = G.M.find b data.b2totalclk in
    let () = if totalclk < totalclk' then raise Giveup in (* クロックが増えたら移動しない *)
    let data = { data with b2totalclk = G.M.add b totalclk' data.b2totalclk } in
    let data = { data with cfg = G.update_vertex b stmts data.cfg } in
    let data = (* livenessの更新. stmtが何かを定義する場合は、出口生存となる. *)
      { data with
            liveness =
              match stmt.def with
                | Some (x, _) -> G.M.add b (S.add x (G.M.find b data.liveness)) data.liveness
                | None -> data.liveness }
    in
      data
  in
  let rec loop data totalclk = function
    | stmt :: que when is_ok stmt.exp ->
        (try
           let stmts = G.find b data.cfg in
           let () =
             let stmts = Common.take_while (fun { s_id = s_id } -> s_id <> stmt.s_id) stmts in (* stmtの前にある命令を集める *)
               if (* stmtの前の文に依存する場合は、移動できない *)
                 List.exists
                   (fun stmt' -> ListScheduling.is_hazardable stmt' stmt)
                   stmts
               then
                 raise Giveup (* [XXX] この場合は, これ以降のqueは見なくて良い *)
           in
           let stmts = List.filter (fun { s_id = s_id } -> s_id <> stmt.s_id) stmts in
           let (_, totalclk', stmts) = ListScheduling.scheduling stmts in
           let () = if totalclk < totalclk' then raise Giveup in (* stmtを除くことによって、bのクロックが増えたら移動しない *)
           let data = { data with cfg = G.update_vertex b stmts data.cfg } in (* stmtを消去する *)
           let data = (* 先行ブロックについてstmtを移動してみる *)
             List.fold_left
               (fun data b' ->
                  assert (b <> b'); (* loopを呼び出すところで, 自己ループがある場合は除いている *)
                  try_moving data b' stmt)
               data (G.pred b data.cfg)
           in (* すべての先行ブロックにstmtを移動することが成功した! *)
           let data = List.fold_right add (G.pred b data.cfg) data in (* 先行ブロックについて、さらに投機的なスケジューリングを行う *)
             loop data totalclk' que
         with Giveup -> loop data totalclk que)
      | stmt :: que -> loop data totalclk que
      | [] -> data
  in
  let pred = G.pred b data.cfg in
    if List.mem b pred || pred = [] then data (* bに自己ループがある場合はコピーしなければならないので、無視する. また、先行ブロックがない場合は、先行ブロックに移動できない *)
    else loop data totalclk (G.find b data.cfg)

(* 下から湧き出させる方式 *)
let rec h data =
  if PQ.is_empty data.worklist then data
  else
    let (b, data) = pop data in
    let stmts = G.find b data.cfg in
    let (_, totalclk, stmts) = ListScheduling.scheduling stmts in
    let data = { data with cfg = G.update_vertex b stmts data.cfg } in
        h (move_from b totalclk data)
 *)


let f prog =
  Format.eprintf "# speculastive scheduling@.";
  simplify (map_cfg (fun _ cfg -> (g (build cfg)).cfg) prog)


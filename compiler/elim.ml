open KNormal

let memi x env = M.mem x env && (match M.find x env with Int _ -> true | _ -> false)
let findi x env = match M.find x env with Int (i) -> i | _ -> assert false


let rec effect = function
  | Let(_, e1, e2) | IfEq(_, _, e1, e2) | IfLE(_, _, e1, e2) -> effect e1 || effect e2
  | LetRec(_, e) | LetTuple(_, _, e) -> effect e
  | ExtFunApp (("create_array" | "create_float_array" | "allocate" | "float_of_int" | "int_of_float" | "sqrt"), _) -> false
  | App _ | Put _ | ExtFunApp _ -> true
  | _ -> false


let elim =
  let is_kiiled env ((target, _) as xi) e =
    let rec f env = function
      (* xiに上書きされた場合にはそれ以上必要でない *)
      | Put (x, y, z) when memi y env && (x, findi y env) = xi -> `Killed
      | Let ((x, t), e1, e2) ->
          let res = f env e1 in
            (match res with
               | `Killed -> `Killed
               | `Needed -> `Needed
               | _ -> f (M.add x e1 env) e2)
      | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
          if f env e1 = `Killed && f env e2 = `Killed then `Killed
          else `Needed
      | LetTuple (_, _, e) -> f env e
      | ExtFunApp (("create_array" | "create_float_array" | "allocate" | "float_of_int" | "int_of_float" | "sqrt"), _) -> `Unknown
      (* 注目している式を読み出す可能性がある場合は, 必要とされているとみなす. *)
      | ExtFunApp _ | App _ | Get _ -> `Needed
      | _ -> `Unknown
    in
      f env e = `Killed
  in
  let rec f env = function
    | IfEq(x, y, e1, e2) -> IfEq(x, y, f env e1, f env e2)
    | IfLE(x, y, e1, e2) -> IfLE(x, y, f env e1, f env e2)
    | Let((_, Type.Unit) as ut, Put (x, y, z), e) when memi y env ->
        if is_kiiled env (x, findi y env) e then
          (* 上書きされる場合はそのPutを消去する. *)
          f env e
        else
          Let (ut, Put (x, y, z), f env e)
    | Let((x, t), e1, e2) ->
        let e1 = f env e1 in
        let e2 = f (M.add x e1 env) e2 in
          if effect e1 || S.mem x (fv e2) then
            Let((x, t), e1, e2)
          else
            ((*Format.eprintf "eliminating variable %s@." x; *)
             e2)
    | LetRec({ name = (x, t); args = yts; body = e1 }, e2) ->
        let e2 = f env e2 in
          if S.mem x (fv e2) then
            LetRec({ name = (x, t); args = yts; body = f env e1 }, e2)
          else
            ((*Format.eprintf "eliminating function %s@." x;*)
             e2)
    | LetTuple(xts, y, e) ->
        let xs = List.map fst xts in
        let e = f env e in
        let live = fv e in
          if List.exists (fun x -> S.mem x live) xs then LetTuple(xts, y, e) else
            ((*Format.eprintf "eliminating variables %s@." (Id.pp_list xs);*)
             e)
    | e -> e
  in
    f M.empty

(* 不要引数除去 *)
let elim_args =
  (* env: 関数名から, 必要な引数の場所にtrueが入ったリスト(e.g. [true; false; ...])への写像 *)
  (* 関数がエスケープしないときのみ実装。 *)
  let rec f env = function
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f env e1, f env e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f env e1, f env e2)
    | Let ((x, t), e1, e2) ->  Let ((x, t), f env e1, f env e2)
    | LetRec ({ name = (x, t); args = yts; body = e1 }, e2) ->
        if free_occur_of_function x e1 || free_occur_of_function x e2 then
          (* do not expand *)
          LetRec ({ name = (x, t); args = yts; body = f env e1 }, f env e2)
        else
          let fvs = fv e1 in
          let ls =
            List.map
              (fun (y, t) ->
                if S.mem y fvs then true
                else ((*Printf.eprintf "Eliminate an argument %s of %s\n" y x; *)false))
              yts
          in
          let yts = List.fold_right2 (fun yt b yts -> if b then yt :: yts else yts) yts ls [] in
          let env = M.add x ls env in
            LetRec ({ name = (x, t); args = yts; body = f env e1 }, f env e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f env e)
    | App (x, ys) when M.mem x env ->
        let ls = M.find x env in
        let ys = List.fold_right2 (fun y b ys -> if b then y :: ys else ys) ys ls [] in
          App (x, ys)
    | e -> e
  in
    f M.empty

exception Impossible

(* 不要返り値除去.
 * 返り値が関数本体に依存しない場合は, それを除去する. *)
let elim_retvalue =
  (* 引数の値が, 関数が呼ばれたあとは常に一定であるものを計算する. *)
  let const_args name args e =
    let init = List.map (fun _ -> true) args in
    let merge = List.map2 (&&) in
    let rec f = function
      | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) | LetRec ({ body = e1 }, e2) ->
          merge (f e1) (f e2)
      | LetTuple (_, _, e) -> f e
      | App (x, ys) when x = name ->
          List.map2 (=) ys args
      | _ -> init
    in
      List.fold_right2
        (fun b y acc ->
           if b then
             S.add y acc
           else
             acc)
        (f e) args S.empty
  in
  (* 関数nameのbodyについて, 返り値と成りうる変数名の集合を得る.
   * さらに, その変数名を削除してUnit型を返す.
   * 返り値がAdd (x, y)のような形であった場合には例外を投げる. *)
  let elim_and_gather_rets name e =
    let rec f = function
      | IfEq (x, y, e1, e2) ->
          let (e1, s1) = f e1 in
          let (e2, s2) = f e2 in
            IfEq (x, y, e1, e2), S.union s1 s2
      | IfLE (x, y, e1, e2) ->
          let (e1, s1) = f e1 in
          let (e2, s2) = f e2 in
            IfLE (x, y, e1, e2), S.union s1 s2
      | Let ((x, t), e1, e2) ->
          let (e2, s2) = f e2 in
            Let ((x, t), e1, e2), s2
      | LetTuple (xts, y, e) ->
          let (e, s) = f e in
            LetTuple (xts, y, e), s
      | LetRec (fundef, e) ->
          let (e, s) = f e in
            LetRec (fundef, e), s
      | App (x, ys) when x = name -> (App (x, ys), S.empty)
      | Var (x) -> (Unit, S.singleton x)
      | _ -> raise Impossible
    in
    let (e, s) = f e in
      (e, S.to_list s)
  in
  (* env: 返り値が削除された関数 -> もとの返り値をどうやって得るか *)
  let rec f env = function
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f env e1, f env e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f env e1, f env e2)
    | Let ((x, t), e1, e2) -> Let ((x, t), f env e1, f env e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f env e)
    | LetRec ({ name = (x, t); args; body = e1 }, e2) ->
        (try
           if free_occur_of_function x e1 || free_occur_of_function x e2 then raise Impossible;
           let ca = const_args x (List.map fst args) e1 in
           let (e1, rets) = elim_and_gather_rets x e1 in
           let env =
             (match rets with
                (* 返り値が常にある引数と等しいとき. *)
                | [y] when S.mem y ca ->
                    M.add x (`Arg (List.map (fun (y', _) -> y' = y) args)) env
                | _ -> raise Impossible)
           in
           let t = Type.Fun (List.map snd args, Type.Unit) in
             (* 関数内部e1はもはや変更しない. *)
             LetRec ({ name = (x, t); args; body = e1 }, f env e2)
         with Impossible ->
           LetRec ({ name = (x, t); args; body = f env e1 }, f env e2))
    | App (x, ys) when M.mem x env ->
        let un = (Id.gentmp Type.Unit, Type.Unit) in
          (match M.find x env with
             | `Arg (bs) ->
                 (* 返り値をretに変更する. *)
                 let ret = snd (List.find fst (List.combine bs ys)) in
                   Let (un, App (x, ys), Var (ret))
             | _ -> assert false)
    | e -> e
  in
    f M.empty



let rec f e =
  let e = elim e in
  let e = elim_args e in
  let e = elim_retvalue e in
    e


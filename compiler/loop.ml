(* 自然なループを計算する. *)

open Block

(* dom: cfg上のdominatorの情報 *)
let create cfg =
  let dom = Dominator.create cfg in
  let backedges =
    G.fold_edge
      (fun b b' _ backedges ->
         if Dominator.dominate b' b dom then
           (b, b') :: backedges
         else
           backedges)
      cfg []
  in
  (* header -> ループに属するブロックの集合 *)
  let loops =
    let rec dfs vis b =
      if S.mem b vis then vis
      else
        let vis = S.add b vis in
          List.fold_left dfs vis (G.pred b cfg)
    in
      List.fold_left
        (fun acc (b, h) ->
           (* ヘッダを共有するループは合併させる. *)
           let this = if M.mem h acc then M.find h acc else S.empty in
             M.add h (S.union this (dfs (S.singleton h) b)) acc)
        M.empty backedges
  in
(*    M.iter
      (fun h ss ->
         Format.eprintf "head %s@.%s@." h (String.concat ", " (S.to_list ss)))
      loops;*)
    loops



type t = (* MinCamlの型を表現するデータ型 (caml2html: type_t) *)
  | Unit
  | Bool
  | Int
  | Float
  | Fun of t list * t (* arguments are uncurried *)
  | Tuple of t list
  | Array of t
  | List of t
  | Ref of t
  | Var of t option ref
  (* キャッシュ上にある配列 *)
  | ArrayC of t

let gentyp () = Var(ref None) (* 新しい型変数を作る *)

let create_array_of = function
  | Float -> "create_float_array"
  | _ -> "create_array"

let string_of t =
  let paren s = "("^s^")" in
  let rec sub level t =
    match t with
      | Unit -> "unit"
      | Bool -> "bool"
      | Int -> "int"
      | Float -> "float"
      | Fun (ls, s) ->
          let res = String.concat " -> " (List.map (sub 1) (ls @ [s])) in
            if level <= 0 then res else paren res
      | Tuple ls ->
          let res = String.concat " * " (List.map (sub 2) ls) in
            if level <= 1 then res else paren res
      | Array s -> let res = sub 3 s ^ " array" in if level < 3 then res else paren res
      | List s -> let res = sub 3 s ^ " list" in if level < 3 then res else paren res
      | Ref s -> let res = sub 3 s ^ " ref" in if level < 3 then res else paren res
      | Var { contents = None } -> "<poly>"
      | Var { contents = Some s } -> "var(" ^ sub level s ^ ")"
      | ArrayC s -> let res = sub 3 s ^ " carray" in if level < 3 then res else paren res
  in 
    sub 0 t


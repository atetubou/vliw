(* データフロー解析による最適化を行う. *)

open Block

(* ブロックを合併させるしきい値 *)
let merge_threthold = ref 5

let merge_block cfg =
  G.fold
    (fun b stmts cfg ->
       match (partition_last stmts, G.succ b cfg) with
         | (({ exp = Jmp }, _), [b'])
             when b <> b' && List.length (G.find b' cfg) <= !merge_threthold
           ->
             let (b'', cfg) = G.copy_block b' cfg in
             let cfg = G.expand (G.add_edge b'') b' cfg cfg in
             let cfg = G.add_edge b b'' Other cfg in
             let cfg = G.remove_edge b b' cfg in
               cfg
         | _ -> cfg)
    cfg cfg

let merge_block prog = map_cfg (fun _ -> merge_block) prog


let f prog =
  Format.eprintf "check consistency.@.";
  check_consistency prog;
    Format.eprintf "# const fold@.";
  let prog = simplify (DConstFold.f prog) in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# const fold@.";
  let prog = simplify (DConstFold.f prog) in
    Format.eprintf "# algebra@.";
  let prog = Algebra.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in

    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
  Format.eprintf "check consistency.@.";
  check_consistency prog;
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
  check_consistency prog;
    Format.eprintf "# funct@.";
  let prog = Funct.f prog in
    Format.eprintf "# cse@.";
  let prog = DCse.f prog in
    Format.eprintf "# const fold@.";
  let prog = simplify (DConstFold.f prog) in
    Format.eprintf "# const fold@.";
  let prog = simplify (DConstFold.f prog) in
    Format.eprintf "# algebra@.";
  let prog = Algebra.f prog in
  Format.eprintf "check consistency.@.";
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# cse@.";
  let prog = DCse.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# skip if@.";
  let prog = simplify (DConstFold.skip_if prog) in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in

    Format.eprintf "# lazy@.";
  let prog = simplify (DLazy.f prog) in
    Format.eprintf "# lazy@.";
  let prog = simplify (DLazy.f prog) in
    Format.eprintf "# lazy@.";
  let prog = simplify (DLazy.f prog) in
    Format.eprintf "# lazy@.";
  let prog = simplify (DLazy.f prog) in
    Format.eprintf "# lazy@.";
  let prog = simplify (DLazy.f prog) in
    Format.eprintf "# lazy@.";
  let prog = simplify (DLazy.f prog) in

    Format.eprintf "# hoisting@.";
  let prog = Hoisting.f prog in
    Format.eprintf "# merge block@.";
  let prog = simplify (merge_block prog) in


    Format.eprintf "# const fold@.";
  let prog = simplify (DConstFold.f prog) in
    Format.eprintf "# algebra@.";
  let prog = Algebra.f prog in
    Format.eprintf "# algebra@.";
  let prog = Algebra.f prog in
    Format.eprintf "# algebra@.";
  let prog = Algebra.f prog in
    Format.eprintf "# algebra@.";
  let prog = Algebra.f prog in
    Format.eprintf "# cse@.";
  let prog = DCse.f prog in

    Format.eprintf "# copy prop4@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop4@.";
  let prog = Copy.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# funct@.";
  let prog = Funct.f prog in
    Format.eprintf "# skip if@.";
  let prog = simplify (DConstFold.skip_if prog) in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# skip if@.";
  let prog = simplify (DConstFold.skip_if prog) in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in

    Format.eprintf "# cse@.";
  let prog = DCse.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# copy prop@.";
  let prog = Copy.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in
    Format.eprintf "# elim@.";
  let prog = DElim.f prog in

  let prog = simplify prog in
    prog




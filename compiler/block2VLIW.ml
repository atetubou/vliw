(* ブロックからVLIWに変換する. *)

open Vliw

let convert_etag = function
  | Block.Then -> Then
  | Block.Else -> Else
  | Block.Other -> Other
                     
let convert_funct = function
  | Block.FPUBit -> FPUBit
  | Block.NegBit -> NegBit
  | Block.AbsBit -> AbsBit

let convert_ii = function
  | Block.V (x) -> V (x)
  | Block.C (i) -> C (i)

let convert_stmt stmt =
  match stmt with
    | { Block.exp = Block.Entry } ->
        [ gen_entry stmt.Block.def ]
    | { Block.exp = Block.Return } ->
        [ gen_return stmt.Block.use ]
    | { Block.exp = Block.Nop } ->
        [ gen_stmt stmt.Block.def Nop ]
    | { Block.exp = Block.Set (i) } when not (Asm.is_bits_of 16 i) ->
        (* 16bit即値に収まらない場合, 定数テーブルから取ってくる. *)
        [ gen_stmt stmt.Block.def (LoadC (Asm.zero, C (ConstTable.pos_of (Int32.of_int i)))) ]
    | { Block.exp = Block.Set (i) } ->
        [ gen_stmt stmt.Block.def (Set (i)) ]
    | { Block.exp = Block.SetLabel (b) } ->
        [ gen_stmt stmt.Block.def (SetLabel (b)) ]
    | { Block.exp = Block.FSet (f) } ->
        [ gen_stmt stmt.Block.def (FLoadC (Asm.zero, C (ConstTable.pos_of (Int32.bits_of_float f)))) ]
    | { Block.exp = Block.Mov (x) } ->
        [ gen_stmt stmt.Block.def (Mov (x)) ]
    | { Block.exp = Block.FMov (x, funct) } ->
        [ gen_stmt stmt.Block.def (FMov (x, List.map convert_funct funct)) ]
    | { Block.exp = Block.Add (x', y) } ->
        [ gen_stmt stmt.Block.def (Add (convert_ii x', y)) ]
    | { Block.exp = Block.Sub (x', y) } ->
        [ gen_stmt stmt.Block.def (Sub (convert_ii x', y)) ]
    | { Block.exp = Block.ShiftL (x, i) } ->
        [ gen_stmt stmt.Block.def (ShiftL (x, i)) ]
    | { Block.exp = Block.ShiftR (x, i) } ->
        [ gen_stmt stmt.Block.def (ShiftR (x, i)) ]
        (*raise (Failure "This architecture does not support shift logical right.")*)
    | { Block.exp = Block.FAdd (x, y, funct) } ->
        [ gen_stmt stmt.Block.def (FAdd (x, y, List.map convert_funct funct)) ]
    | { Block.exp = Block.FMul (x, y, funct) } ->
        [ gen_stmt stmt.Block.def (FMul (x, y, List.map convert_funct funct)) ]
    | { Block.exp = Block.FInv (x, funct) } ->
        [ gen_stmt stmt.Block.def (FInv (x, List.map convert_funct funct)) ]
    | { Block.exp = Block.FSqrt (x, funct) } ->
        [ gen_stmt stmt.Block.def (FSqrt (x, List.map convert_funct funct)) ]
    | { Block.exp = Block.Load (x, Block.C (i)) } ->
        [ gen_stmt stmt.Block.def (Load (x, i)) ]
    | { Block.exp = Block.Load (x, Block.V (y)) } ->
        let addr = Id.genid "addr" in
          [ gen_stmt [addr, Type.Int] (Add (V (x), y));
            gen_stmt stmt.Block.def (Load (addr, 0)) ]
    | { Block.exp = Block.Store (x, y, Block.C (i)) } ->
        [ gen_stmt stmt.Block.def (Store (x, y, i)) ]
    | { Block.exp = Block.Store (x, y, Block.V (z)) } ->
        let addr = Id.genid "addr" in
          [ gen_stmt [addr, Type.Int] (Add (V (y), z));
            gen_stmt stmt.Block.def (Store (x, addr, 0)) ]
    | { Block.exp = Block.FLoad (x, Block.C (i)) } ->
        [ gen_stmt stmt.Block.def (FLoad (x, i)) ]
    | { Block.exp = Block.FLoad (x, Block.V (y)) } ->
        let addr = Id.genid "addr" in
          [ gen_stmt [addr, Type.Int] (Add (V (x), y));
            gen_stmt stmt.Block.def (FLoad (addr, 0)) ]
    | { Block.exp = Block.FStore (x, y, Block.C (i)) } ->
        [ gen_stmt stmt.Block.def (FStore (x, y, i)) ]
    | { Block.exp = Block.FStore (x, y, Block.V (z)) } ->
        let addr = Id.genid "addr" in
          [ gen_stmt [addr, Type.Int] (Add (V (y), z));
            gen_stmt stmt.Block.def (FStore (x, addr, 0)) ]
    | { Block.exp = Block.LoadC (x, y') } ->
        [ gen_stmt stmt.Block.def (LoadC (x, convert_ii y')) ]
    | { Block.exp = Block.StoreC (x, y, Block.C (i)) } ->
        [ gen_stmt stmt.Block.def (StoreC (x, y, i)) ]
    | { Block.exp = Block.StoreC (x, y, Block.V (z)) } ->
        let addr = Id.genid "addr" in
          [ gen_stmt [addr, Type.Int] (Add (V (y), z));
            gen_stmt stmt.Block.def (StoreC (x, addr, 0)) ]
    | { Block.exp = Block.FLoadC (x, y') } ->
        [ gen_stmt stmt.Block.def (FLoadC (x, convert_ii y')) ]
    | { Block.exp = Block.FStoreC (x, y, Block.C (i)) } ->
        [ gen_stmt stmt.Block.def (FStoreC (x, y, i)) ]
    | { Block.exp = Block.FStoreC (x, y, Block.V (z)) } ->
        let addr = Id.genid "addr" in
          [ gen_stmt [addr, Type.Int] (Add (V (y), z));
            gen_stmt stmt.Block.def (FStoreC (x, addr, 0)) ]
    | { Block.exp = Block.Send (x) } ->
        [ gen_stmt stmt.Block.def (Send (x)) ]
    | { Block.exp = Block.Recv } ->
        [ gen_stmt stmt.Block.def Recv ]
    | { Block.exp = Block.ItoF (x) } ->
        [ gen_stmt [] (StoreC (x, Asm.zero, ConstTable.swap_location));
          gen_stmt stmt.Block.def (FLoadC (Asm.zero, C (ConstTable.swap_location))) ]
    | { Block.exp = Block.FtoI (x) } ->
        [ gen_stmt [] (FStoreC (x, Asm.zero, ConstTable.swap_location));
          gen_stmt stmt.Block.def (LoadC (Asm.zero, C (ConstTable.swap_location))) ]
    | { Block.exp = Block.Call (l, xs) } ->
        [ stmt_of_half { s_id = gensid (); exp = Call (l, xs); use = stmt.Block.use; def = stmt.Block.def } ]
    | { Block.exp = Block.IfEq (x, y') } ->
        [ gen_stmt stmt.Block.def (IfEq (x, convert_ii y')) ]
    | { Block.exp = Block.IfLt (x, y') } ->
        [ gen_stmt stmt.Block.def (IfLt (x, convert_ii y')) ]
    | { Block.exp = Block.IfFEq (x, y) } ->
        [ gen_stmt stmt.Block.def (IfFEq (x, y)) ]
    | { Block.exp = Block.IfFLt (x, y) } ->
        [ gen_stmt stmt.Block.def (IfFLt (x, y)) ]
    | { Block.exp = Block.Jmp } ->
        [ gen_stmt stmt.Block.def Jmp ]
    | { Block.exp = Block.JumpTo (x) } ->
        [ gen_stmt stmt.Block.def (JumpTo (x)) ]

let convert_cfg cfg =
  let acc = G.empty in
  let acc =
    Block.G.fold
      (fun b stmts acc ->
         let stmts' =
           List.fold_right
             (fun stmt acc -> convert_stmt stmt @ acc)
             stmts []
         in
           G.add_vertex b stmts' acc)
      cfg acc
  in
  let acc =
    Block.G.fold_edge
      (fun b b' e_tag acc ->
         G.add_edge b b' (convert_etag e_tag) acc)
      cfg acc
  in
    acc

let convert_call_graph call_graph =
  let acc = F.empty in
  let acc = Block.F.fold F.add_vertex call_graph acc in
  let acc = Block.F.fold_edge F.add_edge call_graph acc in
    acc

(* bitをアドレスaddrにStoreする命令列を生成.
 * 16bit符号付き即値代入とshiftと加算を駆使して, 32bitのビット表現を作り出す. *)
let store_at (bit : int32) addr =
  (* iを16bit符号なし整数だと思って, 32bit符号あり整数に符号拡張する. *)
  let ext_sign i =
    (* TODO: -1だと失敗するみたい. *)
    if not (0 <= i && i < (1 lsl 16)) then Format.eprintf "NO %d (%d)@." i (Int32.to_int bit);
    assert (0 <= i && i < (1 lsl 16));
    if i land (1 lsl 15) <> 0 then
      (lnot 0xffff) lor i
    else
      i
  in
  let open Int32 in
  let hi = shift_right_logical bit 16 in
  let lo = logand bit 0xffffl in
  (* loの最上位ビットが1の場合は, 符号拡張されてしまうのでhiに1を足しておく. *)
  let hi = if shift_right_logical lo 15 = 0l then hi else succ hi in
  let x_hi = Id.genid "hi" in
  let x_hi' = Id.genid "hi" in
  let x_lo = Id.genid "lo" in
  let x_res = Id.genid "res" in
    if lo <> 0l then
      [ gen_stmt [x_hi, Type.Int] (Set (ext_sign (to_int hi)));
        gen_stmt [x_hi', Type.Int] (ShiftL (x_hi, 16));
        gen_stmt [x_lo, Type.Int] (Set (ext_sign (to_int lo)));
        gen_stmt [x_res, Type.Int] (Add (V (x_hi'), x_lo));
        gen_stmt [] (StoreC (x_res, Asm.zero, addr)); ]
    else
      [ gen_stmt [x_hi, Type.Int] (Set (ext_sign (to_int hi)));
        gen_stmt [x_res, Type.Int] (ShiftL (x_hi, 16));
        gen_stmt [] (StoreC (x_res, Asm.zero, addr)); ]



let f { Block.call_graph = call_graph; Block.entries = entries; Block.global_variables = global_variables } =
  let entries =
    M.map
      (fun { Block.cfg = cfg; Block.v_entry = v_entry;  Block.args = args; Block.rets = rets; Block.stack_size = stack_size } ->
         { cfg = convert_cfg cfg; v_entry; args = args; rets = rets; stack_size = stack_size })
      entries
  in
  let entries =
    (* 定数テーブルに代入する文を追加 *)
    let func = M.find Id.main_label entries in
    let func =
      { func with
            cfg =
              G.map_stmt_list
                (function
                   | { left = { exp = Entry } } as stmt ->
                       stmt ::
                       (* [XXX] ゼロレジスタを再び設定する. store_atはゼロレジスタを必要とするため.
                        * これによって, 2度ゼロレジスタが設定されることになる. (Virtual2Blockでも設定されるので.) *)
                       [gen_stmt [Asm.zero, Type.Int] (Set (0))] @
                       ConstTable.fold
                         (fun bit pos acc -> store_at bit pos @ acc)
                         []
                   | stmt -> [stmt])
                func.cfg }
    in
      M.add Id.main_label func entries
  in
    remove_redundancy { call_graph = convert_call_graph call_graph; entries = entries; global_variables = global_variables }


(* 破壊的で全体集合が定まっている集合を扱う. *)

(* creator_ofで全体集合を保持した情報を構成する.
 * その情報を使って、集合に関する操作を破壊的に行う. *)
module Make =
  functor (Ord : Set.OrderedType) -> struct
    type elt = Ord.t

    module S = Set.Make (Ord)
    module M = Map.Make (Ord)
    module MI = Map.Make (struct type t = int let compare = compare end)

    (* 全体集合とBitvのインデクスの対応付けをする情報 *)
    type bijection = {
      elt2id : int M.t;
      id2elt : elt MI.t;
      count : int;
    }

    (* 集合の型. 集合の同一性は = で調べることができる. *)
    type t = {
      bitv : Bitv.t;
      info : bijection;
    }

    (* eltsを全体集合とした集合のコンストラクタを構築する.
     * creator: bool -> t *)
    let creator_of elts =
      let (count, elt2id) =
        S.fold
          (fun elt (count, elt2id) -> (count + 1, M.add elt count elt2id))
          elts (0, M.empty)
      in
      let id2elt = M.fold (fun elt id -> MI.add id elt) elt2id MI.empty in
      let bijection = {
        elt2id = elt2id;
        id2elt = id2elt;
        count = count;
      } in
        (* b = trueなら全体集合, b = falseなら空集合を返す関数 *)
        (fun b ->
           {
             bitv = Bitv.create bijection.count b;
             info = bijection;
           })

    (* 集合をコピーする *)
    let copy t = { t with bitv = Bitv.copy t.bitv }

    (* 第一オペランドの集合に破壊的に演算する *)
    let inter { bitv = x } { bitv = y } = Bitv.bw_and  x y
    let union { bitv = x } { bitv = y } = Bitv.bw_or   x y
    let diff  { bitv = x } { bitv = y } = Bitv.bw_diff x y
    let move  { bitv = x } { bitv = y } = Bitv.move    x y

    let equal { bitv = x } { bitv = y } = x = y

    (* 全体集合に含まれるか? *)
    let is_elt elt { info = { elt2id = elt2id } } = M.mem elt elt2id

    let is_empty { bitv = v } = Bitv.all_zeros v
    let is_universe { bitv = v } = Bitv.all_ones v

    let mem elt { bitv = v; info = { elt2id = elt2id } } =
      assert (M.mem elt elt2id);
      Bitv.get v (M.find elt elt2id)

    let add elt { bitv = v; info = { elt2id = elt2id } } =
      assert (M.mem elt elt2id);
      Bitv.set v (M.find elt elt2id) true

    (* tに属する要素を全て消去 *)
    let reset t = Bitv.reset t.bitv

    let remove elt { bitv = v; info = { elt2id = elt2id } } =
      assert (M.mem elt elt2id);
      Bitv.set v (M.find elt elt2id) false

    let to_list { bitv = v; info = { id2elt = id2elt } } =
      Bitv.foldi_left
        (fun acc id b -> if b then (MI.find id id2elt) :: acc else acc)
        [] v

    let subset { bitv = x } { bitv = y } = Bitv.is_subset x y

    let exists p { bitv = v; info = { id2elt = id2elt; count = n } } =
      let rec loop id =
        if id < n then
          (if Bitv.get v id && p (MI.find id id2elt) then
             true
           else loop (id + 1))
        else false
      in
        loop 0

    let find p { bitv = v; info = { id2elt = id2elt; count = n } } =
      let rec loop id =
        if id < n then
          (if Bitv.get v id && p (MI.find id id2elt) then
             MI.find id id2elt
           else loop (id + 1))
        else assert false
      in
        loop 0


  end


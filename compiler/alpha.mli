val f : KNormal.t -> KNormal.t
val g : Id.t M.t -> KNormal.t -> KNormal.t (* for Inline.g *)
val h : Id.t M.t -> KNormal.t -> KNormal.t (* for LambdaLifting *)

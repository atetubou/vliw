(* 投機的なスケジューリングを行う *)

open Vliw

module L = VListScheduling

module PQ =
  Set.Make
    (struct
       type t = int * G.vertex
       let compare = compare
     end)

type data = {
  (* 注目しているCFG *)
  cfg : G.t;
  (* cfgの基本ブロックの出口生存情報 *)
  liveness : S.t M.t;
  (* 優先度 *)
  priority : int M.t;
  (* 優先度付きキュー *)
  worklist : PQ.t;
}

let add_worklist b data =
  { data with worklist = PQ.add (M.find b data.priority, b) data.worklist }

    
let build cfg =
  let priority =
    let order = snd (G.topological_sort cfg) in
    let (_, acc) =
      List.fold_left
        (fun (i, acc) b -> (i+1, M.add b i acc))
        (0, M.empty) order
    in
      acc
  in
  let liveness = VLiveness.to_map (VLiveness.create cfg) in
  let data =
    {
      cfg = cfg;
      liveness = liveness;
      priority = priority;
      worklist = PQ.empty;
    }
  in
  let data = G.fold (fun b _ -> add_worklist b) data.cfg data in
    data


(* 投機的に分岐命令を超えて移動させてよい命令か. *)
let is_ok = function
  | Nop | Set _ | SetLabel _ | Mov _ | FMov _
  | Add _ | Sub _ | ShiftL _ | ShiftR _
  | FAdd _ | FMul _ | FInv _ | FSqrt _
  | Load _ | FLoad _
  | LoadC _ | FLoadC _
    -> true
  (* Call, Send, Recvや、分岐は"副作用"があるので不可能 *)
  | _ -> false

exception Giveup

let g_threshold = ref 5



(* 後続ブロックb'の命令をbに移動してみる.
 * 必要であればb'はコピーする. *)
let migrate b b' data =
  assert (G.has_edge b b' data.cfg);
  try
    if b = b' then raise Giveup;
    (* 自己ループがある場合は, 無限にブロックをコピーしてしまう. *)
    if G.has_edge b' b' data.cfg then raise Giveup;
    (* 必要であればb'をコピーして, b'の先行ブロックがbのみであるように変形する. *)
    let (b', data) =
      if G.pred b' data.cfg = [b] then
        (b', data)
      else if List.length (G.find b' data.cfg) <= !g_threshold then
        let (b'', cfg) = G.copy_block b' data.cfg in
        let cfg = G.expand (G.add_edge b'') b' cfg cfg in
        let cfg = G.add_edge b b'' (G.tag_of_edge b b' cfg) cfg in
        let cfg = G.remove_edge b b' cfg in
        let data = { data with liveness = M.add b'' (M.find b' data.liveness) data.liveness } in
        let data = { data with priority = M.add b'' (M.find b' data.priority) data.priority } in
          (b'', { data with cfg = cfg })
      else
        raise Giveup
    in
    assert (G.pred b' data.cfg = [b]);
    let open L in
    let { graph = graph; stmts = stmts' } = build (G.find b' data.cfg) in
    let stmts = G.find b data.cfg in
    let live = M.find b data.liveness in
    let updated = ref false in
    let (stmts, live, graph) =
      List.fold_left
        (fun ((stmts, live, graph) as next) v ->
           (* stmtをstmtsに繋げてみる *)
           let stmt = D.find v graph in
             if is_ok stmt.left.exp && is_ok stmt.right.exp &&
                (* b'の命令と依存関係を持たない *)
                D.in_degree_of v graph = 0 &&
                (* 生きている変数をつぶさない *)
                List.for_all (fun (x, _) -> not (S.mem x live)) (def_of stmt)
             then
               let (stmts', remain) = schedule (build ~stmtss:(stmts_before b data.cfg) ~ext:[stmt] (stmts @ [stmt])) in
                 if remain = [] && total_clk_of stmts' <= total_clk_of stmts then
                   (* [XXX] stmtが定義する変数は全て生きていると仮定して, bの出口でも生存していることにする.
                    * 保守的な近似になっている. 本当はuseの変数の一部は死ぬことになるだろうし, より精密にした方が良い. *)
                   let live = List.fold_left (fun live (x, _) -> S.add x live) live (def_of stmt) in
                   let graph = D.remove_vertex v graph in
                     (updated := true;
                      (stmts', live, graph))
                 else next
             else next)
        (stmts, live, graph) (snd (D.topological_sort graph))
    in
    if not !updated then raise Giveup;
    let data = { data with cfg = G.update_block b stmts data.cfg } in
      (*
      Format.eprintf "BLOCK@.%s@. -> @.%s@." (string_of_block "before" stmts')
        (string_of_block "after"
           (List.filter (fun stmt -> D.mem (id_of stmt) graph) stmts'));
       *)
    let stmts' = List.filter (fun stmt -> D.mem (id_of stmt) graph) stmts' in
    let stmts' = schedule_all ~stmtss:(stmts_before b' data.cfg) stmts' in
    let data = { data with cfg = G.update_block b' stmts' data.cfg } in
    let data = { data with liveness = M.add b live data.liveness } in
    (* 新しい文が加わったので, bの先行ブロックをスケジューリングする. *)
    let data = List.fold_right (fun b' data -> if b' = b then data else add_worklist b' data) (G.pred b data.cfg) data in
      (*
    (* b'もコピーされたのでもう一度スケジューリングする. *)
    let data = add_worklist b' data in
      if !copy_flag then
          Format.eprintf "%s, copy %s -> %s@." b (before_b) b';
       *)
      data
  with Giveup -> data


(* ワークリストから取り出して, そのブロックを更新する. *)
let update data =
  let (_, b) as elt = PQ.max_elt data.worklist in
  let data = { data with worklist = PQ.remove elt data.worklist } in
    try
      let stmtss = L.stmts_before b data.cfg in
      let stmts = G.find b data.cfg in
      let stmts = L.schedule_all ~stmtss stmts in
      let data = { data with cfg = G.update_block b stmts data.cfg } in
      let data =
        if
          (* Nopがあるか, ストールしていれば投機的スケジューリングを試みる *)
          List.exists
            (function
               | { left = { exp = Nop } } | { right = { exp = Nop } } -> true
               | stmt -> stmt.stall > 0)
            stmts
        then
          List.fold_left
            (fun data b' ->
               migrate b b' data)
            data
            (List.sort
               (Common.compare_on
                  (fun b' ->
                     let prob = try VProfile.prob_of b b' with Not_found -> 0.0 in
                       (-. prob, b')))
               (G.succ b data.cfg))
        else
          data
      in
        data
    with L.Impossible ->
      Format.eprintf "Failed scheduling: %s@." (string_of_block b (G.find b data.cfg));
      data

 
(* cfgに対して投機的スケジューリングを行う. *)
let speculate cfg =
  let rec loop data =
    if not (PQ.is_empty data.worklist) then
      loop (update data)
    else
      data
  in
  let data = build cfg in
  let data = loop data in
    data.cfg

let f ?(threshold = 5) prog =
  g_threshold := threshold;
  Format.eprintf "# Speculative Scheduling@.";
  Time.start "Speculative Scheduling";
  let prog = map_cfg (fun _ -> G.map (fun _ -> L.fill_delay_slot)) prog in
  let prog = map_cfg (fun _ -> speculate) prog in
  let prog = map_cfg (fun _ -> G.map (fun _ -> L.fill_delay_slot)) prog in
  Time.stop "Speculative Scheduling";
    prog



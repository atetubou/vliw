(* データフロー解析を用いた共通部分式除去.
 *
 *   x <- y + z
 *   (...)
 *   t <- y + z
 * というプログラムがあったときに,
 * (...)の間でx, y, zの値が変更されない場合に,
 *   x <- y + z
 *   (...)
 *   t <- x
 * に変更する.
 *
 * 一般的な大域的な共通部分式除去とは, 一時変数を作らないという点で異なる. *)


open Block

let is_target = function
  | { def = [x, _]; exp =
        Set _ | SetLabel _ | FSet _ | Mov _ | FMov _ | Add _ | Sub _ | ShiftL _ | ShiftR _
        | FAdd _ | FMul _ | FInv _ | FSqrt _ | Load _ | FLoad _ | LoadC _ | FLoadC _ | ItoF _ | FtoI _
    } -> true
  | _ -> false

(* 可換な式を適当な順番に入れ替える *)
let normalize = function
  | FMov (x, funct) -> FMov (x, List.sort compare funct)
  | FAdd (x, y, funct) when x > y && not (List.mem FPUBit funct) -> FAdd (y, x, List.sort compare funct)
  | FAdd (x, y, funct) -> FAdd (x, y, List.sort compare funct)
  | FMul (x, y, funct) when x > y && not (List.mem FPUBit funct) -> FMul (y, x, List.sort compare funct)
  | FMul (x, y, funct) -> FMul (x, y, List.sort compare funct)
  | FInv (x, funct) -> FInv (x, List.sort compare funct)
  | FSqrt (x, funct) -> FSqrt (x, List.sort compare funct)
  | Add (V (x), y) when x > y -> Add (V (y), x)
  | Load (x, V (y)) when x > y -> Load (y, V (x))
  | FLoad (x, V (y)) when x > y -> FLoad (y, V (x))
  | LoadC (x, V (y)) when x > y -> LoadC (y, V (x))
  | FLoadC (x, V (y)) when x > y -> FLoadC (y, V (x))
  | exp -> exp

let g cfg =
  let cfg = G.map_stmt (fun stmt -> { stmt with exp = normalize stmt.exp }) cfg in
  let avail = Avail.create ~is_target cfg in
  let cfg =
    G.map
      (fun b stmts ->
         let set = Avail.avail_of b avail in
           List.map
             (fun stmt ->
                let stmt' =
                  match stmt, Avail.exp_of avail stmt.exp set with
                    | { def = [_, Type.Float] }, x :: _ ->
                        gen_stmt stmt.def (FMov (x, []))
                    | { def = [_, _] }, x :: _ ->
                        gen_stmt stmt.def (Mov (x))
                    | _ -> stmt
                in
                  Avail.transfer avail set stmt;
                  stmt')
             stmts)
      cfg
  in
    cfg

let f prog = map_cfg (fun l cfg -> g cfg) prog


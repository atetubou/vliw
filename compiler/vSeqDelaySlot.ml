(* VSeq.tに対して遅延スロットを埋める. *)

open Vliw
open VSeq

module L = VListScheduling

let is_branch_stmt stmt = is_branch stmt.right.exp

exception Giveup


                            (*
let rec parent_of b env =
  if M.mem b env then
    parent_of (M.find b env)
  else
    b
                             *)


(* b'のすぐ後にbを追加.
 * ただし, bはsucc, prevともなし. *)
let insert b b' seq =
  let { prev; succ } = seq in
    assert (not (M.mem b prev));
    assert (not (M.mem b succ));
    if M.mem b' succ then
      let c = M.find b' succ in
      let prev = M.add c b prev in
      let prev = M.add b b' prev in
      let succ = M.add b' b succ in
      let succ = M.add b c succ in
        { seq with prev; succ }
    else
      let succ = M.add b' b succ in
      let prev = M.add b b' prev in
        { seq with prev; succ }

          (*
let insert b b' seq =
  if M.mem "b.234843" seq.prev then
    exit 1;
  if b = "b.1107907" && b' = "b.273445" then
    Format.eprintf "%s %s %s@." b' b (M.find b' seq.succ);
  let seq = insert b b' seq in
    if M.mem "b.234843" seq.prev then
      Format.eprintf "FOUND! %s %s %s@." b b' (M.find b' seq.succ);
    seq
           *)

let f seq =
  G.fold
    (fun b stmts seq ->
       try
         let cfg = seq.all_cfg in
           match partition_block (G.find b cfg) with
             | (stmts, ({ right = { exp = Jmp } } as branch), delay)
                 when List.for_all is_nop delay &&
                      (try VProfile.count_of b > 10000 with Not_found -> false)
               ->
                 let b' = G.succ_of b Other cfg in
                 let stmts' = G.find b' cfg in
                 let (new_delay, new_stmts') =
                   (match stmts' with
                      | ({ stall = 0 } as stmt1) :: stmt2 :: stmts' when not (is_branch_stmt stmt1) && not (is_branch_stmt stmt2) ->
                          ([stmt1; stmt2], stmts')
                      | ({ stall = 0 } as stmt1) :: stmts' when not (is_branch_stmt stmt1) ->
                          ([stmt1; gen_stmt [] Nop], stmts')
                      | stmt1 :: stmts' when not (is_branch_stmt stmt1) ->
                          ([stmt1], stmts')
                      | _ -> raise Giveup)
                 in
                 let cfg = G.update_block b (stmts @ [branch] @ new_delay) cfg in
                 let cfg = G.update_block b' new_delay cfg in
                 let (b'', cfg) = G.add_block new_stmts' cfg in
                 (* bからb''へ付け替え *)
                 let cfg = G.add_edge b b'' Other (G.remove_edge b b' cfg) in
                 (* b'の後続ブロックをb''に付け替え *)
                 let cfg = G.expand (fun c e_tag cfg -> G.add_edge b'' c e_tag (G.remove_edge b' c cfg)) b' cfg cfg in
                 (* b'のすぐ後にb''を配置 *)
                 let cfg = G.add_edge b' b'' Other cfg in
                 let seq = insert b'' b' seq in
                   { seq with all_cfg = cfg }
             | _ -> raise Giveup
       with Invalid_argument "partition_block" | Giveup -> seq)
    seq.all_cfg seq



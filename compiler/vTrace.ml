(* tail duplicationを行わないでスーパーブロックを検出する. *)

open Vliw

let max_element compare = function
  | [] -> invalid_arg "max_element"
  | x :: ls ->
      List.fold_left
        (fun x x' -> if compare x x' < 0 then x' else x)
        x ls

(* スーパーブロックの選択を行う *)
let select prog =
  let f cfg =
    let rec backward (acc, vis) b =
      let vis = S.add b vis in
      let pred = G.pred b cfg in
        (* 先行ブロックがちょうど1つの場合は, スーパーブロックを伸張する. *)
        match pred with
          | [b'] when not (S.mem b' vis) ->
              backward (b' :: acc, vis) b'
          | _ -> (acc, vis)
    in
    let rec forward (acc, vis) b =
      let vis = S.add b vis in
      let succ = G.succ b cfg in
        if succ = [] then
          (acc, vis)
        else
          let b' = max_element (Common.compare_on (fun b' -> Profile.count_of_edge b b')) succ in
            if S.mem b' vis || G.in_degree_of b' cfg <> 1 then
              (acc, vis)
            else
              forward (b' :: acc, vis) b'
    in
    let rec loop (acc, vis) = function
      | [] -> (acc, vis)
      | b :: ls when S.mem b vis -> loop (acc, vis) ls
      | b :: ls ->
          let (trace1, vis) = backward ([], vis) b in
          let (trace2, vis) = forward ([], vis) b in
            loop ((trace1 @ [b] @ List.rev trace2) :: acc, vis) ls
    in
    let vertices = G.fold (fun b _ acc -> b :: acc) cfg [] in
    let vertices = List.sort (Common.compare_on (fun b -> - Profile.count_of b)) vertices in
    let (traces, _) = loop ([], S.empty) vertices in
      traces
  in
    M.fold
      (fun l func acc ->
         let traces = f func.cfg in
           M.add l traces acc)
      prog.entries M.empty


(* スーパーブロックになっているか確認する. *)
let rec check_superblock cfg = function
  | [] -> ()
  | trace :: traces ->
      assert (trace <> []);
      List.iter
        (fun b -> assert (G.in_degree_of b cfg = 1))
        (List.tl trace);
      check_superblock cfg traces
  

let f prog =
  let func2traces = select prog in
    M.iter
      (fun l traces ->
         check_superblock (M.find l prog.entries).cfg traces)
      func2traces;
    func2traces



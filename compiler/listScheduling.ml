open Block

(* 依存グラフ *)
module D =
  Graph.Make
    (struct
       type vertex = Id.t
       type v_tag = stmt
       (* 辺の始点が発行された後, 何クロックで演算結果が利用可能になるか.
        * すなわち, 先行命令のレイテンシ. *)
       type e_tag = int
     end)


module PQ = Set.Make (struct type t = int * Id.t let compare = compare end)

let stall = ref 0 (* stall count *)

type data = {
  (* 依存グラフ *)
  graph : D.t;
  (* 発行完了を待っている命令の集合 *)
  waiting : PQ.t;
  (* 発行可能な命令の集合 *)
  ready : PQ.t;
  (* 命令の発行が完了するクロックの下限 *)
  finclk : int D.M.t;
  (* readyから取り出す命令の優先順位 *)
  priority : int D.M.t;
}

let is_dependant ?(branch_ok = false) exp exp' =
  let is_store = function
    | Store _ | FStore _ -> true
    | _ -> false
  in
  let is_load = function
    | Load _ | FLoad _ -> true
    | _ -> false
  in
  let is_io = function
    | Send _ | Recv -> true
    | _ -> false
  in
  (* いかなる命令とも入れ替えられないもの = 分岐 or Call, 疑似命令 *)
  let is_critical = function
    | Call _ | Entry | Return -> true
    | exp when not branch_ok -> is_branch exp
    | _ -> false
  in
    is_critical exp || is_critical exp' ||
    (is_io exp && is_io exp') ||
    (is_store exp && is_store exp') ||
    (is_store exp && is_load exp')  ||
    (is_load exp && is_store exp')

(* 演算結果が使用可能になるまで、間に何clock必要か。 *)
let get_latency_of = function
  (* FPU *)
  | FMov _ | FAdd _ | FMul _ | FInv _ | FSqrt _
    -> 2
  (* load from SRAM *)
  | Load _ | FLoad _
    -> 2
  | _ -> 0

(* 実際に発行される命令数 *)
let get_clk_of = function
  | Nop | Entry -> 0
  | Set (i) -> if -(1 lsl 15) <= i && i < (1 lsl 15) then 1 else 2
  | FSet (f) ->
      let bits = Int32.to_int (Int32.bits_of_float (f)) in
        (* 下位16bitがゼロなら1clk *)
        if bits land (1 lsl 16 - 1) = 0 then 1 else 2
(*  | Mov _ | FMov _ -> 0 (* もしかするとcoalescingされる *)*)
  | _ -> 1

(* stmt -> stmt'の順番で命令があるとき, RAWハザードが生ずるか *)
let is_raw stmt stmt' =
  List.exists (fun (x, _) -> List.mem x stmt'.use) stmt.def

let is_war_or_waw stmt stmt' =
  (* WAR *)
  List.exists (fun (x, _) -> List.mem x stmt.use) stmt'.def ||
  (* WAW *)
  List.exists (fun (x, _) -> List.mem_assoc x stmt.def) stmt'.def

let is_hazardable ?(branch_ok = false) s t = is_raw s t || is_war_or_waw s t || is_dependant ~branch_ok s.exp t.exp

let build stmts =
  let graph =
    List.fold_left
      (fun graph stmt ->
         let graph = D.add_vertex stmt.s_id stmt graph in
         let graph =
           D.fold
             (fun _ stmt' graph ->
                if stmt <> stmt' && is_hazardable stmt' stmt then
                  (* RAWハザードの場合は、演算結果が使用可能になるまでlatencyだけ待つ必要がある *)
                  D.add_edge stmt'.s_id stmt.s_id (if is_raw stmt' stmt then get_latency_of stmt'.exp else 0) graph
                else
                  graph)
             graph graph
         in
           graph)
      D.empty stmts
  in
  let init = D.fold (fun v _ acc -> D.M.add v 0 acc) graph D.M.empty in
  (* グラフを逆順からDPして、出口までの最大の長さ（クリティカルパス）を求める *)
  let priority =
    D.topological
      (fun v w _ env -> 
         let p' = max (D.M.find v env + 1) (D.M.find w env) in
           D.M.add w p' env)
      (D.rev graph) init
  in
  let priority = D.M.mapi (fun v p -> get_latency_of (D.find v graph).exp + p) priority in
  let ready = D.fold (fun v _ ready -> if D.pred v graph = [] then PQ.add (D.M.find v priority, v) ready else ready) graph PQ.empty in
    {
      graph = graph;
      waiting = PQ.empty;
      ready = ready;
      finclk = init;
      priority = priority;
    }

(* クロックをclkまで進める *)
let next_clk data clk =
  let rec loop data =
    if PQ.is_empty data.waiting then
      data
    else
      let (clk', v) as elt = PQ.min_elt data.waiting in
        (* vをreadyに移動させる *)
        if clk' <= clk then
          let data =
            { data with
                  waiting = PQ.remove elt data.waiting;
                  ready = PQ.add (D.M.find v data.priority, v) data.ready }
          in
            loop data
        else data
  in
    if PQ.is_empty data.waiting then
      data
    else
      loop data

let scheduling stmts =
  let data = build stmts in
  (* すべての演算結果が利用可能になるまでの時間 *)
  let totalclk = ref 0 in
  let rec loop data clk =
    match (PQ.is_empty data.ready, PQ.is_empty data.waiting) with
      | (true, true) -> assert (D.is_empty data.graph); (0, [])
      (* クロックを進めてwaitingからreadyへ移動させる *)
      | (true, false) ->
          let (clk', _) = PQ.min_elt data.waiting in
          let data = next_clk data clk' in
          let (stall, stmts) = loop data clk' in
            (stall + (clk' - clk), stmts)
      (* readyから命令を発行する *)
      | (false, _) ->
          let (_, v) as ready_node = PQ.max_elt data.ready in
          let data = { data with ready = PQ.remove ready_node data.ready } in
          let stmt = D.find v data.graph in
          (* 命令を発行したので、クロックを進める *)
          let clk = clk + get_clk_of stmt.exp in
          let () = totalclk := max !totalclk (clk + get_latency_of stmt.exp) in
          let data =
            D.expand
              (fun w latency data ->
                 let data =
                   { data with
                         finclk = D.M.add w (max (D.M.find w data.finclk) (clk + latency)) data.finclk }
                 in
                 let data =
                   { data with
                         waiting =
                           (* vにしか依存していないなら、あとはデータがそろうのを待つだけ *)
                           if D.pred w data.graph = [v] then
                             PQ.add (D.M.find w data.finclk, w) data.waiting
                           else
                             data.waiting }
                 in
                   data)
              v data.graph data
          in
          (* 発行した命令を、graphから削除 *)
          let data = { data with graph = D.remove_vertex v data.graph } in
          let data = next_clk data clk in
          let (stall, stmts) = loop data clk in
            (stall, stmt :: stmts)
  in
  let (stall, stmts) = loop data 0 in
    (stall, !totalclk, stmts)

let g cfg =
  G.map (fun _ stmts -> let (st, _, stmts) = scheduling stmts in stall := !stall + st; stmts) cfg



let f prog =
  let res =
    { prog with
          entries =
            M.map (fun ({ cfg = cfg } as fundef) -> { fundef with cfg = g cfg }) prog.entries }
  in
    Format.eprintf "Scheduling: predicted stall = %d@." !stall;
    res


(* bの後続ブロックのうち、bに移せる可能性のあるものを優先度順に列挙する *)
let ready_of_succ b cfg =
  let ready =
    G.expand
      (fun b' _ ->
         let stmts = G.find b' cfg in
         let data = build stmts in
           PQ.fold (fun (pr, v) acc -> (pr, b', D.find v data.graph) :: acc) data.ready)
      b cfg []
  in
    List.map (fun (_, b, stmt) -> (b, stmt)) (List.rev (List.sort compare ready))


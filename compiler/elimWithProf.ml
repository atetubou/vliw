(* プロファイル情報によってブロックの削除を行う.
 * 必要なブロックを--elim <file>として与えてやると, それ以外のブロックが削除される. *)

open Block

let g needed cfg =
  G.fold
    (fun b _ cfg ->
       if not (S.mem b needed) then
         (* 先行ブロックがif文であればJmpに置き換える *)
         let cfg =
           List.fold_left
             (fun cfg b' ->
                match partition_last (G.find b' cfg) with
                  | ({ exp = IfEq _ | IfLt _ | IfFEq _ | IfFLt _ }, stmts) ->
                      (* Jmpで置き換え. *)
                      let cfg = G.update_block b' (stmts @ [gen_stmt [] Jmp]) cfg in
                      (* 辺をOtherで置き換え *)
                      let cfg = G.expand (fun b e_tag -> G.add_edge b' b Other) b' cfg cfg in
                        cfg
                  | _ -> cfg)
             cfg (G.pred b cfg)
         in
           G.remove_vertex b cfg
       else
         cfg)
    cfg cfg

let f elim_file prog =
  let ic = open_in elim_file in
  let rec loop acc =
    try
      loop (S.add (input_line ic) acc)
    with End_of_file -> acc
  in
  let needed = loop S.empty in
    map_cfg (fun _ -> g needed) prog


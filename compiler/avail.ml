(* 利用可能式のデータフロー解析
 *
 * 一変数xを定義する式expについて，xとexpの値を変えることなく到達するかどうか.
 * メモリの状態を変える命令があった場合は，Load系の式は全て消滅する. *)
open Block

module DS =
  DestructiveSet.Make
    (struct
       (* def, exp *)
       type t = Id.t * exp
       let compare = compare
     end)

module ME =
  Map.Make
    (struct
       type t = exp
       let compare = compare
     end)

type t = {
  avail : DS.t M.t;
  transfer : DS.t -> stmt -> unit;
  (* 変数 -> その変数を定義する式のリスト *)
  var2defs : exp list M.t;
  (* 変数 -> その変数が現れるavailの要素のリスト *)
  var2elts : (Id.t * exp) list M.t;
  (* 式 -> その式が代入される変数のリスト *)
  exp2vars : Id.t list ME.t;
}

let transfer t set stmt = t.transfer set stmt

let avail_of b t = DS.copy (M.find b t.avail)

(* setから変数xを定義する式を返す.
 * xを定義する利用可能式は高々1つであるので，exp option型を返す. *)
let def_of t x set =
  try
    Some
      (List.find
         (fun exp -> DS.mem (x, exp) set)
         (M.find x t.var2defs))
  with Not_found -> None

(* setから式expと同じ値を持つ変数のリストを得る *)
let exp_of t exp set =
  try
    List.filter (fun x -> DS.mem (x, exp) set) (ME.find exp t.exp2vars)
  with Not_found -> []

(* stmtからdefとexpの組を作る *)
let extract = function
  | { def = [x, _]; exp = exp } -> (x, exp)
  | _ -> assert false

(* is_target : stmt -> そのステートメントの式に対して利用可能式の解析を行うか *)
let create ~is_target cfg =
  Time.start "Available expressions analysis";
  let direction = DataFlow.Forward in
  (* 対象となる式について，一変数のみを定義するというのが必要条件.
   * さらに，a <- a + b のような式は，すぐにaの値が変更されるので無視する． *)
  let is_target = function { def = [x, _] } as stmt when not (List.mem x stmt.use) -> is_target stmt | _ -> false in
  let universe =
    G.fold_stmt
      (fun stmt acc -> if is_target stmt then DS.S.add (extract stmt) acc else acc)
      cfg DS.S.empty
  in
  let creator = DS.creator_of universe in
  (* メモリからロードする命令を集める *)
  let mems = creator false in
  let mems_c = creator false in
  let () =
    G.iter
      (fun _ ->
         List.iter
           (function
              | { exp = Load _ | FLoad _ } as stmt
                  when is_target stmt
                ->
                  DS.add (extract stmt) mems
              | { exp = LoadC _ | FLoadC _ } as stmt
                  when is_target stmt
                ->
                  DS.add (extract stmt) mems_c
              | _ -> ()))
      cfg
  in
  (* var2defsなどは，重複を含み得るリストであることに注意 *)
  let (var2defs, var2elts, exp2vars) =
    G.fold_stmt
      (fun stmt (var2defs, var2elts, exp2vars) ->
         if is_target stmt then
           let (x, exp) = extract stmt in
             (
               M.add_ls x exp var2defs,
               List.fold_left
                 (fun acc y -> M.add_ls y (x, exp) acc)
                 var2elts (x :: stmt.use),
               ME.add exp
                 (x :: if ME.mem exp exp2vars then ME.find exp exp2vars else [])
                 exp2vars
             )
         else (var2defs, var2elts, exp2vars))
      cfg (M.empty, M.empty, ME.empty)
  in
  let transfer set stmt =
    (* メモリの状態が変更される場合は，Load系の式を全て消去
     * Callもメモリの状態を変更しうることに注意． *)
    (match stmt with
       | { exp = Call _ } ->
           DS.diff set mems;
           DS.diff set mems_c
       | { exp = Store _ | FStore _ } ->
           DS.diff set mems
       | { exp = StoreC _ | FStoreC _ } ->
           DS.diff set mems_c
       | _ -> ());
    (* Storeが行われた直後は, Loadしても前にStoreした値に等しい.
     * ゆえに冗長性が除去できる. *)
    (match stmt.exp with
       | Store (x, y, z) ->
           let pair = (x, Load (y, z)) in
             if DS.is_elt pair set then
               DS.add pair set
       | FStore (x, y, z) ->
           let pair = (x, FLoad (y, z)) in
             if DS.is_elt pair set then
               DS.add pair set
       | StoreC (x, y, z) ->
           let pair = (x, LoadC (y, z)) in
             if DS.is_elt pair set then
               DS.add pair set
       | FStoreC (x, y, z) ->
           let pair = (x, FLoadC (y, z)) in
             if DS.is_elt pair set then
               DS.add pair set
       | _ -> ());
    (* stmt.defによって消滅させる *)
    List.iter (fun (x, _) -> if M.mem x var2elts then List.iter (fun s -> DS.remove s set) (M.find x var2elts)) stmt.def;
    (* 式が生成される *)
    if is_target stmt then DS.add (extract stmt) set
  in
  let (gen, mask) = DataFlow.transfer_of_block ~direction ~transfer ~creator cfg in
  (* 境界条件: pred(b) = \emptyset ならば reach(b) = \emptyset *)
  let avail = G.fold (fun b _ -> M.add b (creator (G.pred b cfg <> []))) cfg M.empty in
  let update b = function
    | [] -> false
    | pred ->
        let this = creator true in
          List.iter
            (fun b' ->
               let temp = DS.copy (M.find b' avail) in
                 DS.union temp (M.find b' gen);
                 DS.inter temp (M.find b' mask);
                 DS.inter this temp)
            pred;
          let old = M.find b avail in
          let updated = not (DS.equal this old) in
            DS.move old this;
            updated
  in
    DataFlow.solution_of_block ~direction ~update cfg;
    Time.stop "Available expressions analysis";
    {
      avail = avail;
      transfer = transfer;
      var2defs = var2defs;
      var2elts = var2elts;
      exp2vars = exp2vars;
    }


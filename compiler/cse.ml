(* common subexpression elimination; cse *)

open KNormal

module ME = 
  Map.Make
    (struct
       type t = KNormal.t
       (* Getを最小元だと考える *)
       let order = function Get _ -> 0 | _ -> 1
       let compare x y = compare (order x, x) (order y, y)
     end)

let rec effect = function
  | Let (_, e1, e2) | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) -> effect e1 || effect e2
  | LetRec (_, e) | LetTuple (_, _, e) -> effect e
  | App _ | Put _ | ExtFunApp _ -> true
  | _ -> false

let find e env = if ME.mem e env then Var (ME.find e env) else e

let add e x env =
  if effect e then
    env
  else ME.add e x env

(* envに含まれるGetを削除する *)
let rec elim_get env =
  if ME.is_empty env then
    env
  else
    match ME.min_binding env with
      | (Get _ as e, _) -> elim_get (ME.remove e env)
      | _ -> env


let rec g env = function
  | IfEq (x, y, e1, e2) ->
      let e' = IfEq (x, y, g env e1, g env e2) in
        find e' env
  | IfLE (x, y, e1, e2) ->
      let e' = IfLE (x, y, g env e1, g env e2) in
        find e' env
  | Let ((x, t), e1, e2) ->
      let e1 = g env e1 in
      (* 式e1とxとの対応を環境に追加する *)
      let env = add e1 x env in
      (* e1に副作用があった場合は, Getを削除する *)
      let env =
        if effect e1 then
          elim_get env
        else env
      in
      let env =
        match e1 with
          | Put (x, y, z) ->
              (* Putの場合は, 同じindexから読み出すGetを生成すると考える *)
              ME.add (Get (x, y)) z env
          | _ -> env
      in
        Let ((x, t), e1, g env e2)
  | LetRec (func, e) ->
      (* 関数をまたいだ共通部分式除去を行うと, クロージャができてしまうので行わない. *)
      LetRec ({ func with body = g ME.empty func.body }, g env e)
  | LetTuple (xts, y, e) ->
      LetTuple (xts, y, g env e)
  | e -> find e env

(* タプルの共通部分式除去
 * 分岐の前にLetTupleが移動してしまうので, 命令数が増えてしまう. *)
let rec h env = function
  | IfEq (x, y, e1, e2) -> IfEq (x, y, h env e1, h env e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, h env e1, h env e2)
  | Let (xt, e1, e2) -> Let (xt, h env e1, h env e2)
  | LetRec ({ body = e1 } as fundef, e2) -> LetRec ({ fundef with body = h M.empty e1 }, h env e2)
  (* 既に分解されていて、利用可能な式がある *)
  | LetTuple (xts, y, e) when M.mem y env ->
      let alpha = List.fold_left2 (fun alpha (x, _) (y, _) -> M.add x y alpha) M.empty xts (M.find y env) in
        (* すでに展開されたものを使うようにする *)
        h env (Alpha.h alpha e)
  | LetTuple (xts, y, e) -> LetTuple (xts, y, h (M.add y xts env) e)
  | e -> e

let f e = g ME.empty e


let limit = ref 15
let output_file = ref ""

let profile = ref ""
let save_inter_code = ref ""
let load_inter_code = ref ""
let elim_file = ref ""

let do_expand = ref true

(* 0: 今後もプロファイル情報を使用しないようなコンパイル.
 * 1: プロファイルを使用しない普通のコンパイル.
 * 2: プロファイルを使って, 不要なブロックの除去とtail duplicationを行う. --loadなどのオプションは必須.
 * 3: プロファイルを使って, スーパーブロックスケジューリングを行う. (tail duplicationはしない.)
 *)
let stage = ref 0

let id x = x

let rec knormal_optimize n e =
  Format.eprintf "Iteration %d/%d@." n !limit;
  if !Common.optimize then
    Elim.f (ConstFold.f (TupleExpand.f (Inline.f (Assoc.f (Beta.f (Cse.f e))))))
  else
    Elim.f (ConstFold.f (TupleExpand.f ((*Inline.f*) (Assoc.f (Beta.f (Cse.f e))))))
      

let save_ref_data () = (!Id.counter, !Global.all)
let restore_ref_data (counter, all) =
  Id.counter := counter;
  Global.all := all

let save prog =
  if !save_inter_code <> "" then (
    Format.eprintf "Save intermediate code to %s@." !save_inter_code;
    let oc = open_out_bin !save_inter_code in
      Marshal.to_channel oc (save_ref_data (), prog) [];
      close_out oc
  )
let load () =
  let ic = open_in_bin !load_inter_code in
  let (refs, prog) = Marshal.from_channel ic in
    restore_ref_data refs;
    close_in ic;
    Format.eprintf "Successfully load intermediate code from %s@." !load_inter_code;
    prog



let run outchan syntax =
  Id.counter := 0;
  Typing.extenv := M.empty;
  (*
  let virt = load () in
      let virt = TypingVirtual.typing virt in
        ignore (virt);
        ()
   *)
  let closure =
    Common.ifdbg (Closure.output "Closure.t.out")
      (Closure.f
         (Common.ifdbg (KNormal.output "KNormal.t.opt.out")
            (Common.iter !limit knormal_optimize
               (Common.iter !limit (fun _ e -> LambdaLifting.f e)
                     ((if !do_expand then Expand2DArray.f else (fun e -> e))
                              ((*(KNormal.output "k.out")*)
                  (Common.iter !limit knormal_optimize
                     (Global.f
                        (ConstArray.f
                           (TupleExpand.h M.empty M.empty
                                ((*(KNormal.output "KNormal.t.out")*)
                                   ((*ConstFold.swap_branch*)
                           (Common.iter !limit knormal_optimize
                                   (InverseUnrolling.f
                                      (Alpha.f
                                         (KNormal.f
                                            (Typing.f
                                               (Common.ifdbg (Syntax.output "Syntax.t.out")
                                                  syntax))))))))))))))))))
  in
    try
      let virt = Virtual.f closure in
      let virt = if !do_expand then ExpandInner.f virt else virt in
      let virt = TypingVirtual.typing virt in
      let block = Virtual2Block.f virt in
      let block = Reference.f block in
      let block = OptDataFlow.f block in
      save block;
      let vliw = Block2VLIW.f block in
      let vliw = if !stage = 0 then VListScheduling.f vliw else vliw in
      let vliw = VRegAlloc.f vliw in
      let vliw = if !stage = 0 then VListScheduling.f vliw else vliw in
        VEmit.f outchan vliw
    with
      | Virtual.NotSimple e ->
          (* なんらかの理由でコンパイルを諦める. *)
          Format.eprintf "Failed: %s@." e;
          exit 1
      | e -> raise e

let parse file =
  if not (Filename.check_suffix file ".ml") then
    failwith ("an incorrect suffix of file name '"^file^"'")
  else
    let inchan = Common.add_file file in
      Parser.main Lexer.token (Lexing.from_channel inchan)

let knormal files =
  let syntaxes = List.map (fun file -> parse file) files in
  let syntax = List.fold_left (fun acc x -> Syntax.concat acc x) Syntax.Unit syntaxes in
    KNormal.f (Typing.f (syntax))

let main_entry () =
  let files = ref [] in
    Arg.parse
      [("--inline", Arg.Int(fun i -> Virtual2Block.threshold := i), "maximum cost of while-loop inlining");
       ("--merge", Arg.Int(fun i -> OptDataFlow.merge_threthold := i), "maximum number of block to merge");
       ("--prof", Arg.String (fun s -> profile := s), "profile file");
       ("--elim", Arg.String (fun s -> elim_file := s), "profile for elim");
       ("--save", Arg.String (fun s -> save_inter_code := s), "save intermediate code file");
       ("--load", Arg.String (fun s -> load_inter_code := s), "load intermediate code file");
       ("--stage", Arg.Int (fun i -> stage := i), "profile-based stage (default = 1)");
       ("--do-not-expand", Arg.Unit (fun i -> do_expand := false), "not expand global array");
       ("-o", Arg.String(fun s -> output_file := s), "output file");
       ("-debug", Arg.Unit(fun () -> Common.debug_flag := true), "debug");
       ("-verbose", Arg.Unit(fun () -> Common.verbose := true), "enable verbose output");
       ("-O0", Arg.Unit(fun () -> Common.optimize := false), "debug")]
      (fun s -> files := !files @ [s])
      ("Tailed Min-Caml Compiler (thanks to Mitou Min-Caml Compiler (C) Eijiro Sumii)\n" ^
       Printf.sprintf "usage: %s [options] files" Sys.argv.(0));
    let outname =
      if !output_file <> "" then
        !output_file
      else if !files <> [] then
        Filename.chop_suffix (List.hd (List.rev !files)) ".ml" ^ ".s"
      else
        "a.s"
    in
    let () = Format.eprintf "output: %s@." outname in
    let outchan = open_out outname in
      (match !stage with
         | 0 | 1 when !files = []->
             Format.eprintf "No input file. Try --help option.@."
         | 0 | 1 ->
             let syntaxes = List.map (fun file -> parse file) !files in
             let syntax = List.fold_left (fun acc x -> Syntax.concat acc x) Syntax.Unit syntaxes in
                 run outchan syntax
         | _ when !load_inter_code = "" ->
             (* 以下の最適化レベルでは中間コードは必須 *)
             Format.eprintf "No intermediate code. Please specify it by --load option.@."
         | 2 ->
             Format.eprintf "Load intermediate code from %s@." !load_inter_code;
             let block = load () in
             (* --elimが指定されていれば, プロファイルから不要なブロックを削除する. *)
             let block =
               if !elim_file = "" then
                 block
               else
                 ElimWithProf.f !elim_file block
             in
             Profile.load !profile block;
             let (traces, block) = Trace.f block in
             let block = OptDataFlow.f block in
             save block;
             let vliw = Block2VLIW.f block in
             let vliw = VRegAlloc.f vliw in
               VEmit.f outchan vliw
         | 3 ->
             let block = load () in
             Profile.load !profile block;
             let vliw = Block2VLIW.f block in
             let vliw = VTraceScheduling.f (VTrace.f vliw) vliw in
             let () = Vliw.check_consistency vliw in
             let vliw = Vliw.simplify vliw in
             let vliw = VMerge.f vliw in
             let vliw = Vliw.simplify vliw in
             let vliw = VListScheduling.f vliw in
             let vliw = VListScheduling.f vliw in
             let vliw = VRegAlloc.f vliw in
             let vliw = VListScheduling.f vliw in
             let vliw = VListScheduling.f vliw in
             let vliw = Vliw.simplify vliw in
               save vliw;
               VEmit.f outchan vliw
         | 4 ->
             let vliw = load () in
             VProfile.load !profile vliw;
             let vliw = VSpeculativeScheduling.f ~threshold:4 vliw in
             let vliw = Vliw.simplify vliw in
             let vliw = VMerge.f vliw in
             let vliw = Vliw.simplify vliw in
             let vliw = VListScheduling.f vliw in
             let vliw = VListScheduling.f vliw in
             let vliw = Vliw.simplify vliw in
               save vliw;
               VEmit.f outchan vliw
         | 5 ->
             let vliw = load () in
             let vliw =
               if !elim_file = "" then
                 vliw
               else
                 VElimWithProf.f !elim_file vliw
             in
             VProfile.load !profile vliw;
             let vseq = VSeq.build vliw in
             let vseq = VSeqListScheduling.f vseq in
             let vseq = VSeqListScheduling.f vseq in
             let vseq = VSeqDelaySlot.f vseq in
             let vseq = VSeqListScheduling.f vseq in
             let vseq = VSeqListScheduling.f vseq in
             let vseq = VSeqDelaySlot.f vseq in
             let vseq = VSeqListScheduling.f vseq in
             let vseq = VSeqListScheduling.f vseq in
               VEmitSeq.f outchan vseq
         | _ -> failwith "no such stage.");
      close_out outchan;
      Common.finalize


let () =
  try main_entry () with
(*    | Failure (s) -> prerr_endline ("Error: " ^ s); exit 1*)
    | Typing.Error (p, t1, t2) ->
        Format.eprintf "Error : '%s' is incompatible with '%s' in %s.@." (Type.string_of t1) (Type.string_of t2) (Common.string_of_position p); exit 1

(* 仮想マシンコードを生成する. *)

(* コンパイルすることを諦めるときに発生する例外 *)
exception NotSimple of string

type t =
  | Unit
  | Int of int
  | Float of float
  | Add of Id.t * Id.t
  | Sub of Id.t * Id.t
  | ShiftL of Id.t * int
  | ShiftR of Id.t * int
  | FNeg of Id.t
  | FAbs of Id.t
  | FAdd of Id.t * Id.t
  | FSub of Id.t * Id.t
  | FMul of Id.t * Id.t
  | FInv of Id.t
  | IfEq of Id.t * Id.t * t * t
  | IfLE of Id.t * Id.t * t * t
  | Let of (Id.t * Type.t) * t * t
  | Var of Id.t
  | Call of Id.t * Id.t list
  | Tuple of Id.t list
  | LetTuple of (Id.t * Type.t) list * Id.t * t
  | Get of Id.t * Id.t
  | Put of Id.t * Id.t * Id.t
  | ExtArray of Id.t
  (* 平方根 *)
  | FSqrt of Id.t
  (* 1byte出力 *)
  | Send of Id.t
  (* 1byte入力 *)
  | Recv
  (* intからfloatへ変換; read_floatのため. *)
  | ItoF of Id.t
  | FtoI of Id.t
  (* 型に合うようなStoreを行う *)
  | Store of Id.t * Id.t * int
  | Load of Type.t * Id.t * int

(* 関数の情報 *)
type func = {
  (* 関数のCFG *)
  body : t;
  (* 引数. *)
  args : (Id.t * Type.t) list;
  (* 型 *)
  typ : Type.t;
  (* キャッシュに上に配置するもの.
   * この集合に属する配列に対してPut or Getする場合は, キャッシュに対してStore or Load.
   * 実は全ての関数で同じ集合を指している. *)
  cache : S.t;
}

let insert e typ f =
  let x = Id.gentmp typ in
    Let ((x, typ), e, f x)

let seq (e1, e2) =
  Let ((Id.gentmp Type.Unit, Type.Unit), e1, e2)


(* read系の関数を導入する. *)
let build_stub prog =
  let prog =
    let l = "read_int" in
    let body =
      insert Recv Type.Int (fun x1 ->
      insert (ShiftL (x1, 24)) Type.Int (fun x1 ->
      insert Recv Type.Int (fun x2 ->
      insert (ShiftL (x2, 16)) Type.Int (fun x2 ->
      insert Recv Type.Int (fun x3 ->
      insert (ShiftL (x3, 8 )) Type.Int (fun x3 ->
      insert Recv Type.Int (fun x4 ->
      insert (Add (x1, x2)) Type.Int (fun x12 ->
      insert (Add (x3, x4)) Type.Int (fun x34 ->
      insert (Add (x12, x34)) Type.Int (fun res ->
             (Var (res))))))))))))
    in
      M.add l { body = body; args = [Id.gentmp Type.Unit, Type.Unit]; typ = Type.Fun ([Type.Unit], Type.Int); cache = S.empty } prog
  in
  let prog =
    let l = "read_float" in
    let body =
      insert Recv Type.Int (fun x1 ->
      insert (ShiftL (x1, 24)) Type.Int (fun x1 ->
      insert Recv Type.Int (fun x2 ->
      insert (ShiftL (x2, 16)) Type.Int (fun x2 ->
      insert Recv Type.Int (fun x3 ->
      insert (ShiftL (x3, 8 )) Type.Int (fun x3 ->
      insert Recv Type.Int (fun x4 ->
      insert (Add (x1, x2)) Type.Int (fun x12 ->
      insert (Add (x3, x4)) Type.Int (fun x34 ->
      insert (Add (x12, x34)) Type.Int (fun res ->
             (ItoF (res))))))))))))
    in
      M.add l { body = body; args = [Id.gentmp Type.Unit, Type.Unit]; typ = Type.Fun ([Type.Unit], Type.Float); cache = S.empty } prog
  in
  let prog =
    (* head + 0 から head + size - 1 までをinitで初期化する. *)
    let l = "fill_for_int" in
    let head = Id.genid "head" in
    let size = Id.genid "size" in
    let init = Id.genid "init" in
    let args = [head, Type.Int; size, Type.Int; init, Type.Int] in
    let body =
      insert (Int (0)) Type.Int (fun zero ->
      insert (Int (1)) Type.Int (fun one ->
      insert (Sub (size, one)) Type.Int (fun index ->
      insert (Add (index, head)) Type.Int (fun pos ->
        IfLE (zero, index,
              seq (Store (init, pos, 0),
                   Call (l, [head; index; init])),
              Unit)))))
    in
      M.add l { body = body; args = args; typ = Type.Fun ([Type.Int; Type.Int; Type.Int], Type.Unit); cache = S.empty } prog
  in
  let prog =
    let l = "fill_for_float" in
    let head = Id.genid "head" in
    let size = Id.genid "size" in
    let init = Id.genid "init" in
    let args = [head, Type.Int; size, Type.Int; init, Type.Float] in
    let body =
      insert (Int (0)) Type.Int (fun zero ->
      insert (Int (1)) Type.Int (fun one ->
      insert (Sub (size, one)) Type.Int (fun index ->
      insert (Add (index, head)) Type.Int (fun pos ->
        IfLE (zero, index,
              seq (Store (init, pos, 0),
                   Call (l, [head; index; init])),
              Unit)))))
    in
      M.add l { body = body; args = args; typ = Type.Fun ([Type.Int; Type.Int; Type.Float], Type.Unit); cache = S.empty } prog
  in


    prog


(* プログラム全体 *)
type prog = func M.t

(* 式のサイズを調べる *)
let rec size = function
  | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) -> 1 + size e1 + size e2
  | LetTuple (_, _, e) -> 1 + size e
  | _ -> 1

(* 自由変数 *)
let rec fv = function
  | Unit | Int(_) | Float(_) | ExtArray(_) | Recv -> S.empty
  | FNeg(x) | FAbs(x) | FInv(x) | FSqrt (x) | ShiftL(x, _) | ShiftR(x, _) | Send (x) | ItoF (x) | FtoI (x) | Load (_, x, _) -> S.singleton x
  | Add(x, y) | Sub(x, y) | FAdd(x, y) | FSub(x, y) | FMul(x, y) | Get(x, y) | Store (x, y, _) -> S.of_list [x; y]
  | IfEq(x, y, e1, e2)| IfLE(x, y, e1, e2) -> S.add x (S.add y (S.union (fv e1) (fv e2)))
  | Let((x, t), e1, e2) -> S.union (fv e1) (S.remove x (fv e2))
  | Var(x) -> S.singleton x
  | Call(_, xs) | Tuple(xs) -> S.of_list xs
  | LetTuple(xts, y, e) -> S.add y (S.diff (fv e) (S.of_list (List.map fst xts)))
  | Put(x, y, z) -> S.of_list [x; y; z]

let rec has_effect = function
  | Let (_, e1, e2) | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) -> has_effect e1 || has_effect e2
  | LetTuple (_, _, e) -> has_effect e
  | Unit | Int _ | Float _ | Add _ | Sub _ | ShiftL _ | ShiftR _ | FNeg _ | FAbs _
  | FAdd _ | FSub _ | FMul _ | FInv _ | Var _ | Tuple _ | Get _ | ExtArray _
  | FSqrt _ | ItoF _ | FtoI _
  | Call (("create_array" | "create_float_array" | "allocate" | "float_of_int" | "int_of_float" | "sqrt" | "fill_for_int" | "fill_for_float" (* TODO これは本当は消してはだめ *)), _)
    -> false
  | _ -> true


let rec g = function
  | Closure.Unit -> Unit
  | Closure.Int (i) -> Int (i)
  | Closure.Float (f) -> Float (f)
  | Closure.Add (x, y) -> Add (x, y)
  | Closure.Sub (x, y) -> Sub (x, y)
  | Closure.ShiftL (x, i) -> ShiftL (x, i)
  | Closure.ShiftR (x, i) -> ShiftR (x, i)
  | Closure.FNeg (x) -> FNeg (x)
  | Closure.FAbs (x) -> FAbs (x)
  | Closure.FAdd (x, y) -> FAdd (x, y)
  | Closure.FSub (x, y) -> FSub (x, y)
  | Closure.FMul (x, y) -> FMul (x, y)
  | Closure.FInv (x) -> FInv (x)
  | Closure.IfEq (x, y, e1, e2) -> IfEq (x, y, g e1, g e2)
  | Closure.IfLE (x, y, e1, e2) -> IfLE (x, y, g e1, g e2)
  | Closure.Let ((x, t), e1, e2) -> Let ((x, t), g e1, g e2)
  | Closure.Var (x) -> Var (x)
  | Closure.MakeCls _ | Closure.AppCls _ -> raise (NotSimple "closure call has been detected")
  | Closure.AppDir (Id.L ("print_char"), [x]) -> Send (x)
  | Closure.AppDir (Id.L ("sqrt"), [x]) -> FSqrt (x)
  | Closure.AppDir (Id.L l, xs) -> Call (l, xs)
  | Closure.Tuple (xs) -> Tuple (xs)
  | Closure.LetTuple (xts, y, e) -> LetTuple (xts, y, g e)
  | Closure.Get (x, y) -> Get (x, y)
  | Closure.Put (x, y, z) -> Put (x, y, z)
  | Closure.ExtArray (Id.L l) when M.mem l !Global.all -> ExtArray (l)
  | Closure.ExtArray (Id.L l) -> raise (NotSimple "external array has been detected")



let f (Closure.Prog (fundefs, main)) =
  let fundefs = { Closure.name = (Id.L (Id.main_label), Type.Fun ([], Type.Unit)); Closure.args = []; Closure.formal_fv = []; Closure.body = main } :: fundefs in
  let prog =
    List.fold_left
      (fun prog { Closure.name = (Id.L (l), t); Closure.args = xts; Closure.formal_fv = yts; Closure.body = e } ->
         if yts <> [] then raise (NotSimple "free variable has been detected");
         M.add l { body = g e; args = xts; typ = t; cache = S.empty } prog)
      M.empty fundefs
  in
  let prog = build_stub prog in
    (* ここで確認してしまうと, create_arrayなどが入っていない.
  (* 外部関数呼び出しがないことを確認 *)
  let rec check = function
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) -> check e1; check e2
    | LetTuple (_, _, e) -> check e
    | Call (Id.L (l), _) when not (M.mem l prog) -> raise (NotSimple (Printf.sprintf "external function call '%s' has been detected" l))
    | _ -> ()
  in
    M.iter (fun _ func -> check func.body) prog; *)
    prog


let string_of tree =
  let rec sub tree depth =
    Common.indent depth ^ (
      match tree with
        | Unit -> "Unit\n"
        | Int n -> "Int("^string_of_int n^")\n"
        | Float f -> "Float("^string_of_float f^")\n"
        | Add (s, t) -> "Add " ^ s ^ " " ^ t ^ "\n"
        | Sub (s, t) -> "Sub " ^ s ^ " " ^ t ^ "\n"
        | ShiftL (s, i) -> "ShiftL " ^ s ^ " " ^ string_of_int i ^ "\n"
        | ShiftR (s, i) -> "ShiftR " ^ s ^ " " ^ string_of_int i ^ "\n"
        | FNeg s -> "FNeg " ^ s ^ "\n"
        | FAbs s -> "FAbs " ^ s ^ "\n"
        | FAdd (s, t) -> "FAdd " ^ s ^ " " ^ t ^ "\n"
        | FSub (s, t) -> "FSub " ^ s ^ " " ^ t ^ "\n"
        | FMul (s, t) -> "FMul " ^ s ^ " " ^ t ^ "\n"
        | FInv s -> "FInv " ^ s ^ "\n"
        | IfEq (s, t, e1, e2) ->
            "If " ^ s ^ " = " ^ t ^ " then\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub e2 (depth+1)
        | IfLE (s, t, e1, e2) ->
            "If " ^ s ^ " <= " ^ t ^ " then\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub e2 (depth+1)
        | Let ((s, t), e1, e2) ->
            "Let " ^ s ^ " : " ^ Type.string_of t ^ " =\n" ^
            sub e1 (depth+1) ^
            sub e2 depth (* there is no nest of let *)
        | Var x -> x ^ "\n"
        | Call (l, ls) ->
            "Call label."^l^" to (" ^ String.concat ", " ls ^ ")\n"
        | Tuple ls ->
            "Tuple (" ^ (String.concat ", " ls) ^ ")\n"
        | LetTuple (ls, s, e) ->
            "LetTuple (" ^ (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) ls)) ^ ") = " ^ s ^ " in\n" ^
            sub e (depth)
        | Get (s, t) -> "Get " ^ s ^ ".(" ^ t ^ ")\n"
        | Put (s, t, u) -> "Put " ^ s ^ ".(" ^ t ^ ") = " ^ u ^ "\n"
        | ExtArray (l) -> "ExtArray labal."^l^"\n"
        | FSqrt (s) -> "FSqrt " ^ s ^ "\n"
        | Send (s) -> "Send " ^ s ^ "\n"
        | Recv -> "Recv\n"
        | ItoF (s) -> "ItoF " ^ s ^ "\n"
        | FtoI (s) -> "FtoI " ^ s ^ "\n"
        | Load (t, y, i) -> "Load " ^ y ^ " + " ^ string_of_int i ^ " : " ^ Type.string_of t ^ "\n"
        | Store (x, y, i) -> "Store " ^ x ^ " -> " ^ y ^ " + " ^ string_of_int i ^ "\n"
    )
  in
    sub tree 1

let output path prog =
  let out = open_out path in
    M.iter
      (fun l { typ; args; body = e } ->
         output_string out ("FUNCTION: " ^ l ^ "  " ^ String.concat ",  " (List.map (fun (x, t) -> x ^ ":" ^ Type.string_of t) args) ^ " : " ^ Type.string_of typ ^ "\n");
         output_string out (string_of e);)
      prog;
    close_out out



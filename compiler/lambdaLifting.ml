open KNormal

(* Appを置き換えたときに、さらに自由変数が生じる可能性がある。よって、1回のLambdaLiftingですべてのクロージャが駆逐できるわけではない。 *)

(* env: 変数 -> 型 *)
(* fvs: 関数の名前 -> 関数適用時に追加すべき自由変数のリスト *)
let rec g env fvs = function
  | IfEq(x, y, e1, e2) -> IfEq(x, y, g env fvs e1, g env fvs e2)
  | IfLE(x, y, e1, e2) -> IfLE(x, y, g env fvs e1, g env fvs e2)
  | Let((x, t) as xt, e1, e2) -> Let(xt, g env fvs e1, g (M.add x t env) fvs e2)
  | LetRec({ name = (x, t); args = yts; body = e1 }, e2) ->
      let env = M.add x t env in
      let free = S.to_list (S.diff (fv e1) (S.of_list (x :: List.map fst yts))) in
      let free = List.filter (fun x -> if M.mem x env then match M.find x env with Type.Fun _ -> false | _ -> true else (Printf.eprintf "Not found %s\n" x; false)) free in (* 関数の自由変数はLiftingしない。 *)
        if free_occur_of_function x e1 || free_occur_of_function x e2 || free = [] then
          LetRec({ name = (x, t); args = yts; body = g (M.add_list yts env) fvs e1 }, g env fvs e2)
        else
          let (freet', alpha) = (* 名前を変えたあとの自由変数のリスト *)
            List.fold_right
              (fun free (yts, alpha) ->
                 let free' = Id.genid free in
                   ((free', M.find free env) :: yts, M.add free free' alpha))
              free ([], M.empty) in
          let yts = freet' @ yts in (* 新しい仮引数鵜 *)
          (* 関数の型を付け替える *)
          let t = match t with Type.Fun (ts, ret) -> Type.Fun (List.map snd yts, ret) | _ -> assert false in
          let env = M.add x t env in
          let fvs = M.add x free fvs in (* 後に関数適用があればfreeを追加する *)
          let fvs' = M.add x (List.map fst freet') fvs in (* 関数本体は。変換後の自由変数を加える *)
          let e1 = Alpha.h alpha e1 in (* 仮引数の名前を変更したので、名前を付け替える. (アルファ変換の性質を維持するため) *)
            if !Common.verbose then Format.eprintf "Lambda Lifting: free variables (%s) found in %s.@." (String.concat ", " free) x;
            LetRec({ name = (x, t); args = yts; body = g (M.add_list yts env) fvs' e1 }, g env fvs e2)
  | App(x, ys) when M.mem x fvs ->
      let free = M.find x fvs in
        App(x, free @ ys)
  | LetTuple(xts, y, e) -> LetTuple(xts, y, g (M.add_list xts env) fvs e)
  | e -> e


let f e = g M.empty M.empty e


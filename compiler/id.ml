(* 変数の名前 *)
type t = string

(* ラベル. 変数と異なることを示唆するときに用いる. *)
type label = string

(* トップレベル関数やグローバル配列のラベル (caml2html: id_l) *)
type l = L of string

let rec pp_list = function
  | [] -> ""
  | [x] -> x
  | x :: xs -> x ^ " " ^ pp_list xs

let cut_off_id s =
  if String.contains s '.' then Filename.chop_extension s else s

let counter = ref 0
let genid s =
  incr counter;
  Printf.sprintf "%s.%d" (cut_off_id s) !counter


let rec id_of_typ = function
  | Type.Unit -> "u"
  | Type.Bool -> "b"
  | Type.Int -> "i"
  | Type.Float -> "d"
  | Type.Fun _ -> "f"
  | Type.Tuple _ -> "t"
  | Type.Array _ -> "a" 
  | Type.Ref _ -> "r" 
  | Type.List _ -> "l"
  | Type.Var _ -> assert false
  | Type.ArrayC _ -> "c"

let gentmp typ =
  incr counter;
  Printf.sprintf "T%s.%d" (id_of_typ typ) !counter

(* トップレベルの関数の名前 *)
let main_label = genid "MAIN"


let escape s =
  let s = String.copy s in
  let rec loop i =
    if i < String.length s then
      (if s.[i] = '.' then s.[i] <- '_';
       loop (i + 1))
  in
    loop 0;
    s


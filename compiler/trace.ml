(* トレースを選び, tail duplicationなどを行う. *)

open Block

(* tail duplicationのしきい値 *)
let threshold = ref 1000


let rec is_trace_edge b b' = function
  | v :: v' :: _ when v = b && v' = b' -> true
  | _ :: trace -> is_trace_edge b b' trace
  | _ -> false


let max_element compare = function
  | [] -> invalid_arg "max_element"
  | x :: ls ->
      List.fold_left
        (fun x x' -> if compare x x' < 0 then x' else x)
        x ls

(* トレースの選択を行う *)
let select prog =
  let f cfg =
    let rec backward (acc, vis) b =
      let vis = S.add b vis in
      let pred = G.pred b cfg in
        if pred = [] then
          (acc, vis)
        else
          let b' = max_element (Common.compare_on (fun b' -> Profile.count_of_edge b' b)) pred in
            if S.mem b' vis then
              (acc, vis)
            else
              backward (b' :: acc, vis) b'
    in
    let rec forward (acc, vis) b =
      let vis = S.add b vis in
      let succ = G.succ b cfg in
        if succ = [] then
          (acc, vis)
        else
          let b' = max_element (Common.compare_on (fun b' -> Profile.count_of_edge b b')) succ in
            if S.mem b' vis then
              (acc, vis)
            else
              forward (b' :: acc, vis) b'
    in
    let rec loop (acc, vis) = function
      | [] -> (acc, vis)
      | b :: ls when S.mem b vis -> loop (acc, vis) ls
      | b :: ls ->
          let (trace1, vis) = backward ([], vis) b in
          let (trace2, vis) = forward ([], vis) b in
            loop ((trace1 @ [b] @ List.rev trace2) :: acc, vis) ls
    in
    let vertices = G.fold (fun b _ acc -> b :: acc) cfg [] in
    let vertices = List.sort (Common.compare_on (fun b -> - Profile.count_of b)) vertices in
    let (traces, _) = loop ([], S.empty) vertices in
      traces
  in
    M.fold
      (fun l func acc ->
         let traces = f func.cfg in
           M.add l traces acc)
      prog.entries M.empty

let rec tail_duplication = function
  | [], _ as acc -> acc
  | trace :: traces, cfg ->
      (* トレースの外部からの辺を持つブロックを見つける *)
      let rec loop (traces, cfg) = function
        | [] -> (traces, cfg)
        | b :: rem when G.in_degree_of b cfg <= 1 ->
            assert (G.in_degree_of b cfg = 1);
            loop (traces, cfg) rem
        | (_ :: _) as rem ->
            (* トレース外からの辺がある場合には複製を行う *)
            let (copied, cfg) =
              List.fold_right
                (fun b (copied, cfg) ->
                   let (b', cfg) = G.copy_block b cfg in
                     ((b, b') :: copied, cfg))
                rem ([], cfg)
            in
            (* トレース以外の辺があった場合には, コピーされたブロックへ付け替える *)
            let cfg =
              List.fold_right
                (fun (b, b') cfg ->
                   let cfg =
                     List.fold_right
                       (fun b'' cfg ->
                          if is_trace_edge b'' b trace then
                            cfg
                          else
                            (Profile.add_prob b'' b' (Profile.prob_of b'' b);
                             G.add_edge b'' b' (G.tag_of_edge b'' b cfg)
                               (G.remove_edge b'' b cfg)))
                       (G.pred b cfg) cfg
                   in
                   let cfg =
                     List.fold_right
                       (fun b'' cfg ->
                          let c = if is_trace_edge b b'' trace then List.assoc b'' copied else b'' in
                            (Profile.add_prob b' c (Profile.prob_of b b'');
                             G.add_edge b' c (G.tag_of_edge b b'' cfg) cfg))
                       (G.succ b cfg) cfg
                   in
                     cfg)
                copied cfg
            in
            (* 各ブロックの実行回数を更新する *)
            (* TODO: なぜか0以下になることがある *)
            let rec calc_cnt cnt = function
              | (b, b') :: ((c, _) :: _ as rem) ->
                  (* コピーされたブロックb'は, 新しいbの実行回数分だけ減る *)
                  Profile.add_count b' (Profile.count_of b - cnt);
                  Profile.add_count b cnt;
                  (* 次のトレースcへ進む回数cntを計算 *)
                  let cnt = int_of_float (float_of_int cnt *. (Profile.prob_of b c) +. 0.5) in
                    calc_cnt cnt rem
              | (b, b') :: [] ->
                  Profile.add_count b' (Profile.count_of b - cnt);
                  Profile.add_count b cnt
              | _ -> assert false
            in
            let () =
              let (head, _) = List.hd copied in
              let cnt = List.fold_left (fun acc b -> acc + Profile.count_of_edge b head) 0 (G.pred head cfg) in
                calc_cnt cnt copied
            in
            (* コピーしたブロック群を新しいトレースとする *)
            let new_trace = List.map snd copied in
              (new_trace :: traces, cfg)
      in
      let significance =
        List.fold_left
          (fun acc b ->
             acc + Profile.count_of b * List.length (G.find b cfg))
          0 trace
      in
      let (traces, cfg) =
        tail_duplication
          (if significance < !threshold then
             (* 実行回数が少ないためにtraceに対してはtail duplicationを行わない. *)
             (traces, cfg)
           else
             loop (traces, cfg) (List.tl trace))
      in
        (trace :: traces, cfg)

(* トレースをスーパーブロック(途中の入口がない)に分解する. *)
let rec trace_to_superblock cfg = function
  | [] -> []
  | trace :: traces ->
      (try
         let b =
           List.find
             (fun b -> G.in_degree_of b cfg > 1)
             (List.tl trace)
         in
         (* bが現れるまではスーパーブロックになっている *)
         let (trace, trace') = Common.span (fun b' -> b <> b') trace in
           trace :: trace_to_superblock cfg (trace' :: traces)
       with Not_found -> trace :: trace_to_superblock cfg traces)

(* スーパーブロックになっているか確認する. *)
let rec check_superblock cfg = function
  | [] -> ()
  | trace :: traces ->
      assert (trace <> []);
      List.iter
        (fun b -> assert (G.in_degree_of b cfg = 1))
        (List.tl trace);
      check_superblock cfg traces

  

let f prog =
  let func2traces = select prog in
  let (func2traces, prog) =
    M.fold
      (fun l func (func2traces, prog) ->
         let (traces, cfg) = tail_duplication (M.find l func2traces, func.cfg) in
         let traces = trace_to_superblock cfg traces in
           (M.add l traces func2traces, { prog with entries = M.add l { func with cfg } prog.entries }))
      prog.entries (func2traces, prog)
  in
    M.iter
      (fun l traces ->
         check_superblock (M.find l prog.entries).cfg traces)
      func2traces;

    (func2traces, prog)



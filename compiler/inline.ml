open KNormal

(* インライン展開する関数の最大サイズ (caml2html: inline_threshold) *)
let threshold = ref 0 (* Mainで-inlineオプションによりセットされる *)

(* 関数呼び出しが存在しないか *)
let rec is_leaf = function
  | IfEq(_, _, e1, e2) | IfLE(_, _, e1, e2) | Let(_, e1, e2) -> is_leaf e1 && is_leaf e2
  | LetRec ({ body = e1 }, e2) -> is_leaf e2
  | App _ -> false
  | LetTuple (_, _, e) -> is_leaf e
  | _ -> true
(* 自己ループがない関数かどうか *)
let rec is_no_loop x = function
  | IfEq(_, _, e1, e2) | IfLE(_, _, e1, e2) | Let(_, e1, e2) -> is_no_loop x e1 && is_no_loop x e2
  | LetRec ({ body = e1 }, e2) -> is_no_loop x e2
  | App (y, _) when x = y -> false (* loop function *)
  | LetTuple (_, _, e) -> is_no_loop x e
  | _ -> true

(*let is_target x e = is_no_loop x e (* is_leaf e *) *)
let there_is_target = ref false
let mode_threshold = ref false
let is_target x e =
  if !mode_threshold then size e < !threshold
  else if is_no_loop x e then (there_is_target := true; true)
       else false

let rec g env = function (* インライン展開ルーチン本体 (caml2html: inline_g) *)
  | IfEq(x, y, e1, e2) -> IfEq(x, y, g env e1, g env e2)
  | IfLE(x, y, e1, e2) -> IfLE(x, y, g env e1, g env e2)
  | Let(xt, e1, e2) -> Let(xt, g env e1, g env e2)
  | LetRec({ name = (x, t); args = yts; body = e1 }, e2) -> (* 関数定義の場合 (caml2html: inline_letrec) *)
      let env = if is_target x e1 then M.add x (yts, e1) env else env in
        LetRec({ name = (x, t); args = yts; body = g env e1}, g env e2)
(*      let env' = M.add x (yts, e1) env in
        LetRec({ name = (x, t); args = yts; body = g env e1(* do not loop unrolling *)}, g env' e2) *)
(*      let env = if size e1 > !threshold then env else M.add x (yts, e1) env in
      LetRec({ name = (x, t); args = yts; body = g env e1}, g env e2) *)
  | App(x, ys) when M.mem x env -> (* 関数適用の場合 (caml2html: inline_app) *)
      let (zs, e) = M.find x env in
        (*Format.eprintf "inlining %s@." x; *)
      let env' =
	List.fold_left2
	  (fun env' (z, t) y -> M.add z y env')
	  M.empty
	  zs
	  ys in
      Alpha.g env' e
  | LetTuple(xts, y, e) -> LetTuple(xts, y, g env e)
  | e -> e

let f e =
  there_is_target := false;
  let res = g M.empty e in
    if !mode_threshold then
      ((*Printf.eprintf "Inlining Mode 'threshold'\n"; *)res)
    else if !there_is_target then
           res
         else
           (mode_threshold := true; g M.empty res)


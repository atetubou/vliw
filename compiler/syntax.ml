type t = (* MinCamlの構文を表現するデータ型 (caml2html: syntax_t) *)
  | PosHint of Common.position * t (* position of Syntax.t *)
  | Unit
  | Bool of bool
  | Int of int
  | Float of float
  | Not of t
  | Add of t * t
  | Sub of t * t
  | ShiftL of t * int
  | ShiftR of t * int
  | FNeg of t
  | FAdd of t * t
  | FSub of t * t
  | FMul of t * t
  | FDiv of t * t
  | Eq of t * t
  | LE of t * t
  | If of t * t * t
  | Let of (Id.t * Type.t) * t * t
  | Var of Id.t
  | LetRec of fundef * t
  | App of t * t list
  | Tuple of t list
  | LetTuple of (Id.t * Type.t) list * t * t
  | Array of t * t
  | ArrayInit of t * t
  | ArrayAllocate of t * Type.t
  | Get of t * t
  | Put of t * t * t
  | Nil of Type.t
  | Cons of t * t
  | Head of t
  | Tail of t
  | Ref of t
  | Deref of t
  | Subst of t * t
and fundef = { name : Id.t * Type.t; args : (Id.t * Type.t) list; body : t }

let rec delete_hint = function
  | PosHint (_, e) -> delete_hint e
  | e -> e

let rec concat s t =
  match s with
    | PosHint(a, e) -> PosHint(a, concat e t)
    | Let (a, b, e) -> Let (a, b, concat e t)
    | LetRec (a, e) -> LetRec (a, concat e t)
    | LetTuple (a, b, e) -> LetTuple (a, b, concat e t)
    | Unit -> t
    | _ -> failwith "cannot concatenate files.(incorrect library format)"

(* rename x as x' in e *)
let rec rename x x' e =
  let k y = if y = x then x' else y in
  let rec f = function
    | PosHint (pos, e) -> PosHint (pos, f e)
    | Unit -> Unit
    | Not x -> Not (f x)
    | Add (x, y) -> Add (f x, f y)
    | Sub (x, y) -> Sub (f x, f y)
    | ShiftL (x, i) -> ShiftL (f x, i)
    | ShiftR (x, i) -> ShiftR (f x, i)
    | FNeg x -> FNeg (f x)
    | FAdd (x, y) -> FAdd (f x, f y)
    | FSub (x, y) -> FSub (f x, f y)
    | FMul (x, y) -> FMul (f x, f y)
    | FDiv (x, y) -> FDiv (f x, f y)
    | Eq (x, y) -> Eq (f x, f y)
    | LE (x, y) -> LE (f x, f y)
    | If (x, y, z) -> If (f x, f y, f z)
    | Let ((y, t), e1, e2) ->
        let e1 = f e1 in
        let e2 = if x = y then e2 else f e2 in
          Let ((y, t), e1, e2)
    | Var x -> Var (k x)
    | LetRec ({name = (y, t); args = yts; body = e1}, e2) ->
        let (e1, e2) = if x = y then (e1, e2) else (f e1, f e2) in
          LetRec ({name = (y, t); args = yts; body = e1}, e2)
    | App (x, ys) -> App (f x, List.map f ys)
    | Tuple xs -> Tuple (List.map f xs)
    | LetTuple (yts, e1, e2) ->
        let e1 = f e1 in
        let e2 = if List.exists (fun (y, t) -> x = y) yts then e2 else f e2 in
          LetTuple (yts, e1, e2)
    | Array (x, y) -> Array (f x, f y)
    | ArrayInit (x, y) -> ArrayInit (f x, f y)
    | ArrayAllocate (x, typ) -> ArrayAllocate (f x, typ)
    | Get (x, y) -> Get (f x, f y)
    | Put (x, y, z) -> Put (f x, f y, f z)
    | Cons (x, y) -> Cons (f x, f y)
    | Head x -> Head (f x)
    | Tail x -> Tail (f x)
    | Ref x -> Ref (f x)
    | Deref x -> Deref (f x)
    | Subst (x, y) -> Subst (f x, f y)
    | x -> x
  in
    f e

(* for debug output *)
let string_of tree =
  let rec sub tree depth =
    Common.indent depth ^ (
      match delete_hint tree with
        | PosHint (_, s) -> assert false
        | Unit -> "Unit\n"
        | Bool b -> "Bool("^string_of_bool b^")\n"
        | Int n -> "Int("^string_of_int n^")\n"
        | Float f -> "Float("^string_of_float f^")\n"
        | Not s -> "Not\n" ^ sub s (depth+1)
        | Add (s, t) -> "Add\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | Sub (s, t) -> "Sub\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | ShiftL (s, i) -> "ShiftL("^string_of_int i^")\n" ^ sub s (depth+1)
        | ShiftR (s, i) -> "ShiftR("^string_of_int i^")\n" ^ sub s (depth+1)
        | FNeg s -> "FNeg\n" ^ sub s (depth+1)
        | FAdd (s, t) -> "FAdd\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | FSub (s, t) -> "FSub\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | FMul (s, t) -> "FMul\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | FDiv (s, t) -> "FDiv\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | Eq (s, t) -> "Eq\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | LE (s, t) -> "LE\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | If (s, t, u) ->
            "If\n" ^ 
            sub s (depth+1) ^
            Common.indent depth ^ "then\n" ^
            sub t (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub u (depth+1)
        | Let ((x, typ), s, t) ->
            "Let " ^ x ^ " : " ^ Type.string_of typ ^ " =\n" ^
            sub s (depth+1) ^
            Common.indent depth ^ "in\n" ^
            sub t (depth+1)
        | Var x -> x ^ "\n"
        | LetRec ({ name = (x, _); args = ls; body = s }, t) ->
            "LetRec " ^ (String.concat " " (x :: List.map fst ls)) ^ " =\n" ^
            sub s (depth+1) ^
            Common.indent depth ^ "in\n" ^
            sub t (depth+1)
        | App (s, ls) ->
            "App\n" ^ sub s (depth+1) ^
            Common.indent depth ^ "to\n" ^
            List.fold_left (fun acc t -> acc ^ sub t (depth+1)) "" ls
        | Tuple ls ->
            "Tuple\n" ^
            List.fold_left (fun acc t -> acc ^ sub t (depth+1)) "" ls
        | LetTuple (ls, s, t) ->
            "LetTuple (" ^ (String.concat ", " (List.map fst ls)) ^ ") =\n" ^
            sub s (depth+1) ^
            Common.indent depth ^ "in\n" ^
            sub t (depth+1)
        | Array (s, t) -> "Array\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | ArrayInit (s, t) -> "ArrayInit\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | ArrayAllocate (s, _) -> "ArrayAllocate\n" ^ sub s (depth+1)
        | Get (s, t) -> "Get\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | Put (s, t, u) -> "Put\n" ^ sub s (depth+1) ^ sub t (depth+1) ^ sub u (depth+1)
        | Nil t -> "Nil : " ^ Type.string_of t ^ "\n"
        | Cons (s, t) -> "Cons\n" ^ sub s (depth+1) ^ sub t (depth+1)
        | Head s -> "Head\n" ^ sub s (depth+1)
        | Tail s -> "Tail\n" ^ sub s (depth+1)
        | Ref s -> "Ref\n" ^ sub s (depth+1)
        | Deref s -> "Deref\n" ^ sub s (depth+1)
        | Subst (s, t) -> "Subst\n" ^ sub s (depth+1) ^ sub t (depth+1)
    )
  in
    sub tree 0

let output path tree =
  let str = string_of tree in
  let out = open_out path in
    output_string out str;
    close_out out;
    tree


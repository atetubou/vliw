(* グローバル配列を破壊的な代入で置き換える
 *
 * グローバル配列が境界外にアクセスしている場合は, 動作は未定義となる. *)

open Block

exception Impossible

(* name: グローバル配列の名前.
 * i: グローバル配列のインデクス. *)
let name_of name i = Printf.sprintf "(%d)%s" i name

let g reach use target cfg =
  (* グローバル配列(のエイリアス)nameについて, 定数のインデクスでのみアクセスするか
   * かつ, 到達する定義文はsid_srcのみか. *)
  let is_ok sid_src name = function
    | { exp = Load (x, C (i)) | FLoad (x, C (i)) | Store (_, x, C (i)) | FStore (_, x, C (i)) } as stmt
      ->
        x = name &&
        List.for_all (fun stmt' -> stmt'.s_id = sid_src) (M.find x (M.find stmt.s_id reach))
    | stmt -> false
  in
  let alias =
    G.fold_stmt
      (fun stmt alias ->
         match stmt with
           | { def = [y, t]; exp = SetLabel (x) } as stmt when x = target ->
              if M.mem stmt.s_id use &&
                 not (List.for_all (is_ok stmt.s_id y) (M.find stmt.s_id use)) then
                raise Impossible;
              S.add y alias
           | _ -> alias)
      cfg S.empty
  in
  let cfg =
    G.map_stmt
      (function
         | { def = [y, t]; exp = SetLabel (x) } when x = target ->
             (* Nopによって削除する. *)
             gen_stmt [] Nop
         | { def = def; exp = Load (x, C (i)) } when S.mem x alias ->
             gen_stmt def (Mov (name_of target i))
         | { def = def; exp = FLoad (x, C (i)) } when S.mem x alias ->
             gen_stmt def (FMov (name_of target i, []))
         | { exp = Store (y, x, C (i)) } when S.mem x alias ->
             gen_stmt [name_of target i, Type.Int(* TODO: 型情報を一応いれておいたほうが良いかも. *)] (Mov (y))
         | { exp = FStore (y, x, C (i)) } when S.mem x alias ->
             gen_stmt [name_of target i, Type.Float] (FMov (y, []))
         | stmt -> stmt)
      cfg
  in
    (* 第一返り値: グローバル配列targetが, この関数内で使用されたか. *)
    (not (S.is_empty alias), cfg)

(* SRAMへアクセスする命令をキャッシュへのアクセス命令に変える. *)
let sram_to_cache = function
  | Load (x, y') -> LoadC (x, y')
  | FLoad (x, y') -> FLoadC (x, y')
  | Store (x, y, z') -> StoreC (x, y, z')
  | FStore (x, y, z') -> FStoreC (x, y, z')
  | exp -> exp

(* グローバル配列targetをキャッシュ上に配置することを試みる. *)
let allocate_to_cache reach use target cfg =
  let rec dfs vis { s_id; def } =
    if S.mem s_id vis then vis
    else
      let vis = S.add s_id vis in
      let uses = if M.mem s_id use then M.find s_id use else [] in
        List.fold_left
          (fun vis stmt ->
             match stmt with
               | { exp = Add _ } ->
                   dfs vis stmt
               (* 最終的にメモリアクセスへぶつかったら終了. *)
               | { exp = Load _ | FLoad _ } ->
                   S.add stmt.s_id vis
               | { exp = Store (x, _, _) | FStore (x, _ ,_) }
                   when (match def with [x', _] -> x <> x' | _ -> assert false)
                 ->
                   S.add stmt.s_id vis
               | _ ->
                   (* Store (x, _, _)の位置にグローバル配列のエイリアスが現れた場合は,
                    * メモリ外にエスケープするので失敗. *)
                   Format.eprintf "FAILED BECAUSE OF %s@." (string_of_stmt stmt);
                   raise Impossible)
          vis uses
  in
  let vis =
    G.fold_stmt
      (fun stmt vis ->
         match stmt with
           | { def = [x, t]; exp = SetLabel (l) } as stmt when l = target ->
               dfs vis stmt
           | _ -> vis)
      cfg S.empty
  in
    G.map_stmt
      (fun stmt ->
         if S.mem stmt.s_id vis then
           { stmt with exp = sram_to_cache stmt.exp }
         else stmt)
      cfg


let gather_extarray_of prog =
  M.fold
    (fun _ func ->
       G.fold_stmt
         (fun stmt acc ->
            match stmt with
              | { exp = SetLabel (x) } -> S.add x acc
              | _ -> acc)
         func.cfg)
    prog.entries S.empty

(* progに含まれるSetLabelを, Global.allの情報を用いて全て変換する *)
let dereference prog =
  let open Global in
  let () =
    (* 不要なグローバル変数を削除する. *)
    let used = gather_extarray_of prog in
      M.iter
        (fun x _ ->
           if not (S.mem x used) then
             (Format.eprintf "Eliminate Global variable '%s'@." x;
              Global.remove x))
        !all
  in
  let globals =
    List.sort
      compare
      (M.fold
         (fun x { length; in_cache } acc -> (in_cache, (length, x)) :: acc)
         !Global.all [])
  in
  let allocate top ls =
    List.fold_left
      (fun (addr, acc) (_, (length, x)) ->
         (addr + length, M.add x addr acc))
      (top, M.empty) ls
  in
  let (caches, srams) = List.partition fst globals in
  let (tail_addr, env) = allocate Asm.global_top srams in
  let (tail_addr_c, env_c) = allocate Asm.cache_global_top caches in
    M.iter
      (fun x addr ->
         Format.eprintf "Global Array '%s' at %d to %d in SRAM@." x addr (addr + (M.find x !Global.all).Global.length - 1))
      env;
    M.iter
      (fun x addr ->
         Format.eprintf "Global Array '%s' at %d to %d in cache@." x addr (addr + (M.find x !Global.all).Global.length - 1))
      env_c;
    Format.eprintf "Static Domain: %d@." (Global.size ());
  let prog =
    map_cfg
      (fun l cfg ->
         G.map_stmt
           (function
              | { exp = SetLabel (x) } as stmt when M.mem x env ->
                  { stmt with exp = Set (M.find x env) }
              | { exp = SetLabel (x) } as stmt when M.mem x env_c ->
                  { stmt with exp = Set (M.find x env_c) }
              | stmt -> stmt)
           cfg)
      prog
  in
    prog
  

let f prog =
  let prog = simplify (DConstFold.f prog) in
  let prog = simplify (DConstFold.f prog) in
  let prog = simplify (DConstFold.f prog) in
  let prog = Copy.f prog in
  let prog = Copy.f prog in
  let prog = DElim.f prog in
  let reach_env =
    M.map
      (fun func -> Reach.create_for_each_statement func.cfg)
      prog.entries
  in
  let prog =
    M.fold
      (fun name { Global.length = len; Global.typ = typ } prog ->
         try
           let typ = match typ with Type.Array (typ) -> typ | _ -> raise Impossible in
           let cnt = ref 0 in
           let prog =
             map_cfg
               (fun l cfg ->
                  let (reach, use) = M.find l reach_env in
                  let (used, cfg) = g reach use name cfg in
                    if used then incr cnt;
                    cfg)
               prog
           in
           let prog =
             if !cnt > 1 then
               (Format.eprintf "'%s' is global variable.@." name;
                let rec loop i prog =
                  if i >= len then prog
                  else
                    loop (i+1)
                      (add_global_variable ~restrict:true (name_of name i, typ) prog)
                in
                  loop 0 prog)
             else
               (Format.eprintf "'%s' is transformed into reference.@." name;
                prog)
           in
             Global.remove name;
             prog
         with Impossible -> prog)
      !Global.all prog
  in
  let prog =
    M.fold
      (fun name { Global.length = len; Global.typ = typ; Global.in_cache = in_cache } prog ->
         try
           if in_cache then (
             Format.eprintf "%s is already in cache.@." name;
             raise Impossible
           );
           (*
           if not (List.mem (Id.cut_off_id name) ["and_net"]) then raise Impossible;
            *)
(*
           if not (List.mem (Id.cut_off_id name) ["objects"; "nvector"; "ptrace_dirvec"; "and_net"; "consts"; "v3"]) then raise Impossible;
*)
           Format.eprintf "CHECK if %s is allocated to cache.@." name;
           let prog =
             map_cfg
               (fun l cfg ->
                  let (reach, use) = M.find l reach_env in
                  let cfg = allocate_to_cache reach use name cfg in
                    cfg)
               prog
           in
           Format.eprintf "Successfully allocated to cache.@.";
             Global.move_to_cache name;
             prog
         with Impossible -> prog)
      !Global.all prog
  in

  let prog = DElim.f prog in
  let prog = DElim.f prog in
  let prog = DElim.f prog in
    simplify (dereference prog)


(* 二次元配列の展開 *)

(* 十分条件をちゃんと確認していないので, かなり嘘の最適化.
 * TODO: 二次元配列に展開した後, 型の変換を完全にはしていない.
 *
 * [XXX] この最適化によって, Addのオペランドとして配列が現れることがある.
 * *)

open KNormal

let memi x env = M.mem x env && (match M.find x env with Int _ -> true | _ -> false)
let findi x env = match M.find x env with Int (i) -> i | _ -> assert false

exception Impossible

type pos = InArray | InTuple of int

let insert e typ f =
  let x = Id.gentmp typ in
    Let ((x, typ), e, f x)

let seq (e1, e2) =
  Let ((Id.gentmp Type.Unit, Type.Unit), e1, e2)


let rec string_of_pos = function
  | InArray :: ls -> "InArray, " ^ string_of_pos ls
  | InTuple (x) :: ls -> "InTuple (" ^ string_of_int x ^ "), " ^ string_of_pos ls
  | [] -> "Here"

let mapi f ls =
  let rec loop i = function
    | [] -> []
    | x :: ls -> f i x :: loop (i + 1) ls
  in
    loop 0 ls

let iteri f ls =
  let rec loop i = function
    | [] -> ()
    | x :: ls -> f i x; loop (i + 1) ls
  in
    loop 0 ls

(* n以上となる最小の2べきの指数 *)
let two_power n =
  let rec loop x i =
    if x >= n then
      i
    else
      loop (2 * x) (i+1)
  in
    loop 1 0


let gather_aliases target e =
  let acc = ref M.empty in
  let updated = ref false in
  let mem x = M.mem x !acc in
  let find x = M.find x !acc in
  (* xの指し示しうる位置lsを追加 *)
  let add x ls =
    if mem x then
      begin
        let lss = find x in
          if not (List.mem ls lss) then
            (updated := true;
             acc := M.add x (ls :: lss) !acc)
      end
    else
      (updated := true;
       acc := M.add x [ls] !acc)
  in
  let add_lss x lss = List.iter (add x) lss in
  (* env: 関数名 -> 仮引数, その関数の返り値のlss
   * dest: 部分式の結果の(もしあれば)代入先
   * 返り値: 部分式の値が指し示す位置のリスト *)
  let rec f env dest = function
    | ExtArray (l) when l = target -> [[]]
    | Get (x, y) when mem x ->
        List.map (fun ls -> ls @ [InArray]) (find x)
    | Put (x, y, z) when mem x ->
        add_lss z (List.map (fun ls -> ls @ [InArray]) (find x));
        []
    | Var (x) when mem x -> find x
    | Tuple (xs) ->
        (match dest with
           | Some (y) when mem y ->
               let lss = find y in
                 iteri
                   (fun i x -> add_lss x (List.map (fun ls -> ls @ [InTuple (i)]) lss))
                   xs;
                 lss
           | _ -> [])
    | ExtFunApp (("create_array" | "create_float_array"), [xn; xi]) ->
        (match dest with
           | Some (y) when mem y ->
               let lss = find y in
                 add_lss xi (List.map (fun ls -> ls @ [InArray]) lss);
                 lss
           | _ -> [])
    | LetTuple (xts, y, e) ->
        if mem y then (
          let lss = find y in
            iteri
              (fun i (x, _) -> add_lss x (List.map (fun ls -> ls @ [InTuple (i)]) lss))
              xts
        );
        f env dest e
    | LetRec ({ name = (x, t); args; body = e1 }, e2) ->
        let rec loop env lss =
          let lss' = f env None e1 in
          let env = M.add x (args, lss') env in
            if lss = lss' then
              env
            else
              loop env lss'
        in
        (* env = M.add x (args, f env None e1) envを満たす最小なenvを求める. *)
        let env = loop env (f env None e1) in
          f env dest e2
    | App (x, ys) when M.mem x env ->
        let (args, lss) = M.find x env in
          (* ysを仮引数argsにコピーするので, yの指し示しうる場所を仮引数vは含むようになる. *)
          List.iter2
            (fun y (v, _) ->
               if mem y then
                 add_lss v (find y))
            ys args;
          lss
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        let lss1 = f env dest e1 in
        let lss2 = f env dest e2 in
          lss1 @ lss2
    | Let ((x, _), e1, e2) ->
        add_lss x (f env (Some (x)) e1);
        f env dest e2
    | _ -> []
  in
  let rec loop () =
    updated := false;
    ignore (f M.empty None e);
    if !updated then
      loop ()
    else
      !acc
  in
  let res = loop () in
    res

let infer_size inner_aliases e =
  (* 内部配列の最大長を推論する.
   * to_inner: 対象となる二次元配列の中の配列の定義式か. *)
  let rec f env to_inner = function
    (* 外部配列が代入される場合 *)
    | ExtArray (x) when to_inner && M.mem x !Global.all ->
        let { Global.length = len } = M.find x !Global.all in
          len
    (* 既に代入されている配列を取り出すだけのとき. *)
    | Get (x, y) when to_inner -> 0
    (* 新たに配列を作るとき. *)
    | ExtFunApp (("create_array" | "create_float_array"), [xn; _])
    | ExtFunApp ("allocate", [xn]) when to_inner ->
        (* 配列長が不定 *)
        if not (memi xn env) then (Format.eprintf "Unbounded size %s@." xn; raise Impossible);
        findi xn env
    | Let ((x, t), e1, e2) ->
        let a = f env (S.mem x inner_aliases) e1 in
        let env = M.add x e1 env in
        let b = f env to_inner e2 in
          max a b
    | LetTuple (xts, y, e) -> f env to_inner e
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        max (f env to_inner e1) (f env to_inner e2)
    | LetRec ({ body = e1 }, e2) ->
        max (f env false e1) (f env to_inner e2)
    | e when to_inner ->
        (* 内部配列への代入が関数適用などよくわからないものであれば諦める *)
        Format.eprintf "Failed unknown %s@." (string_of e);
        raise Impossible
    | _ -> 0
  in
    f M.empty false e

(* 内部配列 -> [代入される配列の名前, 配列でのインデクス] を計算する.
 * また, 内部の型も計算する. *)
let calc_inner aliases inner_aliases e =
  let merge =
    M.merge
      (fun x a b ->
         match a, b with
           | Some (a1, a2), Some (b1, b2) ->
               Format.eprintf "FAILED EXPAND: Multiple Substitution %s -> %s.(%s) and %s.(%s)@." x a1 a2 b1 b2;
               raise Impossible
           | Some _, _ -> a
           | _ -> b)
  in
  let inner_type = ref None in
  let rec f = function
    | Let ((x, t), e1, e2) when S.mem x inner_aliases ->
        (match !inner_type with
           (* 何故か型が合わないときは失敗. *)
           | Some (t') when t <> t' ->
               Format.eprintf "WHY? type inconsistent %s <-> %s@." (Type.string_of t) (Type.string_of t');
               raise Impossible
           | _ -> ());
        inner_type := Some (t);
        merge (f e1) (f e2)
    | Put (x, y, z) when S.mem x aliases && S.mem z inner_aliases ->
        M.add z (x, y) M.empty
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) | LetRec ({ body = e1 }, e2) ->
        merge (f e1) (f e2)
    | LetTuple (_, _, e) -> f e
    | _ -> M.empty
  in
  let res = f e in
    match !inner_type with
      | Some (t) -> (res, t)
      (* 型が決まらなかった場合は失敗. *)
      | _ ->
          Format.eprintf "UNKNOWN TYPE@."
          ;raise Impossible


(* targetに含まれる変数集合の定義をなるべく遅くする. *)
(* TODO: メモリ外に逃げたとき, 意味を変えてしまう *)
let localize target e =
  let rec f = function
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f e1, f e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f e1, f e2)
    | Let ((x, t), e1, e2) when S.mem x target ->
        let e1 = f e1 in
        let e2 = f e2 in
        (* なるべく後ろにlet x : t = e1 in ...を配置する. *)
        let rec loop = function
          | Let ((x', t'), e1', e2') when not (S.mem x (fv e1')) ->
              Let ((x', t'), e1', loop e2')
          | e ->
              Let ((x, t), e1, e)
        in
          if Elim.effect e1 then
            Let ((x, t), e1, e2)
          else
            loop e2
    | Let ((x, t), e1, e2) -> Let ((x, t), f e1, f e2)
    | LetRec ({ body = e1 } as fundef, e2) ->
        LetRec ({ fundef with body = f e1 }, f e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f e)
    | e -> e
  in
    f e

(* 変換後の, グローバル配列のエイリアスの型を求める.
 * 第一要素は第二要素のプレフィックスである必要がある.
 * 第一要素: 型を得る対象のグローバル変数の位置
 * 第二要素: 展開対象の二次元配列の位置
 * 第三要素: 対象としているグローバル配列の型 *)
let rec change_type = function
  | InArray :: ls, InArray :: ls', Type.Array (t) ->
      change_type (ls, ls', t)
  | InTuple (i) :: ls, InTuple (i') :: ls', Type.Tuple (ts) when i = i' ->
      change_type (ls, ls', Common.at i ts)
  | [], InArray :: ls', Type.Array (t) ->
      Type.Array (change_type ([], ls', t))
  | [], InTuple (i) :: ls', Type.Tuple (ts) ->
      Type.Tuple
        (mapi
           (fun j t ->
              if i = j then change_type ([], ls', t) else t)
           ts)
  | [], [], Type.Array (Type.Array (t)) -> Type.Array (t)
  | _ -> assert false

let rec is_prefix ls ls' =
  match (ls, ls') with
    | x :: ls, y :: ls' when x = y -> is_prefix ls ls'
    | [], _ -> true
    | _ -> false

(* グローバル変数を更新する. *)
let update_globals targets shift =
  let open Global in
    List.iter
      (fun (x, ls) ->
         let { typ; length } as old = M.find x !all in
           Format.eprintf "type of %s has been changed: %s -> %s@." x (Type.string_of typ) (Type.string_of (change_type ([], ls, typ)));
         let typ = change_type ([], ls, typ) in
         let length =
           if ls = [] then
             (Format.eprintf "Global 2D Array %s is expanded from %d to %d.@." x length (length lsl shift);
              length lsl shift)
           else
             length
         in
           Global.update x { old with typ; length })
      targets


(* targetsの位置にある二次元配列をまとめて展開する. *)
let expand x2aliases targets e =
  try
    Format.eprintf "# EXPAND@.";
      List.iter
        (fun (x, ls) ->
           Format.eprintf "  %s of (%s)@."
             x (string_of_pos ls))
        targets;
    let var2lss =
      M.fold
        (fun x aliases acc ->
           M.fold
             (fun y lss acc ->
                M.add_ls y (x, lss) acc)
             aliases acc)
        x2aliases M.empty
    in
    (* 配列のエイリアスと内部配列のエイリアスを計算する. *)
    let (aliases, inner_aliases) =
      List.fold_left
        (fun (acc, acc') (x, ls) ->
           let aliases = M.find x x2aliases in
             M.fold
               (fun y lss (acc, acc') ->
                  ((if List.mem ls lss then S.add y acc else acc),
                   (if List.mem (ls @ [InArray]) lss then S.add y acc' else acc')))
               aliases (acc, acc'))
        (S.empty, S.empty) targets
    in
    let size = infer_size inner_aliases e in
    let shift = two_power size in
    (* サイズが小さければ無視しておく. *)
    if size <= 1 then raise Impossible;
    let (inner2index, inner_type) = calc_inner aliases inner_aliases e in
    (* 簡単のため, 内部配列になる変数の定義を遅くする. *)
    let e = localize (M.fold (fun x _ -> S.add x) inner2index S.empty) e in
    (* 先頭head, インデクスindexの配列の位置を得る *)
    let insert_pos (head, index) f =
      insert (ShiftL (index, shift)) Type.Int
        (fun k ->
           insert (Add (head, k)) inner_type
             (fun pos ->
                f pos))
    in
    (* 展開した二次元配列の型を変更する. *)
    let change_type (x, t) =
      let open Global in
        try
          let types =
            List.map
              (function
                 | global, lss when List.mem_assoc global targets ->
                     let global_type = (M.find global !all).typ in
                     let ls' = List.assoc global targets in
                       List.map
                         (fun ls ->
                            if is_prefix ls ls' then
                              ((*Format.eprintf "%s : %s@." x (Type.string_of (change_type (ls, ls', global_type)));*)
                               change_type (ls, ls', global_type))
                            else
                              t)
                         lss
                 | _ -> [])
              (M.find x var2lss)
          in
          let types = List.concat types in
          if types = [] then raise Not_found;
          let typ = List.hd types in
            if List.for_all
                 (fun t -> typ = t)
                 types
            then
              (x, typ)
            else
              (Format.eprintf "Not inconsistent %s types: %s@." x (String.concat ", " (List.map Type.string_of types));
               raise Impossible)
        (* 変換する必要のない場合はNot_found *)
        with Not_found -> (x, t)
    in
      (*
      if S.mem x aliases then
        (x, inner_type)
      else
        (x, t)
          (*
      if M.mem x var2lss then
        let (global, lss) = List.hd (M.find x var2lss) in
        let ls = List.hd lss in
          (x, type_from_pos (M.find x !Global.all).Global.typ lss)
      else (x, t)
           *)
    in
       *)
    let change_types = List.map change_type in
    (* 実際に代入先などを変更する.
     * env: これまでに定義された変数の集合 *)
    let rec f env = function
      | Let ((x, t), ExtFunApp (("create_array" | "create_float_array") as ca, [xn; xi]), e)
        when S.mem x aliases ->
          (* 配列の長さをのばす. *)
          let e = f (S.add x env) e in
            insert (ShiftL (xn, shift)) (Type.Int)
              (fun xn ->
                 Let (change_type (x, t), ExtFunApp (ca, [xn; xi]), e))
      | Let ((x, t), ExtArray (y), e) when S.mem x aliases ->
          if not (List.mem_assoc y targets) then (Format.eprintf "FAILED BECAUSE OF ExtArray(%s)@." y; raise Impossible);
          Let (change_type (x, t), ExtArray (y), f (S.add x env) e)
      | Let ((x, t), ExtFunApp (("create_array" | "create_float_array" as ca), [xn; xi]), e)
        when M.mem x inner2index ->
          let (a, y) = M.find x inner2index in
          (* a, yがまだ定義されていなければ失敗する. *)
            (*
          if not (S.mem a env) then Format.eprintf "FAILED: No such %s@." a;
          if not (S.mem y env) then Format.eprintf "FAILED: No such %s@." y;
             *)
          if not (S.mem a env && S.mem y env) then raise Impossible;
          let e = f (S.add x env) e in
          let e =
            insert_pos (a, y)
              (fun pos ->
                 Let (change_type (x, t), Var (pos),
                      seq (ExtFunApp ((if ca = "create_array" then "fill_for_int" else "fill_for_float"), [x; xn; xi]), e)))
          in
            e
      | Get (x, y) when S.mem x aliases ->
          insert_pos (x, y)
            (fun pos -> Var (pos))
      | Put (x, y, z) when S.mem x aliases ->
          if M.mem z inner2index then
            (* この場合は, zの初期化の時点で必要なコードが挿入されているので何もしなくてよい. *)
            Unit
          else
            ((*Format.eprintf "Not traced %s.(%s) <- %s@." x y z;*)
             raise Impossible)
      | Let ((x, t), e1, e2) ->
          let e1 = f env e1 in
          let env = S.add x env in
          let e2 = f env e2 in
            Let (change_type (x, t), e1, e2)
      | IfEq (x, y, e1, e2) -> IfEq (x, y, f env e1, f env e2)
      | IfLE (x, y, e1, e2) -> IfLE (x, y, f env e1, f env e2)
      | LetTuple (xts, y, e) ->
          let xts = change_types xts in
            LetTuple (xts, y, f env e)
      | LetRec ({ name; args; body = e1 }, e2) ->
          let env = S.add (fst name) env in
          let env' = List.fold_right (fun (x, _) -> S.add x) args env in
          let args = change_types args in
          let name =
            match name with
              | (x, Type.Fun (_, ret)) -> (x, Type.Fun (List.map snd args, ret))
              | _ -> assert false
          in
            LetRec ({ name; args; body = f env' e1 } , f env e2)
      | e -> e
    in
      (*
      Format.eprintf "Inner Type: %s@." (Type.string_of inner_type);

      M.iter
        (fun x ls ->
           Format.eprintf "%s -> %s@." x (String.concat " or " (List.map (fun (x, y) -> x ^ ".(" ^ y ^ ")") [ls])))
        inner2index;
      Format.eprintf "SIZE %d -> %d@." size (1 lsl shift);
      Format.eprintf "Inner Array: %s@." (String.concat ", " (S.to_list inner_aliases));
      Format.eprintf "Array: %s@." (String.concat ", " (S.to_list aliases));
      Format.eprintf "@.";
       *)
    let e = f S.empty e in
      update_globals targets shift;
      Format.eprintf "Successfully expanded 2D array (%s)!@."
        (String.concat "; " (List.map (fun (x, ls) -> x ^ " of " ^ string_of_pos ls) targets));
      e
  with Impossible -> (*Format.eprintf "Failed to expand@.";*) e


let f e =
  (*
  Format.eprintf "OUTPUT TO a.out@.";
    ignore (KNormal.output "a.out" e);
   *)

  (* 対象となる二次元配列の場所を計算する. *)
  let x2lss =
    M.fold
      (fun x { Global.length = len; Global.typ = typ } acc ->
         let rec f = function
           | Type.Array (Type.Array (typ)) ->
               [[]] @
               List.map (fun ls -> InArray :: InArray :: ls) (f typ)
           | Type.Array (typ) ->
               List.map (fun ls -> InArray :: ls) (f typ)
           | Type.Tuple (ts) ->
               let lss = mapi (fun i typ -> List.map (fun ls -> InTuple i :: ls) (f typ)) ts in
                 List.fold_left (@) [] lss
           | _ -> []
         in
         let lss = f typ in
           M.add x lss acc)
      !Global.all M.empty
  in
  (* グローバル配列のエイリアスとなるものを集める *)
  let x2aliases =
    M.mapi
      (fun x _ -> gather_aliases x e)
      x2lss
  in
  (* (グローバル配列, 位置)の同値関係を集める.
   * すなわち, 同じエイリアスを持つ位置は同値だと考える *)
  let equiv =
    M.fold
      (fun x aliases ->
         let target = M.find x x2lss in
           M.fold
             (fun y lss acc ->
                if List.exists (fun ls -> List.mem ls target) lss then
                  (* yが最適化に関係ある場所を示しているならば. *)
                  M.fold
                    (fun x' aliases acc ->
                       if M.mem y aliases then
                         ((x, lss), (x', M.find y aliases)) :: acc
                       else acc)
                    x2aliases acc
                else acc)
             aliases)
      x2aliases []
  in
  (* 対称閉包を求める. *)
  let equiv = List.fold_left (fun acc (x, y) -> (y, x) :: acc) equiv equiv in
  (* 同値なものをまとめて一度に最適化する. *)
  let (_, e) =
    M.fold
      (fun x lss (did, e) ->
         (* なるべく中にある配列から展開する.
          * [XXX] aliasを更新しないかぎりは, 内側から展開しないとバグるはず. *)
         let lss = List.sort (Common.compare_on (fun ls -> - List.length ls)) lss in
           List.fold_left
             (fun (did, e) ls ->
                if List.mem (x, ls) did then
                  (* 既に最適化された場合 *)
                  (did, e)
                else
                  let targets =
                    let rec loop acc =
                      let updated = ref false in
                      let acc =
                        List.fold_left
                          (fun acc edge ->
                             match edge with
                               | (y, [ls]), (y', [ls']) ->
                                   if List.mem (y, ls) acc && not (List.mem (y', ls') acc) then
                                     (updated := true;
                                      (y', ls') :: acc)
                                   else
                                     acc
                               | _ -> acc)
                          acc equiv
                      in
                        if !updated then
                          loop acc
                        else
                          acc
                    in
                      loop [(x, ls)]
                  in
                    (targets @ did, expand x2aliases targets e))
             (did, e) lss)
      x2lss ([], e)
  in
    (*
    M.iter
      (fun x lss ->

         if (*x = "dirvecs.2692" &&*) lss <> [] then (
           Format.eprintf "ALIAS of %s@." x;
           let aliases = M.find x x2aliases in
             M.iter
               (fun y lss ->
                  Format.eprintf "  %s: %s@." y (String.concat "; " (List.map (fun ls -> string_of_pos ls) lss)))
               aliases
         ))
      x2lss;
     *)

      (*
  let () =
    List.iter
      (fun ((x, lss), (x', lss')) ->
         Format.eprintf "(%s of (%s)) <-> (%s of (%s))@."
           x (String.concat "; " (List.map (fun ls -> string_of_pos ls) lss))
           x' (String.concat "; " (List.map (fun ls -> string_of_pos ls) lss')))
      equiv
  in
       *)
    (*
  Format.eprintf "OUTPUT TO b.out@.";
    ignore (KNormal.output "b.out" e);
     *)

    e
        



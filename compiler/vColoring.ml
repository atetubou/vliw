(* 干渉グラフの彩色を行う *)


open Vliw

(* スピルした変数を二度とスピルしないために保存しておく. *)
let spilled_variables = ref S.empty

(* Worklist of coalescing *)
module SS = Set.Make (struct type t = Id.t * Id.t let compare = compare end)

(* Worklist of spilling *)
module PQ = Set.Make (struct type t = float * Id.t let compare = compare end)

type data = {
  (* 既彩色. *)
  precolored : S.t;
  (* 使用可能なレジスタの個数 = S.cardinal precolored *)
  k : int;

  (* k未満の次数をもつ変数集合 *)
  simplify_worklist : S.t;
  (* k未満の次数で、Moveのオペランドになっている変数集合 *)
  freeze_worklist : S.t;
  (* k以上の次数をもつ変数集合 *)
  spill_worklist : PQ.t;
  (* グラフから取り除かれた変数のスタック. *)
  select_stack : Id.t list;
  (* スピルされた変数集合 *)
  spilled_nodes : S.t;

  (* 変数をスピルするときに生ずる推定コスト. *)
  spill_cost : float M.t;

  (* Move命令: (x, y) は x <- y を意味する. *)
  (* Coalescingの準備ができていない集合. *)
  active_moves : SS.t;
  (* CoalescingできるMove命令の集合 *)
  worklist_moves : SS.t;

  (* 合併されたノード -> 合併後の名前
   * 定義域はcoalescedNodesに等しい. *)
  alias : Id.t M.t;
  (* 変数名 -> レジスタ名 *)
  color : Id.t M.t;

  (* 干渉グラフから取り除かれた変数集合.
   * coalescedNodesとselectStackの和集合に等しい. *)
  removed_set : S.t;

  (* 干渉グラフの辺集合. *)
  adj_set : SS.t;

  (* 以下の関数は, initial(彩色対象の変数集合)を定義域にもつ全関数. *)
  (* 干渉グラフの隣接リスト表現 *)
  adj_list : S.t M.t;
  (* 干渉グラフの次数. *)
  degree : int M.t;

  (* 以下の関数は, initial or precolored を定義域にもつ全関数. *)
  (* 変数名 -> その変数名が関係するMove命令の集合 *)
  move_list : SS.t M.t;
}

(* move_listに追加するための関数. *)
let append x v x2mvs =
  (* 全関数にしているはず. *)
  assert (M.mem x x2mvs);
  M.add x (SS.add v (M.find x x2mvs)) x2mvs

(* spill_worklistを操作するための補助関数. *)
let remove_spill x data = PQ.remove (M.find x data.spill_cost, x) data.spill_worklist
let add_spill x data = PQ.add (M.find x data.spill_cost, x) data.spill_worklist
let mem_spill x data = PQ.mem (M.find x data.spill_cost, x) data.spill_worklist


(* 低次数かどうか.
 * precoloredの場合は, 過剰次数として扱う. *)
let is_insignificant x data = not (S.mem x data.precolored) && M.find x data.degree < data.k

(* 干渉グラフに辺を追加する.
 * ただし, initialに含まれる頂点( = data.adj_listの定義域)のみについて隣接情報を保持する. *)
let add_edge x y data =
  if x <> y && not (SS.mem (x, y) data.adj_set) then
    let data = { data with adj_set = SS.add (x, y) (SS.add (y, x) data.adj_set) } in
    let add x y data =
      if not (S.mem x data.precolored) then
        { data with
              adj_list = M.add x (S.add y (M.find x data.adj_list)) data.adj_list;
              degree = M.add x (M.find x data.degree + 1) data.degree; }
      else
        data
    in
      add x y (add y x data)
  else
    data


(* これから合併するか考慮すべきMove命令. *)
let node_moves x data =
  SS.inter
    (M.find x data.move_list)
    (SS.union data.active_moves data.worklist_moves)

(* Coalescingをする対象になりうるか.
 * まだワークリストに処理していないMove命令があるときにtrueとなる. *)
let is_move_related x data =
  not (SS.is_empty (node_moves x data))


(* 干渉グラフ上で隣接する頂点を取り出す.
 * adj_listとは, 取り除かれた頂点が含まれない, という点で異なる. *)
let adjacent x data =
  (* xはprecoloredでないはず. *)
  assert (M.mem x data.adj_list);
  S.diff (M.find x data.adj_list) data.removed_set

(* xに関連するMove命令をワークリストに移動する. *)
let enable_moves x data =
  let moves = SS.inter (M.find x data.move_list) data.active_moves in
    SS.fold
      (fun mv data ->
         { data with
               active_moves = SS.remove mv data.active_moves;
               worklist_moves = SS.add mv data.worklist_moves; })
      moves data

(* 次数を減少させる.
 * それによって生ずるワークリストの移動を行う.
 * ただし, precoloredである場合は無視する. *)
let decrement_degree x data =
  if S.mem x data.precolored then data
  else
    let d = M.find x data.degree in
    let data = { data with degree = M.add x (d-1) data.degree } in
      if d = data.k then
        (* xとその隣接する変数が、合併できる可能性が生ずる. *)
        let data = enable_moves x data in
        let data = S.fold enable_moves (adjacent x data) data in
        (* 次数が新たにk未満になったので, 適切なワークリストへ移動する. *)
        let data = { data with spill_worklist = remove_spill x data } in
        let data =
          if is_move_related x data then
            { data with freeze_worklist = S.add x data.freeze_worklist }
          else
            { data with simplify_worklist = S.add x data.simplify_worklist }
        in
          data
      else
        data

(* k未満の次数をもつ変数を, 干渉グラフから取り除く. *)
let simplify data =
  let x = S.choose data.simplify_worklist in
  let data =
    { data with
         simplify_worklist = S.remove x data.simplify_worklist;
         select_stack = x :: data.select_stack;
         removed_set = S.add x data.removed_set; }
  in
    (* 変数xが干渉グラフから取り除かれたので,
     * 隣接する頂点に対して次数を減らす. *)
    S.fold decrement_degree (adjacent x data) data


(* 合併された後の変数名を得る *)
let rec alias_of x data =
  if M.mem x data.alias then
    alias_of (M.find x data.alias) data
  else
    x

(* xがもはや転送関連でなくなった場合は, freeze_worklistからsimplify_worklistへ移動する. *)
let add_worklist x data =
  if is_insignificant x data && not (is_move_related x data) then
    { data with
          freeze_worklist = S.remove x data.freeze_worklist;
          simplify_worklist = S.add x data.simplify_worklist; }
  else
    data

(* xとyについて, 保守的合併ができるか. *)
let conservative x y data =
  let briggs =
    (* 合併したあとで干渉する変数について,
     * 単純化できない変数の個数kを数える.
     * 合併した後に単純化を繰り返すことで, 合併した後の変数と干渉する頂点の個数はk個以下となる.
     * よって、 kがdata.k未満であれば, 合併しても単純化可能である. *)
    let new_adj = S.union (adjacent x data) (adjacent y data) in
    let k = S.cardinal (S.filter (fun z -> not (is_insignificant z data)) new_adj) in
      k < data.k
  in
  let george x y =
    (* xと干渉する全ての変数について,
     * (1) 既にyと干渉しているか,
     * (2) 次数がk未満で単純化できる
     * のいずれかであるとき, 合併しても単純化できる. *)
    S.for_all
      (fun z ->
         (* 既にyと干渉している場合は, zについて次数は増えない *)
         SS.mem (z, y) data.adj_set ||
         (* zが単純化できるとき, zを除いて考えてもよい. *)
         is_insignificant z data)
      (adjacent x data)
  in
    briggs || george x y || george y x


(* yを消去してxに合併する. *)
let combine x y data =
  (* yは転送関連の命令であるから,
   * freeze or spill worklistに含まれる. yを削除するので, そのワークリストから削除する. *)
  let data =
    assert (S.mem y data.freeze_worklist || mem_spill y data);
    if S.mem y data.freeze_worklist then
      { data with freeze_worklist = S.remove y data.freeze_worklist }
    else
      { data with spill_worklist = remove_spill y data }
  in
  (* alias(y) = xとする *)
  let data = { data with alias = M.add y x data.alias } in
  (* yを干渉グラフから削除する. *)
  let data = { data with removed_set = S.add y data.removed_set } in
  (* yに関連するMove命令がCoalescingできるようになるかもしれない.
   * [XXX] x, yに隣接するノードとxについては, decrement_degreeによってenable_movesが行われる. *)
  let data = enable_moves y data in
  (* Move関連の命令について, xとyを合併させる. *)
  let data =
    { data with
          move_list = M.add x (SS.union (M.find x data.move_list) (M.find y data.move_list)) data.move_list } in
  (* 干渉グラフにおいてxとyを合併させる. *)
  let data =
    S.fold
      (fun z data ->
         (* yと干渉していた変数は, xと干渉するようになる. *)
         let data = add_edge z x data in
         (* 一方, yが干渉グラフから削除されるので次数を減らす. *)
         let data = decrement_degree z data in
           data)
      (adjacent y data) data
  in
  let data =
    (* 保守的合併であっても、一時的にxの次数が過剰次数となることはあり得る.
     * 過剰次数であるのにもかかわらず, freeze_worklistに存在する場合はspill_worklistに移動させる. *)
    if not (S.mem x data.precolored) &&
       M.find x data.degree >= data.k &&
       S.mem x data.freeze_worklist
    then
      { data with
            freeze_worklist = S.remove x data.freeze_worklist;
            spill_worklist = add_spill x data; }
    else
      data
  in
    data


(* 合併を行う. *)
let coalesce data =
  let (x, y) as mv = SS.choose data.worklist_moves in
  let data = { data with worklist_moves = SS.remove mv data.worklist_moves } in
  let x = alias_of x data in
  let y = alias_of y data in
  let (x, y) =
    if S.mem y data.precolored then
      (y, x)
    else (x, y)
  in
    match () with
      (* もともと合併されている *)
      | _ when x = y ->
          add_worklist x data
      (* xとyが干渉する場合, 合併を諦める.
       * すなわち, x, yが共にprecoloredであるか,
       * もしくは(x, y)が干渉グラフに辺をもつ場合. *)
      | _ when S.mem y data.precolored || SS.mem (x, y) data.adj_set ->
          (* xがprecoloredになるようにx, yを交換しているので, yがprecoloredならxもprecoloredなはず. *)
          if S.mem y data.precolored then assert (S.mem x data.precolored);
          let data = add_worklist x data in
          let data = add_worklist y data in
            data
      | _ when
          (S.mem x data.precolored &&
           (* xがprecoloredであるとき, xとyを合併できるか. *)
           S.for_all
             (fun t ->
                (* tがxと既に干渉している *)
                S.mem t data.precolored ||
                SS.mem (t, x) data.adj_set ||
                (* tは低次数であって単純化される *)
                is_insignificant t data)
             (adjacent y data)) ||
          (not (S.mem x data.precolored) &&
           (* xがprecoloredでないとき. *)
           conservative x y data)
        ->
          (* xとyの合併を行う *)
          (* yを消去してxへ合併. *)
          let data = combine x y data in
          let data = add_worklist x data in
            data
      (* 保守的合併に失敗した場合, active_movesに戻す. *)
      | _ ->
          { data with active_moves = SS.add mv data.active_moves }


(* xに関連するMove命令をfreezeする. *)
let freeze_moves x data =
  (* xは, 直前まで何かのワークリストに入っていた変数である.
   * よって, 合併により消去されている, ということはないはず. *)
  assert (x = alias_of x data);
  SS.fold
    (fun ((u, v) as mv) data ->
       let u = alias_of u data in
       let v = alias_of v data in
       assert (x = u || x = v);
       (* xと異なる変数をyとおく. *)
       let t = if x = u then v else u in
       (* mvをワークリストから完全に削除する. *)
       let data = { data with active_moves = SS.remove mv data.active_moves } in
         (* tがもはやMove関連でなくなった場合に, ワークリストを適切に移動する. *)
         if not (is_move_related t data) && S.mem t data.freeze_worklist then
           { data with
                 freeze_worklist = S.remove t data.freeze_worklist;
                 simplify_worklist = S.add t data.simplify_worklist; }
         else
           data)
    (node_moves x data) data


(* 合併するのを諦める. *)
let freeze data =
  let x = S.choose data.freeze_worklist in
  let data =
    { data with
          freeze_worklist = S.remove x data.freeze_worklist;
          simplify_worklist = S.add x data.simplify_worklist; }
  in
    freeze_moves x data


(* コストの低い変数を(潜在)スピルする.
 * スピルした変数はsimplify_worklistに追加し, 後で干渉グラフから取り除く. *)
let select_spill data =
  let (cost, x) as elt = PQ.min_elt data.spill_worklist in
  Format.eprintf "potential spill %s with cost %f@." x cost;
  let data =
    { data with
          spill_worklist = PQ.remove elt data.spill_worklist;
          simplify_worklist = S.add x data.simplify_worklist; }
  in
    (* xは干渉グラフから取り除くので, 関連するMove命令はfreezeする. *)
    freeze_moves x data


(* select_stackから取り出しながら彩色を行う.
 * 実スピルが生じた場合は, それをspilled_nodesに加える. *)
let assign_colors reg2cost data =
  (* 変数xに割り当てられた色を取得する. *)
  let get_color_of x data =
    if M.mem x data.color then
      Some (M.find x data.color)
    else if S.mem x data.precolored then
      Some (x)
    else
      None
  in
  let cost_of x = float_of_int (M.find x reg2cost) in
  let precolored = S.fold (fun x -> PQ.add (cost_of x, x)) data.precolored PQ.empty in
  (* select_stackの先頭から取り出して彩色する. *)
  let data =
    List.fold_left
      (fun data x ->
         let ok_colors =
           S.fold
             (fun y acc ->
                (* yに隣接する頂点の色をaccから除く. *)
                let y = alias_of y data in
                  match get_color_of y data with
                    | Some (c) -> PQ.remove (cost_of c, c) acc
                    | _ -> acc)
             (M.find x data.adj_list) precolored
         in
           if PQ.is_empty ok_colors then
             let () = Format.eprintf "actual spill %s@." x in
             (* 割り当てることができるレジスタがない場合, 実スピルする. *)
             { data with spilled_nodes = S.add x data.spilled_nodes }
           else
             { data with color = M.add x (snd (PQ.min_elt ok_colors)) data.color })
      data data.select_stack
  in
  (* 合併された変数に対して彩色する *)
  let data =
    M.fold
      (fun x _ data ->
         let y = alias_of x data in
         (* data.colorの定義域はinitialであって, precoloredは含まれない. *)
           match get_color_of y data with
             | Some (c) -> { data with color = M.add x c data.color }
             (* spillが起きている場合は, yが割り当てられていないかもしれない. *)
             | _ -> assert (S.mem y data.spilled_nodes); data)
      data.alias data
  in
    data

let stack_pos_of l pos =
  if l = Id.main_label then
    (* メイン関数では静的領域の先頭から使用する. *)
    (Asm.zero, Asm.stack_top + pos)
  else
    (* 関数の先頭で$stackをスタックフレーム分確保するので,
     * スタックフレームは$stack - stack_size から $stack - 1の間. *)
    (Asm.stack, -pos - 1)

let load_of l typ pos =
  let (x, i) = stack_pos_of l pos in
    match typ with
      | Type.Float -> FLoadC (x, C (i))
      | _ -> LoadC (x, C (i))

let store_of l typ pos z =
  let (x, i) = stack_pos_of l pos in
    match typ with
      | Type.Float -> FStoreC (z, x, i)
      | _ -> StoreC (z, x, i)

(* spilled_nodesに含まれる変数をスタック上に割りつける. *)
let rewrite_program l typ data func =
  Format.eprintf "Actual spilling occurred. (%s)@."
    (String.concat ", " (S.to_list data.spilled_nodes));
  (* レジスタはスピルされないはず. *)
  S.iter
    (fun x ->
       (* レジスタはスピルされない *)
       assert (not (Asm.is_reg x));
       (* 一度スピルされた変数がさらにスピルされていたら失敗. *)
       if S.mem x !spilled_variables then
         failwith "Failed because of insufficient registers")
    data.spilled_nodes;
  (* スタック上の位置を変数のために確保する. *)
  let (var2pos, stack_size) =
    S.fold
      (fun x (var2pos, stack_size) ->
         (M.add x stack_size var2pos, stack_size + 1))
      data.spilled_nodes (M.empty, func.stack_size)
  in
  let func = { func with stack_size = stack_size } in
  (* スピルされた変数の使用と定義の前後にload, storeを挟む. *)
  let func =
    let cfg = func.cfg in
    (* defの後にstoreをはさむ. *)
    let typ_env = ref M.empty in
    let cfg =
      G.map_stmt_list
        (function
           | stmt when List.exists (fun (x, _) -> M.mem x var2pos) (def_of stmt) ->
               (* 型の情報を作っておく. strictであるから, var2posは必ず一度は定義されるはずで, typ_envはvar2posと同じ定義域をもつ. *)
               let () = List.iter (fun (x, t) -> if M.mem x var2pos then typ_env := M.add x t !typ_env) (def_of stmt) in
               let (env, stores) =
                 List.fold_left
                   (fun (env, stores) (x, t) ->
                      if M.mem x var2pos then
                        let x' = Id.genid x in
                          spilled_variables := S.add x' !spilled_variables;
                          (M.add x x' env,
                           gen_stmt [] (store_of l typ (M.find x var2pos) x') :: stores)
                      else
                        (env, stores))
                   (M.empty, []) (def_of stmt)
               in
                 replace_def_stmt env stmt :: stores
           | stmt -> [stmt])
        cfg
    in
    (* useの前にloadをはさむ. *)
    let cfg =
      G.map_stmt_list
        (function
           | stmt when List.exists (fun x -> M.mem x var2pos) (use_of stmt) ->
               let (env, loads) =
                 List.fold_left
                   (fun (env, loads) x ->
                      if M.mem x var2pos then
                        let x' = Id.genid x in
                          spilled_variables := S.add x' !spilled_variables;
                          (M.add x x' env,
                           gen_stmt
                             [x', (assert (M.mem x !typ_env); M.find x !typ_env)]
                             (load_of l typ (M.find x var2pos)) :: loads)
                      else
                        (env, loads))
                   (M.empty, []) (use_of stmt)
               in
                 loads @ [replace_use_stmt env stmt]
           | stmt -> [stmt])
        cfg
    in
      { func with cfg = cfg }
  in
    func


let build_spill_cost func data =
  let data =
    G.fold
      (fun b ->
         (* 統計情報が利用できる場合は, bが何回実行されるかを得る. *)
         let cnt = try VProfile.count_of b with Not_found -> 1 in
           List.fold_right
             (fun stmt data ->
                let add x data =
                  if M.mem x data.spill_cost then
                    { data with spill_cost = M.add x (M.find x data.spill_cost +. float_of_int cnt) data.spill_cost }
                  else data
                in
                let data = List.fold_right add (use_of stmt) data in
                let data = List.fold_right add (List.map fst (def_of stmt)) data in
                  data))
      func.cfg data
  in
  (* 使用回数 / 干渉グラフ上での次数 をヒューリスティクスとする. *)
  let data =
    { data with
          spill_cost =
            M.mapi
              (fun x cost ->
                 (* スピルしていた変数であれば, もうスピルさせないようにする. *)
                 let extra = if S.mem x !spilled_variables then 10000000.0 else 0.0 in
                   extra +.  M.find x data.spill_cost /. float_of_int (M.find x data.degree))
              data.spill_cost }
  in
    data
  

(* dataを型typについて構築する.
 * 彩色の対象となるものは, 型typをもつ変数とレジスタである。*)
let build typ func =
  (* dataと、彩色対象の変数集合initialを構築する. *)
  let (data, initial) =
    let precolored = S.of_list (match typ with Type.Float -> Asm.fregs | _ -> Asm.regs) in
    let precolored = S.remove Asm.link(*TODO: 今のところ, リンクレジスタは完全に別扱い *) precolored in
    let initial =
      G.fold_stmt
        (fun stmt ->
           List.fold_right
             (fun (x, t) acc ->
                (* 型にあう変数であり, 既彩色されていない変数のみを追加する. *)
                if (t = Type.Float) = (typ = Type.Float) && not (S.mem x precolored) then
                  S.add x acc
                else acc)
             (def_of stmt))
        func.cfg S.empty
    in
    let data =
      {
        precolored = precolored;
        k = S.cardinal precolored;

        simplify_worklist = S.empty;
        freeze_worklist = S.empty;
        spill_worklist = PQ.empty;
        select_stack = [];
        spilled_nodes = S.empty;

        spill_cost = S.fold (fun x -> M.add x 0.0) initial M.empty;

        active_moves = SS.empty;
        worklist_moves = SS.empty;

        alias = M.empty;
        color = M.empty;

        removed_set = S.empty;

        adj_set = SS.empty;

        (* 処理が簡単になるように、全関数にしておく. *)
        adj_list = S.fold (fun x -> M.add x S.empty) initial M.empty;
        degree = S.fold (fun x -> M.add x 0) initial M.empty;

        move_list = S.fold (fun x -> M.add x SS.empty) (S.union initial precolored) M.empty;
      }
    in
      (data, initial)
  in
  (* 今回の彩色の対象となるものか. *)
  let is_target x = S.mem x data.precolored || S.mem x initial in

  let liveness = VLiveness.create ~is_target func.cfg in
  (* 彩色する対象の変数を集める.
   * プログラムがstrictであることを仮定しているので、 def集合から集める.
   * 型の合わないレジスタも除かなければいけないことに注意. *)
  let data =
    G.fold
      (fun b stmts data ->
         snd
           (List.fold_right
              (fun stmt (live, data) ->
                 let data =
                   fold_half
                     (fun half data ->
                        let (live, data) =
                          match (half.def, half.exp, typ = Type.Float) with
                            (* 型にあうMove命令の場合のみ考慮する. *)
                            | [x, _], FMov (y, []), true
                            | [x, _],  Mov (y), false ->
                                (* xとyは等しいので, yがこの文で出口生存であってもxと干渉させない. *)
                                (S.remove y live,
                                 { data with
                                       move_list = append x (x, y) (append y (x, y) data.move_list);
                                       worklist_moves = SS.add (x, y) data.worklist_moves })
                            | _ -> (live, data)
                        in
                        let def = List.filter is_target (List.map fst half.def) in
                        let set =
                          List.fold_left
                            (fun acc (x, _) -> if is_target x then S.add x acc else acc)
                            live (def_of stmt)
                        in
                        (* この文halfが定義するdefと, 出口生存なもの + 文全体のdefを干渉させる.
                         * + 文全体のdefが必要なのは, 一度に同じレジスタに書き込めないという制約から生ずる. *)
                        let data =
                          List.fold_right
                            (fun x -> S.fold (fun y data -> add_edge x y data) set)
                            def data
                        in
                        let data =
                          match half with
                            | { def = rets; exp = Call (_, xs) } ->
                                (* Callの場合, 引数が同じレジスタに割り当てられては困るので, 人為的に干渉させる. *)
                                let data =
                                  let xs = List.filter is_target xs in
                                    List.fold_right (fun x -> List.fold_right (fun y -> add_edge x y) xs) xs data
                                in
                                (* 返り値は, link, stack, heap, zeroなどに割り当てられては行けない. *)
                                let data =
                                  let xs = List.filter is_target (List.map fst rets) in
                                  let ys =
                                    let allregs = Asm.regs @ Asm.fregs in
                                      List.filter is_target (List.filter (fun x -> not (List.mem x (Asm.argregs @ Asm.argfregs))) allregs)
                                  in
                                    List.fold_right (fun x -> List.fold_right (fun y -> add_edge x y) ys) xs data
                                in
                                  data
                            | _ -> data
                        in
                          data)
                     stmt data
                 in
                   (VLiveness.transfer ~is_target stmt live, data))
              stmts (VLiveness.live_of b liveness, data)))
      func.cfg data
  in
  Format.eprintf "Number of variables: %d@." (S.cardinal initial);
  Format.eprintf "Number of moves: %d@." (SS.cardinal data.worklist_moves);
  let data = build_spill_cost func data in
  (* worklistを構築. *)
  let data =
    S.fold
      (fun x data ->
         match () with
           | _ when M.find x data.degree >= data.k ->
               { data with spill_worklist = add_spill x data }
           (* TODO: 本当は, 最初は 転送関連かどうかは, M.find x data.move_list <> []かどうかでわかる. *)
           | _ when is_move_related x data ->
               { data with freeze_worklist = S.add x data.freeze_worklist }
           | _ ->
               { data with simplify_worklist = S.add x data.simplify_worklist })
      initial data
  in
    data


(* 関数funcのレジスタの割り当てを求める.
 * reg2cost: レジスタ -> そのレジスタを使った場合にかかるコスト *)
let f reg2cost l func =
  let rec g typ l func =
    let rec loop = function
      | { simplify_worklist = w } as data when not (S.is_empty w)  -> loop (simplify data)
      | { worklist_moves = w    } as data when not (SS.is_empty w) -> loop (coalesce data)
      | { freeze_worklist = w   } as data when not (S.is_empty w)  -> loop (freeze data)
      | { spill_worklist = w    } as data when not (PQ.is_empty w) -> loop (select_spill data)
      | data -> data
    in
  
    Format.eprintf "Register allocation (type = %s)@." (match typ with Type.Float -> "float" | _ -> "int");

    Time.start "Build";
    let data = build typ func in
    Time.stop "Build";
    Time.start "Loop";
    let data = loop data in
    Time.stop "Loop";
    Time.start "Assign";
    let data = assign_colors reg2cost data in
    Time.stop "Assign";

      if S.is_empty data.spilled_nodes then
        (* 今回の反復でスピルしなかった場合は, 彩色結果と, (rewrite_programで変形された)funcを返す. *)
        (data.color, func)
      else
        (* スピルした場合は, プログラムを書きなおしてもう一度. *)
        g typ l (rewrite_program l typ data func)
  in
  (* 浮動小数点レジスタと汎用レジスタそれぞれについて彩色する. *)
  let (color1, func) = g Type.Float l func in
  let (color2, func) = g Type.Int l func in
  let color =
    M.merge
      (fun x a b ->
         (* レジスタは彩色されないはず. *)
         assert (not (Asm.is_reg x));
         match (a, b) with
           (* 同じ変数がType.IntかつType.Floatであるのはおかしい. *)
           | Some a, Some b ->Format.eprintf "%s -> %s and %s@." x a b;  assert false
           | Some _, _ -> a
           | _ -> b)
      color1 color2
  in
  let replace (x, t) = if M.mem x color then (M.find x color, t) else (x, t) in
  (* 実際に変数をレジスタで置換する. *)
  let func =
    { func with
          cfg = G.map_stmt (replace_stmt color) func.cfg;
          args = List.map replace func.args;
          rets = List.map replace func.rets; }
  in
    func


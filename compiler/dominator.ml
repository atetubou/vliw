(* ループの最適化を行う *)

open Block

module DS =
  DestructiveSet.Make
    (struct
       type t = Id.t
       let compare = compare
     end)


type t = {
  dom : DS.t M.t;
  creator : bool -> DS.t;
}

let creator t = t.creator

let dom_of b t = DS.copy (M.find b t.dom)
                   
let inter = DS.inter

let mem = DS.mem

(* bがb'を支配するか. *)
let dominate b b' t = DS.mem b (M.find b' t.dom)

let create cfg =
  Time.start "Dominator";
  let direction = DataFlow.Forward in
  let universe = G.fold (fun b _ -> DS.S.add b) cfg DS.S.empty in
  let creator = DS.creator_of universe in
  let dom =
    G.fold
      (fun b _ ->
         let set =
           if G.pred b cfg = [] then
             let res = creator false in
               DS.add b res;
               res
           else
             creator true
         in
           M.add b set)
      cfg M.empty
  in
  let update b = function
    | [] -> false
    | pred ->
        let this = creator true in
          List.iter
            (fun b' -> DS.inter this (M.find b' dom))
            pred;
          DS.add b this;
          let old = M.find b dom in
          let updated = not (DS.equal this old) in
            DS.move old this;
            updated
  in
    DataFlow.solution_of_block ~direction ~update cfg;
    Time.stop "Dominator";
    (*
    G.iter
      (fun b _ ->
         Format.eprintf "%s: %s@." b (String.concat ", " (DS.to_list (M.find b dom))))
      cfg
     *)
    { dom; creator }


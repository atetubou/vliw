(* 到達定義解析による最適化.
 * 定数畳み込みと, 部分冗長な分岐の除去を行う. *)

open Block

(* 整数か浮動小数点の定数か. *)
let is_constant = function Set _ | FSet _ -> true | _ -> false

(* [XXX] 32bitの環境では正しくないことに注意. 最上位bitが切り捨てられてしまう *)
let trunci x = Int32.(to_int (of_int x))
let truncf f = Int32.(float_of_bits (bits_of_float f))

(* x, yを引数としてfunctとともに評価する. *)
let eval_with_funct op x y funct =
  let { Funct.fpu = fpu; Funct.abs = abs; Funct.neg = neg } = Funct.of_funct funct in
  let y = if fpu then -. y else y in
  let res = op x y in
  let res = if abs then abs_float res else res in
  let res = if neg then -. res else res in
    res

(* 何らかの作業が不可能である場合に発生する. *)
exception Impossible

(* stmtに含まれる変数の使用を, 定数に置き換えられるならば置き換える. 
 * value_of: 変数 -> stmtに到達する定義文が全て同じ定数を定義しているときに, その定数を与える. *)
let constantiate value_of stmt =
  let findi x = match value_of x with Set (i) -> i | _ -> assert false in
  let findf x = match value_of x with FSet (f) -> f | _ -> assert false in
  let mem x = try ignore (value_of x); true with Impossible -> false in
  let f = function
    | Mov (x) when mem x -> Set (findi x)
    | Add (V(x), y) when mem x && Asm.fit_immediate (findi x) -> Add (C(findi x), y)
    | Add (V(y), x) when mem x && Asm.fit_immediate (findi x) -> Add (C(findi x), y)
    | Add (C(i), x) when mem x -> Set (i + findi x)
    | Sub (V(x), y) when mem x && Asm.fit_immediate (findi x) -> Sub (C(findi x), y)
    | Sub (V(y), x) when mem x && Asm.fit_immediate (-findi x) -> Add (C(-findi x), y)
    | Sub (C(i), x) when mem x -> Set (i - findi x)
    | ShiftL (x, i) when mem x -> Set (findi x lsl i)
    | ShiftR (x, i) when mem x -> Set (findi x lsr i)
    | FMov (x, func) when mem x -> FSet (eval_with_funct (fun x y -> y) 0.0 (findf x) func)
    | FAdd (x, y, funct) when mem x && mem y -> (FSet (eval_with_funct (fun x y -> x +. y) (findf x) (findf y) funct))
    | FMul (x, y, funct) when mem x && mem y -> (FSet (eval_with_funct (fun x y -> x *. y) (findf x) (findf y) funct))
    | FInv (x, funct) when mem x ->    (FSet (eval_with_funct (fun x y -> 1.0 /. y) 0.0 (findf x) funct))
    | FSqrt (x, funct) when mem x ->   (FSet (eval_with_funct (fun x y -> sqrt y) 0.0 (findf x) funct))
    | Load (x, C(i)) when mem x && Asm.fit_immediate (findi x + i) -> Load (Asm.zero, C (findi x + i))
    | Load (x, V(y)) when mem x && Asm.fit_immediate (findi x) -> Load (y, C (findi x))
    | Load (y, V(x)) when mem x && Asm.fit_immediate (findi x) -> Load (y, C (findi x))
    | FLoad (x, C(i)) when mem x && Asm.fit_immediate (findi x + i) -> FLoad (Asm.zero, C (findi x + i))
    | FLoad (x, V(y)) when mem x && Asm.fit_immediate (findi x) -> FLoad (y, C (findi x))
    | FLoad (y, V(x)) when mem x && Asm.fit_immediate (findi x) -> FLoad (y, C (findi x))
    | Store (x, y, C(i)) when mem y && Asm.fit_immediate (findi y + i) -> Store (x, Asm.zero, C (findi y + i))
    | Store (x, z, V(y)) when mem y && Asm.fit_immediate (findi y) -> Store (x, z, C (findi y))
    | Store (x, y, V(z)) when mem y && Asm.fit_immediate (findi y) -> Store (x, z, C (findi y))
    | Store (x, y, z') when mem x && findi x = 0 -> Store (Asm.zero, y, z')
    | FStore (x, y, C(i)) when mem y && Asm.fit_immediate (findi y + i) -> FStore (x, Asm.zero, C (findi y + i))
    | FStore (x, z, V(y)) when mem y && Asm.fit_immediate (findi y) -> FStore (x, z, C (findi y))
    | FStore (x, y, V(z)) when mem y && Asm.fit_immediate (findi y) -> FStore (x, z, C (findi y))
    | LoadC (x, C(i)) when mem x && Asm.fit_immediate (findi x + i) -> LoadC (Asm.zero, C (findi x + i))
    | LoadC (x, V(y)) when mem x && Asm.fit_immediate (findi x) -> LoadC (y, C (findi x))
    | LoadC (y, V(x)) when mem x && Asm.fit_immediate (findi x) -> LoadC (y, C (findi x))
    | FLoadC (x, C(i)) when mem x && Asm.fit_immediate (findi x + i) -> FLoadC (Asm.zero, C (findi x + i))
    | FLoadC (x, V(y)) when mem x && Asm.fit_immediate (findi x) -> FLoadC (y, C (findi x))
    | FLoadC (y, V(x)) when mem x && Asm.fit_immediate (findi x) -> FLoadC (y, C (findi x))
    | StoreC (x, y, C(i)) when mem y && Asm.fit_immediate (findi y + i) -> StoreC (x, Asm.zero, C (findi y + i))
    | StoreC (x, z, V(y)) when mem y && Asm.fit_immediate (findi y) -> StoreC (x, z, C (findi y))
    | StoreC (x, y, V(z)) when mem y && Asm.fit_immediate (findi y) -> StoreC (x, z, C (findi y))
    | StoreC (x, y, z') when mem x && findi x = 0 -> StoreC (Asm.zero, y, z')
    | FStoreC (x, y, C(i)) when mem y && Asm.fit_immediate (findi y + i) -> FStoreC (x, Asm.zero, C (findi y + i))
    | FStoreC (x, z, V(y)) when mem y && Asm.fit_immediate (findi y) -> FStoreC (x, z, C (findi y))
    | FStoreC (x, y, V(z)) when mem y && Asm.fit_immediate (findi y) -> FStoreC (x, z, C (findi y))
    | IfEq (x, V (y)) when mem y && Asm.fit_small_immediate (findi y) -> IfEq (x, C (findi y))
    | IfEq (y, V (x)) when mem y && Asm.fit_small_immediate (findi y) -> IfEq (x, C (findi y))
    | IfLt (x, V (y)) when mem y && Asm.fit_small_immediate (findi y) -> IfLt (x, C (findi y))
    | ItoF (x) when mem x -> FSet (Int32.float_of_bits (Int32.of_int (findi x)))
    | FtoI (x) when mem x -> Set (Int32.to_int (Int32.bits_of_float (findf x)))
    | exp -> exp
  in
  let f exp =
    let exp' = f exp in
    let exp' =
      (* 定数畳み込みをする際には32bitに丸め込む. *)
      match exp' with
        | Set (i) -> Set (trunci i)
        | FSet (f) -> FSet (truncf f)
        | exp' -> exp'
    in
      if exp = exp' then raise Impossible;
      (*
      (* 16bitに収まらない場合は定数畳み込みしない. *)
      match exp' with Set (x) when not (is_bits_of 16 x) -> raise Impossible | _ -> ();
       *)
      exp'
  in
  let rec loop exp = try loop (f exp) with Impossible -> exp in
    try
      let exp = f stmt.exp in
      let exp = loop exp in
        { (gen_stmt stmt.def exp) with s_id = stmt.s_id }
    with Impossible -> stmt
  

(* * 定数畳み込み
 *
 * * データフローの更新について
 * 到達する定義の集合(Reach.reach_of)が、実際のもの以上の大きさであれば、間違うことはない。
 * Reach.use_ofについても同様。
 *
 * プログラム中でCFGに行う変更
 * 1. 文のuse集合を空集合にする(定数文で置き換え)をする。
 * 2. 1に加えて辺を削除する. (ifの確定)
 *
 * 以上1, 2はReach.reach_of, Reach.use_ofの集合を小さくするような変更であるから、
 * データフローを更新する必要はない。
 *)
let constfold reach cfg =
  (* setに含まれるxの値を求める *)
  let value_of set x =
    let defs = Reach.def_of reach x set in
      (* プログラムはstrictなはず. *)
      assert (defs <> []);
      if not (List.for_all (fun stmt -> is_constant stmt.exp) defs) then raise Impossible;
      let value = (List.hd defs).exp in
        (* 全ての定義文が同じ定数を定義しているか. *)
        if not (List.for_all (fun stmt -> value = stmt.exp) defs) then raise Impossible;
        value
  in
  let cfg =
    G.map
      (fun b stmts ->
         let set = Reach.reach_of b reach in
           List.rev
             (List.fold_left
                (fun acc stmt ->
                   let acc = constantiate (value_of set) stmt :: acc in
                     Reach.transfer reach set stmt;
                     acc)
                [] stmts))
      cfg
  in
  (* IfLtのオペランドを逆転させる *)
  let cfg =
    G.fold
      (fun b stmts cfg ->
         match partition_last stmts with
           | ({ exp = IfLt (x, V (y)) } , stmts) ->
               let set = Reach.reach_of b reach in
                 Reach.transfer_of_block reach set b;
                 (try
                    let i = match value_of set x with Set (i) -> i | _ -> assert false in
                      if not (Asm.fit_small_immediate (i + 1)) then raise Impossible;
                      let cfg = G.update_block b (stmts @ [gen_stmt [] (IfLt (y, C (i + 1)))]) cfg in
                      (* ThenとElseの付け替え *)
                      let cfg = G.add_edge b (G.succ_of b Else cfg) Then (G.add_edge b (G.succ_of b Then cfg) Else cfg) in
                        cfg
                  with Impossible -> cfg)
           | _ -> cfg)
      cfg cfg
  in
    cfg

(* x_ls, y_lsの全てについて p x y か. *)
let for_all2 p x_ls y_ls = List.for_all (fun x -> List.for_all (fun y -> p (x, y)) y_ls) x_ls

let find' x env = match x with C (i) -> [Set (i)] | V (y) -> M.find y env

(* envの元で分岐を確定できるかどうか調べる.
 * b = trueの場合はThen節へ, b = falseの場合はElse節へ. *)
let infer b env = function
  | IfEq (x, y') ->
      for_all2
        (function (x, y) -> (x = y) <> (not b))
        (M.find x env) (find' y' env)
  | IfLt (x, y') ->
      for_all2
        (function (Set (i), Set (j)) -> (i < j) <> (not b) | _ -> assert false)
        (M.find x env) (find' y' env)
  | IfFEq (x, y) ->
      for_all2
        (function (x, y) -> (x = y) <> (not b))
        (M.find x env) (M.find y env)
  | IfFLt (x, y) ->
      for_all2
        (function (FSet (i), FSet (j)) -> (i < j) <> (not b) | _ -> assert false)
        (M.find x env) (M.find y env)
  | _ -> assert false

(* 部分的に冗長な分岐の除去
 *
 * b' -> bという経路を通った場合に、分岐の経路が確定している場合は、
 * 分岐を含むブロックをスキップする。
 * 
 * 到達する定義を計算して、その定義がすべて定数である場合に,
 * 分岐が確定できるかチェックする。
 *
 * これによって、定数回のループなどはwhile-doからrepeat-untilループに変換される。
 *)
let skip_if reach cfg =
  G.fold_edge
    (fun b b' _ cfg ->
       match partition_last (G.find b' cfg) with
         | ({ exp = (IfEq _ | IfLt _ | IfFEq _ | IfFLt _) } as branch, stmts) ->
             (* b -> b'の経路を通ったとき, b'の末尾に到達する定義文の集合を計算する. *)
             let set = Reach.reach_of b reach in
             let () =
               Reach.transfer_of_block reach set b;
               Reach.transfer_of_block reach set b'
             in
             (* env: 変数 -> その変数を定義する定義文の式のリスト *)
             let env = List.fold_right (fun x -> M.add x (Reach.def_of reach x set)) branch.use M.empty in
             let env = M.map (List.map (fun stmt -> stmt.exp)) env in
               (try
                  (* 同じブロックの場合は簡単のため無視する． *)
                  if b = b' then raise Impossible;
                  (* 到達する定義文がすべて定数か. *)
                  if not (M.for_all (fun _ defs -> List.for_all (fun exp -> is_constant exp) defs) env) then raise Impossible;
                  match (infer true env branch.exp, infer false env branch.exp) with
                    | is_then, is_else when is_then <> is_else ->
                        let t = G.succ_of b' (if is_then then Then else Else) cfg in
                        (* 最後の分岐をJmpに変更してb'をb''にコピー *)
                        let (b'', cfg) = G.copy_block b' cfg in
                        let cfg = G.update_block b'' (snd (partition_last (G.find b'' cfg)) @ [gen_stmt [] Jmp]) cfg in
                        (* 確定した方向 = tへ分岐を確定させる *)
                        let cfg = G.add_edge b'' t Other cfg in
                        (* 新しい経路b''へbの行き先を変更. *)
                        let cfg = G.add_edge b b'' (G.tag_of_edge b b' cfg) (G.remove_edge b b' cfg) in
                          cfg
                    | true, true -> assert false
                    | _ -> raise Impossible
                with
                  Impossible -> cfg)
         | _ -> cfg)
    cfg cfg

(* 到達定義解析の結果を用いて，プログラムがstrictであるか調べる. *)
let check_strictness reach cfg =
  G.iter
    (fun b stmts ->
       let set = Reach.reach_of b reach in
         List.iter
           (fun stmt ->
              List.iter
                (fun x ->
                   if Reach.def_of reach x set = [] then
                     Format.eprintf "WARNING: %s can be undefined value in %s.@." x (string_of_stmt stmt)(*; failwith "This program is not strict."*))
                stmt.use;
              Reach.transfer reach set stmt)
           stmts)
    cfg

let g cfg =
  let reach = Reach.create cfg in

  check_strictness reach cfg;

  Time.start "Constant folding";
  let cfg = constfold reach cfg in
  Time.stop "Constant folding";
  Time.start "Skipping if";
  let cfg = skip_if reach cfg in
  Time.stop "Skipping if";

    cfg

let f prog = map_cfg (fun l cfg -> g cfg) prog

let skip_if prog = map_cfg (fun l cfg -> skip_if (Reach.create cfg) cfg) prog



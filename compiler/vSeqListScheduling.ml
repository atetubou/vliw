(* vSeqのためのリストスケジューリング *)

open Vliw
open VSeq
open VListScheduling

let f seq =
  Time.start "# VSeq List Scheduling";
  let seq =
    G.fold
      (fun b stmts ({ all_cfg = cfg } as seq) ->
         try
           let stmts' = schedule_all ~stmtss:(stmts_before b cfg) stmts in
             { seq with all_cfg = G.update_block b stmts' cfg }
         with Impossible -> failwith "Failed because of list scheduling"
           (*
           (* 基本ブロック内でスケジューリングできない場合は, 後続ブロックを新たに作って遅延スロットを分配 *)
           let (stmts, branch, delay) = partition_block stmts in
           let cfg = G.update_block b (stmts @ [branch]) cfg in
             G.expand
               (fun b' e_tag ({ all_cfg = cfg } as seq) ->
                  let (b'', cfg) = G.add_block (List.map copy_stmt (delay @ [gen_stmt [] Jmp])) cfg in
                  let cfg =
                    G.add_edge b'' b' Other
                      (G.add_edge b b'' e_tag
                         (G.remove_edge b b' cfg))
                  in
                    (try
                       VProfile.add_prob b b'' (VProfile.prob_of b b');
                       VProfile.add_prob b'' b' 1.0;
                       VProfile.add_count b'' (VProfile.count_of_edge b b')
                     with Not_found -> ());
                    cfg)
               b cfg seq*))
      seq.all_cfg seq
(*

         G.map
           (fun b stmts ->
              schedule_all ~stmtss:(stmts_before b cfg) stmts)
           cfg)
 *)
  in
  Time.stop "# VSeq List Scheduling";
  seq


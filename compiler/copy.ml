(* コピー伝播 *)

open Block

let is_target = function
  | { def = [x, _]; exp = Mov _ | FMov (_, []) } -> true
  | _ -> false

(* コピー伝播させてもよいか.
 * Entry, Returnは疑似命令なので伝播させない. *)
let is_ok = function
  | { exp = Entry | Return } -> false
  | _ -> true

let g cfg =
  let avail = Avail.create ~is_target cfg in
  let updated = ref false in
  let cfg =
    G.map
      (fun b stmts ->
         let set = Avail.avail_of b avail in
           List.map
             (fun stmt ->
                let env =
                  List.fold_left
                    (fun env x ->
                       match Avail.def_of avail x set with
                         | Some (Mov (y) | FMov (y, [])) ->
                             (* xを定義する式が，xとyの値を変えないまま到達するとき *)
                             M.add x y env
                         | _ -> env)
                    M.empty stmt.use
                in
                (* stmtにコピー伝播させてもよいときのみ, 伝播させる. *)
                let env = if is_ok stmt then env else M.empty in
                  if not (M.is_empty env) then updated := true;
                  Avail.transfer avail set stmt;
                  replace_use_stmt env stmt)
             stmts)
      cfg
  in
    Format.eprintf "updated: %s@." (string_of_bool !updated);
    cfg

let f prog = map_cfg (fun l cfg -> g cfg) prog


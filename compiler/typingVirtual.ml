(* Virtual.tに対して, * ヒープ領域に割り当てられる配列は, ArrayCの型に変更する. *)

open Virtual


(* グローバル配列の型が変更されたときにtrueになる. *)
let global_updated = ref false

(* ArrayとArrayCであれば, ArrayCに変更する.
 * それ以外は左側の型を優先する. *)
let rec unify t1 t2 =
  match t1, t2 with
    | t1, t2 when t1 = t2 -> t1
    | Type.Fun(t1s, t1'), Type.Fun(t2s, t2') ->
        Type.Fun (List.map2 unify t1s t2s, unify t1' t2')
    | Type.Tuple(t1s), Type.Tuple(t2s) when List.length t1s = List.length t2s ->
        Type.Tuple (List.map2 unify t1s t2s)
    | Type.Array(t1), Type.Array(t2) ->
        Type.Array (unify t1 t2)
    | Type.ArrayC (t1), Type.ArrayC (t2) ->
        Type.ArrayC (unify t1 t2)
    | Type.ArrayC (t1), Type.Array (t2)
    | Type.Array (t1),  Type.ArrayC (t2)
      ->
        Type.ArrayC (unify t1 t2)
    | Type.List(t1), Type.List(t2) ->
        Type.List (unify t1 t2)
    | t, _ -> t

(* 部分式eの
 * 型, 環境, 部分式
 * を返す *)
let rec f env e =
  match e with
    | Unit ->
        (Type.Unit, env, Unit)
    | Int _ | Sub _ | ShiftL _ | ShiftR _ ->
        (Type.Int, env, e)
    | Float _ | FNeg _ | FAbs _ | FInv _ | FSqrt _
    | FAdd _ | FSub _ | FMul _ ->
        (Type.Float, env, e)
    | Add (x, y) ->
        (unify (M.find x env) (M.find y env), env, e)
    | IfEq (x, y, e1, e2) ->
        let (t1, env, e1) = f env e1 in
        let (t2, env, e2) = f env e2 in
          (unify t1 t2, env, IfEq (x, y, e1, e2))
    | IfLE (x, y, e1, e2) ->
        let (t1, env, e1) = f env e1 in
        let (t2, env, e2) = f env e2 in
          (unify t1 t2, env, IfLE (x, y, e1, e2))
    | Let ((x, t), e1, e2) ->
        let (t1, env, e1) = f env e1 in
        let t = unify t t1 in
        let t = if M.mem x env then unify t (M.find x env) else t in
        let env = M.add x t env in
        let (t2, env, e2) = f env e2 in
          (t2, env, Let ((x, t), e1, e2))
    | LetTuple (xts, y, e) ->
        let t = M.find y env in
        let ts =
          (match t with
             | Type.Tuple (ts) -> ts
             | _ -> assert false)
        in
        let xts = List.map2 (fun (x, t) t' -> (x, unify t t')) xts ts in
        let env = M.add_list xts env in
        let (t, env, e) = f env e in
          (t, env, LetTuple (xts, y, e))
    | Var (x) ->
        (M.find x env, env, e)
    | Call (("create_array" | "create_float_array"), [xn; xi]) as e ->
        (Type.ArrayC (M.find xi env), env, e)
    | Call ("float_of_int", _) ->
        (Type.Float, env, e)
    | Call ("int_of_float", _) ->
        (Type.Int, env, e)
    | Call (x, ys) when M.mem x env ->
        let (tx, ret) =
          (match M.find x env with
             | Type.Fun (ts, ret) ->
                 (Type.Fun (List.map2 unify ts (List.map (fun y -> M.find y env) ys), ret), ret)
             | _ -> assert false)
        in
        let env = M.add x tx env in
          (ret, env, e)
    (* 外部関数呼び出しの場合. *)
    | Call (x, ys) ->
        Format.eprintf "UNKNOWN %s@." x;
        (Type.gentyp (), env, e)
    | Tuple (xs) ->
        (Type.Tuple (List.map (fun x -> M.find x env) xs), env, e)
    | Get (x, y) ->
        let t =
          (match M.find x env, M.find y env with
             | (Type.Array (t) | Type.ArrayC (t)), Type.Int -> t
             | _ ->
                 Format.eprintf "failed %s : %s ,  %s : %s @."
                   x (Type.string_of (M.find x env))
                   y (Type.string_of (M.find y env));
                 assert false)
        in
          (t, env, e)
    | Put (x, y, z) ->
        let tx = M.find x env in
        let tz = M.find z env in
        let tx = unify tx (Type.Array (tz)) in
        let env =
          match tx with
            | Type.Array (tz) | Type.ArrayC (tz) ->
                M.add x tx (M.add z tz env)
            | _ -> assert false
        in
          (Type.Unit, env, e)
    | ExtArray (l) ->
        ((M.find l !Global.all).Global.typ, env, e)
    | Send (x) ->
        (Type.Unit, env, e)
    | Recv ->
        (Type.Int, env, e)
    | ItoF (x) ->
        (Type.Float, env, e)
    | FtoI (x) ->
        (Type.Int, env, e)
    | Store (x, y, _) ->
        (Type.Unit, env, e)
    | Load (t, x, _) -> (t, env, e)

(* envの型情報を用いてグローバル配列の型を変える
 * 本来ならば全てのエイリアスを求めてやるべきところだが,
 * carrayがグローバル配列に代入されるのはand_netしかないため, アドホックに実装する. *)
let change_for_global env prog =
  (* グローバル配列lの型をtyp'とunifyしてみる. *)
  let update l typ' =
    let { Global.typ = typ } as global = M.find l !Global.all in
    let typ' = unify typ typ' in
      if typ <> typ' then
        (global_updated := true;
         Format.eprintf "%s: %s to %s@." l (Type.string_of typ) (Type.string_of typ');
         Global.update l { global with Global.typ = typ' })
  in
  (* alias: 変数 -> その変数が指し示すグローバル配列 *)
  let rec f alias = function
    | Let ((x, t), ExtArray (l), e) ->
        update l (M.find x env);
        f (M.add x l alias) e
    | Put (x, y, z) when M.mem x alias ->
        update (M.find x alias) (Type.Array (M.find z env))
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) ->
        f alias e1;
        f alias e2
    | LetTuple (_, _, e) -> f alias e
    | _ -> ()
  in
    M.iter
      (fun _ func ->
         f M.empty func.body)
      prog

(* プログラム全体の型を局所的に変更. *)
let change_type f prog =
  let fxt = List.map (fun (x, t) -> (x, f t)) in
  let rec g = function
    | Let ((x, t), e1, e2) ->
        Let ((x, f t), g e1, g e2)
    | LetTuple (xts, y, e) ->
        LetTuple (fxt xts, y, g e)
    | IfEq (x, y, e1, e2) -> IfEq (x, y, g e1, g e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, g e1, g e2)
    | Load (t, x, i) -> Load (f t, x, i)
    | e -> e
  in
  let prog =
    M.map
      (fun func ->
         { func with
               body = g func.body;
               args = fxt func.args;
               typ = f func.typ })
      prog
  in
  let () =
    M.iter
      (fun x global ->
         Global.update x { global with Global.typ = f global.Global.typ })
      !Global.all
  in
    prog



let typing prog =
  (* BoolをIntに, Array型を型変数の内部にいれる. *)
  let prog =
    let rec f = function
      | Type.Bool        -> Type.Int
      | Type.Fun (ts, t) -> Type.Fun (List.map f ts, f t)
      | Type.Tuple (ts)  -> Type.Tuple (List.map f ts)
      | Type.Array (t)   -> Type.Array (f t)
                              (*
      (* 配列の場合は, 後から書き換えができるように型変数にしておく. *)
      | Type.Array (t)   -> Type.Var (ref (Some (Type.Array (f t))))
                               *)
      | Type.List (t)    -> Type.List (f t)
      (* typing.mlで削除されているはず. *)
      | Type.Ref _       -> assert false
      (* 以下の2つはまだ現れないはず. *)
      | Type.ArrayC _    -> assert false
      | Type.Var _       -> assert false
      | t                -> t
    in
      change_type f prog
  in
  let prog = OptVirtual.elim prog in
  let rec loop prog env =
    Format.eprintf "cardinal: %d@." (M.cardinal env);
    global_updated := false;
    let (prog', env') =
      M.fold
        (fun l func (prog, env) ->
           let (t, env, e) = f env func.body in
           let typ = match M.find l env with Type.Fun (ts, t') -> Type.Fun (ts, unify t' t) | _ -> assert false in
           let typ = unify typ func.typ in
           let args = List.map (fun (x, t) -> (x, unify t (M.find x env))) func.args in
           let args =
             match typ with
               | Type.Fun (ts, _) ->
                   List.map2 (fun t (x, t') -> (x, unify t t')) ts args
               | _ -> assert false
           in
           let func = { func with body = e; typ; args } in
           let prog = M.add l func prog in
           let env = M.add l func.typ (M.add_list func.args env) in
             (prog, env))
        prog (prog, env)
    in
      change_for_global env' prog';
      if M.equal (fun func func' -> func = func') prog prog' && not (!global_updated) then
        prog'
      else
        loop prog' env'
  in
  let env =
    M.fold
      (fun l func env ->
         let env = M.add l func.typ (M.add_list func.args env) in
           (match func.typ with
              | Type.Fun (ts, t) ->
                  (List.iter2 (fun t (_, t') -> assert (t = t')) ts func.args);
              | _ -> assert false);
           env)
      prog M.empty
  in
  let prog = loop prog env in
    M.iter
      (fun l ({ Global.typ = typ } as global) ->
         (* ArrayC型になっていたらキャッシュに配置する. *)
         match typ with
           | Type.ArrayC _ ->
               Global.update l { global with Global.in_cache = true }
           | _ -> ())
      !Global.all;
   prog


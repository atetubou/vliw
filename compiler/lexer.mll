{
  open Parser
  open Type
  open Common
  let store_newline lexbuf = accum_line := Lexing.lexeme_end lexbuf :: !accum_line
}

let space = [' ' '\t' '\r']
let newline = ['\n']
let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']

rule token = parse
  | space+ { token lexbuf }
  | newline { store_newline lexbuf; token lexbuf}
  | "(*" { comment lexbuf; token lexbuf }
  | '(' { LPAREN }
  | ')' { RPAREN }
  | "true" { BOOL true }
  | "false" { BOOL false }
  | "not" { NOT }
  | "fun" { FUN }
  | "if" { IF }
  | "then" { THEN }
  | "else" { ELSE }
  | "let" { LET }
  | "in" { IN }
  | "rec" { REC }
  | "match" { MATCH }
  | "with" { WITH }
  | "|" { BAR }
  | "ref" { REF }
  | ":=" { COLONEQ }
  | "!" { DEREF }
  | "or" { OR }
  | "&&" { AND }
  | "land" { LAND }
  | "lsl" { LSL }
  | "lsr" { LSR }
  | "->" { ARROW }
  | digit+ as n { INT (int_of_string n) }
  | digit+ ('.' digit*)? (['e' 'E'] ['+' '-']? digit+)? as f { FLOAT (float_of_string f) }
  | "-." { MINUS_DOT }
  | "+." { PLUS_DOT }
  | "*." { AST_DOT }
  | "/." { SLASH_DOT }
  | '-' { MINUS }
  | '+' { PLUS }
  | '*' { AST }
  | '/' { SLASH }
  | "==" | "=" { EQUAL }
  | "<>" { LESS_GREATER }
  | "<=" { LESS_EQUAL }
  | ">=" { GREATER_EQUAL }
  | '<' { LESS }
  | '>' { GREATER }
  | ',' { COMMA }
  | "[|" { LBRABAR }
  | "|]" { RBRABAR }
  | "**" { ASTAST }
  | "[]" { NIL }
  | "::" { CONS }
  | '_' { IDENT(Id.gentmp Type.Unit) }
  | "Array.create" | "Array.make" { ARRAY_CREATE }
  | "Array.allocate" { ARRAY_ALLOCATE }
  | "Array.init" { ARRAY_INIT }
  | "List.hd" { LIST_HD }
  | "List.tl" { LIST_TL }
  | '.' { DOT }
  | "<-" { LESS_MINUS }
  | ';' { SEMICOLON }
  | eof { EOF }
  | lower (digit|lower|upper|'_')* as s { IDENT s }
  | _
      {
        let (line, col) = num2pos (Lexing.lexeme_start lexbuf) in
          failwith
            (Printf.sprintf "unknown token %s in line %d, column %d."
              (Lexing.lexeme lexbuf) line col)
      }
and comment = parse
  | "*)" { () }
  | "(*" { comment lexbuf; comment lexbuf }
  | newline { store_newline lexbuf; comment lexbuf}
  | eof { Printf.printf "Warning: Unterminated comment.\n" }
  | _ { comment lexbuf }

open KNormal

let memi x env =
  try (match M.find x env with Int(_) -> true | _ -> false)
  with Not_found -> false
let memf x env =
  try (match M.find x env with Float(_) -> true | _ -> false)
  with Not_found -> false
let memt x env =
  try (match M.find x env with Tuple(_) -> true | _ -> false)
  with Not_found -> false

let mem_simple_if x env =
  M.mem x env && (match M.find x env with IfEq (x, y, Int _, Int _) | IfLE (x, y, Int _, Int _) -> true | _ -> false)

(* 結果の片方がIntであるような分岐であるか. *)
let mem_if x env =
  M.mem x env &&
  (match M.find x env with
     | IfEq (_, _, Int _, _) | IfEq (_, _, _, Int _) -> true
     |  _ -> false)

let findi x env = (match M.find x env with Int(i) -> i | _ -> raise Not_found)
let findf x env = (match M.find x env with Float(d) -> d | _ -> raise Not_found)
let findt x env = (match M.find x env with Tuple(ys) -> ys | _ -> raise Not_found)

let rec g env = function
  | Var(x) when memi x env -> Int(findi x env)
  | Var(x) when memf x env -> Float(findf x env)
  | Var(x) when memt x env -> Tuple(findt x env)
  | Add(x, y) when memi x env && memi y env -> Int(findi x env + findi y env)
  | Add(x, y) when memi x env && M.find x env = Int(0) -> Var y
  | Add(x, y) when memi y env && M.find y env = Int(0) -> Var x
  | Sub(x, y) when memi x env && memi y env -> Int(findi x env - findi y env)
  | Sub(x, y) when memi y env && M.find y env = Int(0) -> Var (x)
  | ShiftL(x, i) when memi x env -> Int((findi x env) lsl i)
  | ShiftR(x, i) when memi x env -> Int((findi x env) asr i)
  | FNeg(x) when memf x env -> Float(-.(findf x env))
  | FAbs(x) when memf x env -> Float(abs_float (findf x env))
  | FAdd(x, y) when memf x env && memf y env -> Float(findf x env +. findf y env)
  | FAdd(x, y) when memf x env && M.find x env = Float (0.) -> Var(y)
  | FAdd(x, y) when memf y env && M.find y env = Float (0.) -> Var(x)
  | FSub(x, y) when memf x env && memf y env -> Float(findf x env -. findf y env)
  | FSub(x, y) when memf x env && M.find x env = Float (0.) -> FNeg(y)
  | FSub(x, y) when memf y env && M.find y env = Float (0.) -> Var(x)
  | FMul(x, y) when memf x env && memf y env -> Float(findf x env *. findf y env)
  | FMul(x, y) when memf x env && M.find x env = Float (0.) -> Float (0.)
  | FMul(x, y) when memf y env && M.find y env = Float (0.) -> Float (0.)
  | FMul(x, y) when memf x env && M.find x env = Float (1.) -> Var (y)
  | FMul(x, y) when memf y env && M.find y env = Float (1.) -> Var (x)
  | FMul(x, y) when memf x env && M.find x env = Float (-1.) -> FNeg (y)
  | FMul(x, y) when memf y env && M.find y env = Float (-1.) -> FNeg (x)
  (*  | FMul(x, y) when memf x env && M.find x env = Float (2.) -> FAdd (y, y)
                                                                  | FMul(x, y) when memf y env && M.find y env = Float (2.) -> FAdd (x, x) *)
  | FInv(x) when memf x env ->
      if findf x env = 0.0 then
        (Printf.eprintf "WARNING: zero division occurred while folding constans. (%s)\n" x; Float (0.0))
      else
        Float (1.0 /. findf x env)
  | IfEq(x, y, e1, e2) when memi x env && memi y env -> if findi x env = findi y env then g env e1 else g env e2
  | IfEq(x, y, e1, e2) when memf x env && memf y env -> if findf x env = findf y env then g env e1 else g env e2
  | IfEq(x, y, e1, e2) when e1 = e2 -> g env e1
  | IfLE(x, y, e1, e2) when memi x env && memi y env -> if findi x env <= findi y env then g env e1 else g env e2
  | IfLE(x, y, e1, e2) when memf x env && memf y env -> if findf x env <= findf y env then g env e1 else g env e2
  | IfLE(x, y, e1, e2) when e1 = e2 -> g env e1

  (* ifの定数畳み込みは、IfEqだけで十分。他は効果なし。 *)
  (* boolまわりの定数畳み込みをする *)
  | IfEq (x, y, e1, e2) when mem_simple_if x env && memi y env ->
      let i = findi y env in
        (match M.find x env with
          | IfEq (z, w, Int i1, Int i2) when i1 = i && i2 = i -> g env e1
          | IfEq (z, w, Int i1, Int i2) when i1 = i           -> IfEq (z, w, g env e1, g env e2)
          | IfEq (z, w, Int i1, Int i2) when           i2 = i -> IfEq (z, w, g env e2, g env e1)
          | IfEq (z, w, Int i1, Int i2)                       -> g env e2
          | IfLE (z, w, Int i1, Int i2) when i1 = i && i2 = i -> g env e1
          | IfLE (z, w, Int i1, Int i2) when i1 = i           -> IfLE (z, w, g env e1, g env e2)
          | IfLE (z, w, Int i1, Int i2) when           i2 = i -> IfLE (z, w, g env e2, g env e1)
          | IfLE (z, w, Int i1, Int i2)                       -> g env e2
          | _ -> assert false)
  | IfEq (y, x, e1, e2) when mem_simple_if x env && memi y env ->
      let i = findi y env in
        (match M.find x env with
          | IfEq (z, w, Int i1, Int i2) when i1 = i && i2 = i -> g env e1
          | IfEq (z, w, Int i1, Int i2) when i1 = i           -> IfEq (z, w, g env e1, g env e2)
          | IfEq (z, w, Int i1, Int i2) when           i2 = i -> IfEq (z, w, g env e2, g env e1)
          | IfEq (z, w, Int i1, Int i2)                       -> g env e2
          | IfLE (z, w, Int i1, Int i2) when i1 = i && i2 = i -> g env e1
          | IfLE (z, w, Int i1, Int i2) when i1 = i           -> IfLE (z, w, g env e1, g env e2)
          | IfLE (z, w, Int i1, Int i2) when           i2 = i -> IfLE (z, w, g env e2, g env e1)
          | IfLE (z, w, Int i1, Int i2)                       -> g env e2
          | _ -> assert false)
  | IfLE (x, y, e1, e2) when mem_simple_if x env && memi y env ->
      let i = findi y env in
        (match M.find x env with
          | IfEq (z, w, Int i1, Int i2) when i1 <= i && i2 <= i -> g env e1
          | IfEq (z, w, Int i1, Int i2) when i1 <= i            -> IfEq (z, w, g env e1, g env e2)
          | IfEq (z, w, Int i1, Int i2) when            i2 <= i -> IfEq (z, w, g env e2, g env e1)
          | IfEq (z, w, Int i1, Int i2)                         -> g env e2
          | IfLE (z, w, Int i1, Int i2) when i1 <= i && i2 <= i -> g env e1
          | IfLE (z, w, Int i1, Int i2) when i1 <= i            -> IfLE (z, w, g env e1, g env e2)
          | IfLE (z, w, Int i1, Int i2) when            i2 <= i -> IfLE (z, w, g env e2, g env e1)
          | IfLE (z, w, Int i1, Int i2)                         -> g env e2
          | _ -> assert false)
  | IfLE (y, x, e1, e2) when mem_simple_if x env && memi y env ->
      let i = findi y env in
        (match M.find x env with
          | IfEq (z, w, Int i1, Int i2) when i <= i1 && i <= i2 -> g env e1
          | IfEq (z, w, Int i1, Int i2) when i <= i1            -> IfEq (z, w, g env e1, g env e2)
          | IfEq (z, w, Int i1, Int i2) when            i <= i2 -> IfEq (z, w, g env e2, g env e1)
          | IfEq (z, w, Int i1, Int i2)                         -> g env e2
          | IfLE (z, w, Int i1, Int i2) when i <= i1 && i <= i2 -> g env e1
          | IfLE (z, w, Int i1, Int i2) when i <= i1            -> IfLE (z, w, g env e1, g env e2)
          | IfLE (z, w, Int i1, Int i2) when            i <= i2 -> IfLE (z, w, g env e2, g env e1)
          | IfLE (z, w, Int i1, Int i2)                         -> g env e2
          | _ -> assert false)

  (* 局所的に計算させるようにする。CSEと相反するかも。 *)
  | IfEq (x, y, Var z, e2) when mem_simple_if z env -> IfEq (x, y, M.find z env, g env e2)
  | IfEq (x, y, e1, Var z) when mem_simple_if z env -> IfEq (x, y, g env e1, M.find z env)
  | IfLE (x, y, Var z, e2) when mem_simple_if z env -> IfLE (x, y, M.find z env, g env e2)
  | IfLE (x, y, e1, Var z) when mem_simple_if z env -> IfLE (x, y, g env e1, M.find z env)

  | IfEq(x, y, e1, e2) -> IfEq(x, y, g env e1, g env e2)
  | IfLE(x, y, e1, e2) -> IfLE(x, y, g env e1, g env e2)

  | Let((x, t), e1, e2) ->
      let e1' = g env e1 in
      let e2' = g (M.add x e1' env) e2 in
      Let((x, t), e1', e2')
  | LetRec({ name = xt; args = yts; body = e1 }, e2) ->
      (* Constant Localization: 定数の自由変数は、Letを挿入して局所化する。 *)
      let fvs = S.diff (fv e1) (S.of_list (List.map fst (xt :: yts))) in
      let (k, rename) =
        S.fold (fun x (k, rename) ->
                  match x with
                    | x when memi x env ->
                        let x' = Id.genid x in
                          ((fun e -> k (Let((x', Type.Int), M.find x env, e))), M.add x x' rename)
                    | x when memf x env ->
                        let x' = Id.genid x in
                          ((fun e -> k (Let((x', Type.Float), M.find x env, e))), M.add x x' rename)
                    | _ -> (k, rename))
          fvs ((fun e -> e), M.empty)
      in
        LetRec({ name = xt; args = yts; body = g env (k (Alpha.h rename e1)) }, g env e2)
  | LetTuple(xts, y, e) when memt y env -> (* タプル展開 *)
      List.fold_left2
	(fun e' xt z -> Let(xt, Var(z), e'))
	(g env e)
	xts
	(findt y env)
  | LetTuple(xts, y, e) -> LetTuple(xts, y, g env e)
  | ExtFunApp ("sqrt", [x]) when memf x env -> Float (sqrt (findf x env))
  | ExtFunApp ("float_of_int", [x]) when memi x env -> Float (float_of_int (findi x env))
  | ExtFunApp ("int_of_float", [x]) when memf x env -> Int (int_of_float (findf x env))
  | e -> e

let rec h env = function (* env: 型環境 *)
  | IfEq (x, y, e1, e2) -> IfEq (x, y, h env (Beta.f (Alpha.f (Let ((x, M.find x env), Var (y), e1)))), h env e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, h env e1, h env e2)
  | Let ((x, t), e1, e2) -> Let ((x, t), h env e1, h (M.add x t env) e2)
  | LetRec ({ name = (x, t); args = yts; body = e1 } as fundef, e2) ->
      let env = M.add x t env in
      let env = List.fold_left (fun env (y, t) -> M.add y t env) env yts in
        LetRec ({ fundef with body = h env e1 }, h env e2)
  | LetTuple (xts, y, e) ->
      let env = List.fold_left (fun env (x, t) -> M.add x t env) env xts in
        LetTuple (xts, y, h env e)
  | e -> e

let swap_branch e =
  Format.eprintf "SWAP BRANCH@.";
  let rec f env = function
    (* 分岐の交換 *)
    | IfEq (x, y, e1, e2) when mem_if x env && memi y env && findi y env = 0 ->
        let i = findi y env in
        let e1 = f env e1 in
        let e2 = f env e2 in
        Format.eprintf "FOUND %s = %s %d %d %d@." x y (size e1) (size e2) (size e);
          (match M.find x env with
(*             | IfEq (z, w, Int (j), e) when i = j -> IfEq (z, w, Alpha.f e1, IfEq (x, y, e1, e2))
             | IfEq (z, w, e, Int (j)) when i = j -> IfEq (z, w, IfEq (x, y, e1, e2), Alpha.f e1)*)
             | IfEq (z, w, Int (j), e) when i <> j -> IfEq (z, w, Alpha.f e2, IfEq (x, y, e1, e2))
             | IfEq (z, w, e, Int (j)) when i <> j -> IfEq (z, w, IfEq (x, y, e1, e2), Alpha.f e2)
             | _ -> IfEq (x, y, e1, e2))
    | IfEq (x, y, e1, e2) ->
        if x = "Ti.4571" then
          Format.eprintf "FAILED %s %s %d@." x y (findi y env);
        IfEq (x, y, f env e1, f env e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f env e1, f env e2)
    | Let ((x, t), e1, e2) ->
        let e1 = f env e1 in
        let e2 = f (M.add x e1 env) e2 in
          Let((x, t), e1, e2)
    | LetRec ({ body = e1 } as fundef, e2) ->
        LetRec ({ fundef with body = f env e1 }, f env e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f env e)
    | e -> e
  in

(*ignore (KNormal.output "before.out" e);*)
  let e =
     f M.empty e
  in
(*ignore (KNormal.output "after.out" e);*)
  e

  

let f e = g M.empty ((*h M.empty*) e)

(* グローバル配列を管理する. *)

open KNormal

type t = {
  (* 配列の大きさ *)
  length : int;
  (* 型 *)
  typ : Type.t;
  (* グローバル配列の配置先. キャッシュ上であればtrue *)
  in_cache : bool;
}

(* グローバル配列の情報 *)
let all = ref M.empty

(* グローバル配列を削除する *)
let remove x = assert (M.mem x !all); all := M.remove x !all

(* グローバル配列の情報を更新 *)
let update x t = assert (M.mem x !all); all := M.add x t !all

(* グローバル配列の合計領域サイズ *)
let size () = M.fold (fun _ { length = i } acc -> i + acc) !all 0

(* キャッシュ上にのせる *)
let move_to_cache x =
  assert (M.mem x !all);
  let old = M.find x !all in
    all := M.add x { old with in_cache = true } !all

(* 型tのグローバル配列を追加 *)
let allocate x n t =
  all := M.add x { length = n; typ = t; in_cache = false } !all

(* リストに静的領域変数がある場合は、Letを挿入して変換する *)
let insert_lets ys h =
  let (k, ys') =
    List.fold_right
      (fun y (k, ys') ->
         if M.mem y !all then
           let t = (M.find y !all).typ in
           let y' = Id.genid y in
             ((fun e -> Let ((y', t), ExtArray (y), k e)), y' :: ys')
         else
           (k, y :: ys'))
      ys ((fun e -> e), [])
  in
    h k ys' (* hは変換された引数のリストと、継続kを受け取る *)

let init_array_of = function
  | Type.Float -> "init_float_array"
  | _ -> "init_array"

let insert e typ f =
  let x = Id.gentmp typ in
    Let ((x, typ), e, f x)

(* グローバル配列を初期化 *)
let rec initialize_directly global init n e =
  insert (ExtArray (global)) (M.find global !all).typ
    (fun pos ->
       let rec loop n =
         if n < 0 then e
         else
           insert (Int (n)) Type.Int
             (fun index ->
                insert (Put (pos, index, init)) Type.Unit
                  (fun _ -> loop (n-1)))
       in
         loop (n-1))


(* グローバル配列を初期化する関数を挿入 *)
let rec initialize global init n e =
  let func = Id.genid "initializer" in
  let cnt = Id.genid "cnt" in
  let body =
  insert (ExtArray (global)) (M.find global !all).typ
    (fun pos ->
    insert (Int (-1)) Type.Int
      (fun minus ->
    IfLE (cnt, minus, Unit,
          insert (Put (pos, cnt, init)) Type.Unit
            (fun _ ->
         insert (Int (1)) Type.Int
           (fun one ->
              insert (Sub (cnt, one)) Type.Int
                (fun cnt ->
                   App (func, [cnt])))))))
  in
    LetRec ({ name = (func, Type.Fun ([Type.Int], Type.Unit)); args = [cnt, Type.Int]; body = body },
            insert (Int (n-1)) Type.Int
              (fun n ->
                 insert (App (func, [n])) Type.Unit
                   (fun _ -> e)))

(* 静的領域変数の収集と、その静的領域変数の参照を変更 *)
let rec g env toplevel = function
  | Let ((x, (Type.Array ti as t)), ExtFunApp (("create_array" | "create_float_array"), [xn; xi]), e2)
      when M.mem xn env && toplevel ->
      let n = M.find xn env in
        allocate x n t;
        if n > 0 then
          g env true (
            if n <= 5 then
              (* 配列の個数が小さいときは直接初期化する *)
              initialize_directly x xi n e2
            else
              initialize x xi n e2
          )
        else
          g env true e2
  | Let ((x, (Type.Array ti as t)), ExtFunApp ("allocate", [xn]), e2)
      when M.mem xn env && toplevel ->
      let n = M.find xn env in
        allocate x n t;
        g env true e2
  | Let ((x, t), Int (i), e2) when toplevel ->
      let env = M.add x i env in
        Let ((x, t), Int (i), g env true e2)
  | Let ((x, t), e1, e2) -> Let ((x, t), g env toplevel e1, g env toplevel e2)
  | IfEq (x, y, e1, e2) -> IfEq (x, y, g env toplevel e1, g env toplevel e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, g env toplevel e1, g env toplevel e2)
  | LetRec ({ name = (x, t); args = yts; body = e1 }, e2) ->
      LetRec ({ name = (x, t); args = yts; body = g env false e1 }, g env toplevel e2)
  | LetTuple (xs, y, e1) -> LetTuple (xs, y, g env toplevel e1)
  | Var x when M.mem x !all ->
      (* クロージャが生成されないように, 外部配列として参照する *)
      ExtArray x
  | App (x, ys) when List.exists (fun y -> M.mem y !all) ys ->
      (* 引数にグローバル変数が使われているときは、ラベルから参照する。 *)
      insert_lets ys (fun k ys -> k (App (x, ys)))
  | ExtFunApp (x, ys) when List.exists (fun y -> M.mem y !all) ys ->
      insert_lets ys (fun k ys -> k (ExtFunApp (x, ys)))
  | Tuple ys when List.exists (fun y -> M.mem y !all) ys ->
      insert_lets ys (fun k ys -> k (Tuple ys))
  | Get (x, y) when List.exists (fun y -> M.mem y !all) [x; y] ->
      insert_lets [x; y] (fun k -> function [x; y] -> k (Get (x, y)) | _ -> assert false)
  | Put (x, y, z) when List.exists (fun y -> M.mem y !all) [x; y; z] ->
      insert_lets [x; y; z] (fun k -> function [x; y; z] -> k (Put (x, y, z)) | _ -> assert false)
  | e -> e

let f e =
  (* 整数定数が集めやすいように定数畳み込みをしておく. *)
  let e = ConstFold.f e in
    g M.empty true e


%{
  open Syntax
  let addtyp x = (x, Type.gentyp ())
  type tuple_pat =
      | TuplePat of tuple_pat list | TupleVar of (Id.t * Type.t)
  let tuple_normalize tuple_pats e1 = (* ネストしたlet tupleの展開 *)
      let rec sub e tups =
          let (xts, k) =
              List.fold_right
          (fun tp (xts, k) ->
              match tp with
              | TupleVar xt -> (xt :: xts, k)
              | TuplePat tps ->
                      let (x, t) as xt = addtyp (Id.genid "TUPLE") in
                      let k' = sub (Var x) tps in
                      (xt :: xts, (fun e -> k (k' e))))
          tups ([], (fun e -> e))
          in
          (fun e' -> LetTuple (xts, e, k e'))
      in
       (* eを受け取りLetTuple ... in eというSyntax.tを返す関数. insertorを返す *)
      (sub e1 tuple_pats)

  let array_normalize xts e1 e2 =
      let (y, t) as yt = addtyp (Id.genid "ARRAY") in
      Let (
        yt, e1,
        fst (List.fold_left
              (fun (e, n) ((x, t) as xt) ->
                  (Let (xt, Get (Var y, Int n), e), n+1))
              (e2, 0) xts))

  let list_normalize xts e1 e2 =
      let ls = Id.genid "LIST" in
      let xts = List.rev xts in
      let (last, xts) = (List.hd xts, List.rev (List.tl xts)) in (* cut off the last one *)
      let (ls, k) =
          List.fold_left
          (fun (ls, k) xt ->
              let ls' = Id.genid "LIST" in
              (
                  ls',
                  (fun e -> k (Let (xt, Head (Var ls), Let (addtyp ls', Tail (Var ls), e))))
              )
          )
          (ls, (fun e -> Let (addtyp ls, e1, e))) xts
      in
      k (Let (last, Var ls, e2))

  let pos x =
    let spos = Parsing.symbol_start () in
    let epos = Parsing.symbol_end () in
    let (sline, scol) = Common.num2pos spos in
    let (eline, ecol) = Common.num2pos epos in
      PosHint (Common.Position(!Common.parsing_file, spos, sline, scol, epos, eline, ecol), x)
  
  let is_two_pow x = (x land -x = abs x)
  let rec log2 x = if x = 1 then 0 else 1 + log2 (x / 2)
  let syntax_mul (x, y) = App (Var "mul", [x; y]) 
  let syntax_div (x, y) = App (Var "div", [x; y]) 
  let to_shift op ng x y =
      if is_two_pow (abs y) then
          if y = 0 then
              Int 0
          else if y < 0 then
              Sub (Int 0, (op (x, log2 (abs y))))
          else
              op (x, log2 (abs y))
      else
          ng (x, Int y)
  let let_translate ({ name = (x, t); args = yts; body = e1 }, e2) =
    let x' = Id.genid x in
      LetRec ({ name = (x', t); args = yts; body = e1 }, rename x x' e2)
%}


%token <bool> BOOL
%token <int> INT
%token <float> FLOAT
%token NOT
%token MINUS
%token PLUS
%token MINUS_DOT
%token PLUS_DOT
%token AST
%token SLASH
%token AST_DOT
%token SLASH_DOT
%token EQUAL
%token LESS_GREATER
%token LESS_EQUAL
%token GREATER_EQUAL
%token LESS
%token GREATER
%token IF
%token THEN
%token ELSE
%token <Id.t> IDENT
%token LET
%token IN
%token REC
%token COMMA
%token ARRAY_CREATE
%token ARRAY_ALLOCATE
%token DOT
%token LESS_MINUS
%token SEMICOLON
%token LPAREN
%token RPAREN
%token EOF

%token FUN ARROW
%token OR AND LAND
%token MATCH WITH BAR
%token COLONEQ REF DEREF
%token LBRABAR RBRABAR /* [|, |] */
%token ARRAY_INIT
%token ASTAST
%token LSL LSR
%token CONS NIL LIST_HD LIST_TL

/* 優先順位とassociativityの定義（低い方から高い方へ） (caml2html: parser_prior) */
%nonassoc prec_min
%right FUN ARROW
%nonassoc prec_tuple
%right prec_let
%right SEMICOLON
%right prec_if
%right LESS_MINUS
%left COLONEQ
%left COMMA
%left OR AND
%left EQUAL LESS_GREATER LESS GREATER LESS_EQUAL GREATER_EQUAL
%left LAND LSL LSR
%right CONS
%left PLUS MINUS PLUS_DOT MINUS_DOT
%left AST SLASH AST_DOT SLASH_DOT
%right ASTAST
%right NOT
%right prec_unary_minus
%left prec_app
%left DOT
%nonassoc  DEREF
%nonassoc BOOL INT FLOAT IDENT LPAREN RPAREN NIL

/* 開始記号の定義 */
%type <Syntax.t> main
%start main

%%

main:
  | toplevels { $1 }
  | exp { $1 }

toplevels:
  | LET IDENT EQUAL exp toplevels { pos (Let (addtyp $2, $4, $5)) }
  | LET fundef toplevels %prec prec_let { pos (let_translate ($2, $3)) }
  | LET REC fundef toplevels %prec prec_let { pos (LetRec ($3, $4)) }
  | EOF { Unit }

simple_exp:
  | LPAREN exp RPAREN { $2 }
  | LPAREN RPAREN { Unit }
  | BOOL { Bool($1) }
  | INT { Int($1) }
  | FLOAT { Float($1) }
  | IDENT { Var($1) }
  | simple_exp DOT LPAREN exp RPAREN { Get($1, $4) }
  | DEREF simple_exp { Deref $2 }
  | NIL { Nil (Type.List (Type.gentyp ())) }

exp:
  | simple_exp %prec prec_min { pos $1 }
  | NOT exp { pos (Not $2) }
  | MINUS exp %prec prec_unary_minus
      { pos (
          match delete_hint $2 with
            | Float(f) ->  Float(-.f)
            | _ -> Sub(Int 0, $2)
        ) }
  | exp CONS exp { pos (Cons ($1, $3)) }
  | exp PLUS exp { pos (Add ($1, $3)) }
  | exp MINUS exp { pos (Sub ($1, $3)) }
  | exp AST exp
      { pos (
         match delete_hint $3 with
           | Int(d) -> to_shift (fun (x, y) -> ShiftL (x, y)) syntax_mul $1 d
           | _ -> syntax_mul ($1, $3) ) }
  | exp ASTAST exp {
      (* Note: x ** y is only allowed of x > 0 *)
      pos ( App (Var "exp", [FMul ($3, App (Var "log", [$1]))]) ) }
  | exp SLASH exp
      { pos (
          match delete_hint $3 with
            | Int(d) -> to_shift (fun (x, y) -> ShiftR (x, y)) syntax_div $1 d
            (* TODO: this is not true (e.g. -10 >> 2 = -3) *)
            | _ -> syntax_div($1, $3) ) }
  | exp LAND INT {
      if $3 <> 1 then failwith "'land' is only allowed for 'land 1'" else
      let (x, t) as xt = addtyp (Id.genid "LAND") in
        pos (Let (xt, $1, 
          Sub (Var x, ShiftL (ShiftR (Var x, 1), 1)))) }
  | exp LSL INT { pos (ShiftL ($1, $3)) }
  | exp LSR INT { pos (ShiftR ($1, $3)) }
  | exp OR exp { pos (If ($1, Bool true, $3)) }
  | exp AND exp { pos (If ($1, $3, Bool false)) }
  | exp EQUAL exp { pos (Eq ($1, $3)) }
  | exp LESS_GREATER exp { pos (Not (Eq ($1, $3))) }
  | exp LESS exp { pos (Not (LE ($3, $1))) }
  | exp GREATER exp { pos (Not (LE ($1, $3))) }
  | exp LESS_EQUAL exp { pos (LE ($1, $3)) }
  | exp GREATER_EQUAL exp { pos (LE ($3, $1)) }
  | exp COLONEQ exp { pos (Subst ($1, $3)) }
  | IF exp THEN exp ELSE exp %prec prec_if { pos (If ($2, $4, $6)) }
  | MINUS_DOT exp %prec prec_unary_minus { pos (FNeg ($2)) }
  | exp PLUS_DOT exp { pos (FAdd ($1, $3)) }
  | exp MINUS_DOT exp { pos (FSub ($1, $3)) }
  | exp AST_DOT exp { pos (FMul ($1, $3)) }
  | exp SLASH_DOT exp { pos (FDiv ($1, $3)) }
  | LET IDENT EQUAL exp IN exp %prec prec_let { pos (Let (addtyp $2, $4, $6)) }
  | LET fundef IN exp %prec prec_let { pos (let_translate ($2, $4)) }
  | LET REC fundef IN exp %prec prec_let { pos (LetRec ($3, $5)) }
  | exp actual_args %prec prec_app { pos (App ($1, $2)) }
  | elems %prec prec_tuple { pos (Tuple ($1)) }
  | LET tuple_pat EQUAL exp IN exp %prec prec_let { pos ((tuple_normalize $2 $4) $6) }
  | LET LBRABAR array_pat RBRABAR EQUAL exp IN exp %prec prec_let { pos (array_normalize $3 $6 $8) }
  | LET list_pat EQUAL exp IN exp %prec prec_let { pos (list_normalize $2 $4 $6) }
  | simple_exp DOT LPAREN exp RPAREN LESS_MINUS exp { pos (Put ($1, $4, $7)) }
  | exp SEMICOLON exp { pos (Let ((Id.gentmp Type.Unit, Type.Unit), $1, $3)) }
  | ARRAY_CREATE simple_exp simple_exp %prec prec_app { pos (Array ($2, $3)) }
  | ARRAY_INIT simple_exp simple_exp %prec prec_app { pos (ArrayInit ($2, $3)) }
  | ARRAY_ALLOCATE simple_exp %prec prec_app { pos (ArrayAllocate ($2, Type.gentyp ())) }
  | LIST_HD simple_exp %prec prec_app { pos (Head $2) }
  | LIST_TL simple_exp %prec prec_app { pos (Tail $2) }
  | REF exp %prec prec_app { pos (Ref $2) }
  | MATCH exp WITH matching
      { pos (
          let (x, t) as xt = addtyp (Id.genid "MATCH") in
            Let (xt, $2, $4 (Var x))) }
  | FUN IDENT ARROW exp
      { pos (
          let x = Id.genid "FUN" in
            LetRec({ name = addtyp x; args = [addtyp $2]; body = $4 }, Var(x))) }
  | error
      {
        let (line, col) = Common.num2pos (Parsing.symbol_start ()) in
          failwith (Printf.sprintf "parse error in line %d, column %d." line col)
      }

fundef:
  | IDENT formal_args EQUAL exp {
      { name = addtyp $1;
        args = List.map fst $2;
        body = List.fold_left (fun e (_, k) -> k e) $4 $2 }
    }

matching:
  | INT ARROW exp BAR matching { (fun e -> If (Eq(e, Int $1), $3, $5 e)) }
  | IDENT ARROW exp { (fun e -> $3)  }

formal_args:
  | formal_pat formal_args { $1 :: $2 }
  | formal_pat { [$1] }

formal_pat:
  | LPAREN RPAREN { ((Id.gentmp Type.Unit, Type.Unit), (fun e -> e)) }
  | IDENT { (addtyp $1, (fun e -> e)) }
  | tuple_pat
      {
        let (x, t) as xt = addtyp (Id.genid "FORMAL") in
          (xt, tuple_normalize $1 (Var (x)))
      }

actual_args:
  | actual_args simple_exp %prec prec_min { $1 @ [$2] }
  | simple_exp %prec prec_min { [$1] }

elems:
  | elems COMMA exp { $1 @ [$3] }
  | exp COMMA exp { [$1; $3] }

tuple_pat:
  | tuple_elems  { $1 }
  | LPAREN tuple_elems RPAREN { $2 }
tuple_var:
  | IDENT { TupleVar (addtyp $1) }
  | LPAREN tuple_elems RPAREN { TuplePat $2 }
tuple_elems:
  | tuple_var COMMA tuple_var { [$1; $3] }
  | tuple_var COMMA tuple_elems { $1 :: $3 }

array_pat:
  | IDENT { [addtyp $1] }
  | IDENT SEMICOLON array_pat { addtyp $1 :: $3 }

list_pat:
  | IDENT CONS NIL { [addtyp $1; addtyp (Id.genid "NIL")] }
  | IDENT CONS IDENT { [addtyp $1; addtyp $3] }
  | IDENT CONS list_pat { addtyp $1 :: $3 }



(* flatten let-bindings (just for prettier printing) *)

open KNormal

let threshold = ref 5

let rec insert ((x, t) as xt) e' = function
  | Let (yt, e1, e2) -> Let (yt, e1, insert xt e' e2)
  | LetRec (fundefs, e) -> LetRec (fundefs, insert xt e' e)
  | LetTuple (yts, z, e) -> LetTuple (yts, z, insert xt e' e)
  | e -> Let (xt, e, e')

(* ifをまたいで展開する。同じ名前が出来うるので、alpha変換しておく。 *)
let rec expand ((x, t) as xt) e' = function
  | IfEq (y, z, e1, e2) -> IfEq (y, z, expand xt e' e1, Alpha.f (expand xt e' e2))
  | IfLE (y, z, e1, e2) -> IfLE (y, z, expand xt e' e1, Alpha.f (expand xt e' e2))
  | Let (yt, e1, e2) -> Let (yt, e1, expand xt e' e2)
  | LetRec (fundefs, e) -> LetRec (fundefs, expand xt e' e)
  | LetTuple (yts, z, e) -> LetTuple (yts, z, expand xt e' e)
  | e -> Let (xt, e, e')

(* ネストしたletの簡約 *)
let rec f = function
  | IfEq(x, y, e1, e2) -> IfEq(x, y, f e1, f e2)
  | IfLE(x, y, e1, e2) -> IfLE(x, y, f e1, f e2)
  (* タプルを定義するときは、タプルが展開されるようにifの中まで展開する *)
  | Let((x, Type.Tuple _ ) as xt, e1, e2) ->
      expand xt (f e2) (f e1)
  (* サイズが小さければifの末尾を展開する.
  | Let((x, t) as xt, e1, e2) when size e1 < !threshold ->
      expand xt (f e2) (f e1) *)
  | Let((x, t) as xt, e1, e2) ->
      insert xt (f e2) (f e1)
  | LetRec({ name = xt; args = yts; body = e1 }, e2) ->
      LetRec({ name = xt; args = yts; body = f e1 }, f e2)
  | LetTuple(xts, y, e) -> LetTuple(xts, y, f e)
  | e -> e

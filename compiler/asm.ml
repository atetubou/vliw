(* 機械語の基本的な情報 *)

(*** メモリ関連 ***)
(** cache上に配置されるもの **)
(* 定数テーブルの先頭アドレス *)
let const_table_top = 0

(* スタックの先頭アドレス *)
let stack_top = 128

(* キャッシュ上のグローバル配列の領域 *)
let cache_global_top = 1024

(* キャッシュ上のヒープ領域 *)
let cache_heap_top = 812

(** SRAM上に配置されるもの *)
(* グローバル配列の先頭アドレス *)
let global_top = 1024

(* ヒープの先頭アドレス *)
(* [XXX] キャッシュのヒープとSRAMのヒープのアドレスは同じにしておく. *)
let heap_top = cache_heap_top


(*** レジスタ関連 ***)

let regnum = 32
let fregnum = 32

(* リンクレジスタ = $r63 *)
(* スタックポインタ = $r62 *)
(* ヒープポインタ = $r61 *)
(* ゼロレジスタ = $r60 *)
(* argregs: 上記以外の（引数に割り当てられる）汎用レジスタ.
 * regs: すべての汎用レジスタ *)
(* TODO: リンクレジスタを$r0などとするとバグる *)
let (link, stack, heap, zero, argregs, regs) =
  let regs = Array.to_list (Array.init regnum (fun i -> Printf.sprintf "$r%d" i)) in
    match List.rev regs with
      | link :: stack :: heap :: zero :: argregs ->
          (link, stack, heap, zero, argregs, regs)
      | _ -> assert false

(* 浮動小数点レジスタ *)
let fregs = Array.to_list (Array.init fregnum (fun i -> Printf.sprintf "$f%d" i))

(* 引数に割り当てられる浮動小数点レジスタ *)
let argfregs = fregs

(* 変数がレジスタかどうか *)
let is_reg x = assert (String.length x >= 1); x.[0] = '$'

(* レジスタの型を得る *)
let reg_type_of x = if not (is_reg x) then Format.eprintf "%s is not register!@." x; assert (String.length x >= 2); if x.[1] = 'f' then Type.Float else Type.Int

(* 整数のビット幅 *)
let is_bits_of k i = -(1 lsl (k-1)) <= i && i < (1 lsl (k-1))

(* Add, Subなどの基本的な命令の即値幅に収まっているか
 * 15bit符号あり即値に収まるか調べる. *)
let fit_immediate = is_bits_of 15

(* 6bit即値に収まるか. 分岐命令の定数比較用. *)
let fit_small_immediate = is_bits_of 6


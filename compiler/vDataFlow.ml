open Vliw

module PQ =
  Set.Make
    (struct
       type t = int * G.vertex
       let compare = compare
     end)

(* データフロー解析の方向 *)
type direction = Forward | Backward

(* 文単位の伝播関数からブロック単位の伝播関数を求める.
 * creator: 集合のコンストラクタ *)
let calc_transfer_of_block ~direction ~transfer ~creator ~init cfg =
  G.fold
    (fun b stmts acc ->
       let this = creator init in
       let stmts =
         (* データフローの方向と同じ向きの順番でtransferを適用する. *)
         match direction with
           | Forward -> stmts
           | Backward -> List.rev stmts
       in
         List.iter (transfer this) stmts;
         M.add b this acc)
    cfg M.empty

(* 文単位の伝播関数~transferから, genとmask( = killの補集合)を計算する *)
let transfer_of_block ~direction ~transfer ~creator cfg =
  (calc_transfer_of_block ~init:false ~direction ~transfer ~creator cfg,
   calc_transfer_of_block ~init:true  ~direction ~transfer ~creator cfg)

let solve_forward ~update cfg =
  (* 更新する優先度を決める. *)
  let b2order =
    let order = snd (G.topological_sort cfg) in
    let order = order in
    let (_, acc) =
      List.fold_left
        (fun (i, acc) b -> (i+1, M.add b i acc))
        (0, M.empty) order
    in
      acc
  in
  let pq_add b pq = PQ.add (M.find b b2order, b) pq in
  let rec loop que =
    if PQ.is_empty que then ()
    else
      let (_, b) as pq = PQ.min_elt que in
      let que = PQ.remove pq que in
      (* bとデータフローの向きの逆方向の情報を使って更新 *)
      let updated = update b (G.pred b cfg) in
        if updated then
          (* bが更新された場合は, データフローの向きのブロックを更新しなおす. *)
          let que = List.fold_left (fun que b -> pq_add b que) que (G.succ b cfg) in
            loop que
        else loop que
  in
    loop (G.fold (fun b _ -> pq_add b) cfg PQ.empty)



let solve_backward ~update cfg =
  (* 更新する優先度を決める. *)
  let b2order =
    let order = snd (G.topological_sort cfg) in
    let order = List.rev order in
    let (_, acc) =
      List.fold_left
        (fun (i, acc) b -> (i+1, M.add b i acc))
        (0, M.empty) order
    in
      acc
  in
  let pq_add b pq = PQ.add (M.find b b2order, b) pq in
  let rec loop que =
    if PQ.is_empty que then ()
    else
      let (_, b) as pq = PQ.min_elt que in
      let que = PQ.remove pq que in
      (* bとデータフローの向きの逆方向の情報を使って更新 *)
      let updated = update b (G.succ b cfg) in
        if updated then
          (* bが更新された場合は, データフローの向きのブロックを更新しなおす. *)
          let que = List.fold_left (fun que b -> pq_add b que) que (G.pred b cfg) in
            loop que
        else loop que
  in
    loop (G.fold (fun b _ -> pq_add b) cfg PQ.empty)





let solution_of_block ~direction =
  match direction with
    | Forward -> solve_forward
    | Backward -> solve_backward


(* レジスタ彩色を行う.
 * プログラムがstrictであることを仮定する.
 * (そうでないと変数の型がdefから決定できない.)
 *
 * 事前条件: $stackに関する操作が存在しないこと.
 *)

open Vliw

(* 各関数に付随する情報 *)
type info = {
  (* Callのsid -> (Callの対象, 退避すべきレジスタ) *)
  calls : (Id.label * S.t) M.t;
  (* レジスタ -> そのレジスタへ割り当てると発生するコスト *)
  reg2cost : int M.t;
  (* 変更しうるレジスタ *)
  kill : S.t;
}

(* 関数呼び出し規約をargs, rets(レジスタと型の組のリスト)に決定する.
 * 関数の入口で今の引数(func.args)にargsから代入する式を追加.
 * func.argsのかわりにargsを入口生存とする. *)
let specify_calling_convention func args rets =
  (*
  Format.eprintf "args: %s <-> %s@."
    (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) func.args))
    (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) args))
    ;
  Format.eprintf "rets: %s <-> %s@."
    (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) func.rets))
    (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) rets))
    ;
   *)
  (* 関数呼び出し規約はレジスタに関する規約なはず. *)
  assert (List.for_all (fun (x, _) -> Asm.is_reg x) (args @ rets));
  let cfg = func.cfg in
  let env =
    List.fold_left2
      (fun acc (x, t) (y, t) ->
         (* レジスタの場合は変更されないはず. *)
         if Asm.is_reg x && x <> y then Format.eprintf "WHY? %s or %s@." x y;
         if Asm.is_reg x then assert (x = y);
         M.add x y acc)
      M.empty (func.args @ func.rets) (args @ rets)
  in
  (* 入口にfunc.argsに新しい引数argsから代入する文を追加.
   * 同時に入口生存なものをfunc.argsからargsに変更する. 
   * 出口に関しても同様. *)
  let cfg =
    G.map_stmt_list
      (function
         | { left = { exp = Entry } } as stmt ->
             replace_def_stmt env stmt ::
             move_list func.args (List.map fst args)
         | { right = { exp = Return } } as stmt ->
             move_list rets (List.map fst func.rets) @
             [replace_use_stmt env stmt]
         | stmt -> [stmt])
      cfg
  in
    { func with
          cfg = cfg;
          args = args;
          rets = rets; }

(* 関数呼び出し規約として, argregs, argfregsから引数, 返り値に割り当てていく. *)
let specify_normal_calling_convention func =
(*   let elim_global = List.filter (fun x -> not (List.mem x (List.map fst prog.global_variables))) in*)
   let take ls =
     (* レジスタ以外の変数があれば，レジスタの0番から割り当てていく *)
     let (_, _, ls) =
       List.fold_right
         (fun (x, t) (regs, fregs, acc) ->
            match (regs, fregs, t = Type.Float) with
              | _ when Asm.is_reg x ->
                  (regs, fregs, (x, t) :: acc)
              | (regs, x :: fregs, true) ->
                  (regs, fregs, (x, Type.Float) :: acc)
              | (x :: regs, fregs, false) ->
                  (regs, fregs, (x, Type.Int) :: acc)
              (* レジスタが足りないとき *)
              | _ -> raise (Failure ("insufficient registers")))
         ls ((*elim_global *)Asm.argregs, (*elim_global *)Asm.argfregs, [])
     in
       ls
   in
     specify_calling_convention
       func
       (take func.args)
       (take func.rets)

(* funcに含まれる複数のCallを同じ変数が引数/返り値になるように変換する.
 * 同時に, 関数呼び出し規約が決まっている場合は, その規約に従ってレジスタを引数/返り値にする. *)
let take_call_apart prog l func =
  (* 自分自身の関数呼び出し規約が決まっていない場合は, 適当に決定しておかなければならない. *)
  let func = specify_normal_calling_convention func in

  let prog = { prog with entries = M.add l func prog.entries } in
  let func =
    (* 関数の実引数/返り値として使う一時変数を生成.
     * ただし, 引数/返り値がレジスタである場合はそのレジスタを使う. *)
    let env =
      M.map
        (fun func ->
           let f = List.map (fun (x, t) -> ((if Asm.is_reg x then x else Id.genid x), t)) in
             (f func.args, f func.rets))
        prog.entries
    in
    let cfg =
      G.map_stmt_list
        (function
           | { left = { exp = Call (r, xs); use = use; def = def } } as stmt ->
               let (args, rets) = M.find r env in
               (* 引数を設定する. *)
               let init_stmt = move_list args xs in
               (* 結果を取り出す. *)
               let fin_stmt = move_list def (List.map fst rets) in
                 init_stmt @
                 [{ stmt with
                        left =
                          { stmt.left with
                                exp = Call (r, List.map fst args);
                                use = List.map fst args;
                                def = rets } }] @
                 fin_stmt
           | stmt -> [stmt])
        func.cfg
    in
      { func with cfg = cfg }
  in
    func


(* 関数呼び出しの前後で生きている変数を退避する *)
let save_and_restore env l func =
  let max_stack_size = ref func.stack_size in
  let info = M.find l env in
  let cfg = func.cfg in
  let cfg =
    G.map_stmt_list
      (function
         | { left = { exp = Call _; s_id = s_id } } as stmt ->
             assert (M.mem s_id info.calls);
             let (r, regs_to_save) = M.find s_id info.calls in
             (* 実際に破壊され得るレジスタのみを退避する. *)
             let regs_to_save = S.inter regs_to_save (M.find r env).kill in
             (* [XXX] リンクレジスタはともかく退避する *)
             let regs_to_save = if l = Id.main_label then regs_to_save else S.add Asm.link regs_to_save in
             let (stores, loads, new_stack_size) =
               S.fold
                 (fun x (stores, loads, i) ->
                    (gen_stmt []                     (VColoring.store_of l (Asm.reg_type_of x) i x) :: stores,
                     gen_stmt [x, Asm.reg_type_of x] (VColoring.load_of  l (Asm.reg_type_of x) i)  :: loads,
                     i + 1))
                 regs_to_save ([], [], func.stack_size)
             in
               Format.eprintf "Call(%s) saved %d@." r (new_stack_size - func.stack_size);
               max_stack_size := max !max_stack_size new_stack_size;
               stores @ [stmt] @ loads
         | stmt -> [stmt])
      cfg
  in
  (* 新たなスタックのサイズ *)
  let stack_size = !max_stack_size in
  (* スタックサイズが確定したので, スタックポインタの設定を行う. *)
  let cfg =
    if l = Id.main_label then
      (* ダミーのスタックポインタの設定の文を, 実際のスタックポインタの位置に設定しなおす. *)
      G.map_stmt
        (map_half
           (function
              | { exp = Set (0); def = [x, _] } as half when x = Asm.stack ->
                  { half with exp = Set (Asm.stack_top + stack_size) }
              | half -> half))
        cfg

    else
      (* メイン関数以外の場合は、関数の入口/出口でスタックを伸張/縮小する. *)
      G.map_stmt_list
        (function
           | { left = { exp = Entry } } as stmt when l <> Id.main_label && stack_size <> 0 ->
               [stmt; gen_stmt [Asm.stack, Type.Int] (Add (C ( stack_size), Asm.stack))]
           | { right = { exp = Return } } as stmt when l <> Id.main_label && stack_size <> 0 ->
               [gen_stmt [Asm.stack, Type.Int] (Add (C (-stack_size), Asm.stack)); stmt]
           | stmt -> [stmt])
        cfg
  in

  let func =
    { func with
          cfg = cfg;
          stack_size = stack_size }
  in
    Format.eprintf "stack size of %s is %d@." l stack_size;
    func


(* その関数を呼び出したときに破壊されうるレジスタを計算する. *)
let calc_kill call_graph env l info =
  let rec dfs (vis, acc) l =
    if S.mem l vis then (vis, acc)
    else
      let vis = S.add l vis in
      let acc = S.union acc (M.find l env).kill in
        List.fold_left dfs (vis, acc) (F.succ l call_graph)
  in
    { info with kill = snd (dfs (S.empty, S.empty) l) }



(* 関数割り当て後の関数lをenvに追加する. *)
let add_env prog l func env =
  let liveness = VLiveness.create func.cfg in
  let calls =
    G.fold
      (fun b stmts acc ->
         snd
           (List.fold_right
              (fun stmt (live, acc) ->
                 let acc =
                   match stmt with
                     | { left = { exp = Call (r, _); s_id = s_id } } ->
                         let set = live in
                         (* グローバル変数とCallが定義する変数は退避しない *)
                         let set = List.fold_left (fun acc (x, _) -> S.remove x acc) set (def_of stmt @ prog.global_variables) in
                           M.add s_id (r, set) acc
                     | _ -> acc
                 in
                   (VLiveness.transfer stmt live, acc))
              stmts (VLiveness.live_of b liveness, acc)))
      func.cfg M.empty
  in
  let info = M.find l env in
  let env =
    M.add l
      { info with
            calls = calls;
            kill = gather_variables_of func; }
      env
  in
    M.fold
      (fun _ (r, regs_to_save) env ->
         (* 関数rの呼び出しの前後で退避すべきregs_to_saveについて,
          * 呼び出し得る関数すべてにおいてコストを増加させる. *)
         let rec dfs (vis, env) r =
           if S.mem r vis then (vis, env)
           else
             let vis = S.add r vis in
             let info = M.find r env in
             let info =
               { info with
                     reg2cost =
                       S.fold
                         (fun x acc -> M.add x (M.find x acc + 1) acc)
                         regs_to_save info.reg2cost }
             in
             let env = M.add r info env in
               List.fold_left dfs (vis, env) (F.succ r prog.call_graph)
         in
           snd (dfs (S.empty, env) r))
      calls env



(* 関数lのレジスタ割り当てを行う *)
let g (env, prog) l =
  Format.eprintf "# Register Allocation of %s@." l;
  let func = M.find l prog.entries in
  (* メイン関数の場合は, ダミーのスタックポインタを直前で設定する.
   * (メイン関数をstrictにするために必要な処理.)
   * $stackの生存区間を短くするために, プログラムの先頭ではなくCallの直前で行う.
   * Note: メイン関数以外では, $stackをグローバル変数に加えることで, 入口/出口生存となっている. *)
  let func =
    if l = Id.main_label then
      { func with
            cfg =
              G.map_stmt_list
                (function
                   | { left = { exp = Call _ } } as stmt ->
                       (* フレームサイズが決定できないので, 適当に定める. *)
                       [gen_stmt [Asm.stack, Type.Int] (Set (0)); stmt]
                   | stmt -> [stmt])
                func.cfg }
    else
      func
  in

  (* Callの関数呼び出し規約を解決する *)
  let func = take_call_apart prog l func in
  (* 実際にレジスタ割り当てを行う *)
  let func = VColoring.f (M.find l env).reg2cost l func in
  let prog = { prog with entries = M.add l func prog.entries } in
  (* 今回のレジスタ割り当てによって生じた関数呼び出し規約を設定. *)
  let prog =
    G.fold_stmt
      (fun stmt prog ->
         match stmt.left with
           | { exp = Call (l, args); def = rets } ->
               let func = M.find l prog.entries in
               let func =
                 specify_calling_convention
                   func
                   (List.map2 (fun (_, t) x -> (x, t)) func.args args)
                   rets
               in
                 { prog with entries = M.add l func prog.entries }
           | _ -> prog)
      func.cfg prog
  in

  let env = add_env prog l func env in

  let () = (* for debug *)
    let used_regs = gather_variables_of func in
    let used_regs = S.union (S.of_list (List.map fst (func.args @ func.rets))) used_regs in
    (* 何故か彩色されなかったもの *)
    S.iter
      (fun x ->
         if not (Asm.is_reg x) then (
           Format.eprintf "WARNING: %s is not register, but not colored@." x;
           G.fold_stmt
             (fun stmt () ->
                List.iter (fun (y, t) -> if x = y then Format.eprintf "%s is type of %s@." x (Type.string_of t)) (def_of stmt))
             func.cfg ()
         )) used_regs;
    let unused = S.diff (S.of_list (Asm.regs @ Asm.fregs)) used_regs in
      Format.eprintf "unused registers: %d (regs: %d)@." (S.cardinal unused) (S.cardinal (S.filter (fun x -> Asm.reg_type_of x <> Type.Float) unused))
  in
    (env, prog)




let f prog =
  let prog = add_global_variable ~is_const:true (Asm.stack, Type.Int) prog in
  Format.eprintf "An approximate program size is %d@." (M.fold (fun _ { cfg = cfg } i -> G.fold (fun _ stmts i -> i + List.length stmts) cfg 0 + i) prog.entries 0);
  M.iter
    (fun l func ->
       Format.eprintf "function %s : args %d, rets %d.@." l (List.length func.args) (List.length func.rets))
    prog.entries;
  let env =
    let reg2cost = List.fold_right (fun x -> M.add x 0) (Asm.regs @ Asm.fregs) M.empty in
      M.map (fun _ -> { calls = M.empty; reg2cost = reg2cost; kill = S.empty }) prog.entries
  in
  (* 関数呼び出し規約を設定. *)
  let prog = map_func (fun _ -> specify_normal_calling_convention) prog in
  (* まずはレジスタ割り当てを行う.
   * ただし, （だいたい）関数呼び出しグラフのトポロジカル順で行う. *)
  let (env, prog) = List.fold_left g (env, prog) (snd (F.topological_sort prog.call_graph)) in
  (* 次に, 各関数について, 呼び出されたときに破壊されるレジスタを計算する. *)
  let env = M.mapi (calc_kill prog.call_graph env) env in
  (* 最後に関数呼び出しの前後での退避を行う. *)
  let prog = map_func (save_and_restore env) prog in
(*  let (env, prog) = List.fold_left save_and_restore (env, prog) (snd (F.topological_sort prog.call_graph)) in*)
  (* 合併によって無駄なコピー命令が生ずるのでそれらを除去する. *)
  let prog = remove_redundancy prog in
    prog



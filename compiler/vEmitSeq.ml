(* Vliw.programを出力する *)

open Vliw
open VSeq

let block_hint = ref M.empty

(* VLIWの命令の区切り *)
let delimiter = " \\ "

let string_of_half succ_of = function
  | { exp = Nop                               } -> Printf.sprintf "nop"
  | { exp = Set (i)            ; def = [x, _] } -> Printf.sprintf "setlo %s %d" x i
  | { exp = SetLabel (l)       ; def = [x, _] } -> Printf.sprintf "setlabel %s %s" x l
  | { exp = Mov (y)            ; def = [x, _] } -> Printf.sprintf "addi %s %s 0" x y
  | { exp = Add (V (y), z)     ; def = [x, _] } -> Printf.sprintf "add %s %s %s" y z x
  | { exp = Add (C (i), z)     ; def = [x, _] } -> Printf.sprintf "addi %s %s %d" x z i
  | { exp = Sub (V (y), z)     ; def = [x, _] } -> Printf.sprintf "sub %s %s %s" y z x
  | { exp = Sub (C (i), z)     ; def = [x, _] } -> Printf.sprintf "subi %s %s %d" x z i
  | { exp = ShiftL (y, i)      ; def = [x, _] } -> Printf.sprintf "shll %s %s %d" x y i
  | { exp = ShiftR (y, i)      ; def = [x, _] } -> Printf.sprintf "shra %s %s %d" x y i
  | { exp = Load (y, i)        ; def = [x, _] } -> Printf.sprintf "loadi %s %s %d" x y i
  | { exp = FLoad (y, i)       ; def = [x, _] } -> Printf.sprintf "floadi %s %s %d" x y i
  | { exp = Store (x, y, i)                   } -> Printf.sprintf "storei %s %s %d" x y i
  | { exp = FStore (x, y, i)                  } -> Printf.sprintf "fstorei %s %s %d" x y i
  | { exp = LoadC (y, V (z))   ; def = [x, _] } -> Printf.sprintf "caldr %s %s %s" y z x
  | { exp = FLoadC (y, V (z))  ; def = [x, _] } -> Printf.sprintf "cafldr %s %s %s" y z x
  | { exp = LoadC (y, C (i))   ; def = [x, _] } -> Printf.sprintf "caldi %s %s %d" x y i
  | { exp = FLoadC (y, C (i))  ; def = [x, _] } -> Printf.sprintf "cafldi %s %s %d" x y i
  | { exp = StoreC (x, y, i)                  } -> Printf.sprintf "casti %s %s %d" x y i
  | { exp = FStoreC (x, y, i)                 } -> Printf.sprintf "cafsti %s %s %d" x y i
  | { exp = FMov (y, funct)    ; def = [x, _] } -> Printf.sprintf "fmv 0 %s %s %s" y x (string_of_funct funct)
  | { exp = FAdd (y, z, funct) ; def = [x, _] } -> Printf.sprintf "fadd %s %s %s %s" y z x (string_of_funct funct)
  | { exp = FMul (y, z, funct) ; def = [x, _] } -> Printf.sprintf "fmul %s %s %s %s" y z x (string_of_funct funct)
  | { exp = FInv (y, funct)    ; def = [x, _] } -> Printf.sprintf "finv 0 %s %s %s" y x (string_of_funct funct)
  | { exp = FSqrt (y, funct)   ; def = [x, _] } -> Printf.sprintf "fsqrt 0 %s %s %s" y x (string_of_funct funct)
  | { exp = Send (x)                          } -> Printf.sprintf "send %s 0 0" x
  | { exp = Recv               ; def = [x, _] } -> Printf.sprintf "recv 0 0 %s" x
  | { exp = Call (l, _)                       } -> Printf.sprintf "jmp 0 0 %s" l
  | { exp = JumpTo (x)                        } -> Printf.sprintf "jr %s 0 0" x
  | { exp = IfEq  (x, V (y)) }                  -> Printf.sprintf "beq %s %s %s" x y (succ_of Then)
  | { exp = IfEq  (x, C (i)) }                  -> Printf.sprintf "beqi %s %d %s" x i (succ_of Then)
  | { exp = IfLt  (x, V (y)) }                  -> Printf.sprintf "blt %s %s %s" x y (succ_of Then)
  | { exp = IfLt  (x, C (i)) }                  -> Printf.sprintf "blti %s %d %s" x i (succ_of Then)
  | { exp = IfFEq (x, y) }                      -> Printf.sprintf "fbeq %s %s %s" x y (succ_of Then)
  | { exp = IfFLt (x, y) }                      -> Printf.sprintf "fblt %s %s %s" x y (succ_of Then)
  | { exp = Jmp }                               -> Printf.sprintf "jmp 0 0 %s" (succ_of Other)
  | stmt -> Format.eprintf "%s is not consistent!@." (string_of_half_stmt stmt); assert false


let emit_label oc label =
  if M.mem label !block_hint then
    Printf.fprintf oc "%s: # %s\n" label (M.find label !block_hint)
  else
    Printf.fprintf oc "%s:\n" label

let rec emit_stmt oc succ_of stmt =
  match stmt with
    | { left = { exp = Entry } } -> ()
    | _ ->
        Printf.fprintf oc
          "\t%30s %s %30s %s %d\n"
          (string_of_half succ_of stmt.left)
          delimiter
          (string_of_half succ_of stmt.right)
          delimiter
          stmt.stall

let build_block_hint cfg =
  let liveness = VLiveness.create cfg in
    G.iter
      (fun b _ ->
         let ss = VLiveness.live_of b liveness in
         block_hint := M.add b (String.concat ", " (S.to_list ss)) !block_hint)
      cfg


let f oc { all_cfg = cfg; start_block = entry; succ; prev } =
  Format.eprintf "# Emit assembly of VLIW (VSeq.t)@.";

  (* 埋まらなかった遅延スロットを埋めておく *)
  let cfg = G.map (fun _ -> VListScheduling.fill_delay_slot) cfg in
  (* ストール数が制約を満たすようにする. *)
  let cfg = VListScheduling.validate_stall_for_cfg cfg in

  build_block_hint cfg;

  let rec loop vis v =
    if S.mem v vis then vis
    else
      let vis = S.add v vis in
      let stmts = G.find v cfg in
      let succ_of e_tag = G.succ_of v e_tag cfg in
        emit_label oc v;
        List.iter (fun stmt -> emit_stmt oc succ_of stmt) stmts;
        if M.mem v succ then
          loop vis (M.find v succ)
        else
          vis
  in
  (* entryには先行ブロックがないはず. *)
  assert (not (M.mem entry prev));
  let vis = loop S.empty entry in
    ignore
      (G.fold
         (fun v _ vis ->
            if M.mem v prev then
              (* 先行ブロックがある場合は, 今は出力しない. *)
              vis
            else
              loop vis v)
         cfg vis);

    Format.eprintf "Compiling has been successfully done.@."


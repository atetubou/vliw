(* 後続辺とブロックが同じであれば合併する. *)

open Vliw

module B =
  Map.Make
    (struct
       (* ブロックの中身, タグと後続ブロックのリスト *)
       type t = block * (branch_tag * Id.t) list
       let compare =
         Common.compare_on
           (fun (stmts, succ) ->
              (List.map (fun stmt -> { (map_half (fun half -> { half with s_id = "" }) stmt) with stall = 0 }) stmts,
               List.sort compare succ))
    end)


let g cfg =
  let (acc, cfg) =
    G.fold
      (fun b stmts (acc, cfg) ->
         let elt = (stmts, List.map (fun b' -> (G.tag_of_edge b b' cfg, b')) (G.succ b cfg)) in
           if B.mem elt acc then
             let b_new = B.find elt acc in
             let cfg = G.contract b_new b (G.find b_new cfg) cfg in
               (acc, cfg)
           else
             let acc = B.add elt b acc in
               (acc, cfg))
      cfg (B.empty, cfg)
  in
    cfg

let f prog = map_cfg (fun _ cfg -> g cfg) prog


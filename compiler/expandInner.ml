(* 配列内のタプル内の配列まで展開する. *)

(* TODO: 関係あるエイリアスに, グローバル配列間で共通部分がないかの確認. *)

open Virtual

let memi x env = M.mem x env && (match M.find x env with Int _ -> true | _ -> false)
let findi x env = match M.find x env with Int (i) -> i | _ -> assert false

let memt x env = try match M.find x env with Tuple _ -> true | _ -> false with Not_found -> false
let findt x env = match M.find x env with Tuple (xs) -> xs | _ -> assert false

exception Impossible

type pos = InArray | InTuple of int


let rec string_of_pos = function
  | InArray :: ls -> "InArray, " ^ string_of_pos ls
  | InTuple (x) :: ls -> "InTuple (" ^ string_of_int x ^ "), " ^ string_of_pos ls
  | [] -> "Here"

let mapi f ls =
  let rec loop i = function
    | [] -> []
    | x :: ls -> f i x :: loop (i + 1) ls
  in
    loop 0 ls

let iteri f ls =
  let rec loop i = function
    | [] -> ()
    | x :: ls -> f i x; loop (i + 1) ls
  in
    loop 0 ls

(* n以上となる最小の2べきの指数 *)
let two_power n =
  let rec loop x i =
    if x >= n then
      i
    else
      loop (2 * x) (i+1)
  in
    loop 1 0



let gather_aliases target prog =
  let acc = ref M.empty in
  let updated = ref false in
  let mem x = M.mem x !acc in
  let find x = M.find x !acc in
  (* xの指し示しうる位置lsを追加 *)
  let add x ls =
    if mem x then
      begin
        let lss = find x in
          if not (List.mem ls lss) then
            (updated := true;
             acc := M.add x (ls :: lss) !acc)
      end
    else
      (updated := true;
       acc := M.add x [ls] !acc)
  in
  let add_lss x lss = List.iter (add x) lss in
  (* env: 関数名 -> その関数の返り値のlss
   * dest: 部分式の結果の(もしあれば)代入先
   * 返り値: 部分式の値が指し示す位置のリスト *)
  let rec f env dest = function
    | ExtArray (l) when l = target -> [[]]
    | Get (x, y) when mem x ->
        List.map (fun ls -> ls @ [InArray]) (find x)
    | Put (x, y, z) when mem x ->
        add_lss z (List.map (fun ls -> ls @ [InArray]) (find x));
        []
    (* 加算は, 二次元配列の展開によって, 先頭アドレスを移動するだけかもしれない. *)
    | Add (x, y) when mem x || mem y ->
        (if mem x then find x else []) @ (if mem y then find y else [])
    | Var (x) when mem x -> find x
    | Tuple (xs) ->
        (match dest with
           | Some (y) when mem y ->
               let lss = find y in
                 iteri
                   (fun i x -> add_lss x (List.map (fun ls -> ls @ [InTuple (i)]) lss))
                   xs;
                 lss
           | _ -> [])
    | Call (("create_array" | "create_float_array"), [xn; xi]) ->
        (match dest with
           | Some (y) when mem y ->
               let lss = find y in
                 add_lss xi (List.map (fun ls -> ls @ [InArray]) lss);
                 lss
           | _ -> [])
    | LetTuple (xts, y, e) ->
        if mem y then (
          let lss = find y in
            iteri
              (fun i (x, _) -> add_lss x (List.map (fun ls -> ls @ [InTuple (i)]) lss))
              xts
        );
        f env dest e
    (* [XXX] fill_for_intとfill_for_floatは無視する. *)
    | Call (x, ys) when M.mem x prog && M.mem x env && x <> "fill_for_int" && x <> "fill_for_float" ->
        let { args } = M.find x prog in
        let lss = M.find x env in
          (* ysを仮引数argsにコピーするので, yの指し示しうる場所を仮引数vは含むようになる. *)
          List.iter2
            (fun y (v, _) ->
               if mem y then
                 add_lss v (find y))
            ys args;
          lss
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        let lss1 = f env dest e1 in
        let lss2 = f env dest e2 in
          lss1 @ lss2
    | Let ((x, _), e1, e2) ->
        add_lss x (f env (Some (x)) e1);
        f env dest e2
    | _ -> []
  in
  let rec loop env =
    updated := false;
    let env' =
      M.mapi
        (fun l func -> f env None func.body)
        prog
    in
    M.iter
      (fun l _ ->
         (*
         Format.eprintf "func %s: %s@." l (String.concat "; " (List.map string_of_pos (M.find l env)));
          *)
         if M.find l env <> M.find l env' then
           updated := true)
      prog;
    if !updated then
      loop env'
    else
      !acc
  in
  let res = loop (M.map (fun _ -> []) prog) in
    res

(* 配列aliasesに代入されうる配列サイズの最大値を計算する. *)
let infer_size upper_env aliases prog =
  (* 内部配列の最大長を推論する.
   * to_inner: 対象となる配列の定義式か. *)
  let rec f to_inner = function
    (* 外部配列が代入される場合 *)
    | ExtArray (x) when to_inner && M.mem x !Global.all ->
        let { Global.length = len } = M.find x !Global.all in
          len
    (* 既に代入されている配列を取り出すだけのとき. *)
    | Get (x, y) when to_inner -> 0
    (* 新たに配列を作るとき. *)
    | Call (("create_array" | "create_float_array"), [xn; _])
    | Call ("allocate", [xn]) when to_inner ->
        if M.mem xn upper_env then
          M.find xn upper_env
        else
          (* 配列の長さの上限がないとき. *)
          raise Impossible
    | Let ((x, t), e1, e2) ->
        let a = f (S.mem x aliases) e1 in
        let b = f to_inner e2 in
          max a b
    | LetTuple (xts, y, e) -> f to_inner e
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        max (f to_inner e1) (f to_inner e2)
    | Add _ when to_inner ->
        (* 内部配列への代入が, 加算によってheadをずらす操作であれば(Expand2DArrayによって生ずるもの),
         * それ以降配列への代入はないので, 無視してよい. *)
        0
    | e when to_inner ->
        (* 内部配列への代入が関数適用などよくわからないものであれば諦める *)
          Format.eprintf "UNKNOWN: %s@." (string_of e);
        raise Impossible
    | _ -> 0
  in
    M.fold
      (fun _ func acc ->
         max (f false func.body) acc)
      prog 0

(* int array型でサイズ1のグローバル配列の値の上限を推論する. *)
let infer_upper_bound_of x alias prog =
  (* Unknown: 不定の値が代入されうる
   * Upper:   xの上限がわかる
   * None:    xの上限は知らないが, 不定の値が代入されることもない. *)
  let merge x y =
    match x, y with
      | `Unknown, _ -> `Unknown
      | _, `Unknown -> `Unknown
      | `Upper (x), `Upper (y) -> `Upper (max x y)
      | `Upper _, _ -> x
      | _ -> y
  in
  let rec f env = function
    | IfLE (x, y, e1, e2) when M.mem x env ->
        (* e2では y < xが成立するので, yは(xの上限) - 1以下. *)
        merge (f env e1) (f (M.add y (M.find x env - 1) env) e2)
    | Let ((x, t), Int (i), e2) ->
        f (M.add x i env) e2
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) | Let (_, e1, e2) ->
        merge (f env e1) (f env e2)
    | LetTuple (_, _, e) ->
        f env e
    | Put (x, _, y) when M.mem x alias && M.mem y env -> `Upper (M.find y env)
    | Put (x, _, _) when M.mem x alias -> `Unknown
    | _ -> `None
  in
  let upper =
    M.fold
      (fun _ func acc ->
         merge acc (f M.empty func.body))
      prog `None
  in
    match upper with
      | `Upper (x) -> Some (x)
      | _ -> None

(* aliasという名前の変数の値の上限がupperの元で, 他の変数の上限を推論する. *)
let infer_with alias upper prog =
  let merge x1 x2 =
    match x1, x2 with
      | Some (x1), Some (x2) -> Some (min x1 x2)
      | Some _, _ -> x1
      | _ -> x2
  in
  (* 第一返り値: 部分式の（もしあれば）上限
   * 第二返り値: 上限の環境 *)
  let rec f env = function
    | ShiftL (x, i) when M.mem x env ->
        (Some (M.find x env lsl i), env)
    | Var (x) when M.mem x env ->
        (Some (M.find x env), env)
    | Add (x, y) when M.mem x env && M.mem y env ->
        (Some (M.find x env + M.find y env), env)
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        let (a1, env) = f env e1 in
        let (a2, env) = f env e2 in
          (merge a1 a2, env)
    | Let ((x, t), e1, e2) ->
        let (a, env) = f env e1 in
        let env =
          match a, e1 with
            | _, Int (i) -> M.add x i env
            | _ when M.mem x alias -> M.add x upper env
            | Some (a), _ ->
                M.add x a env
            | _ -> env
        in
        let (a, env) = f env e2 in
          (a, env)
    | LetTuple (_, _, e) -> f env e
    | _ -> (None, env)
  in
    M.fold
      (fun _ func env ->
         snd (f env func.body))
      prog M.empty


(* 内部配列 -> [代入される配列の名前, 配列でのインデクス, タプルの要素中での先頭位置, 要素のサイズ
 * を計算する.
 * g_alias : グローバル配列のエイリアス. *)
let calc_inner_env tuple_info g_alias prog =
  let merge =
    M.merge
      (fun x a b ->
         match a, b with
           | Some _, Some _ -> Format.eprintf "FAILED EXPAND: Multiple Substitution@."; raise Impossible
           | Some _, _ -> a
           | _ -> b)
  in
  let rec f env = function
    | Let ((x, _), e1, e2) ->
        merge (f env e1) (f (M.add x e1 env) e2)
    | Put (x, index, z) when S.mem x g_alias ->
        (* グローバル配列への代入で, 中身のタプルが外部から来たときは諦める. *)
        if not (memt z env) then raise Impossible;
        let ws = findt z env in
          List.fold_left2
            (fun acc w (typ, alias, size, pos) ->
               match typ with
                 | Type.Array _ ->
                     (* 既に他の配列が代入されている場合は失敗 *)
                     if M.mem w acc then (Format.eprintf "FAILED: Other Array Subst@."; raise Impossible);
                     if not (S.mem w alias) then (Format.eprintf "WHY ? %s is not in alias@." w; raise Impossible);
                     M.add w (x, index, pos, size) acc
                 | _ -> acc)
            M.empty ws tuple_info
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) -> merge (f env e1) (f env e2)
    | LetTuple (_, _, e) -> f env e
    | _ -> M.empty
  in
    M.fold
      (fun _ func acc ->
         merge acc (f M.empty func.body))
      prog M.empty

             (*
  let rec f is_target = function
    | Let ((x, _), e1, e2) ->
        merge (f (S.mem x tuples) e1) (f is_target e2)
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        merge (f e1) (f e2)
    | LetTuple (_, _, e) -> f is_target e
    | Tuple (xs) when is_target ->
        List.fold_left
          (fun acc x ->
             if M.mem x inner_env then
               
        M.add z (x, y) M.empty
    | e when is_target ->
        Format.eprintf "  Escape as %s@." (string_of e);
        raise Impossible;
    | _ -> M.empty
  in
    M.fold
      (fun _ func acc ->
         merge acc (f false func.body))
      prog M.empty
              *)


      (*
(* targetに含まれる変数集合の定義をなるべく遅くする. *)
let localize target prog =
  let rec f = function
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f e1, f e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f e1, f e2)
    | Let ((x, t), e1, e2) when S.mem x target ->
        let e1 = f e1 in
        let e2 = f e2 in
        (* なるべく後ろにlet x : t = e1 in ...を配置する. *)
        let rec loop = function
          | Let ((x', t'), e1', e2') when not (S.mem x (fv e1')) ->
              Let ((x', t'), e1', loop e2')
          | e ->
              Let ((x, t), e1, e)
        in
          if has_effect e1 then
            Let ((x, t), e1, e2)
          else
            loop e2
    | Let ((x, t), e1, e2) -> Let ((x, t), f e1, f e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f e)
    | e -> e
  in
    M.map
      (fun func ->
         { func with body = f func.body })
      prog
       *)

(* targetに含まれる変数集合の定義をなるべく早くする.
 * ただし, ifなどをまたいで移動はしない. *)
(* TODO: メモリ外に逃げたとき, 意味を変えてしまう *)
let globalize target prog =
  let rec f = function
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f e1, f e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f e1, f e2)
    | Let ((x, t), e1, e2) ->
        let rec loop = function
          | Let ((x', t'), e1', e2') when S.mem x' target && not (has_effect e1') ->
              let (acc, e) = loop e2' in
                ((x', t', e1') :: acc, e)
          | Let ((x', t'), e1', e2') as e ->
              let (acc, e2') = loop e2' in
                if List.exists (fun (_, _, e) -> S.mem x' (fv e)) acc then
                  ([], e)
                else
                  (acc, Let ((x', t'), e1', e2'))
          | e -> ([], e)
        in
        let (lift_list, e) = loop (Let ((x, t), f e1, f e2)) in
          List.fold_right
            (fun (x, t, e1) e2 ->
               Let ((x, t), e1, e2))
            lift_list e
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f e)
    | e -> e
  in
    M.map
      (fun func ->
         { func with body = f func.body })
      prog

(* 実際にプログラムを書き換える. *)
let expand shift aliases tuple_info prog =
  (* fを満たす位置へのエイリアスを集める. *)
  let take f =
    M.fold
      (fun y lss acc ->
         if List.exists f lss then S.add y acc else acc)
      aliases S.empty
  in
    (*
  let inner_env =
    M.fold
      (fun y lss acc ->
         let lss = List.filter (function [InArray; InTuple _] -> true | _ -> false) lss in
         let lss = List.map (function [InArray; InTuple (i)] -> i | _ -> assert false) lss in
           match lss with
             | [] -> acc
             | [i] -> M.add y i acc
             (* 複数のタプルの位置に代入される場合は諦める. *)
             | _ -> raise Impossible)
      aliases M.empty
  in
     *)
  let inner_env =
    calc_inner_env
      tuple_info
      (take (function [] -> true | _ -> false))
      prog
  in
  let prog =
    let target =
      M.fold
        (fun _ (y, index, _, _) acc -> S.add y (S.add index acc))
        inner_env S.empty
    in
      globalize target prog
  in

  (* 配列内のタプルのエイリアス *)
  let tuples = take (function [InArray] -> true | _ -> false) in
  (* グローバル配列のエイリアス *)
  let global = take (function [] -> true | _ -> false) in

    (*
  let prog =
    let localize_target =
      M.fold
        (fun x _ -> S.add x)
        inner_env S.empty
    in
      localize localize_target prog
  in
     *)
  let rec f env = function
    (* グローバル配列からの読み出しの場合 *)
    | Get (x, y) when S.mem x global ->
        (* タプルの先頭アドレスを設定 *)
        insert (ShiftL (y, shift)) Type.Int
          (fun y' ->
             Add (x, y'))
    (* グローバル配列への書き込みの場合.
     * 内部配列以外を書き込む. *)
    | Put (x, y, z) when S.mem x global ->
        if not (memt z env) then raise Impossible;
        let ws = findt z env in
          insert (ShiftL (y, shift)) Type.Int
            (fun y' ->
               insert (Add (x, y')) Type.Int
                 (fun head ->
                    List.fold_left2
                      (fun e w (t, _, _, pos) ->
                         match t with
                           | Type.Array _ -> e
                           | _ -> seq (Store (w, head, pos), e))
                      Unit ws tuple_info))
    (* グローバル配列内のタプルから要素を取り出すとき. *)
    | LetTuple (xts, y, e) when S.mem y tuples ->
        List.fold_left2
          (fun e (x, t) (_, _, _, pos) ->
             match t with
               | Type.Array _ ->
                   insert (Int (pos)) Type.Int
                     (fun pos ->
                        Let ((x, t), Add (y, pos), e))
               | _ ->
                   Let ((x, t), Load (t, y, pos), e))
          (f env e) xts tuple_info
    (* [XXX] 二次元配列によって生じた先頭アドレスの移動を変更する.
     * 元々120個の要素がある二次元配列dirvecsは, dirvecs + (index<<7) によって内部配列を取り出していたが,
     * dirvecsを展開することによって, 二次元配列の要素のサイズが(1<<shift)倍(shift = 9)になるので, dirvecs + (index<<16)に変更する. *)
    | Add (x, y) when S.mem x global ->
        insert (ShiftL (y, shift)) Type.Int
          (fun y' -> Add (x, y'))
    (* タプルに代入される配列を作るとき, *)
    | Let ((x, t), Call (("create_array" | "create_float_array") as ca, [x_size; init]), e2) when M.mem x inner_env ->
        let (y, index, pos, size) = M.find x inner_env in
          if not (M.mem y env) || not (M.mem index env) then
            (Format.eprintf "  FAILED because of %s or %s@." y index; raise Impossible);
(*          Format.eprintf "# %s@.%s@." x (string_of e1);*)
          let e =
            if size < 10 then
              let rec loop i =
                if i < size then
                  insert (Int (i)) Type.Int (fun z -> seq (Put (x, z, init), loop (i + 1)))
                else
                  f env e2
              in
                loop 0
            else
              let fill_func =
                match ca with
                  | "create_array" -> "fill_for_int"
                  | "create_float_array" -> "fill_for_float"
                  | _ -> assert false
              in
                seq (Call (fill_func, [x; x_size; init]), f env e2)
                (*
                insert (Int (size)) Type.Int
                  (fun size ->
                     seq (Call (fill_func, [x; size; init]), f env e2))
                 *)
          in
            Let ((x, t),
               insert (ShiftL (index, shift)) Type.Int
                 (fun index ->
                    insert (Int (pos)) Type.Int
                      (fun pos ->
                         insert (Add (pos, y)) Type.Int
                           (fun acc -> Add (acc, index)))),
              e)
    | Let ((x, t), ExtArray _, e2) when M.mem x inner_env ->
        let (y, index, pos, size) = M.find x inner_env in
          if not (M.mem y env) || not (M.mem index env) then
            (Format.eprintf "  FAILED because of %s or %s@." y index; raise Impossible);
(*          Format.eprintf "# %s@.%s@." x (string_of e1);*)
          Let ((x, t),
             insert (ShiftL (index, shift)) Type.Int
               (fun index ->
                  insert (Int (pos)) Type.Int
                    (fun pos ->
                       insert (Add (pos, y)) Type.Int
                         (fun acc -> Add (acc, index)))),
            f env e2)


    | Let ((x, t), e1, e2) when M.mem x inner_env -> raise Impossible
    | Let ((x, t), e1, e2) ->
        let e1 = f env e1 in
          Let ((x, t), e1, f (M.add x e1 env) e2)
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f env e1, f env e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f env e1, f env e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f env e)
    | e -> e
  in
  let prog =
    M.map
      (fun func ->
         (* 引数も適当な値が定義されているとみなす *)
         let env = List.fold_right (fun (x, _) -> M.add x Unit) func.args M.empty in
           { func with body = f env func.body })
      prog
  in

(*    output "b.out" prog;*)
    M.iter
      (fun x (y, index, pos, size) ->
         Format.eprintf "%s:  to %s.(%s) , pos %d, size %d @." x y index pos size)
      inner_env;
(* tuple_info: (i, typ, alias, infer_size upper_env alias prog) *)
    ignore (inner_env);
    prog


let expand_inner upper_env x2aliases equiv prog =
  Format.eprintf "CHECK (%s)@." (String.concat ", " (S.to_list equiv));
  try
    let env =
      M.mapi
        (fun x aliases ->
           Format.eprintf "# CHECK %s@." x;
           (* [TODO] ピクセル関連のを展開する場合は, 同値類を計算して展開しなければならない! *)
(*           if String.contains x '.' && Filename.chop_extension x <> "line" then raise Impossible;*)
           let global = M.find x !Global.all in
           let tuple_type =
             match global with
               | { Global.typ = Type.Array (Type.Tuple t) } -> t
               | _ -> assert false
           in
           (* タプル内の各要素について, 必要な情報を計算する. *)
           let (_, size, tuple_info) =
             List.fold_left
               (fun (i, pos, acc) typ ->
                  let ls = [InArray; InTuple (i)] in
                  (* 位置がlsであるようなエイリアスのみを集める *)
                  let alias =
                    M.fold
                      (fun y lss acc ->
                         if List.mem ls lss then
                           S.add y acc
                         else
                           acc)
                      aliases S.empty
                  in
                  let size =
                    match typ with
                      | Type.Array _ ->
                          infer_size upper_env alias prog
                      | _ -> 1
                  in
                    (i + 1, pos + size, acc @ [i, typ, alias, size]))
               (0, 0, []) tuple_type
           in
           (* 大きい配列をタプルの始めにもってくる. *)
           let tuple_info =
             List.map (fun (_, typ, alias, size, pos) -> (typ, alias, size, pos))
               (List.sort
                  compare
                  (snd
                     (List.fold_right
                        (fun (i, typ, alias, size) (pos, acc) ->
                           Format.eprintf "%s of %d -> %d@." x i pos;
                           (pos + size, (i, typ, alias, size, pos) :: acc))
                        (List.sort (Common.compare_on (fun (_, _, _, size) -> size)) tuple_info)
                        (0, []))))
           in
           let shift = two_power size in
             (shift, tuple_info, aliases, global))
        (M.filter (fun x _ -> S.mem x equiv) x2aliases)
    in
    let prog =
      try
        let (_, (shift, tuple_info, _, _)) = M.choose env in
        let (aliases, tuple_info) =
          M.fold
            (fun x (shift', tuple_info', aliases', _) (aliases, tuple_info) ->
               if shift' <> shift then failwith "NotEqual";
               let tuple_info =
                 List.map2
                   (fun (typ, alias, size, pos) (typ', alias', size', pos') ->
                      if (typ, size, pos) <> (typ', size', pos') then failwith "NotEqual";
                      (typ, S.union alias alias', size, pos))
                   tuple_info tuple_info'
               in
               let aliases =
                 M.merge
                   (fun y a b ->
                      match a, b with
                        | Some (a), Some (b) -> Some (a @ b)
                        | Some _, _ -> a
                        | _ -> b)
                   aliases aliases'
               in
                 (aliases, tuple_info))
            env (M.empty, tuple_info)
        in
        (*
        M.iter
          (fun y lss ->
             Format.eprintf "%s : %s@." y (String.concat ", " (List.map string_of_pos lss)))
          aliases;
         *)
        expand shift aliases tuple_info prog
      with Failure "NotEqual" ->
        (* 似たグローバル配列ではないときは, 個別に展開する. *)
        M.fold
          (fun x (shift, tuple_info, aliases, global) prog ->
             expand shift aliases tuple_info prog)
          env prog
    in
      M.iter
        (fun x (shift, _, _, global) ->
           let { Global.length = length } = global in
             (* サイズを拡張する. *)
             Format.eprintf "Expand global array '%s' with length changed %d to %d@." x length (length * (1 lsl shift));
             Global.update x { global with Global.length = length * (1 lsl shift) })
        env;
      prog
  with Impossible -> Format.eprintf "Give up@."; prog

let f prog =
(*    (let file = "a.out" in Format.eprintf "OUTPUT %s@." file; output file prog);*)
  (* グローバル配列の内部をさす変数を計算する. *)
  let x2aliases =
    M.fold
      (fun x { Global.typ = typ } acc ->
         match typ with
           | Type.Array (Type.Tuple _) -> M.add x (gather_aliases x prog) acc
           | _ -> acc)
      !Global.all M.empty
  in
    (*
    M.iter
      (fun x aliases ->
           (*if String.contains x '.' && Filename.chop_extension x = "line" then*)(
         Format.eprintf "# alias of %s@." x;
         M.iter
           (fun y lss ->
              Format.eprintf "  %s: %s@." y (String.concat "; " (List.map (fun ls -> string_of_pos ls) lss)))
           aliases))
      x2aliases;
     *)
  (* 変数の値の上限を計算する.
   * 内部配列の最大長を推論するため. *)
  let upper_env =
    M.fold
      (fun x { Global.typ = typ; Global.length = len } acc ->
         match typ, len with
           | Type.Array (Type.Int), 1 ->
               (* まずグローバルな参照について推論する. *)
               let alias = gather_aliases x prog in
                 (match infer_upper_bound_of x alias prog with
                    | Some (upper) ->
                        let env = infer_with alias upper prog in
                          M.merge
                            (fun _ a b ->
                               match a, b with
                                 | Some (a), Some (b) -> Some (min a b)
                                 | Some _, _ -> a
                                 | _ -> b)
                            env acc
                    | _ -> acc)
           | _ -> acc)
      !Global.all M.empty
  in
  (* 最適化に関係のあるエイリアスのみを取り出す. *)
  let x2target =
    M.mapi
      (fun x aliases ->
         M.fold
           (fun y lss acc ->
              if List.exists
                   (function
                      | [] | [InArray] -> true
                      | [InArray; InTuple (i)] ->
                          let ts =
                            match M.find x !Global.all with
                              | { Global.typ = Type.Array (Type.Tuple (ts)) } -> ts
                              | _ -> assert false
                          in
                            (match Common.at i ts with Type.Array _ -> true | _ -> false)
                      | _ -> false)
                   lss
              then
                S.add y acc
              else acc)
           aliases S.empty)
      x2aliases
  in
  (* (グローバル配列, 位置)の同値関係を集める.
   * すなわち, 同じエイリアスを持つ位置は同値だと考える *)
  let rec parent_of x env =
    try
      let x' = M.find x env in
        if x' = x then x
        else parent_of x' env
    with Not_found -> x
  in
  let equiv =
    M.fold
      (fun x ->
         S.fold
           (fun y ->
              M.fold
                (fun x' target acc ->
                   if x <> x' && S.mem y target then
                     M.add (parent_of x acc) (parent_of x' acc) acc
                   else acc)
                x2target))
      x2target M.empty
  in
  let (_, prog) =
    M.fold
      (fun x _ (did, prog) ->
         if S.mem x did then (did, prog)
         else
           let eq =
             M.fold
               (fun x' _ eq ->
                  if parent_of x equiv = parent_of x' equiv then
                    S.add x' eq
                  else eq)
               x2aliases S.empty
           in
             (S.union eq did, expand_inner upper_env x2aliases eq prog))
      x2aliases (S.empty, prog)
  in
  let prog = M.map (fun func -> { func with body = OptVirtual.constfold func.body }) prog in
  let prog = M.map (fun func -> { func with body = OptVirtual.expand_array func.body }) prog in
  let prog = M.map (fun func -> { func with body = OptVirtual.constfold func.body }) prog in

(*    (let file = "c.out" in Format.eprintf "OUTPUT %s@." file; output file prog);*)
    prog



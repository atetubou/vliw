open KNormal

(* 引数に含まれるタプルを展開 *)
(* env: 関数の名前 -> (展開前の)引数の型のリスト *)
(* 関数適用以外の場所に関数が現れると（e.g. 他の変数へ代入）面倒なので、そうでない場合のみ。free_occur_of_functionでそれをチェックしている. *)
let rec g env = function
  | App (x, xs) when M.mem x env ->
      let (k, xs) =
        List.fold_right2
          (fun x t (k, xs) ->
             match t with
               | Type.Tuple ts -> (* タプル型の実引数はfresh variableで展開 *)
                   let yts = List.map (fun t -> (Id.gentmp t, t)) ts in
                   let ys = List.map fst yts in
                     ((fun e -> LetTuple (yts, x, k e)), ys @ xs)
               | _ -> (k, x :: xs))
          xs (M.find x env) ((fun e -> e), []) in
        (* kは引数に指定されたタプルを展開するための継続 *)
        k (App (x, xs))
  | IfEq (x, y, e1, e2) -> IfEq (x, y, g env e1, g env e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, g env e1, g env e2)
  | Let (xt, e1, e2) -> Let (xt, g env e1, g env e2)
  | LetRec({ name = (x, t); args = yts; body = e1 }, e2) ->
      if free_occur_of_function x e1 || free_occur_of_function x e2 then
        (* do not expand *)
        LetRec({ name = (x, t); args = yts; body = g env e1 }, g env e2)
      else
        let ts = List.map snd yts in
        let env = M.add x ts env in
        let (yts, e1) =
          List.fold_right
            (fun (y, t) (yts, e1) ->
              match t with
                | Type.Tuple ts ->
                    (*Printf.eprintf "Expand a tuple %s in arguments\n" y; *)
                    (* 仮引数をfresh variableで展開 *)
                    let zts = List.map (fun t -> (Id.gentmp t, t)) ts in
                    let zs = List.map fst zts in
                      (
                        zts @ yts,
                        (* 展開する前の引数yを作っておく *)
                        Let ((y, t), Tuple zs, e1)
                      )
                | _ -> ((y, t) :: yts, e1))
            yts ([], e1)
        in
        (* 関数の新しい型を設定する. *)
        let t = match t with Type.Fun (_, ret) -> Type.Fun (List.map snd yts, ret) | _ -> assert false in
          LetRec( { name = (x, t); args = yts; body = g env e1 }, g env e2)
  | LetTuple (xts, y, e) -> LetTuple (xts, y, g env e)
  | e -> e

(* Id.tの二重タプルの平滑化が不可能な場合に生ずる例外 *)
exception Impossible of Id.t

(* 型のリストtsを, タプルについて平坦化 *)
let flatten_type ts =
  List.fold_right
    (fun t acc ->
       match t with
         | Type.Tuple (ts) -> ts @ acc
         | _ -> t :: acc)
    ts []

(* タプルtsの型をもつネストしたタプルzを平坦化する *)
let flatten_nest z ts f =
  let xts = List.map (fun t -> (Id.gentmp t, t)) ts in
  let (xts', k) =
    List.fold_right
      (fun (x, t) (xts, k) ->
         match t with
           | Type.Tuple (ts) ->
               let tmps = List.map (fun t -> Id.gentmp t, t) ts in
                 (tmps @ xts, (fun e -> LetTuple (tmps, x, k e)))
           | _ -> ((x, t) :: xts, k))
      xts ([], (fun e -> e))
  in
  let a = Id.genid "nest_store" in
    LetTuple (xts, z,
      k
        (Let ((a, Type.Tuple (List.map snd xts')), Tuple (List.map fst xts'),
           (f a))))


(* 配列内の二重タプルを平坦化
 * env: 対象とする配列の集合 -> 平坦化前の配列内のタプルの型
 * tuples : 平坦化すべきタプルの集合 -> そのタプルが属している配列の名前
 * 前提条件: 配列がAddの両辺に現れない.
 * *)
let rec h env tuples = function
  (* タプルや配列の自由な出現があった場合には, 失敗する.
   * ただし, IfEq (x, y, _, _)のx, yに現れることは検査していない.
   * (そもそもタプルと配列の同値性判定はサポートしていない) *)
  | Var (x) | Put (_, _, x) when M.mem x env -> raise (Impossible (x))
  | Var (x) | Put (_, _, x) when M.mem x tuples -> raise (Impossible (M.find x tuples))
  | App (_, xs) | Tuple (xs) | ExtFunApp (_, xs)
      when List.exists (fun x -> M.mem x env) xs
    ->
      raise (Impossible (List.find (fun x -> M.mem x env) xs))
  | App (_, xs) | Tuple (xs) | ExtFunApp (_, xs)
      when List.exists (fun x -> M.mem x tuples) xs
    ->
      raise (Impossible (M.find (List.find (fun x -> M.mem x tuples) xs) tuples))

  (* ネストしたタプルを要素にもつ配列が作られたとき *)
  | Let ((x, Type.Array (Type.Tuple (ts))), ExtFunApp ("create_array", [xn; xi]), e2)
      when List.exists (function Type.Tuple _ -> true | _ -> false) ts
    ->
      (try
         let env = M.add x ts env in
         let c_a = flatten_nest xi ts (fun xi -> ExtFunApp ("create_array", [xn; xi])) in
         let ts = flatten_type ts in
         let e2 = h env tuples e2 in
           Format.eprintf "%s is flattened.@." x;
           Let ((x, Type.Array (Type.Tuple (ts))), c_a, e2)
       with
           Impossible (x') when x = x' ->
             (* この配列の二重タプルを展開できないとき *)
             Let ((x, Type.Array (Type.Tuple (ts))), ExtFunApp ("create_array", [xn; xi]), e2))
  | Let ((x, Type.Array (Type.Tuple (ts))), ExtFunApp ("allocate", [xn]), e2)
      when List.exists (function Type.Tuple _ -> true | _ -> false) ts
    ->
      (try
         let env = M.add x ts env in
         let ts = flatten_type ts in
         let e2 = h env tuples e2 in
           Format.eprintf "%s is flattened.@." x;
           Let ((x, Type.Array (Type.Tuple (ts))), ExtFunApp ("allocate", [xn]), e2)
       with
           Impossible (x') when x = x' ->
             (* この配列の二重タプルを展開できないとき *)
             Let ((x, Type.Array (Type.Tuple (ts))), ExtFunApp ("allocate", [xn]), e2))
  (* 展開すべき配列へのアクセスだった場合 *)
  | Let ((a, Type.Tuple (ts)), Get (x, y), e) when M.mem x env ->
      let tuples = M.add a x tuples in
      let e = h env tuples e in
        Let ((a, Type.Tuple (flatten_type ts)), Get (x, y), e)
  (* 配列へのアクセスがLetとともに現れなかった場合は失敗. *)
  | Get (x, y) when M.mem x env -> raise (Impossible (x))
  | Put (x, y, z) when M.mem x env ->
      let ts = M.find x env in
        flatten_nest z ts (fun z -> Put (x, y, z))
  (* 配列由来のネストしたタプルであった場合 *)
  | LetTuple (xts, y, e) when M.mem y tuples ->
      let e = h env tuples e in
      let (xts, e) =
        List.fold_right
          (fun (x, t) (xts, e) ->
             match t with
               | Type.Tuple (ts) ->
                   let tmps = List.map (fun t -> (Id.gentmp t, t)) ts in
                   let xts = tmps @ xts in
                   let e = Let ((x, t), Tuple (List.map fst tmps), e) in
                     (xts, e)
               | _ -> ((x, t) :: xts, e))
          xts ([], e)
      in
        LetTuple (xts, y, e)
  | Let ((x, t), e1, e2) -> Let ((x, t), h env tuples e1, h env tuples e2)
  | IfEq (x, y, e1, e2) -> IfEq (x, y, h env tuples e1, h env tuples e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, h env tuples e1, h env tuples e2)
  | LetTuple (xts, y, e1) -> LetTuple (xts, y, h env tuples e1)
  | LetRec ({ name = (x, t); args = yts; body = e1 }, e2) ->
      LetRec ({ name = (x, t); args = yts; body = h env tuples e1 }, h env tuples e2)
  | e -> e

let f e =
  let e = g M.empty e in
    e


open Block

type t = {
  count : int M.t;
  prob : float M.t M.t;
}
           
let prof = ref { count = M.empty; prob = M.empty }

let add_count b count =
  let count =
    if count < 0 then
      (Format.eprintf "Warning: count of %s is negative %d.@." b count; 0)
    else
      count
  in
    prof := { !prof with count = M.add b count !prof.count }

let count_of b = M.find b !prof.count

let add_prob b b' prob =
  if prob < 0.0 || 1.0 < prob then
    Format.eprintf "Warning: probability of the edge of (%s, %s) is %f@." b b' prob;
  let old = if M.mem b !prof.prob then M.find b !prof.prob else M.empty in
    prof := { !prof with prob = M.add b (M.add b' prob old) !prof.prob }

let prob_of b b' = M.find b' (M.find b !prof.prob)

let count_of_edge b b' = int_of_float (prob_of b b' *. float_of_int (count_of b) +. 0.5)


let f prof_file prog =
  let add_count b count prof =
    if count < 0 then
      Format.eprintf "Warning: count of %s is negative %d.@." b count;
    { prof with count = M.add b count prof.count }
  in
  let count_of b prof = M.find b prof.count
  in
  let add_prob b b' prob prof =
    if prob < 0.0 || 1.0 < prob then
      Format.eprintf "Warning: probability of the edge of (%s, %s) is %f@." b b' prob;
    let old = if M.mem b prof.prob then M.find b prof.prob else M.empty in
      { prof with prob = M.add b (M.add b' prob old) prof.prob }
  in
  let prob_of b b' prof = M.find b' (M.find b prof.prob)
  in

  let ic = open_in prof_file in
  let rec loop acc =
    try
      let read_one () = Scanf.fscanf ic " %s %d " (fun s c -> (s, c)) in
      let rec read i =
        if i <= 0 then
          []
        else
          read_one () :: read (i - 1)
      in
      let (b, cnt, branch) = Scanf.fscanf ic " %s %d %d " (fun a b c -> (a, b, c)) in
      let acc = add_count b cnt acc in
      let succ = read branch in
      let acc = List.fold_right (fun (b', c) -> add_prob b b' (if cnt = 0 then 0.0 else float_of_int c /. float_of_int cnt)) succ acc in
        loop acc
    with End_of_file -> acc
  in
  (* 後続ブロックにcallがあるときは, 辺の情報を縮約する. *)
  let rec contract prof =
    let updated = ref false in
    let prob =
      M.map
        (fun succ ->
           let res =
             M.fold
               (fun b _ acc ->
                  match acc with
                    | Some _ -> acc
                    | _ when Id.cut_off_id b = "call" ->
                        Some (b)
                    | _ -> None)
               succ None
           in
             match res with
               | Some (b) ->
                   updated := true;
                   M.find b prof.prob
               | _ -> succ)
        prof.prob
    in
    let prof = { prof with prob = prob } in
      if !updated then
        contract prof
      else
        prof
  in
  let prof = loop { count = M.empty; prob = M.empty } in
  let prof = contract prof in
  let () = close_in ic in
  (* 不要な情報を削除する. *)
  let prof =
    M.fold
      (fun _ func acc ->
         let acc =
           G.fold
             (fun b _ acc ->
                add_count b (count_of b prof) acc)
             func.cfg acc
         in
         let acc =
           G.fold_edge
             (fun b b' _ acc ->
                add_prob b b' (prob_of b b' prof) acc)
             func.cfg acc
         in
           acc)
      prog.entries { count = M.empty; prob = M.empty }
  in
    prof


let load prof_file prog =
  try
    prof := f prof_file prog
  with Not_found -> Format.eprintf "Some data is missing in %s@." prof_file; exit 1


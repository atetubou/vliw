open Block


let string_with_funct x y funct =
  let fpu = List.mem FPUBit funct in
  let neg = List.mem NegBit funct in
  let abs = List.mem AbsBit funct in
  let inner = x ^ (if fpu then "-" else "") ^ y in
    match (neg, abs) with
      | (true, true) -> "-|" ^ inner ^ "|"
      | (true, false) -> "-(" ^ inner ^ ")"
      | (false, true) -> "|" ^ inner ^ "|"
      | (false, false) -> inner

let emit_stmt oc = function
  | { exp = Entry }                             -> ()
  | { exp = Set (i)            ; def = [x, _] } -> Printf.fprintf oc "\t%s = %d\n" x i
  | { exp = SetLabel (l)       ; def = [x, _] } -> Printf.fprintf oc "\t%s = &%s\n" x l
  | { exp = FSet (f)           ; def = [x, _] } -> Printf.fprintf oc "\t%s = %.10g\n" x f
  | { exp = Add (y', z)        ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s + %s\n" x (string_of_ii y') z
  | { exp = Sub (y', z)        ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s - %s\n" x (string_of_ii y') z
  | { exp = ShiftL (y, i)      ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s << %d\n" x y i
  | { exp = ShiftR (y, i)      ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s >> %d\n" x y i
  | { exp = Load (y, z')       ; def = [x, _] } -> Printf.fprintf oc "\t%s = M[%s + %s]\n" x y (string_of_ii z')
  | { exp = FLoad (y, z')      ; def = [x, _] } -> Printf.fprintf oc "\t%s = M[%s + %s]\n" x y (string_of_ii z')
  | { exp = Store (x, y, z')                  } -> Printf.fprintf oc "\tM[%s + %s] = %s\n" y (string_of_ii z') x
  | { exp = FStore (x, y, z')                 } -> Printf.fprintf oc "\tM[%s + %s] = %s\n" y (string_of_ii z') x
  | { exp = Mov (y)            ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x y
  | { exp = FMov (y, funct)    ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x (string_with_funct "" y funct)
  | { exp = FAdd (y, z, funct) ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x (string_with_funct (y^" + ") z funct)
  | { exp = FMul (y, z, funct) ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x (string_with_funct (y^" * ") z funct)
  | { exp = FInv (y, funct)    ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x (string_with_funct "/ " y funct)
  | { exp = FSqrt (y, funct)   ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x (string_with_funct "sqrt " y funct)
  | { exp = Send (x)                          } -> Printf.fprintf oc "\tsend %s\n" x
  | { exp = Recv               ; def = [x, _] } -> Printf.fprintf oc "\trecv %s\n" x
  | { exp = ItoF (y)           ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x y
  | { exp = FtoI (y)           ; def = [x, _] } -> Printf.fprintf oc "\t%s = %s\n" x y
  | { exp = Call (l, _)                       } -> Printf.fprintf oc "\tcall %s\n" l
(*  | _                                           -> assert false (* 分岐などはここで出力しない *)*)
  | stmt -> Format.eprintf "%s is not consistent!@." (string_of_stmt stmt)

let emit_block oc stmts = (* stmtsの分岐以外を出力して, 分岐を返す *)
  let (branch, stmts) =
    match List.rev stmts with
      | branch :: stmts -> (branch, List.rev stmts)
      | _ -> assert false (* 分岐もない空のブロック *)
  in
    List.iter (emit_stmt oc) stmts;
    branch

let emit_if oc v = function
  | { exp = IfEq  (x, y') } -> Printf.fprintf oc "\tif %s = %s then goto %s\n" x (string_of_ii y') v
  | { exp = IfLt  (x, y') } -> Printf.fprintf oc "\tif %s < %s then goto %s\n" x (string_of_ii y') v
  | { exp = IfFEq (x, y ) } -> Printf.fprintf oc "\tif %s = %s then goto %s\n" x               y   v
  | { exp = IfFLt (x, y ) } -> Printf.fprintf oc "\tif %s < %s then goto %s\n" x               y   v
  | _ -> assert false

let g oc prog l = (* 関数lのemit *)
  let { cfg = cfg; v_entry = v_entry } = M.find l prog.entries in
  let rec dfs vis v =
    if S.mem v vis then vis
    else
      let vis = S.add v vis in
        Printf.fprintf oc "%s:\n" v;
        let branch = emit_block oc (G.find v cfg) in
        let edges = G.expand (fun w e_tag acc -> (e_tag, w) :: acc) v cfg [] in
          match branch with
            | { exp = (IfEq _ | IfLt _ | IfFEq _ | IfFLt _) } ->
                assert (List.mem_assoc Then edges && List.mem_assoc Else edges);
                let b_then = List.assoc Then edges in
                let b_else = List.assoc Else edges in
                  emit_if oc b_then branch;
                  let vis =
                    if S.mem b_else vis then (* 既にelse節が出力されている場合はjmpで飛ぶ *)
                      (Printf.fprintf oc "\tjmp %s\n" b_else; vis)
                    else (* そうでない場合は、ifの後に出力 *)
                      dfs vis b_else
                  in
                    dfs vis b_then (* then節を出力 *)
            | { exp = Return } when l = Id.main_label -> (* Main関数からのリターン = halt *)
                Printf.fprintf oc "\thalt\n";
                vis
            | { exp = Return } ->
                Printf.fprintf oc "\tjr %s\n" Asm.link;
                vis
            | { exp = Jmp } ->
                assert (List.mem_assoc Other edges);
                let w = List.assoc Other edges in
                  if S.mem w vis then (* 既に出力されている *)
                    (Printf.fprintf oc "\tjmp %s\n" w; vis)
                  else
                    dfs vis w
            | { exp = JumpTo (x) } ->
                Printf.fprintf oc "\tjr %s\n" x;
                List.fold_left dfs vis (G.succ v cfg)
            | _ -> assert false
  in
    if l <> Id.main_label then Printf.fprintf oc "%s:\n" l;
    ignore (dfs S.empty v_entry)


let f oc prog =
  Format.eprintf "# Emit assembly@.";
  Printf.fprintf oc "\tdefine %s as $r61\n" Asm.heap;
  Printf.fprintf oc "\tdefine %s as $r62\n" Asm.stack;
  Printf.fprintf oc "\tdefine %s as $r63\n" Asm.link;
  Printf.fprintf oc "\tdefine %s as $r60\n" Asm.zero;

  let order = snd (F.topological_sort prog.call_graph) in
    List.iter (g oc prog) order


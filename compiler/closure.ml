type closure = { entry : Id.l; actual_fv : Id.t list }
type t = (* クロージャ変換後の式 (caml2html: closure_t) *)
  | Unit
  | Int of int
  | Float of float
  | Add of Id.t * Id.t
  | Sub of Id.t * Id.t
  | ShiftL of Id.t * int
  | ShiftR of Id.t * int
  | FNeg of Id.t
  | FAbs of Id.t
  | FAdd of Id.t * Id.t
  | FSub of Id.t * Id.t
  | FMul of Id.t * Id.t
  | FInv of Id.t
  | IfEq of Id.t * Id.t * t * t
  | IfLE of Id.t * Id.t * t * t
  | Let of (Id.t * Type.t) * t * t
  | Var of Id.t
  | MakeCls of (Id.t * Type.t) * closure * t
  | AppCls of Id.t * Id.t list
  | AppDir of Id.l * Id.t list
  | Tuple of Id.t list
  | LetTuple of (Id.t * Type.t) list * Id.t * t
  | Get of Id.t * Id.t
  | Put of Id.t * Id.t * Id.t
  | ExtArray of Id.l
type fundef = { name : Id.l * Type.t;
		args : (Id.t * Type.t) list;
		formal_fv : (Id.t * Type.t) list;
		body : t }
type prog = Prog of fundef list * t

let rec fv = function
  | Unit | Int(_) | Float(_) | ExtArray(_) -> S.empty
  | FNeg(x) | FAbs(x) | FInv(x) | ShiftL(x, _) | ShiftR(x, _) -> S.singleton x
  | Add(x, y) | Sub(x, y) | FAdd(x, y) | FSub(x, y) | FMul(x, y) |  Get(x, y) -> S.of_list [x; y]
  | IfEq(x, y, e1, e2)| IfLE(x, y, e1, e2) -> S.add x (S.add y (S.union (fv e1) (fv e2)))
  | Let((x, t), e1, e2) -> S.union (fv e1) (S.remove x (fv e2))
  | Var(x) -> S.singleton x
  | MakeCls((x, t), { entry = l; actual_fv = ys }, e) -> S.remove x (S.union (S.of_list ys) (fv e))
  | AppCls(x, ys) -> S.of_list (x :: ys)
  | AppDir(_, xs) | Tuple(xs) -> S.of_list xs
  | LetTuple(xts, y, e) -> S.add y (S.diff (fv e) (S.of_list (List.map fst xts)))
  | Put(x, y, z) -> S.of_list [x; y; z]

let toplevel : fundef list ref = ref []

let rec g env known = function (* クロージャ変換ルーチン本体 (caml2html: closure_g) *)
  | KNormal.Unit -> Unit
  | KNormal.Int(i) -> Int(i)
  | KNormal.Float(d) -> Float(d)
  | KNormal.Add(x, y) -> Add(x, y)
  | KNormal.Sub(x, y) -> Sub(x, y)
  | KNormal.ShiftL(x, i) -> ShiftL(x, i)
  | KNormal.ShiftR(x, i) -> ShiftR(x, i)
  | KNormal.FNeg(x) -> FNeg(x)
  | KNormal.FAbs(x) -> FAbs(x)
  | KNormal.FAdd(x, y) -> FAdd(x, y)
  | KNormal.FSub(x, y) -> FSub(x, y)
  | KNormal.FMul(x, y) -> FMul(x, y)
  | KNormal.FInv(x) -> FInv(x)
  | KNormal.IfEq(x, y, e1, e2) -> IfEq(x, y, g env known e1, g env known e2)
  | KNormal.IfLE(x, y, e1, e2) -> IfLE(x, y, g env known e1, g env known e2)
  | KNormal.Let((x, t), e1, e2) -> Let((x, t), g env known e1, g (M.add x t env) known e2)
  | KNormal.Var(x) -> Var(x)
  | KNormal.LetRec({ KNormal.name = (x, t); KNormal.args = yts; KNormal.body = e1 }, e2) -> (* 関数定義の場合 (caml2html: closure_letrec) *)
      (* 関数定義let rec x y1 ... yn = e1 in e2の場合は、
	 xに自由変数がない(closureを介さずdirectに呼び出せる)
	 と仮定し、knownに追加してe1をクロージャ変換してみる *)
      let toplevel_backup = !toplevel in
      let env' = M.add x t env in
      let known' = S.add x known in
      let e1' = g (M.add_list yts env') known' e1 in
      (* 本当に自由変数がなかったか、変換結果e1'を確認する *)
      (* 注意: e1'にx自身が変数として出現する場合はclosureが必要!
         (thanks to nuevo-namasute and azounoman; test/cls-bug2.ml参照) *)
      let zs = S.diff (fv e1') (S.of_list (List.map fst yts)) in
      let known', e1' =
	if S.is_empty zs then known', e1' else
	(* 駄目だったら状態(toplevelの値)を戻して、クロージャ変換をやり直す *)
	(Format.eprintf "free variable(s) %s found in function %s@." (Id.pp_list (S.elements zs)) x;
	 Format.eprintf "WARNING: function %s cannot be directly applied in fact@." x;
	 toplevel := toplevel_backup;
	 let e1' = g (M.add_list yts env') known e1 in
	 known, e1') in
      let zs = S.elements (S.diff (fv e1') (S.add x (S.of_list (List.map fst yts)))) in (* 自由変数のリスト *)
      let zts = List.map (fun z -> (z, M.find z env')) zs in (* ここで自由変数zの型を引くために引数envが必要 *)
      toplevel := { name = (Id.L(x), t); args = yts; formal_fv = zts; body = e1' } :: !toplevel; (* トップレベル関数を追加 *)
      let e2' = g env' known' e2 in
      if S.mem x (fv e2') then (* xが変数としてe2'に出現するか *)
	MakeCls((x, t), { entry = Id.L(x); actual_fv = zs }, e2') (* 出現していたら削除しない *)
      else
	((*Format.eprintf "eliminating closure(s) %s@." x; *)
	 e2') (* 出現しなければMakeClsを削除 *)
  | KNormal.App(x, ys) when S.mem x known -> (* 関数適用の場合 (caml2html: closure_app) *)
      (*Format.eprintf "directly applying %s@." x; *)
      AppDir(Id.L(x), ys)
  | KNormal.App(f, xs) -> AppCls(f, xs)
  | KNormal.Tuple(xs) -> Tuple(xs)
  | KNormal.LetTuple(xts, y, e) -> LetTuple(xts, y, g (M.add_list xts env) known e)
  | KNormal.Get(x, y) -> Get(x, y)
  | KNormal.Put(x, y, z) -> Put(x, y, z)
  | KNormal.ExtArray(x) -> ExtArray(Id.L(x))
  | KNormal.ExtFunApp(x, ys) -> AppDir(Id.L(x), ys)

let f e =
  toplevel := [];
  let e' = g M.empty S.empty e in
  Prog(List.rev !toplevel, e')

    (*
let stack_tuple (Prog (fundefs, e)) =
  let all = (S.of_list (List.map (fun { name = (Id.L l, _) } -> l) fundefs)) in
  let res =
    List.fold_left
      (fun s { name = (Id.L cur, _); body = e } ->
         let rec f tail = function (* タプルをスタック上にのせて返せない関数の集合より大きい集合 *)
           | Unit | Int _ | Float _ | Add _ | Sub _ | ShiftL _ | ShiftR _
           | FNeg _ | FAbs _ | FAdd _ | FSub _ | FMul _ | FInv _
           | AppCls _ | Var _
           | Tuple _ | Get _ | Put _ | ExtArray _ -> S.empty
           | IfEq (x, y, e1, e2) | IfLE (x, y, e1, e2) -> S.union (f tail e1) (f tail e2)
           | Let ((x, t), AppDir (Id.L l, _), (* すぐに返り値を使う場合はよい *)
                  LetTuple (_, y, e)) when x = y && not (S.mem x (fv e)) -> f tail e
           | Let ((x, t), e1, e2) -> S.union (f false e1) (f tail e2)
           | MakeCls ((x, t), { entry = Id.L l }, e) -> S.union (S.singleton l) (f tail e) (* 関数がエスケープするときは駄目。 *)
           | AppDir (Id.L l, _) when tail && cur = l -> S.empty (* 自己末尾再帰の場合はよい *)
           | AppDir (Id.L l, _) -> S.singleton l (* 条件に当てはまらない関数呼び出しは駄目 *)
           | LetTuple (_, _, e) -> f tail e
         in
           S.diff s (f true e))
      all ({ name = (Id.L "MAIN", Type.Unit); args = []; formal_fv = []; body = e } :: fundefs)
  in
    res

     *)
(* for debug output *)
let string_of (Prog (funs, t)) =
  let rec sub tree depth =
    Common.indent depth ^ (
      match tree with
        | Unit -> "Unit\n"
        | Int n -> "Int("^string_of_int n^")\n"
        | Float f -> "Float("^string_of_float f^")\n"
        | Add (s, t) -> "Add " ^ s ^ " " ^ t ^ "\n"
        | Sub (s, t) -> "Sub " ^ s ^ " " ^ t ^ "\n"
        | ShiftL (s, i) -> "ShiftL " ^ s ^ " " ^ string_of_int i ^ "\n"
        | ShiftR (s, i) -> "ShiftR " ^ s ^ " " ^ string_of_int i ^ "\n"
        | FNeg s -> "FNeg " ^ s ^ "\n"
        | FAbs s -> "FAbs " ^ s ^ "\n"
        | FAdd (s, t) -> "FAdd " ^ s ^ " " ^ t ^ "\n"
        | FSub (s, t) -> "FSub " ^ s ^ " " ^ t ^ "\n"
        | FMul (s, t) -> "FMul " ^ s ^ " " ^ t ^ "\n"
        | FInv s -> "FInv " ^ s ^ "\n"
        | IfEq (s, t, e1, e2) ->
            "If " ^ s ^ " = " ^ t ^ " then\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub e2 (depth+1)
        | IfLE (s, t, e1, e2) ->
            "If " ^ s ^ " <= " ^ t ^ " then\n" ^
            sub e1 (depth+1) ^
            Common.indent depth ^ "else\n" ^
            sub e2 (depth+1)
        | Let ((s, _), e1, e2) ->
            "Let " ^ s ^ " =\n" ^
            sub e1 (depth+1) ^
            sub e2 depth (* there is no nest of let *)
        | Var x -> x ^ "\n"
        | MakeCls ((s, _), { entry = Id.L _; actual_fv = ls }, e) ->
            "MakeCls "^s^" with fvs = (" ^ String.concat ", " ls ^ ")\n" ^
            sub e depth (* no indent *)
        | AppCls (s, ls) ->
            "AppCls "^s^" to (" ^ String.concat ", " ls ^ ")\n"
        | AppDir (Id.L l, ls) ->
            "AppDir label."^l^" to (" ^ String.concat ", " ls ^ ")\n"
        | Tuple ls ->
            "Tuple (" ^ (String.concat ", " ls) ^ ")\n"
        | LetTuple (ls, s, e) ->
            "LetTuple (" ^ (String.concat ", " (List.map fst ls)) ^ ") = " ^ s ^ " in\n" ^
            sub e (depth+1)
        | Get (s, t) -> "Get " ^ s ^ ".(" ^ t ^ ")\n"
        | Put (s, t, u) -> "Put " ^ s ^ ".(" ^ t ^ ") = " ^ u ^ "\n"
        | ExtArray (Id.L l) -> "ExtArray labal."^l^"\n"
    )
  in
    List.fold_left
      (fun acc { name = (Id.L l, _); args = ls; formal_fv = fvs; body = t } ->
           acc ^
           "DefineLabel labal."^l^" (" ^ String.concat ", " (List.map fst ls) ^ ") with fvs = (" ^ String.concat ", " (List.map fst fvs) ^ ")\n" ^
           sub t 1) 
      "" funs ^
    "Main\n" ^
    sub t 1

let output path tree =
  let str = string_of tree in
  let out = open_out path in
    output_string out str;
    close_out out;
    tree


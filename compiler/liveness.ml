(* 生存解析
 * 疎なデータフローであるから，Bit Vectorは使わない方が速い. *)

open Block

type t = {
  live : S.t ref M.t;
  kill : S.t M.t;
  gen  : S.t M.t;
}

(* 文単位の伝播関数. 副作用なし. *)
let transfer stmt set =
  List.fold_left
    (fun acc x -> S.add x acc)
    (List.fold_left
       (fun acc (x, _) -> S.remove x acc)
       set
       stmt.def)
    stmt.use

(* 出口生存な変数の集合. *)
let live_of b t = !(M.find b t.live)

(* 入口生存な変数の集合 *)
let live_in_of b t = S.union (M.find b t.gen) (S.diff !(M.find b t.live) (M.find b t.kill))

(* ブロックbの出口生存なものにxを追加 *)
let add b x t =
  let this = M.find b t.live in
    this := S.add x !this

(* ブロックbの出口生存の情報をb'へコピーする *)
let copy_to b b' t = { t with live = M.add b' (ref !(M.find b t.live)) t.live }

let create cfg =
  Time.start "Liveness analysis";
  let direction = DataFlow.Backward in
  (* block -> そのブロックで定義される変数の集合 *)
  let def =
    G.fold
      (fun b stmts acc ->
         M.add b
           (List.fold_left
              (fun def { def = d } ->
                 List.fold_right (fun (x, _) -> S.add x) d def)
              S.empty stmts)
           acc)
      cfg M.empty
  in
  (* block -> そのブロック内において、先行命令によって定義されない変数の集合 *)
  let use =
    G.fold
      (fun b stmts acc ->
         M.add b
           (fst
              (List.fold_left
                (fun (use, env) { use = u; def = d } ->
                   let use = S.union (S.diff (S.of_list u) env) use in
                   let env = List.fold_right (fun (x, _) -> S.add x) d env in
                     (use, env))
                (S.empty, S.empty) stmts))
           acc)
      cfg M.empty
  in
  let live =
    let live = G.fold (fun b _ acc -> M.add b (ref S.empty) acc) cfg M.empty in
    let update b succ =
      let this =
        List.fold_left
          (fun acc b' -> S.union acc (S.union (S.diff !(M.find b' live) (M.find b' def)) (M.find b' use)))
          S.empty succ
      in
      let old = M.find b live in
      let updated = not (S.equal this !old) in
        old := this;
        updated
    in
      DataFlow.solution_of_block ~direction ~update cfg;
      live
  in
    Time.stop "Liveness analysis";
    {
      live = live;
      kill = def;
      gen  = use;
    }




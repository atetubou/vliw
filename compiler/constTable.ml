(* 定数テーブル *)

module CT =
  Map.Make
    (struct
       type t = int32
       let compare = compare
     end)

(* itof, ftoiでやり取りするためのメモリ上の領域 *)
let swap_location = Asm.const_table_top
(* 定数テーブルの末尾 *)
let tail = ref (swap_location + 1)

(* 定数テーブル.
 * Int32 -> その値が格納されたアドレス *)
let const_env = ref CT.empty

(* 定数テーブルに存在する場合は, そのアドレスを返す.
 * そうでない場合は, 新たに定数テーブルを確保する. *)
let pos_of bit =
  if CT.mem bit !const_env then
    CT.find bit !const_env
  else (
    Format.eprintf "CONST %d or %f@." (Int32.to_int bit) (Int32.float_of_bits bit);
    let addr = !tail in
      incr tail;
      (* 定数テーブルの次はスタック領域なので, その先頭以下でないといけない. *)
      assert (!tail <= Asm.stack_top);
      const_env := CT.add bit addr !const_env;
      addr
  )

(* (定数の値, アドレス)を畳み込む. *)
let fold (f : int32 -> int -> 'a) =
  CT.fold f !const_env



(* ループ不変式の巻き上げ *)

open Block

let is_target ?(no_store = false) ?(no_store_c = false) = function
  (* TODO loadはstoreがループ内に存在し得ることによって, 値が変わってしまうので今のところ除いておく *)
  (* Mov, FMovやSetを巻き上げると, レジスタ圧が高くなりすぎるので無視する. *)
  (*| Set _ | SetLabel _ | FSet _*) (*| Mov _ | FMov _ *)| Add _ | Sub _
  | ShiftL _ | ShiftR _ | FAdd _ | FMul _ | FInv _ | FSqrt _ | ItoF _ | FtoI _
    -> true
  (* storeがループ内に存在しない場合は, Loadも巻き上げてよい *)
  | Load _ | FLoad _ when no_store -> true
  | LoadC _ | FLoadC _ when no_store_c -> true
  | _ -> false

(* ループ不変式を巻き上げる.
 * dominatorの情報は, loop pre headerを加えることで更新されなければならない. *)
let hoist (reach, use) liveness dom cfg (head, pre, loop) =
(*  Format.eprintf "Hoisting of size %d@." (S.cardinal loop);*)
  (* このループ内で一度しか定義されない変数の集合 *)
  let def_once =
    let acc =
      S.fold
        (fun b ->
           List.fold_right
             (fun stmt ->
                List.fold_right
                  (fun (x, _) acc ->
                     let this = if M.mem x acc then M.find x acc else 0 in
                       M.add x (this + 1) acc)
                  stmt.def)
             (G.find b cfg))
        loop M.empty
    in
      M.fold
        (fun x i acc ->
           assert (i > 0);
           if i = 1 then
             S.add x acc
           else
             acc)
        acc S.empty
  in
  (* ループの出口を支配するブロックを計算する *)
  let dominate_exit =
    let acc = Dominator.creator dom true in
      S.iter
        (fun b ->
           (* ループ外への辺があるとき.
            * すなわち, bがループの出口であるとき. *)
           if List.exists
                (fun b' -> not (S.mem b' loop))
                (G.succ b cfg)
           then
             Dominator.inter acc (Dominator.dom_of b dom))
        loop;
      acc
  in
  (* ループの内部の文を集める *)
  let stmt_in_loop =
    S.fold
      (fun b acc ->
         List.fold_left
           (fun acc stmt -> S.add stmt.s_id acc)
           acc (G.find b cfg))
      loop S.empty
  in
  (* ループの出口で生存している変数全体 *)
  let live_out =
    S.fold
      (fun b acc ->
         if List.exists
              (fun b' -> not (S.mem b' loop))
              (G.succ b cfg)
         then
           S.union acc (Liveness.live_of b liveness)
         else acc)
      loop S.empty
  in
  (* ループ内にStore命令が存在しないか. *)
  let no_store =
    not
      (S.exists
         (fun b ->
            List.exists
              (function { exp = Store _ | FStore _ } -> true | _ -> false)
              (G.find b cfg))
         loop)
  in
  let no_store_c =
    not
      (S.exists
         (fun b ->
            List.exists
              (function { exp = StoreC _ | FStoreC _ } -> true | _ -> false)
              (G.find b cfg))
         loop)
  in

    (*
    if no_store then
      Format.eprintf "this loop(%d) does not have store@." (S.cardinal loop)
    else Format.eprintf "this loop(%d) HAS store@." (S.cardinal loop);
     *)
  (* stack: 巻き上げできる文のリスト
   * moved: stackに属する文のs_idの集合 *)
  let rec repeat acc =
    let updated = ref false in
    let acc =
      S.fold
        (fun b ->
           List.fold_right
             (fun stmt (stack, moved) ->
                if not (S.mem stmt.s_id moved) &&
                   (* 巻き上げしてよい式か *)
                   is_target ~no_store ~no_store_c stmt.exp &&
                   (match stmt.def with
                      | [x, _] ->
                          (* ループ内で一度しか定義されないか *)
                          S.mem x def_once &&
                          (* ループ内でxを使用するならば, そのxに到達する定義文はstmtのみ *)
                          List.for_all
                            (fun stmt ->
                               let var2def = M.find stmt.s_id reach in
                                 assert (M.mem x var2def);
                                 let cnt = List.length (M.find x var2def) in
                                   assert (cnt <> 0);
                                   cnt = 1)
                            (if M.mem stmt.s_id use then M.find stmt.s_id use else (Format.eprintf "Warning: %s is not used!@." (string_of_stmt stmt);[]) (* TODO useにstmtが属さない理由を考える *)) &&
                          (
                            (* このブロックがループの出口を支配する *)
                            Dominator.mem b dominate_exit ||
                            (* ループの外部でxが使用されることがない *)
                            not (S.mem x live_out)
                          )
                      | _ -> assert false (* is_targetであるものはちょうど1つだけ定義するはず. *)) &&
                   (* stmtの使用する全ての変数について *)
                   List.for_all
                     (fun x ->
                        let var2def = M.find stmt.s_id reach in
                        let defs = M.find x var2def in
                          (* 到達する定義が全てループの外部 *)
                          List.for_all
                            (fun stmt -> not (S.mem stmt.s_id stmt_in_loop))
                            defs ||
                          (* 到達する定義が1つのみで, 既にその定義文はstackに入っているか.
                           * (すなわちpre headerから定義がやってくるか.)
                           * この条件の不変式は, pre headerに入れる順番に注意しなければならない. *)
                          (match defs with
                             | [stmt] -> S.mem stmt.s_id moved
                             | _ -> false))
                     stmt.use
                then
                  (updated := true;
(*                   Format.eprintf "MOVE %s@." (string_of_stmt stmt);*)
                   (stmt :: stack,
                    S.add stmt.s_id moved))
                else
                  (stack, moved))
               (G.find b cfg))
        loop acc
    in
      if !updated then
        repeat acc
      else
        acc
  in
  let (stack, moved) = repeat ([], S.empty) in
  (* movedに属する文を削除 *)
  let cfg =
    G.filter_stmt
      (fun stmt -> not (S.mem stmt.s_id moved))
      cfg
  in
  (* pre headerにstackを追加する *)
  assert (match G.find pre cfg with [{ exp = Jmp }] -> true | _ -> false);
  let cfg =
    G.update_block
      pre
      (List.rev stack @ [gen_stmt [] Jmp])
      cfg
  in
    cfg


let create_pre_header (loops, cfg) =
  let (loops, cfg) =
    M.fold
      (fun head loop (loops, cfg) ->
         let (pre, cfg) = G.add_block [gen_stmt [] Jmp] cfg in
         let cfg =
           List.fold_left
             (fun cfg b -> 
                if S.mem b loop then
                  cfg
                else
                  (* ループ外部からheadに向かう辺をpreに付け替える *)
                  G.add_edge b pre (G.tag_of_edge b head cfg)
                    (G.remove_edge b head cfg))
             cfg (G.pred head cfg)
         in
         let cfg = G.add_edge pre head Other cfg in
           (M.add head (pre, loop) loops, cfg))
      loops (M.empty, cfg)
  in
  let loops =
    M.map
      (fun (pre, loop) ->
         (pre,
          S.fold
            (fun b acc ->
               (* 他のループのヘッダを含んでいたらpreも追加する. *)
               if M.mem b loops then
                 let (pre, _) = M.find b loops in
                   S.add pre acc
               else acc)
            loop loop))
      loops
  in
    (loops, cfg)

let g cfg =
  let loops = Loop.create cfg in
  let (loops, cfg) = create_pre_header (loops, cfg) in
  (* 内部ループを先に最適化するため, サイズの小さいものから並べる. *)
  let loops =
    List.sort
      (fun (_, _, loop) (_, _, loop') -> compare (S.cardinal loop) (S.cardinal loop'))
      (M.fold (fun h (pre, loop) acc -> (h, pre, loop) :: acc) loops [])
  in
  (* pre headerをつけてからdominatorをもう一度計算する. *)
  let dom = Dominator.create cfg in
  let reach_and_use = Reach.create_for_each_statement cfg in
  let liveness = Liveness.create cfg in
  let cfg = List.fold_left (hoist reach_and_use liveness dom) cfg loops in
    cfg

let f prog =
  Time.start "Hoisting of loop invariants";
  let prog = map_cfg (fun _ cfg -> g cfg) prog in
    Time.stop "Hoisting of loop invariants";
    prog


  

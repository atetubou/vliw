(* 生存解析による不要文除去. *)

open Vliw

(* レジスタの値を変える以外の意味で副作用があるか *)
let has_effect = function
  | Set _ | SetLabel _ | Mov _ | FMov _ | Add _ | Sub _ | ShiftL _ | ShiftR _
  | FAdd _ | FMul _ | FInv _ | FSqrt _
  | Load _ | FLoad _ | LoadC _ | FLoadC _ -> false
  | _ -> true

let g cfg =
  let liveness = VLiveness.create cfg in
  let cfg =
    G.map
      (fun b stmts ->
         snd
           (List.fold_right
              (fun stmt (live, stmts) ->
                 let stmts =
                   filter_half
                     (fun half ->
                        has_effect half.exp ||
                        List.exists
                          (fun (x, _) -> S.mem x live)
                          half.def)
                     stmt
                   :: stmts
                 in
                   (VLiveness.transfer stmt live, stmts))
              stmts (VLiveness.live_of b liveness, [])))
      cfg
  in
    cfg

let f prog = remove_redundancy (map_cfg (fun l cfg -> g cfg) prog)


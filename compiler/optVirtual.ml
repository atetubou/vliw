(* Virtualに対する最適化を行う *)

open Virtual

let seq (e1, e2) = Let ((Id.gentmp Type.Unit, Type.Unit), e1, e2)


let memi x env = try match M.find x env with Int _ -> true | _ -> false with Not_found -> false
let findi x env = match M.find x env with Int (i) -> i | _ -> assert false

let memt x env = try match M.find x env with Tuple _ -> true | _ -> false with Not_found -> false
let findt x env = match M.find x env with Tuple (xs) -> xs | _ -> assert false

(* 小さいcreate_array, create_float_arrayを展開する. *)
let expand_array e =
  let rec f env = function
    | Let ((x, t), Call (("create_array" | "create_float_array"), [xn; xi]), e)
        when memi xn env && findi xn env <= 5
      ->
        let rec loop i e =
          if i < 0 then
            e
          else
            insert (Int (i)) Type.Int
              (fun y ->
                 loop (i-1) (seq (Put (x, y, xi), e)))
        in
          Let ((x, t), Call (("allocate"), [xn]),
               loop (findi xn env - 1) (f env e))
    | Let ((x, t), e1, e2) ->
        Let ((x, t), f env e1, f (M.add x e1 env) e2)
    | IfEq (x, y, e1, e2) -> IfEq (x, y, f env e1, f env e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, f env e1, f env e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, f env e)
    | e -> e
  in
    f M.empty e



(* 不要なタプルの生成を抑制する.
 * KNormal.tのConstFold.f + Elim.fとほとんど同じことをやる. *)
let constfold e =
  let is_needed env ((target, _) as xi) e =
    let rec f env = function
      (* xiに上書きされた場合にはそれ以上必要でない *)
      | Put (x, y, z) when memi y env && (x, findi y env) = xi -> `Killed
      | Let ((x, t), e1, e2) ->
          let res = f env e1 in
            (match res with
               | `Killed -> `Killed
               | `Needed -> `Needed
               | _ -> f (M.add x e1 env) e2)
      | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
          if f env e1 = `Killed && f env e2 = `Killed then `Killed
          else `Needed
      | LetTuple (_, _, e) -> f env e
      | Call (("read_int" | "read_float"), _) -> `Unknown
      | Call _ | Load _ | Get _ -> `Needed
      | _ -> `Unknown
    in
      not (f env e = `Killed)
  in
  let rec g env = function
    | IfEq (x, y, e1, e2) -> IfEq (x, y, g env e1, g env e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, g env e1, g env e2)
    | Let ((u, Type.Unit), Put (x, y, z), e) when memi y env ->
        if is_needed env (x, findi y env) e then
          Let ((u, Type.Unit), Put (x, y, z), g env e)
        else
          (* このPutが上書きされる場合は消去する *)
          g env e
    | Let ((x, t), e1, e2) ->
        let e1 = g env e1 in
        let env = M.add x e1 env in
        let e2 = g env e2 in
        if has_effect e1 || S.mem x (fv e2) then
          Let ((x, t), e1, e2)
        else
          ((*Format.eprintf "eliminating variable %s@." x;*)
           e2)
    | LetTuple (xts, y, e) when memt y env ->
        List.fold_left2
          (fun e xt z ->
             Let (xt, Var (z), e))
          (g env e) xts (findt y env)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, g env e)
    | e -> e
  in
    g M.empty e


(* 不要な関数と関数適用を消去する. *)
let elim prog =
  let elimed =
    M.fold
      (fun l func acc ->
         (* 自己関数呼び出し以外に副作用があるか調べる *)
         let rec f = function
           | Let (_, e1, e2) | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) -> f e1 || f e2
           | LetTuple (_, _, e) -> f e
           | Unit | Int _ | Float _ | Add _ | Sub _ | ShiftL _ | ShiftR _ | FNeg _ | FAbs _
           | FAdd _ | FSub _ | FMul _ | FInv _ | Var _ | Tuple _ | Get _ | ExtArray _
           | FSqrt _ | ItoF _ | FtoI _ -> false
           | Call (l', _) when l = l' -> false
           | _ -> true
         in
         let t = match func.typ with Type.Fun (_, t) -> t | _ -> assert false in
           if t = Type.Unit && not (f func.body) then
             (Format.eprintf "%s is not needed@." l;
              S.add l acc)
           else
             acc)
      prog S.empty
  in
  let prog = M.filter (fun l _ -> not (S.mem l elimed)) prog in
    M.map
      (fun func ->
         let rec f = function
           | Let ((x, t), e1, e2) ->
               Let ((x, t), f e1, f e2)
           | LetTuple (xts, y, e) ->
               LetTuple (xts, y, f e)
           | IfEq (x, y, e1, e2) -> IfEq (x, y, f e1, f e2)
           | IfLE (x, y, e1, e2) -> IfLE (x, y, f e1, f e2)
           | Call (x, _) when S.mem x elimed -> Unit
           | e -> e
         in
           { func with
                 body = f func.body })
      prog

      (*
(* progに含まれるヒープ上に配置される配列を, キャッシュに割り当てるように変更する. *)
let allocate_to_cache prog =
  (*
  let rec acc = ref S.empty in
  let updated = ref false in
  let add x = if not (S.mem x !acc) then (updated := true; acc := S.add x !acc) in
  (* env: 関数名 -> その関数の返り値がキャッシュに割り当てられるべきか. *)
  let rec f env = function
    | Call (("create_array" | "create_float_array" | "allocate"), _) ->
        true
    | Let ((x, t), e1, e2) ->
        if f env e1 then add x;
        f env e2
    | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) ->
        let b1 = f env e1 in
        let b2 = f env e2 in
        if b1 <> b2 then failwith "failed to allocate heap to cache";
        b1
    | LetTuple (xts, y, e) ->
        f env e
    | Var (x) -> S.mem x !acc
    | _ -> false
  in
  let rec loop env =
    updated := false;
    let env' =
      M.mapi
        (fun l func -> f env None func.body)
        prog
    in
    M.iter
      (fun l _ ->
         (*
         Format.eprintf "func %s: %s@." l (String.concat "; " (List.map string_of_pos (M.find l env)));
          *)
         if M.find l env <> M.find l env' then
           updated := true)
      prog;
    if !updated then
      loop env
    else
      !acc
  in
  let res = loop (M.map (fun _ -> []) prog) in
    res
   *)
  prog


       *)

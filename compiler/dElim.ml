(* 生存解析による不要文除去.
 * ただし, レジスタへの代入は消去しないことにする． *)

open Block

(* レジスタの値を変える以外の意味で副作用があるか *)
let has_effect = function
  | Entry | Return | Call _ | Store _ | FStore _ | StoreC _ | FStoreC _ | Send _ | Recv -> true
  | exp -> is_branch exp

let g cfg =
  let liveness = Liveness.create cfg in
  let cfg =
    G.map
      (fun b stmts ->
         snd
           (List.fold_right
              (fun stmt (live, stmts) ->
                 let stmts =
                   if not (has_effect stmt.exp) &&
                      List.for_all
                        (fun (x, _) -> not (S.mem x live))
                        stmt.def &&
                      (* ゼロレジスタとヒープレジスタの設定は削除しない.
                       * 特に, ゼロレジスタは最適化によって新たに現れる可能性がある. *)
                      (match stmt.def with [x, Type.Int] when x = Asm.zero || x = Asm.heap -> false | _ -> true)
                   then
                     stmts
                   else
                     stmt :: stmts
                 in
                   (Liveness.transfer stmt live, stmts))
              stmts (Liveness.live_of b liveness, [])))
      cfg
  in
    cfg

let f prog = map_cfg (fun l cfg -> g cfg) prog

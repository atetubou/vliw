(* プロファイル情報によってブロックの削除を行う.
 * 必要なブロックを--elim <file>として与えてやると, それ以外のブロックが削除される. *)

open Vliw

let g needed cfg =
  G.fold
    (fun b _ cfg ->
       if not (S.mem b needed) then
         (* 先行ブロックがif文であればJmpに置き換える *)
         let cfg =
           List.fold_left
             (fun cfg b' ->
                try
                  (match partition_block (G.find b' cfg) with
                     | (stmts, ({ right = { exp = IfEq _ | IfLt _ | IfFEq _ | IfFLt _ } } as branch), delay) ->
                         (* Jmpで置き換え. *)
                         let cfg = G.update_block b' (stmts @ [{ branch with right = gen_half_stmt [] Jmp }] @ delay) cfg in
                         (* 辺をOtherで置き換え *)
                         let cfg = G.expand (fun b e_tag -> G.add_edge b' b Other) b' cfg cfg in
                           cfg
                     | _ -> cfg)
                with Invalid_argument "partition_block" -> cfg)
             cfg (G.pred b cfg)
         in
           G.remove_vertex b cfg
       else
         cfg)
    cfg cfg

let f elim_file prog =
  Format.eprintf "elim with profile@.";
  let ic = open_in elim_file in
  let rec loop acc =
    try
      loop (S.add (input_line ic) acc)
    with End_of_file -> acc
  in
  let needed = loop S.empty in
    map_cfg (fun _ -> g needed) prog


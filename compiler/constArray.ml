open KNormal

(* 配列がエスケープするか; 配列がGet or Putの第一引数以外で現れるか *)
let rec escape x = function
  | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2)
  | Let (_, e1, e2) | LetRec ({ body = e1 }, e2) -> escape x e1 || escape x e2
  | LetTuple (_, _, e) -> escape x e
  | Var y | Put (_, _, y) -> x = y
  | App (_, ys) | ExtFunApp (_, ys) | Tuple ys -> List.exists (fun y -> x = y) ys
  | _ -> false

let rec has_app = function (* 関数適用があるか *)
  | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2)
  | Let (_, e1, e2) | LetRec ({ body = e1 }, e2) -> has_app e1 || has_app e2
  | LetTuple (_, _, e) -> has_app e
  | App (_, ys) -> true
  | _ -> false

(* TODO: 命令を入れ替えるには...
 * メモリロードGetはPutによって入れ替えられなくなる.
 * その条件をいれなければいけない. *)
let rec insert z k = function (* xの定義kを挿入する. ただし、副作用があってはいけない *)
  | IfEq (x, y, _, _) | IfLE (x, y, _, _) as e when x = z || y = z -> k e
  | IfEq (x, y, e1, e2) ->
      (match (S.mem z (fv e1), S.mem z (fv e2)) with
         | (true, true) -> k (IfEq (x, y, e1, e2))
         | (true, false) -> IfEq (x, y, insert z k e1, e2)
         | (false, true) -> IfEq (x, y, e1, insert z k e2)
         | (false, false) -> assert false) (* elim.mlで削除されるはず *)
  | IfLE (x, y, e1, e2) ->
      (match (S.mem z (fv e1), S.mem z (fv e2)) with
         | (true, true) -> k (IfLE (x, y, e1, e2))
         | (true, false) -> IfLE (x, y, insert z k e1, e2)
         | (false, true) -> IfLE (x, y, e1, insert z k e2)
         | (false, false) -> assert false)
  | Let (xt, e1, e2) ->
      if S.mem z (fv e1) then
        k (Let (xt, e1, e2))
      else
        Let (xt, e1, insert z k e2)
  | LetRec ({ body = e1 } as fundef, e2) ->
      (match (S.mem z (fv e1), S.mem z (fv e2)) with
         | (true, true) | (true, false) -> k (LetRec (fundef, e2)) (* 関数内まではinsertしない。kに自由変数が含まれている可能性があるため *)
         | (false, true) -> LetRec ({ fundef with body = e1 }, insert z k e2)
         | (false, false) -> assert false)
  | LetTuple (_, y, _) as e when y = z -> k e
  | LetTuple (xts, y, e) -> LetTuple (xts, y, insert z k e)
  | e -> k e

(* 副作用の無いLetとLetRecの定義をなるべく下に配置する *)
let rec localize = function (* 単体で実行すると命令数はなぜか増加する *)
  | Let ((x, t), e1, e2) ->
      let e1 = localize e1 in
      let e2 = localize e2 in
        if Elim.effect e1 then
          Let ((x, t), e1, e2)
        else
          insert x (fun e -> Let ((x, t), e1, e)) e2
  | LetRec ({ name = (x, t); args = yts; body = e1 } as fundef, e2) ->
      let e1 = localize e1 in
      let e2 = localize e2 in
        insert x (fun e -> LetRec ({ fundef with body = e1 }, e)) e2
  | LetTuple (xts, y, e) -> LetTuple (xts, y, localize e)
  | IfEq (x, y, e1, e2) -> IfEq (x, y, localize e1, localize e2)
  | IfLE (x, y, e1, e2) -> IfLE (x, y, localize e1, localize e2)
  | e -> e

(* トップレベルで定義される配列を定数化する *)
let rec const e =
  let memi x env = M.mem x env && (match M.find x env with Int _ -> true | _ -> false) in
  let findi x env = match M.find x env with Int i -> i | _ -> assert false in
  let memc x env = M.mem x env && (match M.find x env with Int _ | Float _ -> true | _ -> false) in
  (* 定数化する必要条件
   * (1) トップレベル以外でPutされない
   * (2) (配列を定義した後の) 関数適用の後にPutが起きないか
   * (3) インデクスの指定は定数か
   *)
  (* f: 上の(1), (2), (3)を調べる。 allow: この先でPutしてよいか *)
  let rec check env z e =
    (* この定義だと、多くの自由変数が現れてしまい、今のところは非効率である。 *)
    let rec f env allow = function
      | IfEq (_, _, e1, e2) | IfLE (_, _, e1, e2) -> f env false e1 && f env false e2
      | Let ((x, _), e1, e2) ->
          f env allow e1 && f (M.add x e1 env) (allow && not (has_app e1)) e2  (* 関数適用があればその後はPutできない *)
      | LetRec ({ body = e1 }, e2) -> f env false e1 && f env allow e2
      | LetTuple (_, _, e) -> f env allow e
      | Put (x, i, y) when x = z && not (memi i env) -> (*Format.eprintf "%s: not const index(put)@." z;*) false
      | Put (x, i, y) when x = z && not allow ->        (*Format.eprintf "%s: in function put@." z;     *) false
(*      | Put (x, i, y) when x = z && not (memc y env) -> false (* [XXX] 代入される値は定数か? *)*)
      | Get (x, i) when x = z && not (memi i env) ->    (*Format.eprintf "%s: not const index(get)@." z;*) false
      | _ -> true
    in
      f env true e
  in
  let rec g env arr toplevel = function
    | Let ((x, (Type.Array t)), ExtFunApp (("create_array" | "create_float_array"), [xn; xi]), e2)
      when toplevel && memi xn env && not (escape x e2) && check env x e2 && memc xn env (* [XXX] 配列の初期値が定数か? *)->
        let n = findi xn env in (* 配列を定数化する *)
        let arr = M.add x (Array.create n xi) arr in
          (Format.eprintf "'%s' is a const array@." x; g env arr toplevel e2)
    | Let ((x, t), e1, e2) ->
        let e1 = g env arr toplevel e1 in
        let env = M.add x e1 env in
        let e2 = g env arr toplevel e2 in
          Let ((x, t), e1, e2)
    | IfEq (x, y, e1, e2) -> IfEq (x, y, g env arr toplevel e1, g env arr toplevel e2)
    | IfLE (x, y, e1, e2) -> IfLE (x, y, g env arr toplevel e1, g env arr toplevel e2)
    | LetTuple (xts, y, e) -> LetTuple (xts, y, g env arr toplevel e)
    | LetRec ({ body = e1 } as fundef, e2) -> LetRec ({ fundef with body = g env arr false e1 }, g env arr toplevel e2)
    | Get (x, y) when M.mem x arr ->
        let a = M.find x arr in
        let index = assert (memi y env); findi y env in
          Var (a.(index))
    | Put (x, y, z) when M.mem x arr ->
        let a = M.find x arr in
        let index = assert (memi y env); findi y env in
          a.(index) <- z; Unit
    | e -> e
  in
    g M.empty M.empty true e

let f e =
  try
    let e = Elim.f e in (* localizeではelimを前提として最適化する *)
    let e = localize e in (* 関数の定義などを、ギリギリまで引き延ばして配列の定数化の条件を求めやすくする *)
    let e = const e in
      e
  with Invalid_argument _ -> Format.eprintf "Warning: detected index out of bounds. give up ConstArray.@."; e  (* 定数の配列外のアクセスがあった場合 *)



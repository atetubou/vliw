(* 関数の入口/出口生存変数について.
 *
 * 関数の入口にはEntry, 出口にはReturnという式がある.
 * Entryは入口生存な変数を仮想的に定義する.
 * Returnは出口生存な変数を仮想的に使用する.
 *
 * グローバル変数は、メイン関数が定義して各関数に渡して値を変更する, という変数である.
 * すなわち全てのCallのuse, defに含まれる.
 * ただし、関数の入口と出口で値が変わらないことが保証されているものは, useにのみ含まれる.
 * e.g. $stack, $zero
 *)

open Block

(* while-loopをインライン展開するしきい値 *)
let threshold = ref (1000)


type data = {
  (* while-loopに展開できる関数全体の部分集合. *)
  known : S.t;
  (* プログラム全体の情報. *)
  prog : program;
}


(* 関数呼び出しグラフから,
 * 関数をインライン展開したときのコードサイズと、しなかったときのサイズの差を求める. *)
let inline_cost_of l g =
  (* インライン展開される回数を計算する. *)
  let count =
    List.fold_left
      (fun size r ->
         (* 自己末尾再帰はインライン展開に関係ないので無視. *)
         if l = r then size
         else
           size + F.tag_of_edge r l g)
      0 (F.pred l g)
  in
    (* (インライン展開される回数 - 1) * 関数lのコードサイズ *)
    (count - 1) * (F.find l g)




(* cfg'をcfgの内部へコピーする.
 * v_exit: cfg'におけるReturnの戻り先. (cfg上のブロック)
 * conv: cfg'のブロック名 -> cfg内でのブロック名
 * alpha: cfg'の変数名 -> cfg内での変数名 *)
let copy_from_other cfg cfg' v_exit =
  let (conv, cfg, alpha) =
    G.fold
      (fun v stmts (conv, cfg, alpha) ->
         (* 文をアルファ変換する. *)
         let (alpha, stmts) =
           List.fold_right
             (fun stmt (alpha, acc) ->
                let (alpha, stmt) = alpha_stmt alpha stmt in
                  (alpha, stmt :: acc))
             stmts (alpha, [])
         in
         let (v', cfg) =
           (* Returnがある場合はv_exitへジャンプ. *)
           match partition_last stmts with
             | ({ exp = Return }, stmts) ->
                 let stmts = stmts @ [gen_stmt [] Jmp] in
                 let (v', cfg) = G.add_block stmts cfg in
                   (v', G.add_edge v' v_exit Other cfg)
             | _ -> G.add_block stmts cfg
         in
         let conv = M.add v v' conv in
           (conv, cfg, alpha))
      cfg' (M.empty, cfg, M.empty)
  in
  (* 辺をコピーする *)
  let cfg =
    G.fold_edge
      (fun v w e_tag cfg ->
         G.add_edge (M.find v conv) (M.find w conv) e_tag cfg)
      cfg' cfg
  in
  (* SetLabelに含まれるブロックのラベルを付け替える. *)
  let cfg =
    G.map_stmt
      (function
         | { exp = SetLabel (l) } as stmt when not (G.mem l cfg) && M.mem l conv ->
             { stmt with exp = SetLabel (M.find l conv) }
         | stmt -> stmt)
      cfg
  in
    (conv, alpha, cfg)

(* 新しいブロックをvに続けて作る.
 * その新しいブロックと, グラフの組を返す. *)
let add_block stmts v cfg =
  let (v', cfg) = G.add_block stmts cfg in
  let cfg = G.add_edge v v' Other cfg in
    (v', cfg)

(* ブロックの最後にJmpを追加してからadd_block *)
let add_stmts stmts =
  add_block (stmts @ [gen_stmt [] Jmp])

(* 複数の変数をコピーするブロックをvに続けて作る. *)
let add_copies dest src v cfg =
  if not (List.length dest = List.length src) then (
    Format.eprintf "%s@. %s@." (String.concat ", " (List.map (fun (x, t) -> x ^ " : " ^ Type.string_of t) dest)) (String.concat ", " src)
  );
  assert (List.length dest = List.length src);
  let stmts = move_list dest src in
    add_stmts stmts v cfg

(* Unit型を取り除く *)
let filter_unit = List.filter (function (_, Type.Unit) -> false | _ -> true)

(* 関数target_funcをCFGに変換する. *)
let cfg_of_function data target_func virt =
  (* while-loopとして展開できるか. *)
  let is_proc = ref true in
(*  let { Virtual.cache = in_cache } = virt in*)
  (* while-loopを既にCFG上に展開している関数 -> (loopの開始ブロック) * (リターンアドレスを保存する変数) *)
(*  let loop_env = ref M.empty in*)
  (* ifの分岐を実現する *)
  let rec add_if env v cfg dest tail e1 e2 branch =
    let (v, cfg) = add_block [gen_stmt [] branch] v cfg in
    (* v1: Then節の開始ブロック, v2: Else節の開始ブロック. *)
    let (v1, cfg) = add_stmts [] v cfg in
    let cfg = G.add_edge v v1 Then cfg in
    let (v2, cfg) = add_stmts [] v cfg in
    let cfg = G.add_edge v v2 Else cfg in
    (* v1からv1'の間のブロックがe1に対応する. *)
    let (v1', cfg) = g env v1 cfg dest tail e1 in
    let (v2', cfg) = g env v2 cfg dest tail e2 in
    (* v': Then, Elseの合流点. *)
    let (v', cfg) = G.add_block [gen_stmt [] Jmp] cfg in
      (* 自己末尾再帰がある場合( = G.null_blockであるとき)は, 合流しないので辺をはらない *)
    let cfg = if v1' = G.null_block then cfg else G.add_edge v1' v' Other cfg in
    let cfg = if v2' = G.null_block then cfg else G.add_edge v2' v' Other cfg in
      (v', cfg)
  (* vに続けてCFGに命令を追加して、最後のブロックとグラフの組を返す.
   * env: 変数名 -> その変数の型 *)
  and g env v cfg dest tail = function
    | Virtual.Unit              -> (v, cfg)
    | Virtual.Int (i)           -> add_stmts [gen_stmt dest (Set (i))] v cfg
    | Virtual.Float (f)         -> add_stmts [gen_stmt dest (FSet (f))] v cfg
    | Virtual.Add (x, y)        -> add_stmts [gen_stmt dest (Add (V (x), y))] v cfg
    | Virtual.Sub (x, y)        -> add_stmts [gen_stmt dest (Sub (V (x), y))] v cfg
    | Virtual.ShiftL (x, i)     -> add_stmts [gen_stmt dest (ShiftL (x, i))] v cfg
    | Virtual.ShiftR (x, i)     -> add_stmts [gen_stmt dest (ShiftR (x, i))] v cfg
    | Virtual.FNeg (x)          -> add_stmts [gen_stmt dest (FMov (x, [NegBit]))] v cfg
    | Virtual.FAbs (x)          -> add_stmts [gen_stmt dest (FMov (x, [AbsBit]))] v cfg
    | Virtual.FAdd (x, y)       -> add_stmts [gen_stmt dest (FAdd (x, y, []))] v cfg
    | Virtual.FSub (x, y)       -> add_stmts [gen_stmt dest (FAdd (x, y, [FPUBit]))] v cfg
    | Virtual.FMul (x, y)       -> add_stmts [gen_stmt dest (FMul (x, y, []))] v cfg
    | Virtual.FInv (x)          -> add_stmts [gen_stmt dest (FInv (x, []))] v cfg
    | Virtual.FSqrt (x)         -> add_stmts [gen_stmt dest (FSqrt (x, []))] v cfg
    | Virtual.Send (x)          -> add_stmts [gen_stmt dest (Send (x))] v cfg
    | Virtual.Recv              -> add_stmts [gen_stmt dest (Recv)] v cfg
    | Virtual.ItoF (x)          -> add_stmts [gen_stmt dest (ItoF (x))] v cfg
    | Virtual.FtoI (x)          -> add_stmts [gen_stmt dest (FtoI (x))] v cfg
    | Virtual.Load (t, y, i)       ->
        let (x, t) = match dest with [x, t'] -> assert (t = t'); (x, t) | _ -> Format.eprintf "failed %s %d@." y i;assert false in
          add_stmts [gen_load t x (y, C (i))] v cfg
    | Virtual.Store (x, y, i)   ->
        add_stmts [gen_store (M.find x env) x (y, C (i))] v cfg
    | Virtual.IfEq (x, y, e1, e2) ->
        (match M.find x env with
           | Type.Bool | Type.Int | Type.List _ ->
               add_if env v cfg dest tail e1 e2 (IfEq (x, V (y)))
           | Type.Float ->
               add_if env v cfg dest tail e1 e2 (IfFEq (x, y))
           | _ -> failwith "equality supported only for bool, int, list, and float")
    | Virtual.IfLE (x, y, e1, e2) ->
        (match M.find x env with
           | Type.Bool | Type.Int ->
               add_if env v cfg dest tail e2 e1 (IfLt (y, V (x)))
           | Type.Float ->
               add_if env v cfg dest tail e2 e1 (IfFLt (y, x))
           | _ -> failwith "inequality supported only for bool, int, and float")
    | Virtual.Let ((x, t), e1, e2) ->
        (* Unit型のdestには意味がないので取り除く. *)
        let (v, cfg) = g env v cfg (filter_unit [x, t]) false e1 in
        let env = M.add x t env in
        let (v, cfg) = g env v cfg dest tail e2 in
          (v, cfg)
    | Virtual.Var (x) ->
        (match M.find x env with
           | Type.Unit  -> (v, cfg)
           | Type.Float -> add_stmts [gen_stmt dest (FMov (x, []))] v cfg
           | _          -> add_stmts [gen_stmt dest (Mov (x))] v cfg)

    | Virtual.Call (l, actual_args) ->
        let actual_args = List.filter (fun x -> M.find x env <> Type.Unit) actual_args in
        let func = M.find l data.prog.entries in
          (match () with
             (* 自己末尾再帰の場合は, Jmpに変換する. *)
             | _ when tail && l = target_func ->
                 (* 実引数を仮引数へコピー. *)
                 let (v, cfg) = add_copies func.args actual_args v cfg in
                 (* 関数の先頭へJmp *)
                 let cfg = G.add_edge v func.v_entry Other cfg in
                   (* if文と合流しないので, G.null_blockを返す. *)
                   (G.null_block, cfg)

             (* while-loopに展開できる場合 *)
             | _ when S.mem l data.known && inline_cost_of l data.prog.call_graph <= !threshold ->
(*                 Format.eprintf "while-loop inlining %s with cost = %d@." l (inline_cost_of l data.prog.call_graph);*)
                 let (v_exit, cfg) = G.add_block [gen_stmt [] Jmp] cfg in
                 let (conv, alpha, cfg) = copy_from_other cfg func.cfg v_exit in
                 (* 引数をセットする. ただし, func.argsはalphaで変換されている.
                  * もしalphaに入っていない場合は、その引数は使われないということである。
                  * その場合は転送元と転送先を同じにして, Move命令が挿入されないようにする. *)
                 let (v, cfg) =
                   let args = List.map2 (fun (x, t) y -> if M.mem x alpha then (M.find x alpha, t) else (y, t)) func.args actual_args in
                     add_copies args actual_args v cfg
                 in
                 (* vからwhile-loopの先頭へ. *)
                 let cfg = G.add_edge v (M.find func.v_entry conv) Other cfg in
                 (* while-loopの結果をdestへ代入するブロック *)
                 let (v_exit, cfg) =
                   (* alphaに含まれていない場合もある. 例えばlet rec loop () = loop ()とすると, 返り値は陽に使われないのでそうなる. *)
                   let rets = List.map2 (fun (x, _) (y, _) -> if M.mem x alpha then M.find x alpha else y) func.rets dest in
                     add_copies dest rets v_exit cfg
                 in
                   (v_exit, cfg)
             (* 単純なCallにするしかない場合. *)
             | _ ->
                 Format.eprintf "give up while-loop inlining %s with cost = %d@." l (inline_cost_of l data.prog.call_graph);
                 (* 自己末尾再帰でない再帰がある場合, while-loopに展開できなくなる. *)
                 if l = target_func then is_proc := false;
                 let (v, cfg) = add_stmts [gen_stmt dest (Call (l, actual_args))] v cfg in
                   (v, cfg)
          )

    | Virtual.Tuple (xs) ->
        let addr = match dest with [(x, _)] -> x | _ -> assert false in
        let (stmts, len) =
          List.fold_left
            (fun (acc, len) x ->
               match M.find x env with
                 | Type.Float ->
                     (gen_stmt [] (FStore (x, addr, C (len))) :: acc, len + 1)
                 | _ ->
                     (gen_stmt [] ( Store (x, addr, C (len))) :: acc, len + 1))
            ([], 0) xs
        in
        (* destにタプルの先頭アドレスを代入, ヒープを伸張. *)
        let stmts =
          [gen_stmt dest (Mov (Asm.heap))] @
          stmts @
          [gen_stmt [Asm.heap, Type.Int] (Add (C (len), Asm.heap))]
        in
          add_stmts stmts v cfg

    | Virtual.LetTuple (xts, y, e) ->
        let fv = Virtual.fv e in
        let (stmts, _) =
          List.fold_left
            (fun (acc, len) (x, t) ->
               match t with
                 (* Unit型への代入はスキップ. *)
                 | Type.Unit -> (acc, len + 1)
                 (* 後で使われないものは無視する. *)
                 | _ when not (S.mem x fv) -> (acc, len + 1)
                 | _ ->
                     (gen_load t x (y, C (len)) :: acc, len + 1))
            ([], 0) xts
        in
        let (v, cfg) = add_stmts stmts v cfg in
        (* 環境を更新する. *)
        let env = M.add_list xts env in
          g env v cfg dest tail e

            (*
    | Virtual.Get (x, y) ->
        (match S.mem x in_cache, M.find x env with
           | _    , Type.Array (Type.Unit)  -> (v, cfg)
           | false, Type.Array (Type.Float) -> add_stmts [gen_stmt dest (FLoad  (x, V (y)))] v cfg
           | false, Type.Array _            -> add_stmts [gen_stmt dest ( Load  (x, V (y)))] v cfg
           | true , Type.Array (Type.Float) -> add_stmts [gen_stmt dest (FLoadC (x, V (y)))] v cfg
           | true , Type.Array _            -> add_stmts [gen_stmt dest ( LoadC (x, V (y)))] v cfg
           | _ -> assert false)
    | Virtual.Put (x, y, z) ->
        (match S.mem x in_cache, M.find x env with
           | _    , Type.Array (Type.Unit)  -> (v, cfg)
           | false, Type.Array (Type.Float) -> add_stmts [gen_stmt dest (FStore  (z, x, V (y)))] v cfg
           | false, Type.Array _            -> add_stmts [gen_stmt dest ( Store  (z, x, V (y)))] v cfg
           | true , Type.Array (Type.Float) -> add_stmts [gen_stmt dest (FStoreC (z, x, V (y)))] v cfg
           | true , Type.Array _            -> add_stmts [gen_stmt dest ( StoreC (z, x, V (y)))] v cfg
           | _ -> assert false)
             *)
    | Virtual.Get (x, y) ->
        (match M.find x env with
           | Type.Array (Type.Unit)   -> (v, cfg)
           | Type.Array (Type.Float)  -> add_stmts [gen_stmt dest (FLoad  (x, V (y)))] v cfg
           | Type.Array _             -> add_stmts [gen_stmt dest ( Load  (x, V (y)))] v cfg
           | Type.ArrayC (Type.Unit)  -> (v, cfg)
           | Type.ArrayC (Type.Float) -> add_stmts [gen_stmt dest (FLoadC (x, V (y)))] v cfg
           | Type.ArrayC _            -> add_stmts [gen_stmt dest ( LoadC (x, V (y)))] v cfg
           | _ -> assert false)
    | Virtual.Put (x, y, z) ->
        (match M.find x env with
           | Type.Array (Type.Unit)   -> (v, cfg)
           | Type.Array (Type.Float)  -> add_stmts [gen_stmt dest (FStore  (z, x, V (y)))] v cfg
           | Type.Array _             -> add_stmts [gen_stmt dest ( Store  (z, x, V (y)))] v cfg
           | Type.ArrayC (Type.Unit)  -> (v, cfg)
           | Type.ArrayC (Type.Float) -> add_stmts [gen_stmt dest (FStoreC (z, x, V (y)))] v cfg
           | Type.ArrayC _            -> add_stmts [gen_stmt dest ( StoreC (z, x, V (y)))] v cfg
           | _ -> assert false)

    | Virtual.ExtArray (x) ->
        (* virtual.mlで保証されているはず *)
        assert (M.mem x !Global.all);
        (* 今のところはラベルを設定するだけにする. *)
        add_stmts [gen_stmt dest (SetLabel (x))] v cfg

  in
  let func = M.find target_func data.prog.entries in
  let (v, cfg) =
    let { Virtual.body = body; Virtual.typ = t; } = virt in
    (* 環境には変数の型をいれておく. *)
    let env = M.add target_func t (M.add_list func.args M.empty) in
      g env func.v_entry func.cfg func.rets true body
  in
  (* while-loopに展開できるならば, knownに新たに追加する. *)
  let data = { data with known = if !is_proc then S.add target_func data.known else data.known } in
    if not !is_proc then Format.eprintf "'%s' is an essential function.@." target_func;
    (* 最後にReturnを追加する. *)
    let (v, cfg) =
      if v = G.null_block then
        (* 関数に出口がない場合 *)
        ( Format.eprintf "Warning: An infinite loop is detected in '%s'.@." target_func; (v, cfg) )
      else
        (* Returnは出口生存なものを保持する. すなわち返り値を保持する. *)
        let live_out = List.map fst func.rets in
          add_block [gen_return live_out] v cfg
    in
    let func = { func with cfg = cfg } in
    let data = { data with prog = { data.prog with entries = M.add target_func func data.prog.entries } } in
      data




(* 組み込み関数やヒープポインタの設定などを行う.
 * 組み込み関数はwhile-loopに変換できるので, knownに挿入する.
 * 関数呼び出しグラフにも挿入する. コードサイズは適当に定める. *)
let build_stub data =
  let add_func l func data =
    { data with prog = { data.prog with entries = M.add l func data.prog.entries } }
  in
  (* 各レジスタの初期設定を行う. *)
  let data =
    let { v_entry = v_entry; cfg = cfg } as main = M.find Id.main_label data.prog.entries in
    let (branch, stmts) = partition_last (G.find v_entry cfg) in
    let stmts =
      stmts @
      [gen_stmt [(Asm.zero, Type.Int)] (Set (0));
       gen_stmt [(Asm.heap, Type.Int)] (Set (Asm.heap_top));
       branch]
    in
    let cfg = G.update_block v_entry stmts cfg in
    let data = { data with prog = { data.prog with entries = M.add Id.main_label { main with cfg = cfg } data.prog.entries } } in
      data
  in
    (*
  (* read_int *)
  let data =
    let l = "read_int" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 10 data.prog.call_graph } }
    in
    let res = Id.genid "res_of_read_int" in
    let stmts =
      (* 一番初めのrecvは, resの上位24bitの値を使用しないのでfalse.
       * このように扱わないと、プログラムがstrictにならない. *)
      [gen_recv res false;
       gen_stmt [res, Type.Int] (ShiftL (res, 8));
       gen_recv res true;
       gen_stmt [res, Type.Int] (ShiftL (res, 8));
       gen_recv res true;
       gen_stmt [res, Type.Int] (ShiftL (res, 8));
       gen_recv res true;
       gen_return [res]]
    in
    let cfg = G.empty in
    let (v, cfg) = G.add_block stmts cfg in
    let func = {
      cfg = cfg;
      v_entry = v;
      (* 本当はUnit型の引数を持つが、Unit型の引数はgen_stmtで削除される. *)
      args = [];
      rets = [res, Type.Int];
      stack_size = 0;
    }
    in
      add_func l func data
  in
  (* read_float *)
  let data =
    let l = "read_float" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 10 data.prog.call_graph } }
    in
    let x = Id.genid "temp" in
    let res = Id.genid "res_of_read_float" in
    let stmts =
      [gen_recv x false;
       gen_stmt [x, Type.Int] (ShiftL (x, 8));
       gen_recv x true;
       gen_stmt [x, Type.Int] (ShiftL (x, 8));
       gen_recv x true;
       gen_stmt [x, Type.Int] (ShiftL (x, 8));
       gen_recv x true;
       gen_stmt [res, Type.Float] (ItoF (x));
       gen_return [res]]
    in
    let cfg = G.empty in
    let (v, cfg) = G.add_block stmts cfg in
      add_func l { cfg = cfg; v_entry = v; args = []; rets = [res, Type.Float]; stack_size = 0 } data
  in
     *)
    (*
  (* init_array *)
  let data =
    let l = "init_array" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 5 data.prog.call_graph } }
    in
    let pos = Id.gentmp Type.Int in
    let n = Id.gentmp Type.Int in
    let x = Id.gentmp Type.Int in
    let cfg = G.empty in
    let (v_entry, cfg) = G.add_block [gen_stmt [] Jmp] cfg in
    let (v_if, cfg) = G.add_block [gen_stmt [] (IfLt (n, C 1))] cfg in
    let (v_exit, cfg) = G.add_block [gen_return []] cfg in
    let cfg = G.add_edge v_if v_exit Then cfg in
    let stmts =
      [gen_stmt [n, Type.Int] (Add (C (-1), n));
       gen_stmt [] (Store (x, pos, V (n)));
       gen_stmt [] Jmp]
    in
    let (v, cfg) = G.add_block stmts cfg in
    let cfg = G.add_edge v_entry v_if Other cfg in
    let cfg = G.add_edge v v_if Other cfg in
    let cfg = G.add_edge v_if v Else cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [(pos, Type.Int); (n, Type.Int); (x, Type.Int)]; rets = []; stack_size = 0 } data
  in
  (* init_float_array *)
  let data =
    let l = "init_float_array" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 5 data.prog.call_graph } }
    in
    let pos = Id.gentmp Type.Int in
    let n = Id.gentmp Type.Int in
    let f = Id.gentmp Type.Float in
    let cfg = G.empty in
    let (v_entry, cfg) = G.add_block [gen_stmt [] Jmp] cfg in
    let (v_if, cfg) = G.add_block [gen_stmt [] (IfLt (n, C 1))] cfg in
    let (v_exit, cfg) = G.add_block [gen_return []] cfg in
    let cfg = G.add_edge v_if v_exit Then cfg in
    let stmts =
      [gen_stmt [n, Type.Int] (Add (C (-1), n));
       gen_stmt [] (FStore (f, pos, V (n)));
       gen_stmt [] Jmp]
    in
    let (v, cfg) = G.add_block stmts cfg in
    let cfg = G.add_edge v_entry v_if Other cfg in
    let cfg = G.add_edge v v_if Other cfg in
    let cfg = G.add_edge v_if v Else cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [(pos, Type.Int); (n, Type.Int); (f, Type.Float)]; rets = []; stack_size = 0 } data
  in
     *)
  (* allocate *)
  let data =
    let l = "allocate" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 3 data.prog.call_graph } }
    in
    let n = Id.gentmp Type.Int in
    let res = Id.gentmp Type.Int in
    let stmts =
      [gen_stmt [res, Type.Int] (Mov (Asm.heap));
       gen_stmt [Asm.heap, Type.Int] (Add (V (n), Asm.heap));
       gen_return [res]]
    in
    let cfg = G.empty in
    let (v_entry, cfg) = G.add_block stmts cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [n, Type.Int]; rets = [res, Type.Int]; stack_size = 0 } data
  in
  (* create_array *)
  let data =
    let l = "create_array" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 10 data.prog.call_graph } }
    in
    let n = Id.gentmp Type.Int in
    let x = Id.gentmp Type.Int in
    (* 本当はArray型を返すが、今のところはType.Intとして差し支えない. *)
    let res = Id.gentmp Type.Int in
    let cfg = G.empty in
    let stmts =
      [gen_stmt [res, Type.Int] (Mov (Asm.heap));
       gen_stmt [Asm.heap, Type.Int] (Add (V (n), Asm.heap));
       gen_stmt [] Jmp]
    in
    let (v_entry, cfg) = G.add_block stmts cfg in
    let (v_exit, cfg) = G.add_block [gen_return [res]] cfg in
    let (v_if, cfg) = G.add_block [gen_stmt [] (IfLt (n, C (1)))] cfg in
    let stmts =
      [gen_stmt [n, Type.Int] (Add (C (-1), n));
       gen_stmt [] (StoreC (x, res, V (n)));
       gen_stmt [] Jmp]
    in
    let (v, cfg) = G.add_block stmts cfg in
    let cfg = G.add_edge v_entry v_if Other cfg in
    let cfg = G.add_edge v_if v_exit Then cfg in
    let cfg = G.add_edge v_if v Else cfg in
    let cfg = G.add_edge v v_if Other cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [(n, Type.Int); (x, Type.Int)]; rets = [res, Type.Int]; stack_size = 0 } data
  in
  (* create_float_array *)
  let data =
    let l = "create_float_array" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 10 data.prog.call_graph } }
    in
    let n = Id.gentmp Type.Int in
    let x = Id.gentmp Type.Float in
    let res = Id.gentmp Type.Int in
    let cfg = G.empty in
    let stmts =
      [gen_stmt [res, Type.Int] (Mov (Asm.heap));
       gen_stmt [Asm.heap, Type.Int] (Add (V (n), Asm.heap));
       gen_stmt [] Jmp]
    in
    let (v_entry, cfg) = G.add_block stmts cfg in
    let (v_exit, cfg) = G.add_block [gen_return [res]] cfg in
    let (v_if, cfg) = G.add_block [gen_stmt [] (IfLt (n, C 1))] cfg in
    let stmts =
      [gen_stmt [n, Type.Int] (Add (C (-1), n));
       gen_stmt [] (FStoreC (x, res, V (n)));
       gen_stmt [] Jmp]
    in
    let (v, cfg) = G.add_block stmts cfg in
    let cfg = G.add_edge v_entry v_if Other cfg in
    let cfg = G.add_edge v_if v_exit Then cfg in
    let cfg = G.add_edge v_if v Else cfg in
    let cfg = G.add_edge v v_if Other cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [(n, Type.Int); (x, Type.Float)]; rets = [res, Type.Int]; stack_size = 0 } data
  in
  (* float_of_int *)
  let data =
    let l = "float_of_int" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 15 data.prog.call_graph } }
    in
    let data = { data with known = S.add l data.known } in
    let x = Id.gentmp Type.Int in
    let f = Id.gentmp Type.Float in
    let i = Id.gentmp Type.Int in
    let res = Id.gentmp Type.Float in
    let cfg = G.empty in
    let stmts =
      [gen_stmt [f, Type.Float] ( FSet (8388608.0));
       gen_stmt [i, Type.Int] (FtoI (f));
       gen_stmt [] (IfLt (x, C (0)))]
    in
    let (v_entry, cfg) = G.add_block stmts cfg in
    let (v_exit, cfg) = G.add_block [gen_return [res]] cfg in
    let stmts =
      [gen_stmt [i, Type.Int] (Sub (V (i), x));
       gen_stmt [res, Type.Float] (ItoF (i));
       gen_stmt [res, Type.Float] (FAdd (f, res, [FPUBit]));
       gen_stmt [] Jmp]
    in
    let (v_neg, cfg) = G.add_block stmts cfg in
    let stmts =
      [gen_stmt [i, Type.Int] (Add (V (i), x));
       gen_stmt [res, Type.Float] (ItoF (i));
       gen_stmt [res, Type.Float] (FAdd (res, f, [FPUBit]));
       gen_stmt [] Jmp]
    in
    let (v_pos, cfg) = G.add_block stmts cfg in
    let cfg = G.add_edge v_entry v_neg Then cfg in
    let cfg = G.add_edge v_entry v_pos Else cfg in
    let cfg = G.add_edge v_neg v_exit Other cfg in
    let cfg = G.add_edge v_pos v_exit Other cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [(x, Type.Int)]; rets = [res, Type.Float]; stack_size = 0 } data
  in
  (* int_of_float *)
  let data =
    let l = "int_of_float" in
    let data =
      { known = S.add l data.known;
        prog = { data.prog with call_graph = F.add_vertex l 15 data.prog.call_graph } }
    in
    let x = Id.gentmp Type.Float in
    let f = Id.gentmp Type.Float in
    let z = Id.gentmp Type.Float in
    let i = Id.gentmp Type.Int in
    let res = Id.gentmp Type.Int in
    let cfg = G.empty in
    let stmts =
      [gen_stmt [f, Type.Float] (FSet (8388608.0));
       gen_stmt [i, Type.Int] (FtoI (f));
       gen_stmt [z, Type.Float] (FSet (0.0));
       gen_stmt [] (IfFLt (x, z))]
    in
    let (v_entry, cfg) = G.add_block stmts cfg in
    let (v_exit, cfg) = G.add_block [gen_return [res]] cfg in
    let stmts =
      [gen_stmt [f, Type.Float] (FAdd (f, x, [FPUBit]));
       gen_stmt [res, Type.Int] (FtoI (f));
       gen_stmt [res, Type.Int] (Sub (V (i), res));
       gen_stmt [] Jmp]
    in
    let (v_neg, cfg) = G.add_block stmts cfg in
    let stmts =
      [gen_stmt [f, Type.Float] (FAdd (f, x, []));
       gen_stmt [res, Type.Int] (FtoI (f));
       gen_stmt [res, Type.Int] (Sub (V (res), i));
       gen_stmt [] Jmp]
    in
    let (v_pos, cfg) = G.add_block stmts cfg in
    let cfg = G.add_edge v_entry v_neg Then cfg in
    let cfg = G.add_edge v_entry v_pos Else cfg in
    let cfg = G.add_edge v_neg v_exit Other cfg in
    let cfg = G.add_edge v_pos v_exit Other cfg in
      add_func l { cfg = cfg; v_entry = v_entry; args = [(x, Type.Float)]; rets = [res, Type.Int]; stack_size = 0 } data
  in
    data



(* プログラムの骨組みを構築する. *)
let build virt =
  (* 関数の情報を構築する. *)
  let add_func l { Virtual.body = body; Virtual.args = args; Virtual.typ = t  } data =
    let rets =
      match t with
        (* Unit型の返り値には意味がないので消去. *)
        | Type.Fun (_, Type.Unit) -> []
        | Type.Fun (_, ret_typ) -> [(Id.genid ("RET_" ^ l), ret_typ)]
        | _ -> assert false
    in
    let (v_entry, cfg) = G.add_block [gen_stmt [] Jmp] G.empty in
    let func = {
      cfg = cfg;
      v_entry = v_entry;
      args = filter_unit args;
      rets = rets;
      stack_size = 0;
    }
    in
    let prog = data.prog in
    let prog =
      { prog with
            entries = M.add l func prog.entries;
            call_graph = F.add_vertex l (Virtual.size body) prog.call_graph } in
    let data = { data with prog = prog } in
      data
  in

  let data = {
    known = S.empty;
    prog = {
      call_graph = F.empty;
      entries = M.empty;
      global_variables = [];
    };
  }
  in
  let data = M.fold add_func virt data in
  let data = build_stub data in
  (* 関数呼び出しグラフの構築 *)
  let call_graph =
    let call_graph = data.prog.call_graph in
    let call_graph =
      M.fold
        (fun l { Virtual.body = e } call_graph ->
           let rec f call_graph = function
             | Virtual.Call (r, _) ->
                 if F.mem r call_graph then
                   (* 呼び出し回数を更新する. *)
                   let count = if F.has_edge l r call_graph then F.tag_of_edge l r call_graph else 0 in
                     F.add_edge l r (count + 1) call_graph
                 else
                  (* 外部関数呼び出しがあった場合 *)
                  raise (Virtual.NotSimple (Printf.sprintf "external function call '%s' has been detected" r))
             | Virtual.IfEq (_, _, e1, e2)
             | Virtual.IfLE (_, _, e1, e2)
             | Virtual.Let (_, e1, e2)
               -> f (f call_graph e1) e2
             | Virtual.LetTuple (_, _, e)
               -> f call_graph e
             | _ -> call_graph
           in
             f call_graph e)
        virt call_graph
    in
      call_graph
  in
  let data = { data with prog = { data.prog with call_graph = call_graph } } in
    data

(* 関数呼び出しのうち, while-loopに変換できるが
 * コードサイズの関係から諦めたものを, CFG上にコピーしてくる *)
let inlining_other_cfg data l =
  let rec split_at_call = function
    | [] -> (None, [], [])
    (* while-loopに変換できるような関数へのCallがあった場合 *)
    | { exp = Call (r, _) } as stmt :: stmts when S.mem r data.known ->
        (Some (stmt), [gen_stmt [] Jmp], stmts)
    | stmt :: stmts ->
        let (s, ls, rs) = split_at_call stmts in
          (s, stmt :: ls, rs)
  in
  let rec dfs (vis, cfg, env) b =
    if S.mem b vis then (vis, cfg, env)
    else
      let vis = S.add b vis in
      let (vis, cfg, env) =
        match split_at_call (G.find b cfg) with
          (* Callがなかった場合 *)
          | (None, stmts, []) ->
              (vis, cfg, env)
          (* while-loopに展開できるようなCallであった場合 *)
          | (Some ({ exp = Call (r, actual_args); def = dest }), stmts, stmts')
              when S.mem r data.known
            ->
              let func = M.find r data.prog.entries in
              let cfg = G.update_block b stmts cfg in
              let (b', cfg) = G.add_block stmts' cfg in
              (* bからb'に辺を付け替える *)
              let cfg = G.expand (G.add_edge b') b cfg cfg in
              let cfg = G.expand (fun w _ cfg -> G.remove_edge b w cfg) b cfg cfg in
              let (cfg, env) =
                if M.mem r env then
                  (cfg, env)
                else
                  let link = Id.genid "Link" in
                  let (loop_exit, cfg) = G.add_block [gen_stmt [] Jmp] cfg in
                  let (conv, alpha, cfg) = copy_from_other cfg func.cfg loop_exit in
                  let (loop_exit, cfg) = add_block [gen_stmt [] (JumpTo (link))] loop_exit cfg in
                  let loop_entry = M.find func.v_entry conv in
                    (cfg, M.add r (loop_entry, loop_exit, alpha, link) env)
              in
              let (loop_entry, loop_exit, alpha, link) = M.find r env in

              (* 返り値をretsから取り出すブロック. loop_exitからジャンプしてくる. *)
              let (v', cfg) =
                let rets = List.map (fun (x, _) -> M.find x alpha) func.rets in
                  add_copies dest rets loop_exit cfg
              in
              let cfg = G.add_edge v' b' Other cfg in
              (* 返り先ブロックv'へのラベルをlinkに保存する. *)
              let (b, cfg) = add_stmts [gen_stmt [link, Type.Int] (SetLabel (v'))] b cfg in
              (* 引数をセット *)
              let (b, cfg) =
                let args = List.map2 (fun (x, t) y -> if M.mem x alpha then (M.find x alpha, t) else (y, t)) func.args actual_args in
                  add_copies args actual_args b cfg
              in
              (* while-loopの先頭へジャンプ. *)
              let cfg = G.add_edge b loop_entry Other cfg in
                dfs (vis, cfg, env) v'
          | _ -> assert false
      in
      let (vis, cfg, env) = List.fold_left dfs (vis, cfg, env) (G.succ b cfg) in
        (vis, cfg, env)
  in
  let func = M.find l data.prog.entries in
  let (_, cfg, _) = dfs (S.empty, func.cfg, M.empty) func.v_entry in
    { data with prog = { data.prog with entries = M.add l { func with cfg = cfg } data.prog.entries } }



let f virt =
  let data = build virt in
  let data =
    List.fold_left
      (fun data l ->
         (* stubの関数はvirtには存在しない *)
         if M.mem l virt then
           cfg_of_function data l (M.find l virt)
         else
           data)
      data
      (List.rev (snd (F.topological_sort data.prog.call_graph)))
  in
(*  let data = { data with prog = simplify data.prog } in TODO: 何故かfunc.entryがCFG上になくてassertに失敗するらしい. *)
(*  let data = List.fold_left (fun data l -> inlining_other_cfg data l) data (snd (F.topological_sort data.prog.call_graph)) in*)
  let prog = data.prog in
  let prog =
    { prog with
          entries =
            M.mapi
              (fun l func ->
                 (* ダミーの式Entryによって関数の入口生存なものを定義する. *)
                 let (v, cfg) = G.add_block [gen_entry func.args; gen_stmt [] Jmp] func.cfg in
                 let cfg = G.add_edge v func.v_entry Other cfg in
                   { func with cfg = cfg; v_entry = v })
              prog.entries }
  in
  let prog = simplify prog in
  let prog = add_global_variable ~restrict:true (Asm.heap, Type.Int) prog in
  let prog = add_global_variable ~is_const:true (Asm.zero, Type.Int) prog in
    prog


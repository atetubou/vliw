module type GraphTag =
sig
  (* 頂点の名前. *)
  type vertex
  (* vertexに付随する情報 *)
  type v_tag
  (* 辺に付随する情報 *)
  type e_tag
end


(* 単純な自己ループありの有向グラフを作る. *)
module Make =
  functor (GI : GraphTag) -> struct
    type vertex = GI.vertex
    type v_tag = GI.v_tag
    type e_tag = GI.e_tag

    module S = Set.Make (struct type t = vertex let compare = compare end)
    module M = Map.Make (struct type t = vertex let compare = compare end)
    type point = { succ : e_tag M.t; pred : e_tag M.t; tag : v_tag; } (* 一つの頂点が持つ情報 *)
    type t = point M.t

    (* 空のグラフを返す *)
    let empty = M.empty

    (* 空なグラフか? *)
    let is_empty g = M.is_empty g
    (* vがグラフに含まれるか? *)
    let mem v g = M.mem v g

    let size g = M.cardinal g

    (* vに関連付けられているタグを取得 *)
    let find v g = (M.find v g).tag

    (* 頂点のリストを返す *)
    let vertices g = M.fold (fun v _ acc -> v :: acc) g

    (* 出次数を求める *)
    let out_degree_of v g = M.cardinal (M.find v g).succ

    (* 入次数を求める *)
    let in_degree_of v g = M.cardinal (M.find v g).pred

    (* 頂点の追加、もしくは頂点のタグを更新する *)
    let add_vertex v v_tag g =
      if M.mem v g then (* 頂点の更新 *)
        M.add v { (M.find v g) with tag = v_tag } g
      else (* 頂点の追加 *)
        M.add v { succ = M.empty; pred = M.empty; tag = v_tag } g

    (* 頂点を削除。削除すべき頂点が存在することを仮定. *)
    let remove_vertex v g =
      assert (M.mem v g);
      let { succ = succ; pred = pred } = M.find v g in
      let g =
        M.fold
          (fun w _ g ->
             let p = M.find w g in
               M.add w { p with pred = M.remove v p.pred } g)
          succ g
      in
      let g =
        M.fold
          (fun w _ g ->
             let p = M.find w g in
               M.add w { p with succ = M.remove v p.succ } g)
          pred g
      in
      let g = M.remove v g in
        g

    (* v -> v'を縮約する.
     * 簡約した後の頂点はvに等しくなり、そのタグはv_tagとなる.
     * ただし, vとv'は等しくてはならない. *)
    let contract v v' v_tag g =
      assert (v <> v');
      let { succ = succ ; pred = pred  }  = M.find v  g in
      let { succ = succ'; pred = pred' }  = M.find v' g in
      (* v -> v'なる辺は, もしあれば削除する *)
      let succ = M.remove v' succ in
      let pred' = M.remove v pred' in
      (* 辺のタグについては、v'に付随するタグよりもvのタグを優先する. *)
      let merger v a b =
        match (a, b) with
          | (Some e_tag, _) -> Some e_tag
          | (None, x) -> x
      in
      (* v'は全てvに置き換えなければならない *)
      (* [XXX] (v', v') ,(v', v)の辺がある場合, succ'のタグは(v', v')が優先されるようになっている. *)
      let succ' = if M.mem v' succ' then M.add v (M.find v' succ') (M.remove v' succ') else succ' in
      let pred' = if M.mem v' pred' then M.add v (M.find v' pred') (M.remove v' pred') else pred' in
      let pv = { succ = M.merge merger succ succ'; pred = M.merge merger pred pred'; tag = v_tag } in
      (* vの情報を更新する *)
      let g = M.add v pv g in
      (* v'と隣接する頂点に対して、v'への参照をvに付け替える *)
      let g =
        M.fold
          (fun w _ g ->
             assert (M.mem w pv.succ);
             let e_tag = M.find w pv.succ in
             let p = M.find w g in
               M.add w { p with pred = M.add v e_tag (M.remove v' p.pred) } g)
          succ' g
      in
      let g =
        M.fold
          (fun w _ g ->
             assert (M.mem w pv.pred);
             let e_tag = M.find w pv.pred in
             let p = M.find w g in
               M.add w { p with succ = M.add v e_tag (M.remove v' p.succ) } g)
          pred' g
      in
      (* v'を削除 *)
      let g = M.remove v' g in
        g
          
    (* 辺を追加. すでに辺がある場合は、タグを更新する. *)
    let add_edge v w e_tag g =
      let g =
        let p = M.find v g in
          M.add v { p with succ = M.add w e_tag p.succ } g
      in
      let g =
        let p = M.find w g in
          M.add w { p with pred = M.add v e_tag p.pred } g
      in
        g

    (* 有向辺 v -> wが存在するか? 頂点v, wが存在することを仮定. *)
    let has_edge v w g =
      assert (M.mem v g && M.mem w g);
      M.mem w (M.find v g).succ

    (* 有向辺 v -> wを削除する. 頂点v, wと辺v -> wが存在することを仮定. *)
    let remove_edge v w g =
      assert (has_edge v w g);
      let g =
        let p = M.find v g in
          M.add v { p with succ = M.remove w p.succ } g
      in
      let g =
        let p = M.find w g in
          M.add w { p with pred = M.remove v p.pred } g
      in
        g

    (* 有向辺 v -> wのタグを得る *)
    let tag_of_edge v w g = assert (has_edge v w g); M.find w (M.find v g).succ

    (* vへの入次辺を持つような頂点のリストを返す *)
    let pred v g =
      assert (M.mem v g);
      M.fold (fun v _ acc -> v :: acc) (M.find v g).pred []

    (* vへの出次辺を持つような頂点のリストを返す *)
    let succ v g =
      assert (M.mem v g);
      M.fold (fun v _ acc -> v :: acc) (M.find v g).succ []

    (* 辺を逆転させる *)
    let rev g =
      M.map (fun p -> { p with succ = p.pred; pred = p.succ }) g

    (* 頂点をfで変更する *)
    let map (f : vertex -> v_tag -> v_tag) g = M.mapi (fun v p -> { p with tag = f v p.tag }) g

    (* 頂点に対して畳み込む *)
    let fold f g init = M.fold (fun v p acc -> f v p.tag acc) g init

    (* 辺に対して畳み込む *)
    let fold_edge f g init = M.fold (fun v p acc -> M.fold (fun w e_tag acc -> f v w e_tag acc) p.succ acc) g init

    (* vから出る辺について畳み込む.  *)
    let expand (f : vertex -> e_tag -> 'a -> 'a) v g init = M.fold f (M.find v g).succ init

    (* 頂点に対して繰り返す *)
    let iter f g = M.iter (fun v p -> f v p.tag) g

    (* bool * vertex list を返す.
     * DAGであれば第一要素がtrueになる.
     * 第二要素は、DAGでなくても全ての頂点をただ一度だけ含むリストを返す. *)
    let topological_sort g =
      let ok = ref true in
      let ls = ref [] in
      let vis = ref S.empty in
      let rec dfs stack v = (* グラフを逆順にたどる *)
        if not (S.mem v !vis) then (
          vis := S.add v !vis;
          let stack = S.add v stack in
            M.iter (fun w _ -> dfs stack w) (M.find v g).succ;
            ls := v :: !ls
        ) else if S.mem v stack then (* 現在のdfsの呼び出しで訪れた頂点に再び到達した *)
          ok := false
      in
        M.iter (fun v _ -> dfs S.empty v) g;
        (!ok, !ls)

    let is_dag graph = fst (topological_sort graph)

    (* トポロジカルソートの順番で辺を畳み込む。DAG上のDP用. *)
    let topological f g init =
      let (is_dag, ls) = topological_sort g in
        (* DAGであるはず *)
        assert is_dag;
        List.fold_left
          (fun acc v ->
             let { succ = succ; tag = v_tag } = M.find v g in
               M.fold
                 (fun w e_tag acc -> f v w e_tag acc)
                 succ acc)
          init ls

    let bfs f g init que =
      let rec next vis acc = function
        | ([], []) -> acc
        | ([], q2) -> next vis acc (List.rev q2, [])
        | (v :: q1, q2) ->
            let { succ = succ; tag = tag } = M.find v g in
            let acc = f v tag acc in
            let (vis, q2) =
              M.fold
                (fun w _ (vis, q2) ->
                   if S.mem w vis then
                     (vis, q2)
                   else
                     (S.add w vis, w :: q2))
                succ (vis, q2)
            in
              next vis acc (q1, q2)
      in
      let vis = List.fold_right S.add que S.empty in
        next vis init (que, [])


  end



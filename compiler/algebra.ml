(* 代数的な等価性を用いて最適化. *)

open Block

exception Impossible

let is_target = function
  | { def = [_]; exp = (Add (C _, _) | ShiftL (_, _)) } -> true
  | _ -> false


let replace def_of stmt =
  (* xの定義がAddであれば, それを得る *)
  let take_add x =
    match def_of x with
      | Some (Add (C (i), y)) -> (i, y)
      | _ -> raise Impossible
  in
  (* xの定義がShiftLであれば, それを得る *)
  let take_lsl x =
    match def_of x with
      | Some (ShiftL (y, i)) -> (y, i)
      | _ -> raise Impossible
  in
  let takeable_add x = try ignore (take_add x); true with Impossible -> false in
    match stmt.exp with
      | Load (x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (Load (y, C (i+j)))]
      | FLoad (x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (FLoad (y, C (i+j)))]
      | Store (z, x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (Store (z, y, C (i+j)))]
      | FStore (z, x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (FStore (z, y, C (i+j)))]
      | LoadC (x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (LoadC (y, C (i+j)))]
      | FLoadC (x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (FLoadC (y, C (i+j)))]
      | StoreC (z, x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (StoreC (z, y, C (i+j)))]
      | FStoreC (z, x, C (i)) ->
          let (j, y) = take_add x in
            [gen_stmt stmt.def (FStoreC (z, y, C (i+j)))]
      | ShiftL (x, 0) -> [gen_stmt stmt.def (Mov (x))]
      | ShiftR (x, 0) -> [gen_stmt stmt.def (Mov (x))]
      | ShiftL (x, i) ->
          let (y, j) = take_lsl x in
            [gen_stmt stmt.def (ShiftL (y, i + j))]
      | Add (C (0), x) -> [gen_stmt stmt.def (Mov (x))]
      | Add (C (i), x) ->
          let (j, y) = take_add x in
          let i' = i+j in
            if not (Asm.fit_immediate i') then raise Impossible;
            [gen_stmt stmt.def (Add (C i', y))]
      | Sub (C (i), x) ->
          let (j, y) = take_add x in
          let i' = i-j in
            if not (Asm.fit_immediate i') then raise Impossible;
            [gen_stmt stmt.def (Sub (C i', y))]
      | IfEq (x, C (i)) ->
          let (j, y) = take_add x in
          let i' = i-j in
            if not (Asm.fit_small_immediate i') then raise Impossible;
            [gen_stmt stmt.def (IfEq (y, C (i')))]
      | IfLt (x, C (i)) ->
          let (j, y) = take_add x in
          let i' = i-j in
            if not (Asm.fit_small_immediate i') then raise Impossible;
            [gen_stmt stmt.def (IfLt (y, C (i')))]
      | Add (V (x), y) ->
          let (x, y) = if takeable_add x then (x, y) else (y, x) in
          let (i, z) = take_add x in
          let at = match stmt.def with [_, t] -> (Id.gentmp t, t) | _ -> assert false in
            [gen_stmt [at] (Add (V (y), z));
             gen_stmt stmt.def (Add (C (i), fst at))]
      | _ -> raise Impossible
    

let g cfg =
  let avail = Avail.create ~is_target cfg in
  let cfg =
    G.map
      (fun b stmts ->
         let set = Avail.avail_of b avail in
           List.fold_left
             (fun acc stmt ->
                let get x = Avail.def_of avail x set in
                let stmts =
                  try
                    replace get stmt
                  with Impossible -> [stmt]
                in
                  Avail.transfer avail set stmt;
                  acc @ stmts)
             [] stmts)
      cfg
  in
    cfg

let f prog = map_cfg (fun l cfg -> g cfg) prog


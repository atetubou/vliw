type id_or_imm = V of Id.t | C of int

let string_of_ii = function
  | V v -> v
  | C c -> string_of_int c

let list_of_ii = function
  | V x -> [x]
  | C c -> []

type fpu_funct =
  (* 右オペランドの符号反転 *)
  | FPUBit
  (* 演算結果の符号反転 *)
  | NegBit
  (* 演算結果の絶対値 *)
  | AbsBit

type fpu_functs = fpu_funct list

let string_of_funct funct =
  String.concat " " (List.map (function FPUBit -> "fpu" | NegBit -> "neg" | AbsBit -> "abs") funct)

type exp =
  (* 関数の入口．その関数において入口生存なものを擬似的に定義する.
   * 疑似命令なので, 他の命令と同時に発行されることはない. *)
  | Entry
  (* 関数の出口．その関数において出口生存なものを擬似的に使用する. *)
  | Return
  | Nop
  (* 16bit符号付き即値を代入. 16bitを超えるものに関しては工夫する. *)
  | Set of int
  (* 同じCFG上のblock idを設定 *)
  | SetLabel of Id.label
  | Mov of Id.t
  | FMov of Id.t * fpu_functs
  | Add of id_or_imm * Id.t
  | Sub of id_or_imm * Id.t
  | ShiftL of Id.t * int
  | ShiftR of Id.t * int
  | FAdd of Id.t * Id.t * fpu_functs
  | FMul of Id.t * Id.t * fpu_functs
  | FInv of Id.t * fpu_functs
  | FSqrt of Id.t * fpu_functs
  | Load of Id.t * int
  | Store of Id.t * Id.t * int
  | FLoad of Id.t * int
  | FStore of Id.t * Id.t * int
  (* キャッシュへの読み書き *)
  | LoadC of Id.t * id_or_imm
  | StoreC of Id.t * Id.t * int
  | FLoadC of Id.t * id_or_imm
  | FStoreC of Id.t * Id.t * int
  | Send of Id.t
  | Recv
  | IfEq of Id.t * id_or_imm
  | IfLt of Id.t * id_or_imm
  | IfFEq of Id.t * Id.t
  | IfFLt of Id.t * Id.t
  | Jmp
  (* Callは最終的にJmpになるが, 分岐だと考えると前後への挿入がやっかいになるので, 分岐だと考えない.
   * 他の命令と同時に発行されない. *)
  | Call of Id.label * Id.t list
  (* 変数に代入されているアドレスへジャンプする *)
  | JumpTo of Id.t

(* 分岐命令は以下のいずれか. 分岐命令は常に右側に埋められる. *)
let is_branch = function
  | IfEq _ | IfLt _ | IfFEq _ | IfFLt _ | Return | Jmp | JumpTo _ -> true
  | _ -> false


type half_stmt = {
  (* 文のID *)
  s_id : Id.t;
  (* 式 *)
  exp : exp;
  (* この文が使用しうるすべての変数.
   * expで使用される変数集合を含む. *)
  use : Id.t list;
  (* この文が定義しうる全ての変数.
   * 順番にも意味があることに注意. *)
  def : (Id.t * Type.t) list;
}

type stmt = {
  (* ストールすべきクロック数
   * emitの直前でのみ意味をもつ. *)
  stall : int;
  (* 左側の命令; メモリ, 入出力は左のみ. *)
  left : half_stmt;
  (* 右側の命令; 分岐命令は右のみ. *)
  right : half_stmt;
}

let def_of stmt = stmt.left.def @ stmt.right.def
let use_of stmt = stmt.left.use @ stmt.right.use

(* 基本ブロック
 * 分岐命令の後に遅延スロットの2命令がある. *)
type block = stmt list

(* s_idを生成する *)
let gensid () = Id.genid "s"
(* blockのIDを生成する. *)
let genbid () = Id.genid "b"

(* スロットの制約 *)
type restriction = LeftOnly | RightOnly | NoRestriction

(* 式の形から制約を得る. *)
let restriction_of = function
  | Entry | Call _  -> LeftOnly
  | Send _ | Recv -> LeftOnly
  | Load _ | FLoad _ | Store _ | FStore _ -> RightOnly
  | LoadC _ | FLoadC _ | StoreC _ | FStoreC _ -> LeftOnly
  | Return | IfEq _ | IfLt _ | IfFEq _ | IfFLt _ | Jmp | JumpTo _ -> RightOnly
  | Nop | Set _ | SetLabel _ | Mov _ | Add _ | Sub _ | ShiftL _ | ShiftR _ -> NoRestriction
  | FMov _ | FInv _ | FSqrt _ -> RightOnly
  | FAdd _ -> RightOnly
  | FMul _ -> RightOnly

(* ブロックを通常の命令と, 分岐と, 遅延スロットに分割する *)
let rec partition_block = function
  | { right = { exp = exp } } as branch :: delay when is_branch exp ->
      ([], branch, delay)
  | stmt :: stmts ->
      let (ls, branch, delay) = partition_block stmts in
        (stmt :: ls, branch, delay)
  (* 分岐命令がない場合は例外を発生させる. *)
  | _ -> invalid_arg "partition_block"


let gen_half_stmt dest exp =
  match (dest, exp) with
  | []                    , Nop                 -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_]                   , Set _               -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_]                   , SetLabel _          -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_]                   , Mov (x)             -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_, Type.Float]       , FMov (x, funct)     -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , Add (x', y)         -> { s_id = gensid (); exp = exp; use = y :: list_of_ii x';      def = dest }
  | [_]                   , Sub (x', y)         -> { s_id = gensid (); exp = exp; use = y :: list_of_ii x';      def = dest }
  | [_]                   , ShiftL (x, i)       -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , ShiftR (x, i)       -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_, Type.Float]       , FAdd (x, y, funct)  -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | [_, Type.Float]       , FMul (x, y, funct)  -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | [_, Type.Float]       , FInv (x, funct)     -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_, Type.Float]       , FSqrt (x, funct)    -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , Load (x, i)         -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | ([] | [_, Type.Unit]) , Store (x, y, i)     -> { s_id = gensid (); exp = exp; use = [x; y];                  def = []   }
  | [_, Type.Float]       , FLoad (x, i)        -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | ([] | [_, Type.Unit]) , FStore (x, y, i)    -> { s_id = gensid (); exp = exp; use = [x; y];                  def = []   }
  | [_]                   , LoadC (x, y')       -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | ([] | [_, Type.Unit]) , StoreC (x, y, i)    -> { s_id = gensid (); exp = exp; use = [x; y];                  def = []   }
  | [_, Type.Float]       , FLoadC (x, y')      -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | ([] | [_, Type.Unit]) , FStoreC (x, y, i)   -> { s_id = gensid (); exp = exp; use = [x; y];                  def = []   }
  | ([] | [_, Type.Unit]) , Send (x)            -> { s_id = gensid (); exp = exp; use = [x];                     def = []   }
  | [_]                   , Recv                -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | []                    , IfEq (x, y')        -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | []                    , IfLt (x, y')        -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | []                    , IfFEq (x, y)        -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | []                    , IfFLt (x, y)        -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | []                    , JumpTo (x)          -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | []                    , Jmp                 -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | _                                         -> assert false

(* 制約を満たすスロットにhalfを入れる. *)
let stmt_of_half half =
  if restriction_of half.exp = RightOnly then
    { stall = 0; left = gen_half_stmt [] Nop; right = half }
  else
    { stall = 0; left = half; right = gen_half_stmt [] Nop }

(* destとexpからstmtを生成する.
 * 片方はnopで埋まっている. (分岐以外は命令を左側, 分岐は右につめなければならない.)
 * ただし, Entry, Returnは生成できない. *)
let gen_stmt dest exp = stmt_of_half (gen_half_stmt dest exp)

(* args: 入口生存なもの *)
let gen_entry  args = stmt_of_half { s_id = gensid (); exp = Entry; use = []; def = args }
(* rets: 出口生存なもの *)
let gen_return rets = stmt_of_half { s_id = gensid (); exp = Return; use = rets; def = [] }


(* 型typに合うstoreやloadをつくる *)
let gen_store typ x (y, i) =
  gen_stmt []
    (match typ with Type.Float -> FStore (x, y, i) | _ -> Store (x, y, i))
let gen_load  typ x (y, i) =
  gen_stmt [x, typ]
    (match typ with Type.Float -> FLoad (y, i) | _ -> Load (y, i))



(* 左と右をfで置換 *)
let map_half f stmt = { stmt with left = f stmt.left; right = f stmt.right }
(* 左と右に関して畳み込み *)
let fold_half f stmt init = f stmt.right (f stmt.left init)
(* fでないものはNopで置き換え *)
let filter_half f stmt = map_half (fun half -> if f half then half else gen_half_stmt [] Nop) stmt

(* sidを付け替える *)
let copy_stmt stmt =
  let copy_half half = { half with s_id = gensid () } in
    map_half copy_half stmt

let is_nop = function
  | { left = { exp = Nop }; right = { exp = Nop } } -> true
  | _ -> false


(* CFGの辺のタグ *)
type branch_tag = Then | Else | Other

(* CFGのデータ構造 *)
module G =
struct
  module GraphTag =
  struct
    type vertex = Id.t
    type v_tag = block
    type e_tag = branch_tag
  end

  module G = Graph.Make (GraphTag)
  include G

  (* 無効なブロックを表す. *)
  let null_block = Id.genid "INVALID"

  (* 新しいブロックを作成する *)
  let add_block stmts g =
    let b = genbid () in
      (b, G.add_vertex b stmts g)

  (* ブロックを更新する. *)
  let update_block b stmts g = assert (G.mem b g); G.add_vertex b stmts g

  (* bの後続ブロックのうち、e_tagの辺で結ばれているものを1つ選ぶ.
   * そのような辺が存在しない場合は, Not_found. *)
  let succ_of b e_tag g =
    assert (M.mem b g);
    let succ = M.filter (fun _ e_tag' -> e_tag' = e_tag) (M.find b g).succ in
      fst (M.choose succ)

  (* v -> v'を縮約する。
   * ただし, vとv'にはられるThen, Elseの辺がある場合は、その分岐をJmpで置き換える.
   * なお、JumpToの出次数も2以上であるが、これは縮約によって辺が併合されてもよい. *)
  let contract v v' v_tag g =
    let is_tag_of_if = function Then | Else -> true | _ -> false in
    let pred = G.pred v g in
    let g =
      List.fold_left
        (fun g w ->
           if G.has_edge w v' g &&
              is_tag_of_if (G.tag_of_edge w v' g)
           then
             (* w -> v, w -> v'の辺があるので、w -> v'が分岐の片方であれば、もう一方の辺も分岐の片方であるはず. *)
             let () = assert (is_tag_of_if (G.tag_of_edge w v g)) in
             (* 辺を削除してから、新しいJmpの辺を追加する. *)
             let g = G.remove_edge w v g in
             let g = G.remove_edge w v' g in
             let g = G.add_edge w v Other g in
             let (stmts, _, delay) = partition_block (G.find w g) in
             let g = G.add_vertex w (stmts @ [gen_stmt [] Jmp] @ delay) g in
               g
           else g)
        g pred
    in
      G.contract v v' v_tag g

  (* ブロックbをコピーする *)
  let copy_block b g =
    let stmts = List.map copy_stmt (G.find b g) in
    let (b', g) = add_block stmts g in
      (b', g)

  (* fがtrueなるブロックのリストを返す. *)
  let filter (f : Id.t -> block -> bool) g = G.fold (fun v stmts acc -> if f v stmts then v :: acc else acc) g []

  (* 各文についてfによって変換する. *)
  let map_stmt (f : stmt -> stmt) g = G.map (fun v -> List.map (fun stmt -> f stmt)) g

  (* 各文について畳み込む *)
  let fold_stmt (f : stmt -> 'a -> 'a) g init = G.fold (fun _ stmts acc -> List.fold_left (fun acc stmt -> f stmt acc) acc stmts) g init

  (* 各文sについて, 文のリストf sで置き換える. *)
  let map_stmt_list (f : stmt -> stmt list) g = G.map (fun v stmts -> List.fold_left (fun acc stmt -> acc @ f stmt) [] stmts) g

(*  let map_stmt_list (f : stmt -> stmt list) g =
    (* TODO: Callが2つのCallになったとき、上手く動かない. *)
    (* Callが基本ブロックの最後にない場合, Callの後ろにある命令を, 後続ブロックに移す.
     * ただし, Callがひとつの基本ブロックの中に2つ以上ある場合は, 一回で完全に正規化できない可能性がある.
     * Callの後に命令を挿入する場合, この関数とG.map_stmt_listを組み合わせると便利. *)
    let normalize_call g =
      G.fold
        (fun b _ g ->
           let rec loop acc = function
             | ({ exp = Call _ } as stmt) :: stmts -> (stmt :: acc, stmts)
             | stmt :: stmts -> loop (stmt :: acc) stmts
             | [] -> (acc, [])
           in
           let (stmts, rem) = loop [] (G.find b g) in
             if rem = [] then g
             else
               let stmts = List.rev stmts in
               let g = update_block b stmts g in
               let b' = succ_of b Other g in
               let g = update_block b' (rem @ G.find b' g) g in
                 g)
        g g
    in
      normalize_call (map_stmt_list f g)*)


  (* 各文についてfを満たさないものを削除する. *)
  let filter_stmt (f : stmt -> bool) g = G.map (fun _ stmts -> List.filter f stmts) g
end

(* 関数の情報 *)
type func = {
  (* 関数のCFG *)
  cfg : G.t;
  (* cfgの入口 *)
  v_entry : Id.t;
  (* 引数. *)
  args : (Id.t * Type.t) list;
  (* 関数の返り値. 複数の値を返せる. *)
  rets : (Id.t * Type.t) list;
  (* スタックフレームのサイズ *)
  stack_size : int;
}

(* 関数呼び出しグラフ *)
module F =
  Graph.Make
    (struct
       type vertex = Id.t
       (* 関数のおおよそのプログラムサイズ *)
       type v_tag = int
       (* 関数の呼び出し箇所の個数 *)
       type e_tag = int
     end)

(* プログラム全体の情報 *)
type program = {
  (* 関数呼び出しグラフ *)
  call_graph : F.t;
  (* 関数名 -> 関数の情報 *)
  entries : func M.t;
  (* ヒープポインタ, ゼロレジスタなど, 異なる関数からアクセスできる変数全体.
   * レジスタでなければならない. *)
  global_variables : (Id.t * Type.t) list;
}


(* プログラムの各関数について変換する *)
let map_func f prog =
  { prog with
        entries =
          M.mapi (fun l func -> f l func) prog.entries }

(* プログラム全体のCFGに対して, 関数fを適用して変換する. *)
let map_cfg f prog =
  map_func (fun l func -> { func with cfg = f l func.cfg }) prog

(* def以外の変数で, envに含まれる変数名を置き換える. *)
let replace_use_half_stmt env stmt =
  let f x = if M.mem x env then M.find x env else x in
  let f' = function V x -> V (f x) | C i -> C i in
  let stmt = { stmt with use = List.map f stmt.use } in
  let exp = match stmt.exp with
    | Mov (x)            -> Mov (f x)
    | Add (x', y)        -> Add (f' x', f y)
    | Sub (x', y)        -> Sub (f' x', f y)
    | ShiftL (x, i)      -> ShiftL (f x, i)
    | ShiftR (x, i)      -> ShiftR (f x, i)
    | Load (x, i)        -> Load (f x, i)
    | Store (x, y, i)    -> Store (f x, f y, i)
    | LoadC (x, y')      -> LoadC (f x, f' y')
    | StoreC (x, y, i)   -> StoreC (f x, f y, i)
    | FMov (x, funct)    -> FMov (f x, funct)
    | FAdd (x, y, funct) -> FAdd (f x, f y, funct)
    | FMul (x, y, funct) -> FMul (f x, f y, funct)
    | FInv (x, funct)    -> FInv (f x, funct)
    | FSqrt (x, funct)   -> FSqrt (f x, funct)
    | FLoad (x, i)       -> FLoad (f x, i)
    | FStore (x, y, i)   -> FStore (f x, f y, i)
    | FLoadC (x, y')     -> FLoadC (f x, f' y')
    | FStoreC (x, y, i)  -> FStoreC (f x, f y, i)
    | Send (x)           -> Send (f x)
    | Call (l, ys)       -> Call (l, List.map f ys)
    | IfEq (x, y')       -> IfEq (f x, f' y')
    | IfLt (x, y')       -> IfLt (f x, f' y')
    | IfFEq (x, y)       -> IfFEq (f x, f y)
    | IfFLt (x, y)       -> IfFLt (f x, f y)
    | JumpTo (x)         -> JumpTo (f x)
    | e                  -> e
  in
    { stmt with exp = exp }

let replace_def_half_stmt env stmt = { stmt with def = List.map (fun (x, t) -> if M.mem x env then (M.find x env, t) else (x, t)) stmt.def }

let replace_use_stmt env stmt = map_half (replace_use_half_stmt env) stmt
let replace_def_stmt env stmt = map_half (replace_def_half_stmt env) stmt

(* envに従って変数名を置き換える. envに属さない変数は置き換えない. *)
let replace_stmt env stmt = replace_use_stmt env (replace_def_stmt env stmt)


(* 関数func内で使用されうる変数を全て集める *)
let gather_variables_of func =
  G.fold
    (fun b ->
       List.fold_right
         (fun stmt ->
            S.union (S.union (S.of_list (use_of stmt)) (S.of_list (List.map fst (def_of stmt))))))
    func.cfg S.empty


(*** begin of debug ***)
let string_of_half_stmt { s_id = s_id; exp = exp; use = use; def = def } =
  "" ^ s_id ^ ": " ^
  (if def = [] then
     ""
   else
     String.concat ", "
       (List.map
          (fun (x, t) -> x ^ " : " ^ Type.string_of t)
          def) ^
     " <- ") ^ 
  (match exp with
     | Nop -> "Nop"
     | Set i -> "Set(" ^ string_of_int i ^ ")"
     | SetLabel l -> "SetLabel(" ^ l ^ ")"
     | Mov x -> "Mov(" ^ x ^ ")"
     | Add (x', y) -> "Add(" ^ string_of_ii x' ^ " + " ^ y ^ ")"
     | Sub (x', y) -> "Sub(" ^ string_of_ii x' ^ " - " ^ y ^ ")"
     | ShiftL (x, i) -> "ShiftL(" ^ x ^ " << " ^ string_of_int i ^ ")"
     | ShiftR (x, i) -> "ShiftR(" ^ x ^ " << " ^ string_of_int i ^ ")"
     | Load (x, i) -> "Load(" ^ x ^ " + " ^ string_of_int i ^ ")"
     | Store (x, y, i) -> "Store(M[" ^ y ^ " + " ^ string_of_int i ^ "] = " ^ x ^ ")"
     | LoadC (x, y') -> "LoadC(" ^ x ^ " + " ^ string_of_ii y' ^ ")"
     | StoreC (x, y, i) -> "StoreC(M[" ^ y ^ " + " ^ string_of_int i ^ "] = " ^ x ^ ")"
     | FMov (x, funct) -> "FMov(" ^ x ^ "; " ^ string_of_funct funct ^ ")"
     | FAdd (x, y, funct) -> "FAdd(" ^ x ^ " + " ^ y ^ "; " ^ string_of_funct funct ^ ")"
     | FMul (x, y, funct) -> "FMul(" ^ x ^ " * " ^ y ^ "; " ^ string_of_funct funct ^ ")"
     | FInv (x, funct) -> "FInv(1.0 /. " ^ x ^ "; " ^ string_of_funct funct ^ ")"
     | FSqrt (x, funct) -> "FSqrt(" ^ x ^ "; " ^ string_of_funct funct ^ ")"
     | FLoad (x, i) -> "FLoad(" ^ x ^ " + " ^ string_of_int i ^ ")"
     | FStore (x, y, i) -> "FStore(M[" ^ y ^ " + " ^ string_of_int i ^ "] = " ^ x ^ ")"
     | FLoadC (x, y') -> "FLoadC(" ^ x ^ " + " ^ string_of_ii y' ^ ")"
     | FStoreC (x, y, i) -> "FStoreC(M[" ^ y ^ " + " ^ string_of_int i ^ "] = " ^ x ^ ")"
     | Send x -> "Send(" ^ x ^ ")"
     | Recv -> "Recv"
     | IfEq (x, y') -> "IfEq(" ^ x ^ " = " ^ string_of_ii y' ^ ")"
     | IfLt (x, y') -> "IfLt(" ^ x ^ " < " ^ string_of_ii y' ^ ")"
     | IfFEq (x, y) -> "IfFEq(" ^ x ^ " = " ^ y ^ ")"
     | IfFLt (x, y) -> "IfFLt(" ^ x ^ " < " ^ y ^ ")"
     | Call (l, xs) -> "Call " ^ l ^ "(" ^ (String.concat ", " xs) ^ ")"
     | Entry -> "Entry"
     | Return -> "Return"
     | Jmp -> "Jmp"
     | JumpTo (x)         -> "JumpTo(" ^ x ^ ")")

let string_of_stmt stmt =
  string_of_half_stmt stmt.left ^ " \\ " ^ string_of_half_stmt stmt.right ^ " \\ " ^ string_of_int stmt.stall

let string_of_block b stmts =
  "" ^ b ^ ":\n" ^
  List.fold_left (fun res stmt -> res ^ "\n" ^ string_of_stmt stmt) "" stmts

let output_cfg_to oc graph =
  Printf.fprintf oc "digraph cfg {\n";
  Printf.fprintf oc "node [shape=box];\n";
  G.fold
    (fun b block () ->
       Printf.fprintf oc "%s [label=\"%s\"];\n" (Id.escape b) (String.escaped (string_of_block b block)))
    graph ();
  G.fold_edge
    (fun v w e_tag () ->
       let s_tag = match e_tag with Then -> "Then" | Else -> "Else" | _ -> "" in
       let style = "" in(*let style = match e_tag with Continue -> "style=dotted" | _ -> "" in *)
         Printf.fprintf oc "%s -> %s [label=\"%s\"%s];\n" (Id.escape v) (Id.escape w) (String.escaped s_tag) style) graph ();
  Printf.fprintf oc "}\n"
let output_cfg path graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    output_cfg_to oc graph;
    close_out oc
let output_prog path prog =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    M.iter (fun l { cfg = cfg } -> output_cfg_to oc cfg) prog.entries;
    close_out oc

let output_call_graph path graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    Printf.fprintf oc "digraph call_graph {\n";
    F.fold
      (fun l _ () ->
         Printf.fprintf oc "%s [label=\"%s\"];\n" l (String.escaped l))
      graph ();
    F.fold_edge
      (fun v w e_tag () ->
         Printf.fprintf oc "%s -> %s;\n" v w) graph ();
    Printf.fprintf oc "}\n";
    close_out oc

(*** end of debug ***)


let check_immediate_bits = function
  | Set (i) -> if not (Asm.is_bits_of 16 i) then Format.eprintf "FAILED %d@." i; assert (Asm.is_bits_of 16 i)
  | Add (C (i), _) | Sub (C (i), _) | Load (_, i) | Store (_, _, i)
  | FLoad (_, i) | FStore (_, _, i) -> assert (Asm.fit_immediate i)
  | IfEq (_, C (i)) | IfLt (_, C (i)) -> assert (Asm.fit_small_immediate i)
  | _ -> ()


(* 文に対して満たすべき性質を確認する. *)
let check_consistency_of_half_stmt stmt =
  (* lsはls'に含まれるか. *)
  let is_subset ls ls' = List.for_all (fun x -> List.mem x ls') ls in
  (* 2つのリストが集合として等しいか. *)
  let is_equal ls ls' = is_subset ls ls' && is_subset ls' ls in
    check_immediate_bits stmt.exp;
    check_immediate_bits stmt.exp;
    match stmt with
      (* Entryは引数やグローバル変数を定義する. *)
      | { def = _; exp = Entry; use = [] } -> ()
      (* Callの場合. useは引数と等しい. *)
      | { exp = Call (l, xs); use = use } when is_equal xs use -> ()
      (* Returnは、出口生存であるもの全てを"使用"する. *)
      | { def = []; exp = Return; use = _ } -> ()
      (* Recvは引数レジスタの下位8bitに代入するので、自分自身も使用する.
       * しかし上位24bitが切り捨てられる場合には, 使用しない. *)
      | { def = [(x, t)]; exp = Recv; use = [y] } when t <> Type.Float && x = y -> ()
      | { def = [(_, t)]; exp = Recv; use = [] } when t <> Type.Float -> ()
      (* 整数を定義するもの *)
      | { def = [(_, t)]; exp = (Set _ | SetLabel _); use = [] } when t <> Type.Float -> ()
      | { def = [(_, t)];
          exp = Mov (x) | Add (C _, x) | Sub (C _, x) | ShiftL (x, _) | ShiftR (x, _) | Load (x, _) | LoadC (x, C _);
          use = [y] }
          when t <> Type.Float && x = y -> ()
      | { def = [(_, t)];
          exp = Add (V (x), y) | Sub (V (x), y) | LoadC (x, V(y));
          use = use }
          when t <> Type.Float && is_equal use [x; y] -> ()
      (* Floatを定義するもの *)
      | { def = [(_, Type.Float)];
          exp = FMov (x, _) | FInv (x, _) | FSqrt (x, _) | FLoad (x, _) | FLoadC (x, C _);
          use = [y] }
          when x = y -> ()
      | { def = [(_, Type.Float)];
          exp = FAdd (x, y, _) | FMul (x, y, _) | FLoadC (x, V (y));
          use = use }
          when is_equal use [x; y] -> ()
      (* 何も定義しないもの *)
      | { def = []; exp = Jmp | Nop; use = [] } -> ()
      | { def = []; exp = Send (x) | IfEq (x, C _) | IfLt (x, C _) | JumpTo (x); use = [y] } when x = y -> ()
      | { def = [];
          exp = IfEq (x, V (y)) | IfLt (x, V (y)) | IfFEq (x, y) | IfFLt (x, y) | Store (x, y, _) | FStore (x, y, _) | StoreC (x, y, _) | FStoreC (x, y, _);
          use = use }
          when is_equal use [x; y] -> ()
      (* 以上以外であれば間違っている. *)
      | _ -> Format.eprintf "A statement '%s' is not consistent@." (string_of_half_stmt stmt); assert false

let check_consistency_of_stmt stmt =
  check_consistency_of_half_stmt stmt.left;
  check_consistency_of_half_stmt stmt.right;
  (match restriction_of stmt.left.exp with
     | RightOnly -> assert false
     | _ -> ());
  (match restriction_of stmt.right.exp with
     | LeftOnly ->
         Format.eprintf "error %s@." (string_of_stmt stmt);
         assert false
     | _ -> ());
  (* 特殊な命令の逆側は埋めない *)
  (match stmt.left with
     | { exp = Call _ | Entry } ->
         assert (stmt.right.exp = Nop)
     | _ -> ())
    (*
    ;
  (match stmt.right with
     | { exp = Return } ->
         assert (stmt.left.exp = Nop)
     | _ -> ())
     *)

(* CFGが満たすべき性質を確認する *)
let check_consistency_of_cfg cfg =
  (* 文IDに重複がないか確認する *)
  ignore
    (G.fold
       (fun b ->
          List.fold_right
            (fun stmt env ->
               fold_half
                 (fun { s_id } env ->
                    assert (not (S.mem s_id env));
                    S.add s_id env)
                 stmt env))
       cfg S.empty);
  (* graph.mlが正常かどうか調べる. *)
  G.iter
    (fun b _ ->
       List.iter (fun b' -> assert (G.mem b' cfg)) (G.succ b cfg);
       List.iter (fun b' -> assert (G.mem b' cfg)) (G.pred b cfg))
    cfg;
  (* 各文について一貫性を調べる. *)
  G.iter (fun _ -> List.iter check_consistency_of_stmt) cfg;
  (*
  (* SetLabelが同じCFG内のブロックのラベルを示しているか, もしくはグローバル配列か. *)
  G.iter (fun _ -> List.iter (function { exp = SetLabel (l) } -> assert (G.mem l cfg || M.mem l !Global.all) | _ ->())) cfg;
   *)
  (* 各ブロックの分岐と, そのブロックから出る辺が一貫しているかどうか調べる. *)
  G.iter
    (fun v stmts ->
       (* 何もないブロックはないはず。最低でもJmpはあるはず. *)
       assert (stmts <> []);
       let stmts_orig = stmts in
       let (stmts, ({ right = branch } as branch_stmt), delay) = partition_block stmts in
       let deg = G.out_degree_of v cfg in
         (match delay with
            | [] | [_] | [{ stall = 0 }; _] -> ()
            | _ -> Format.eprintf "FAILED ABOUT DELAY SLOT@.%s@.branch: %s@." (string_of_block v stmts_orig) (string_of_stmt branch_stmt); assert false);
         (* 基本ブロックの途中には分岐命令はないはず *)
         assert (List.for_all (fun stmt -> not (is_branch stmt.left.exp) && not (is_branch stmt.right.exp)) (stmts @ delay));
         (* 遅延スロットにCallは属さないはず. *)
         assert (List.for_all (function { left = { exp = Call _ } } -> false | _ -> true) delay);
         (* 基本ブロックの最後の命令は分岐であるはず *)
         assert (is_branch branch.exp);
         (try
            (match branch.exp with
               | Call _ ->
                   (* 戻り先ブロック *)
                   assert (deg = 1);
                   ignore (G.succ_of v Other cfg)
               | IfEq _ | IfLt _ | IfFEq _ | IfFLt _ ->
                   if not (deg = 2) then Format.eprintf "failed %s@." v;
                   assert (deg = 2);
                   ignore (G.succ_of v Then cfg); (* Then, Elseに一致する辺のタグがあるか？なければNot_found *)
                   ignore (G.succ_of v Else cfg)
               | Jmp ->
                   assert (deg = 1);
                   ignore (G.succ_of v Other cfg)
               | Return ->
                   (* CFGの最後を意味するので、どこにも行かないはず. *)
                   assert (deg = 0);
               | JumpTo (x) -> ()
               | _ -> assert false)
          with
              Not_found -> assert false (* 辺のタグに一致するものがなかったとき発生する(G.succ_ofで) *)))
    cfg

(* プログラム全体に対して、一貫性を確認する. *)
let check_consistency prog =
  M.iter
    (fun _ func ->
       (* CFGについて一貫性を調べる. *)
       check_consistency_of_cfg func.cfg)
    prog.entries;
  M.iter
    (fun _ func ->
       G.iter
         (fun _ ->
            List.iter
              (function
                 | { left = { exp = Call (l, xs); def = def } } ->
                     (* 関数の型が一致することを確認する. *)
                     assert (M.mem l prog.entries);
                     let func = M.find l prog.entries in
                       assert (List.length def = List.length func.rets);
                       assert (List.length xs = List.length func.args)
                 | _ -> ()))
         func.cfg)
    prog.entries


(* 不要なJmpを除去する. *)
let rec simplify prog =
  check_consistency prog;
  (*
                 
  (* 到達可能な関数とブロックを計算する.
   * 関数名とブロック名の末尾にはId.counterが付加されているので一意的である. *)
  let rec dfs_func vis l =
    if S.mem l vis then vis
    else
      let vis = S.add l vis in
      let func = M.find l prog.entries in
        dfs_block func.cfg vis func.v_entry
  and dfs_block cfg vis v =
    if S.mem v vis then vis
    else
      let vis = S.add v vis in
      (* 制御フローによる遷移をたどる *)
      let vis = List.fold_left (dfs_block cfg) vis (G.succ v cfg) in
      (* Callによる遷移をたどる *)
      let stmts = G.find v cfg in
        List.fold_left
          (fun vis stmt ->
             match stmt.exp with
               | Call (l, _) -> dfs_func vis l
               | _ -> vis)
          vis stmts
  in
  let reachable = dfs_func S.empty Id.main_label in
  (* 到達できない関数を削除 *)
  let prog =
    { prog with
          call_graph =
            F.fold
              (fun l _ acc -> if S.mem l reachable then acc else F.remove_vertex l acc)
              prog.call_graph prog.call_graph;
          entries = M.filter (fun l _ -> S.mem l reachable) prog.entries; }
  in
  (* 到達できないブロックを削除 *)
  let prog =
    map_cfg
      (fun l cfg ->
         G.fold
           (fun v _ cfg -> if S.mem v reachable then cfg else G.remove_vertex v cfg)
           cfg cfg)
      prog
  in
  check_consistency prog;
   *)
  (* (1) Jmpだけのブロックを縮約.
   * (2) Jmp先のブロックの先行ブロックがただ一つであるようなJmpを縮約.
   * conv: 元のブロック -> 新しいブロック *)
  let remove_jmp push v (conv, cfg) =
    (* 縮約をするので、vはすでにCFGから削除されているかもしれない. *)
    if not (G.mem v cfg) then (conv, cfg)
    else
      match partition_block (G.find v cfg) with
        (* Jmpだけのブロックの場合. *)
        | (stmts, branch, delay)
          when branch.right.exp = Jmp && List.for_all is_nop (stmts @ [stmt_of_half branch.left] @ delay) ->
            (* Jmp先のブロックwを取り出す *)
            let w = G.succ_of v Other cfg in
              if v = w then (conv, cfg)
              else (
                push v;
                (* wは削除されるので, convに追加しておく. *)
                let conv = M.add w v conv in
                let cfg = G.contract v w (G.find w cfg) cfg in
                  (conv, cfg)
              )
        | (stmts, branch, delay)
          when branch.right.exp = Jmp ->
            let w = G.succ_of v Other cfg in
              if v = w || G.pred w cfg <> [v] then (conv, cfg)
              else (
                (* v <> wで, wの唯一の先行ブロックがvである場合 *)
                push v;
                let conv = M.add w v conv in
                let cfg = G.contract v w (stmts @ [stmt_of_half branch.left] @ delay @ G.find w cfg) cfg in
                  (conv, cfg)
              )
        | _ -> (conv, cfg)
  in
  let prog =
    { prog with
          entries =
            M.map
              (fun func ->
                 (* Jmpを分岐として持つブロックについて、ワークリストに挿入する. *)
                 let que = G.filter (fun v stmts -> match partition_block stmts with (_, { right = { exp = Jmp } }, _) -> true | _ -> false) func.cfg in
                 let (conv, cfg) = Common.do_worklist que (M.empty, func.cfg) remove_jmp in
                 (* CFGの辺以外でブロックを参照する場合は, それを変換する. *)
                 let convert x = if M.mem x conv then M.find x conv else x in
                 let cfg =
                   G.map_stmt
                     (map_half
                        (function
                           | { exp = SetLabel (l) } as stmt ->
                               { stmt with exp = SetLabel (convert l) }
                           | stmt -> stmt))
                     cfg
                 in
                   { func with
                         cfg = cfg;
(*                         v_entry = convert func.v_entry;*) })
              prog.entries }
  in
  check_consistency prog;
  prog

let remove_redundancy prog =
(*  check_consistency prog;*)
  (* 明らかに無駄な命令を省く. *)
  let prog =
    map_cfg
      (fun _ ->
         G.map_stmt
           (filter_half
              (function
                 (* コピー命令の移動元と移動先が同じ. *)
                 | { exp = Mov (x) | FMov (x, []); def = [(y, _)] }
                     when x = y -> false
                 | _ -> true)))
      prog
  in
  let prog =
    map_cfg
      (fun _ ->
         G.filter_stmt
           (function
              (* 左も右もNop *)
              | { left = { exp = Nop }; right = { exp = Nop } } -> false
              | _ -> true))
      prog
  in

(*  check_consistency prog;*)
    prog


(* srcからdestへコピーする. destは型付き.
 * srcとdestに共通部分があったらシャッフルして移動する. *)
let move_list (dest : (Id.t * Type.t) list) (src : Id.t list) =
  let mvs =
    try
      List.combine src dest
    with Invalid_argument _ as e->
      Format.eprintf "move (%s) <- (%s) is not consistent.@."
        (String.concat ", " (List.map (fun (x, t) -> x ^ ":" ^ Type.string_of t) dest))
        (String.concat ", " src);
      raise e
  in
  let mvs =
    List.filter
      (function
         (* コピー元とコピー先が同じである場合は無視する. *)
         | (s, (d, _)) when d = s -> false
         (* Unit型はコピーしない *)
         | (_, (_, Type.Unit)) -> false
         | _ -> true)
      mvs
  in
  (* (s, (d, _))をグラフの辺だとみなして辺をトポロジカルソートする. *)
  let rec shuffle xys =
    match List.partition (fun (_, (y, _)) -> List.mem_assoc y xys) xys with
      | [], [] -> []
      | (x, (y, t)) :: xys, [] ->
          let sw = Id.genid "SWAP" in
            (* yをswに退避してから, xをyへコピーする. *)
            (y, (sw, t)) :: (x, (y, t)) ::
            shuffle (List.map (function (y', (z, t)) when y = y' -> (sw, (z, t)) | yz -> yz) xys)
      | xys, acyc -> acyc @ shuffle xys
  in
  let mvs = shuffle mvs in
    (* コピー命令の列に変換する. *)
    List.map
      (function
         | (s, (d, Type.Float)) -> gen_stmt [(d, Type.Float)] (FMov (s, []))
         | (s, (d, t)) -> gen_stmt [(d, t)] (Mov (s)))
      mvs


(* グローバル変数varを追加する.
 *
 * (1) メイン関数以外の全ての関数内で入口生存かつ出口生存となる.
 * 
 * (2) Call命令のuse, defに追加される.
 * ただし, is_constがtrueのときはdefには追加されない.
 * (すなわち, 関数内で不変な場合にis_const = trueとする.)
 * 
 * (3) 全ての関数引数と戻り値にグローバル変数が追加される.
 * ただし, is_constがtrueのときには戻り値には追加されない.
 *
 * restrict = trueの場合は,
 * (2), (3)について, その関数を呼び出すことによって使用, 定義されるときのみ追加される.
 *
 * 
 * (注)
 * 1. レジスタ割当てでグローバル変数と競合するような割当てはできないので,
 * 全ての関数内で入口/出口生存としておく. (1)の条件を弱めるのは難しい.
 *
 * 2. restrictは, 後でグローバル変数の使用や定義を追加しないときにのみ, trueにすること.
 *
 *)
let add_global_variable ?(is_const = false) ?(restrict = false) (var, typ) prog =
  let prog = { prog with global_variables = (var, typ) :: prog.global_variables } in

  (* 入口生存, 出口生存に追加. *)
  let prog =
    map_cfg
      (fun l cfg ->
         if l = Id.main_label then cfg
         else
           G.map_stmt
             (function
                | { left = { exp = Entry; def = def } as entry } as stmt ->
                    { stmt with left = { entry with def = (var, typ) :: def } }
                | { right = { exp = Return; use = use } as return } as stmt ->
                    { stmt with right = { return with use = var :: use } }
                | stmt -> stmt)
             cfg)
      prog
  in

  let prog =
    map_cfg
      (fun _ cfg ->
         G.map_stmt
           (function
              | { left = { exp = Call (l, args); use = use; def = def } as half } as stmt ->
                  { stmt with left = { half with exp = Call (l, args @ [var]); use = use @ [var] } }
              | stmt -> stmt)
           cfg)
      prog
  in

  (* 関数定義にも追加する. *)
  let prog =
    { prog with
          entries =
            M.mapi
              (fun l func ->
                 if l = Id.main_label then func
                 else
                   { func with args = func.args @ [var, typ] })
              prog.entries }
  in
    prog




(* Profile.tによってトレース(スーパーブロック)スケジューリングを行う. *)

open Vliw

module L = VListScheduling


let debug_mode = ref false
(* for debug *)
let output_trace_to ~depth oc trace graph =
  let rec dfs lim vis v =
    if lim < 0 || S.mem v vis then vis
    else
      let vis = S.add v vis in
      let vis = List.fold_left (dfs (lim - 1)) vis (G.succ v graph @ G.pred v graph) in
        vis
  in
  let target =
    List.fold_left
      (fun acc b -> S.union acc (dfs depth S.empty b))
      S.empty trace
  in
  let is_target x = S.mem x target in
  Printf.fprintf oc "digraph cfg {\n";
  Printf.fprintf oc "node [shape=box];\n";
  G.fold
    (fun b block () ->
       if is_target b then
         let style = if List.mem b trace then ",color=red" else "" in
           Printf.fprintf oc "%s [label=\"%s\"%s];\n" (Id.escape b) (String.escaped (string_of_block b block)) style)
    graph ();
  G.fold_edge
    (fun v w e_tag () ->
       if is_target v && is_target w then
         let s_tag = match e_tag with Then -> "Then" | Else -> "Else" | _ -> "" in
         let style = "" in(*let style = match e_tag with Continue -> "style=dotted" | _ -> "" in *)
           Printf.fprintf oc "%s -> %s [label=\"%s\"%s];\n" (Id.escape v) (Id.escape w) (String.escaped s_tag) style) graph ();
  Printf.fprintf oc "}\n"
let output_trace ?(depth = 2) path trace graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    output_trace_to ~depth oc trace graph;
    close_out oc

(* if文の前に移動してよいか *)
let is_movable = function
  | Nop | Set _ | SetLabel _ | Mov _ | FMov _ | Add _ | Sub _ 
  | ShiftL _ | ShiftR _ | FAdd _ | FMul _ | FInv _ | FSqrt _
  | Load _ | FLoad _
  | LoadC _ | FLoadC _
  | IfEq _ | IfLt _ | IfFEq _ | IfFLt _
    -> true
  (* Jmp, JumpToなどはそもそもスケジューリングの対象ではないので, trueでもfalseでも変わらない.
   * (JumpToは今のところそもそも存在しないことになっている.) *)
  | _ -> false


(* 依存グラフ *)
module D =
  Graph.Make
    (struct
       type vertex = Id.t
       type v_tag = stmt
       (* 辺の始点が発行された後, 何クロックで演算結果が利用可能になるか.
        * すなわち, 先行命令のレイテンシ.
        * ただし, -1のときは逆依存関係(WAR)を表すことにする. *)
       type e_tag = int
     end)

(* ready集合 *)
module PQ =
  Set.Make
    (struct
       (* int: 優先度の符号を反転させたもの.
        * すなわち小さい要素から最初に取り出すべき.
        * (こうしているのはS.iterが昇順であるから.) *)
       type t = int * Id.t
       let compare = compare
     end)

type data = {
  (* 依存グラフ *)
  graph : D.t;
  (* 依存関係は解消されたが, オペランドが用意できるのを待っているhalf_stmtの集合 *)
  waiting : PQ.t;
  (* 今すぐに発行できるhalf_stmtの集合 *)
  ready : PQ.t;
  (* オペランドが用意できるクロックの下限 *)
  finclk : int M.t;
  (* D.vertex -> その命令の優先度 *)
  priority : int M.t;
}

let rec output_graph path graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
  Printf.fprintf oc "digraph dep_graph {\n";
  Printf.fprintf oc "node [shape=box];\n";
  D.fold
    (fun b stmt () ->
       Printf.fprintf oc "%s [label=\"%s\"];\n" (Id.escape b) (String.escaped (string_of_stmt stmt)))
    graph ();
  D.fold_edge
    (fun v w e_tag () ->
       let style = "" in(*let style = match e_tag with Continue -> "style=dotted" | _ -> "" in *)
         Printf.fprintf oc "%s -> %s [label=\"%d\"%s];\n" (Id.escape v) (Id.escape w) e_tag style) graph ();
  Printf.fprintf oc "}\n";
  close_out oc


(* stmtのID
 * 分岐命令のIDがhalf.s_idと等しくなるように, 左側がNopであれば右側のs_idを使う. *)
let id_of = function
  | { left = { exp = Nop }; right = right } -> right.s_id
  | { left = left } -> left.s_id

(* waitingの元を作る *)
let waiting_node_of v data = (M.find v data.finclk, v)
let add_to_waiting v data = { data with waiting = PQ.add (waiting_node_of v data) data.waiting }

(* ready集合の元を作る. S.iterのために負号をつけていることに注意. *)
let ready_node_of v data = (- M.find v data.priority, v)

(* リストスケジューリングに必要なデータを, 命令列stmtsについて構築
 * stmtss: このブロックの先行ブロックになる文のリストのリスト
 * liveout: 分岐のoff-traceなジャンプ先ブロックで入口生存な変数集合の情報 *)
let build ?(stmtss = []) ~liveout stmts =
  let stmts =
    List.fold_right
      (fun stmt stmts ->
         let expanded =
           match (L.is_half_raw stmt.left stmt.right, L.is_half_raw stmt.right stmt.left) with
             (* 文を分解できる場合は分解する.
              * 分岐は必ず分解できるので, 分岐のIDはs_idに等しくなる. *)
             | (false, _) -> [stmt_of_half stmt.left; stmt_of_half stmt.right]
             | (true, false) -> [stmt_of_half stmt.right; stmt_of_half stmt.left]
             (* 順序関係をつけると正しくなくなる場合はそのままにする.
              * 例えば, x <- y \ y <- x はxとyのスワップを意味するが, これは同時に発行しなければ正しくない. *)
             | (true, true) -> [stmt]
         in
           expanded @ stmts)
      stmts []
  in
  (* nopのみの文は消去する. *)
  let stmts = List.filter (function { left = { exp = Nop }; right = { exp = Nop } } -> false | _ -> true) stmts in
  (* 依存グラフを構築
   * ブロックの先頭からみていって, 既に加えられた命令との依存関係を調べていく. *)
  let graph =
    List.fold_left
      (fun graph stmt ->
         let id = id_of stmt in
         let graph = D.add_vertex id stmt graph in
         let graph =
           D.fold
             (fun id' stmt' graph ->
                match () with
                  | _ when id = id' -> graph
                  | _ when
                      (* stmt'が分岐命令で *)
                      is_branch stmt'.right.exp &&
                      (* 基本ブロックを超えて移動できないか,
                       * off-traceで生きている変数をつぶしてしまうか. *)
                      (not (is_movable stmt.left.exp && is_movable stmt.right.exp) ||
                       let live = M.find id' liveout in
                         List.exists
                           (fun (x, _) -> S.mem x live)
                           (def_of stmt))
                    ->
                      (* 分岐が確定するまではstmtは発行してはならない. *)
                      D.add_edge id' id L.delay_slot graph
                  | _ when L.is_raw stmt' stmt || L.is_waw stmt' stmt ->
                      D.add_edge id' id (L.seq_latency_of stmt' stmt) graph
                  | _ when L.is_dependant stmt' stmt ->
                      (* その他の依存関係の場合 *)
                      D.add_edge id' id 0 graph
                  | _ when L.is_war stmt' stmt ->
                      (* 逆依存関係しかない場合は(同時に発行でき得るので)レイテンシ = -1とする. *)
                      D.add_edge id' id (-1) graph
                  | _ -> graph)
             graph graph
         in
           graph)
      D.empty stmts
  in
  let init = D.fold (fun v _ -> M.add v 0) graph M.empty in
  (* グラフを逆順からDPして、出口までの最大の長さ（クリティカルパス）を求める *)
  let priority =
    D.topological
      (fun v w latency env -> 
         let p' = max (M.find v env + 1 + latency) (M.find w env) in
           M.add w p' env)
      (D.rev graph) init
  in
  let branch_dist =
    D.topological
      (fun v w latency env -> 
         let p = M.find v env in
           if p = -1 then
             env
           else
             M.add w (max (p + 1 + latency) (M.find w env)) env)
      (D.rev graph)
      (M.mapi (fun v _ -> if is_branch (D.find v graph).right.exp then 0 else -1) init)
  in
  (* 分岐からパスがない場合は優先度に加算しない. パスがある場合は, (最長路 + 1) * 100を優先度に加算する. *)
  let priority = M.mapi (fun v x -> x + (M.find v branch_dist + 1) * 100) priority in
  (* stmtssとのレイテンシを考えて発行可能なクロックを計算する. *)
  let finclk =
    D.fold
      (fun v stmt -> M.add v (L.stall_of stmtss stmt))
      graph M.empty
  in
  let data =
    {
      graph = graph;
      waiting = PQ.empty;
      ready = PQ.empty;
      finclk = finclk;
      priority = priority;
    }
  in
    (* waiting集合に, 次数0の頂点を追加. *)
    D.fold
      (fun v _ data ->
         if D.in_degree_of v graph = 0 then
           add_to_waiting v data
         else
           data)
      graph data


(* クロックをclkまで進める.
 * すなわち, 命令を(もしあれば)waitingからreadyへ移動させる. *)
let next_clk clk data =
  let rec loop data =
    if PQ.is_empty data.waiting then
      data
    else
      let (clk', v) as elt = PQ.min_elt data.waiting in
        (* vをreadyに移動させる *)
        if clk' <= clk then
          loop
            { data with
                  waiting = PQ.remove elt data.waiting;
                  ready = PQ.add (ready_node_of v data) data.ready }
        else
          data
  in
    loop data

(* 同時に発行できる命令を見つけたときに投げる例外 *)
exception Found of stmt

(* 命令vを発行する.
 * ただし, 同時発行できる可能性もあるので, まだクロックは進めない. *)
let issue clk v data =
  (* ready集合から取り除く. *)
  let data = { data with ready = PQ.remove (ready_node_of v data) data.ready } in
  (* 命令を発行したので, クロックを進める *)
  let clk' = clk + 1 in
  let data =
    D.expand
      (fun w latency data ->
         (* wはstmtの演算結果が利用可能になるまで( = clk' + latency )は発行できない. *)
         let data =
           { data with
                 finclk = M.add w (max (M.find w data.finclk) (clk' + latency)) data.finclk }
         in
         (* vにしか依存していなかった場合は, waiting集合に移動させる. *)
         let data =
           if D.pred w data.graph = [v] then
             add_to_waiting w data
           else
             data
         in
           data)
      v data.graph data
  in
  let stmt = D.find v data.graph in
  (* 今発行した命令stmtをグラフから削除 *)
  let data = { data with graph = D.remove_vertex v data.graph } in
  (* 分岐命令が発行された場合は, 他の分岐命令は2clk先まで発行できなくなる. *)
  let data =
    if is_branch stmt.right.exp then
      let data =
        D.fold
          (fun w stmt data ->
             if is_branch stmt.right.exp then
               { data with
                     finclk = M.add w (max (M.find w data.finclk) (clk' + L.delay_slot)) data.finclk }
             else data)
          data.graph data
      in
      (* readyから分岐命令をwaitingへ移動 *)
      let data =
        PQ.fold
          (fun ((_, v) as elt) data ->
             if is_branch (D.find v data.graph).right.exp then
               add_to_waiting v
                 { data with
                       ready = PQ.remove elt data.ready }
             else data)
          data.ready data
      in
      (* waitingに属する命令の発行可能クロックを更新 *)
      let data =
        let waiting =
          PQ.fold
            (fun (_, v) acc ->
               PQ.add (waiting_node_of v data) acc)
            data.waiting PQ.empty
        in
          { data with waiting = waiting }
      in

               (*
      (* 一度分岐命令をすべて消去 *)
      let filter_branch (_, v) = not (is_branch (D.find v data.graph).right.exp) in
      let data =
        { data with
              waiting = PQ.filter filter_branch data.waiting;
              ready = PQ.filter filter_branch data.waiting; }
      in
      (* 再びwaitingに加え直す *)
      let data =
        D.fold
          (fun v stmt data ->
             if D.in_degree_of v data.graph = 0 && is_branch stmt.right.exp then
               add_to_waiting v data
             else
               data)
          data.graph data
      in
                *)
        data
    else data
  in
  (* waitingが変更されたので(clk'ではなく)clkまで進める.
   * この時点で新たにreadyに追加されるものは, stmtと逆依存関係をもつ命令である.
   * (その場合はstmtと同時に発行してもよい.) *)
  let data = next_clk clk data in
    (clk', data)


(* 発行すべき命令を発行できなかった場合に生ずる例外 *)
exception FailedIssue

(* dataをスケジューリングする. ディレイスロットも埋める.
 * 返り値
 * 第一要素: スケジューリング後の命令列.
 * 第二要素: このブロックにスケジューリングされなかった命令のリスト *)
let schedule data =
  let rec loop clk data =
    match (PQ.is_empty data.ready, PQ.is_empty data.waiting) with
      (* 全ての命令が発行し終わったとき. *)
      | (true, true) -> assert (D.is_empty data.graph); []
      (* クロックを進めてwaitingからreadyへ移動させる *)
      | (true, false) ->
          let (clk', _) = PQ.min_elt data.waiting in
          let data = next_clk clk' data in
          let stall = clk' - clk in
            assert (stall >= 0);
            (match loop clk' data with
               | stmt :: stmts ->
                   (* ストールしなければならない *)
                   { stmt with stall = stall } :: stmts
               | [] -> [])
      (* readyから命令を発行する *)
      | (false, _) ->
          let (clk, stmt, data) =
            let (_, v) = PQ.min_elt data.ready in
            let stmt = D.find v data.graph in
            let (clk', data) = issue clk v data in
            let (stmt, (clk'', data)) =
              try
                PQ.iter
                  (fun (_, v) ->
                     let stmt' = D.find v data.graph in
                       if L.is_combinable stmt stmt' then
                         raise (Found (stmt')))
                  data.ready;
                (* 同時に発行できる命令が見つからなかったとき. *)
                (stmt, (clk', data))
              with Found (stmt') ->
                (L.combine_stmt stmt stmt',
                 issue clk (id_of stmt') data)
            in
            (* 今のところは全ての命令は発行すると + 1クロックになるので, clk' = clk''なはず. *)
            let () = assert (clk' = clk'') in
            let clk' = max clk' clk'' in
            (* 命令を発行したのでクロックを進める.
             * 注意: issueではクロックは進まない. *)
            let data = next_clk clk' data in
              (clk', stmt, data)
          in
            stmt :: loop clk data
  in
  let stmts = loop 0 data in
    stmts

(* stmtsから2clk内の命令を取り出す *)
let take_delay_slot stmts =
  let rec f lim = function
    | stmt :: stmts when stmt.stall < lim ->
        let (res, rem) = f (lim - (stmt.stall + 1)) stmts in
          (stmt :: res, rem)
    | stmts -> ([], stmts)
  in
    f L.delay_slot stmts

(* bに含まれる分岐を分解し, (ただし, bの分岐には遅延スロットはないと考える.)
 * 最終的にontraceの辺をb_endへ繋げる *)
let rec split_block ~branchenv b b_end cfg =
  try
    let (stmts, branch, rem) = partition_block (G.find b cfg) in
    let (ontrace, offtrace, bookkeep) = M.find branch.right.s_id branchenv in
    let cfg =
      List.fold_left
        (fun cfg (b', e_tag, _) -> G.add_edge b b' e_tag cfg)
        cfg offtrace
    in
    let cfg = G.update_block b (List.map copy_stmt (stmts @ [branch])) cfg in
      (match ontrace with
         | [_, e_tag] ->
             let (b', cfg) = G.add_block rem cfg in
             let cfg = G.add_edge b b' e_tag cfg in
               split_block ~branchenv b' b_end cfg
         (* ontraceなる辺は1つは存在するはず.
          * もし存在しないとすれば, その分岐はトレースの最後にある分岐であり, それはbookkeepには含まれないから. *)
         | _ -> assert false)
  with Invalid_argument ("partition_block") ->
    (* もう分岐を分解しつくしたとき.
     * 最後にJmpを加えてb_endへ飛ぶ *)
    let cfg = G.update_block b (List.map copy_stmt (G.find b cfg @ [gen_stmt [] Jmp])) cfg in
    let cfg = G.add_edge b b_end Other cfg in
      cfg

(* bにある分岐命令を複数含んだ命令列を, 分解しながらbookkeepを挿入していく. *)
let rec compansate ?(issued = S.empty) ~succ_of_trace ~branchenv b cfg =
  try
    (* stmts: このブロックに収まる分岐 + 遅延スロット
     * rem: 収まらなかった命令列
     * の2つに分解する. *)
    let (stmts, branch, rem) = partition_block (G.find b cfg) in
    let (stmts, rem) =
      let (delay, rem) = take_delay_slot rem in
      let stmts = stmts @ [branch] @ delay in
        (stmts, rem)
    in
    (* bをstmtsで更新. *)
    let cfg = G.update_block b (List.map copy_stmt stmts) cfg in
    (* この分岐が確定する前に発行される命令の集合を計算する. *)
    let issued =
      List.fold_left
        (fun acc stmt ->
           fold_half
             (fun half acc -> S.add half.s_id acc)
             stmt acc)
        issued stmts
    in
    let (ontrace, offtrace, bookkeep) = M.find branch.right.s_id branchenv in
    (* この分岐が確定される前に発行されるべきもののうち, 実際に発行されなかったものを計算する. *)
    let bookkeep =
      List.map
        (filter_half
           (fun { s_id } -> not (S.mem s_id issued)))
        bookkeep
    in
    (* offtraceのときの補償コードを付け加える *)
    let cfg =
      List.fold_left
        (fun cfg (b', e_tag, _) ->
           (* 辺 b -> b' がofftraceの辺なので, 間にbookkeepを挟む *)
           let (b'', cfg) = G.add_block bookkeep cfg in
           let cfg = G.add_edge b b'' e_tag cfg in
           (* b''に含まれる分岐命令を分解し, 最終的なontraceの辺をb'に繋げる *)
           let cfg = split_block ~branchenv b'' b' cfg in
             cfg)
        cfg offtrace
    in
    (* ontraceの処理. *)
      assert (List.length ontrace <= 1);
    let cfg =
      List.fold_left
        (fun cfg (_, e_tag) ->
           let (b', cfg) = G.add_block rem cfg in
           let cfg = G.add_edge b b' e_tag cfg in
             compansate ~issued ~succ_of_trace ~branchenv b' cfg)
        cfg ontrace
    in
      cfg
  with Invalid_argument ("partition_block") ->
    (* 分岐命令がないときは, トレースの出口がJmpであって,
     * スケジューリングの前に削除されてしまったことに起因する. *)
    let cfg = G.update_block b (List.map copy_stmt (G.find b cfg @ [gen_stmt [] Jmp])) cfg in
      if not (List.length succ_of_trace = 1) then
      assert (List.length succ_of_trace = 1);
    let cfg = G.add_edge b (List.hd succ_of_trace) Other cfg in
      cfg




let g traces cfg =
  let rec loop liveness = function
    | [], cfg -> ([], cfg)
    | trace :: traces, cfg ->
(*        Format.eprintf "Trace Scheduling of (%s)@." (String.concat ", " trace);*)
(*        if List.hd trace = "b.151504" then debug_mode := true; *)
        (*
        if !debug_mode then
          output_trace "a.dot" trace cfg;
         *)
        (* トレース全体の命令列stmtsとifのジャンプ先ifenvを計算する. *)
        let (stmts, branchenv) =
          List.fold_right
            (fun b acc ->
               List.fold_right
                 (fun stmt (acc, branchenv) ->
                    let stmt =
                      (* Jmpは削除する. *)
                      if stmt.right.exp = Jmp then
                        stmt_of_half stmt.left
                      else
                        stmt
                    in
                    (stmt :: acc,
                     if is_branch stmt.right.exp then
                       let (ontrace, offtrace) =
                         List.partition
                           (fun b' -> Trace.is_trace_edge b b' trace)
                           (G.succ b cfg)
                       in
                       let ontrace = List.map (fun b' -> (b', G.tag_of_edge b b' cfg)) ontrace in
                       (* offtraceの辺で生存している変数を計算する. *)
                       let offtrace =
                         List.map
                           (fun b' ->
                              (b', G.tag_of_edge b b' cfg,
                               VLiveness.(live_in_of (G.find b' cfg) (live_of b' liveness))))
                           offtrace
                       in
                       (* この分岐の前に発行されるべき命令を順に集める
                        * すなわち, b以前のトレースのブロックに含まれる命令全体. *)
                       let bookkeep =
                         let before = Common.take_while (fun b' -> b <> b') trace in
                           List.fold_left
                             (fun acc b ->
                                acc @ G.find b cfg)
                             [] (before @ [b])
                       in
                       (* Jmpは不要なので削除 *)
                       let bookkeep = List.filter (fun stmt -> stmt.right.exp <> Jmp) bookkeep in
                         M.add stmt.right.s_id
                           (ontrace, offtrace, bookkeep)
                           branchenv
                     else branchenv))
                 (G.find b cfg) acc)
            trace ([], M.empty)
        in
        (* スケジューリングのためのdataを構築 *)
        let data =
          let liveout =
            M.map
              (fun (_, offtrace, bookkeep) ->
                 List.fold_left
                   (fun acc (_, _, live) ->
                      S.union acc live)
                   S.empty offtrace)
              branchenv
          in
            (* TODO: 前のブロックの状態を考えるべきだろうか? *)
(*            build ~stmtss:(L.stmts_before (List.hd trace) cfg) ~liveout stmts*)
            build ~liveout stmts
        in
        (* 実際にスケジューリングを行う *)
        let stmts = schedule data in
        (* 補償コードを追加する *)
        let (traces, cfg) =
          let b = List.hd trace in
          (* トレースの出口がJmpだった場合には, stmtsにもbranchenvにも出口の情報がないことになるので,
           * トレースの出口の情報を渡す *)
          let succ_of_trace = G.succ (List.hd (List.rev trace)) cfg in
          (* とりあえずブロックbにstmtsを更新. stmtsは複数の分岐を含みうることに注意. *)
          let cfg = G.update_block b stmts cfg in
          (* bから出る辺は削除する. *)
          let cfg = G.expand (fun b' _ cfg -> G.remove_edge b b' cfg) b cfg cfg in
          (* いったんb以外のトレースを削除する. *)
          let cfg = List.fold_right G.remove_vertex (List.tl trace) cfg in
          let cfg = compansate ~succ_of_trace ~branchenv b cfg in
            (traces, cfg)
        in
        let (traces, cfg) = loop liveness (traces, cfg) in
          (trace :: traces, cfg)
  in
  let liveness = VLiveness.create cfg in
    loop liveness (traces, cfg)

(* TODO: 本当はトレースの情報を更新すべきだが, off-traceのコードは無視する. *)
let f func2traces prog =
  Time.start "# Trace Scheduling";
  (* 遅延スロットがあるとbookkeepingの処理が難しくなるので, 遅延スロットは破壊しておく. *)
  let prog = L.cut_off_delay_slot prog in
  let prog =
    let entries =
      M.mapi
        (fun l func ->
           let traces = M.find l func2traces in
           let (traces, cfg) = g traces func.cfg in
             (* TODO: tracesの更新 *)
             { func with cfg })
        prog.entries
    in
      { prog with entries }
  in
  Time.stop "# Trace Scheduling";
  prog


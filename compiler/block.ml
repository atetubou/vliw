type id_or_imm = V of Id.t | C of int

let string_of_ii = function
  | V v -> v
  | C c -> string_of_int c

let list_of_ii = function
  | V x -> [x]
  | C c -> []

(* Alpha変換してはいけない変数か
 * レジスタ or 先頭が?の変数 *)
let is_global x = Asm.is_reg x || x.[0] = '?'

type fpu_funct =
  (* 右オペランドの符号反転 *)
  | FPUBit
  (* 演算結果の符号反転 *)
  | NegBit
  (* 演算結果の絶対値 *)
  | AbsBit

type fpu_functs = fpu_funct list

type exp =
  (* 関数の入口．その関数において入口生存なものを擬似的に定義する. *)
  | Entry
  (* 関数の出口．その関数において出口生存なものを擬似的に使用する. *)
  | Return
  | Nop
  | Set of int
  (* 同じCFG上のblock idを設定 or グローバル配列の名前 *)
  | SetLabel of Id.label
  | FSet of float
  | Mov of Id.t
  | FMov of Id.t * fpu_functs
  | Add of id_or_imm * Id.t
  | Sub of id_or_imm * Id.t
  | ShiftL of Id.t * int
  | ShiftR of Id.t * int
  | FAdd of Id.t * Id.t * fpu_functs
  | FMul of Id.t * Id.t * fpu_functs
  | FInv of Id.t * fpu_functs
  | FSqrt of Id.t * fpu_functs
  | Load of Id.t * id_or_imm
  | Store of Id.t * Id.t * id_or_imm
  | FLoad of Id.t * id_or_imm
  | FStore of Id.t * Id.t * id_or_imm
  | LoadC of Id.t * id_or_imm
  | StoreC of Id.t * Id.t * id_or_imm
  | FLoadC of Id.t * id_or_imm
  | FStoreC of Id.t * Id.t * id_or_imm
  | Send of Id.t
  (* 下位8bitに標準入力から代入. use集合はdefと等しいか空であるのどちらか. *)
  | Recv
  | ItoF of Id.t
  | FtoI of Id.t
  | Call of Id.label * Id.t list (* jump with link; これは分岐だと考えない *)
  | IfEq of Id.t * id_or_imm
  | IfLt of Id.t * id_or_imm
  | IfFEq of Id.t * Id.t
  | IfFLt of Id.t * Id.t
  | Jmp
  (* 変数に代入されているアドレスへジャンプする *)
  | JumpTo of Id.t

type stmt = {
  (* 文のID *)
  s_id : Id.t;
  (* 式 *)
  exp : exp;
  (* この文が使用しうるすべての変数.
   * expで使用される変数集合を含む. *)
  use : Id.t list;
  (* この文が定義しうる全ての変数.
   * 順番にも意味があることに注意. *)
  def : (Id.t * Type.t) list;
}

(* 基本ブロック *)
type block = stmt list

(* s_idを生成する *)
let gensid () = Id.genid "s"
(* blockのIDを生成する. *)
let genbid () = Id.genid "b"

(* リストを 最後の要素と,それ以外の要素に分解する *)
let partition_last stmts =
  let stmts = List.rev stmts in
    (List.hd stmts, List.rev (List.tl stmts))

(* destとexpからstmtを生成する. ただし, Entry, Return, Recvは生成できない. *)
let gen_stmt dest exp =
  match (dest, exp) with
  | []                    , Nop                 -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_]                   , Set _               -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_]                   , SetLabel _          -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_, Type.Float]       , FSet _              -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_]                   , Mov (x)             -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_, Type.Float]       , FMov (x, funct)     -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , Add (x', y)         -> { s_id = gensid (); exp = exp; use = y :: list_of_ii x';      def = dest }
  | [_]                   , Sub (x', y)         -> { s_id = gensid (); exp = exp; use = y :: list_of_ii x';      def = dest }
  | [_]                   , ShiftL (x, i)       -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , ShiftR (x, i)       -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_, Type.Float]       , FAdd (x, y, funct)  -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | [_, Type.Float]       , FMul (x, y, funct)  -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | [_, Type.Float]       , FInv (x, funct)     -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_, Type.Float]       , FSqrt (x, funct)    -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , Load (x, y')        -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | ([] | [_, Type.Unit]) , Store (x, y, z')    -> { s_id = gensid (); exp = exp; use = x :: y :: list_of_ii z'; def = []   }
  | [_, Type.Float]       , FLoad (x, y')       -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | ([] | [_, Type.Unit]) , FStore (x, y, z')   -> { s_id = gensid (); exp = exp; use = x :: y :: list_of_ii z'; def = []   }
  | [_]                   , LoadC (x, y')       -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | ([] | [_, Type.Unit]) , StoreC (x, y, z')   -> { s_id = gensid (); exp = exp; use = x :: y :: list_of_ii z'; def = []   }
  | [_, Type.Float]       , FLoadC (x, y')      -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | ([] | [_, Type.Unit]) , FStoreC (x, y, z')  -> { s_id = gensid (); exp = exp; use = x :: y :: list_of_ii z'; def = []   }
  | ([] | [_, Type.Unit]) , Send (x)            -> { s_id = gensid (); exp = exp; use = [x];                     def = []   }
  | [_]                   , Recv                -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | [_, Type.Float]       , ItoF (x)            -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | [_]                   , FtoI (x)            -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | []                    , IfEq (x, y')        -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | []                    , IfLt (x, y')        -> { s_id = gensid (); exp = exp; use = x :: list_of_ii y';      def = dest }
  | []                    , IfFEq (x, y)        -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | []                    , IfFLt (x, y)        -> { s_id = gensid (); exp = exp; use = [x; y];                  def = dest }
  | []                    , JumpTo (x)          -> { s_id = gensid (); exp = exp; use = [x];                     def = dest }
  | []                    , Jmp                 -> { s_id = gensid (); exp = exp; use = [];                      def = dest }
  | _                     , Call (l, xs)        -> { s_id = gensid (); exp = exp; use = xs;                      def = dest }
  | _                                         -> assert false

(* args: 入口生存なもの *)
let gen_entry  args = { s_id = gensid (); exp = Entry; use = []; def = args }
(* rets: 出口生存なもの *)
let gen_return rets = { s_id = gensid (); exp = Return; use = rets; def = [] }

(* 型typに合うstoreやloadをつくる *)
let gen_store typ x (y, z) =
  gen_stmt []
    (match typ with Type.Float -> FStore (x, y, z) | _ -> Store (x, y, z))
let gen_load  typ x (y, z) =
  gen_stmt [x, typ]
    (match typ with Type.Float -> FLoad (y, z) | _ -> Load (y, z))

let gen_store_c typ x (y, z) =
  gen_stmt []
    (match typ with Type.Float -> FStoreC (x, y, z) | _ -> StoreC (x, y, z))
let gen_load_c  typ x (y, z) =
  gen_stmt [x, typ]
    (match typ with Type.Float -> FLoadC (y, z) | _ -> LoadC (y, z))





(* CFGの辺のタグ *)
type branch_tag = Then | Else | Other

(* CFGのデータ構造 *)
module G =
struct
  module GraphTag =
  struct
    type vertex = Id.t
    type v_tag = block
    type e_tag = branch_tag
  end

  module G = Graph.Make (GraphTag)
  include G

  (* 無効なブロックを表す. *)
  let null_block = Id.genid "INVALID"

  (* 新しいブロックを作成する *)
  let add_block stmts g =
    let b = genbid () in
      (b, G.add_vertex b stmts g)

  (* ブロックを更新する. *)
  let update_block b stmts g = assert (G.mem b g); G.add_vertex b stmts g

  (* bの後続ブロックのうち、e_tagの辺で結ばれているものを1つ選ぶ.
   * そのような辺が存在しない場合は, Not_found. *)
  let succ_of b e_tag g =
    assert (M.mem b g);
    let succ = M.filter (fun _ e_tag' -> e_tag' = e_tag) (M.find b g).succ in
      fst (M.choose succ)

  (* v -> v'を縮約する。
   * ただし, vとv'にはられるThen, Elseの辺がある場合は、その分岐をJmpで置き換える.
   * なお、JumpToの出次数も2以上であるが、これは縮約によって辺が併合されてもよい. *)
  let contract v v' v_tag g =
    let is_tag_of_if = function Then | Else -> true | _ -> false in
    let pred = G.pred v g in
    let g =
      List.fold_left
        (fun g w ->
           if G.has_edge w v' g &&
              is_tag_of_if (G.tag_of_edge w v' g)
           then
             (* w -> v, w -> v'の辺があるので、w -> v'が分岐の片方であれば、もう一方の辺も分岐の片方であるはず. *)
             let () = assert (is_tag_of_if (G.tag_of_edge w v g)) in
             (* 辺を削除してから、新しいJmpの辺を追加する. *)
             let g = G.remove_edge w v g in
             let g = G.remove_edge w v' g in
             let g = G.add_edge w v Other g in
             let (_, stmts) = partition_last (G.find w g) in
             let g = G.add_vertex w (stmts @ [gen_stmt [] Jmp]) g in
               g
           else g)
        g pred
    in
      G.contract v v' v_tag g

  (* ブロックbをコピーする *)
  let copy_block b g =
    let stmts = List.map (fun stmt -> { stmt with s_id = gensid () }) (G.find b g) in
    let (b', g) = add_block stmts g in
      (b', g)

  (* fがtrueなるブロックのリストを返す. *)
  let filter (f : Id.t -> block -> bool) g = G.fold (fun v stmts acc -> if f v stmts then v :: acc else acc) g []

  (* 各文についてfによって変換する. *)
  let map_stmt (f : stmt -> stmt) g = G.map (fun v -> List.map (fun stmt -> f stmt)) g

  (* 各文について畳み込む *)
  let fold_stmt (f : stmt -> 'a -> 'a) g init = G.fold (fun _ stmts acc -> List.fold_left (fun acc stmt -> f stmt acc) acc stmts) g init

  (* 各文sについて, 文のリストf sで置き換える. *)
  let map_stmt_list (f : stmt -> stmt list) g = G.map (fun v stmts -> List.fold_left (fun acc stmt -> acc @ f stmt) [] stmts) g

  (* 各文についてfを満たさないものを削除する. *)
  let filter_stmt (f : stmt -> bool) g = G.map (fun _ stmts -> List.filter f stmts) g
end

(* 関数の情報 *)
type func = {
  (* 関数のCFG *)
  cfg : G.t;
  (* CFG上での関数の入り口 *)
  v_entry : G.vertex;
  (* 引数. *)
  args : (Id.t * Type.t) list;
  (* 関数の返り値. 複数の値を返せる. *)
  rets : (Id.t * Type.t) list;
  (* スタックフレームのサイズ *)
  stack_size : int;
}


(* 関数呼び出しグラフ *)
module F =
  Graph.Make
    (struct
       type vertex = Id.t
       (* 関数のおおよそのプログラムサイズ *)
       type v_tag = int
       (* 関数の呼び出し箇所の個数 *)
       type e_tag = int
     end)


(* プログラム全体の情報 *)
type program = {
  (* 関数呼び出しグラフ *)
  call_graph : F.t;
  (* 関数名 -> 関数の情報 *)
  entries : func M.t;
  (* ヒープポインタ, ゼロレジスタなど, 異なる関数からアクセスできる変数全体.
   * レジスタでなければならない. *)
  global_variables : (Id.t * Type.t) list;
}


(* プログラムの各関数について変換する *)
let map_func f prog =
  { prog with
        entries =
          M.mapi (fun l func -> f l func) prog.entries }

(* プログラム全体のCFGに対して, 関数fを適用して変換する. *)
let map_cfg f prog =
  map_func (fun l func -> { func with cfg = f l func.cfg }) prog


let is_branch = function (* 必ず基本ブロックの最後の命令はこれらのいずれか. *)
  | IfEq _ | IfLt _ | IfFEq _ | IfFLt _ | Return | Jmp | JumpTo _ -> true
  | _ -> false

(* def以外の変数で, envに含まれる変数名を置き換える. *)
let replace_use_stmt env stmt =
  let f x = if M.mem x env then M.find x env else x in
  let f' = function V x -> V (f x) | C i -> C i in
  let stmt = { stmt with use = List.map f stmt.use } in
  let exp = match stmt.exp with
    | Mov (x)            -> Mov (f x)
    | Add (x', y)        -> Add (f' x', f y)
    | Sub (x', y)        -> Sub (f' x', f y)
    | ShiftR (x, i)      -> ShiftR (f x, i)
    | ShiftL (x, i)      -> ShiftL (f x, i)
    | Load (x, y')       -> Load (f x, f' y')
    | Store (x, y, z')   -> Store (f x, f y, f' z')
    | LoadC (x, y')      -> LoadC (f x, f' y')
    | StoreC (x, y, z')  -> StoreC (f x, f y, f' z')
    | FMov (x, funct)    -> FMov (f x, funct)
    | FAdd (x, y, funct) -> FAdd (f x, f y, funct)
    | FMul (x, y, funct) -> FMul (f x, f y, funct)
    | FInv (x, funct)    -> FInv (f x, funct)
    | FSqrt (x, funct)   -> FSqrt (f x, funct)
    | FLoad (x, y')      -> FLoad (f x, f' y')
    | FStore (x, y, z')  -> FStore (f x, f y, f' z')
    | FLoadC (x, y')     -> FLoadC (f x, f' y')
    | FStoreC (x, y, z') -> FStoreC (f x, f y, f' z')
    | Send (x)           -> Send (f x)
    | ItoF (x)           -> ItoF (f x)
    | FtoI (x)           -> FtoI (f x)
    | Call (l, ys)       -> Call (l, List.map f ys)
    | IfEq (x, y')       -> IfEq (f x, f' y')
    | IfLt (x, y')       -> IfLt (f x, f' y')
    | IfFEq (x, y)       -> IfFEq (f x, f y)
    | IfFLt (x, y)       -> IfFLt (f x, f y)
    | JumpTo (x)         -> JumpTo (f x)
    | e                  -> e
  in
    { stmt with exp = exp }

(* envに従って変数名を置き換える. envに属さない変数は置き換えない. *)
let replace_stmt env stmt =
  let stmt = replace_use_stmt env stmt in
  let stmt = { stmt with def = List.map (fun (x, t) -> if M.mem x env then (M.find x env, t) else (x, t)) stmt.def } in
    stmt

(* envで束縛されていない変数についてはアルファ変換.
 * envで束縛されている変数xに対しては、env(x)に変換する.
 * ただし、グローバル変数は変換しない.
 * (env, stmt)を返す.
 * env: 新たに束縛された変数を含む環境.
 * stmt: 変換後の文. s_idも変換される. *)
let alpha_stmt env stmt =
  let re = ref env in
  let find = function
    (* グローバル変数( or レジスタ)の場合は変換しない *)
    | x when is_global x -> x
    | x when M.mem x !re -> M.find x !re
    (* envに束縛されていないものについては、新たな変数名によって束縛する *)
    | x ->
        let x' = Id.genid x in
          re := M.add x x' !re;
          x'
  in
  let find' = function
    | V (x) -> V (find x)
    | C (i) -> C (i)
  in
  let def = List.map (fun (x, t) -> (find x, t)) stmt.def in
  let use = List.map find stmt.use in
  let exp = match stmt.exp with
    | Mov (x)            -> Mov (find x)
    | Add (x', y)        -> Add (find' x', find y)
    | Sub (x', y)        -> Sub (find' x', find y)
    | ShiftR (x, i)      -> ShiftR (find x, i)
    | ShiftL (x, i)      -> ShiftL (find x, i)
    | Load (x, y')       -> Load (find x, find' y')
    | Store (x, y, z')   -> Store (find x, find y, find' z')
    | LoadC (x, y')      -> LoadC (find x, find' y')
    | StoreC (x, y, z')  -> StoreC (find x, find y, find' z')
    | FMov (x, funct)    -> FMov (find x, funct)
    | FAdd (x, y, funct) -> FAdd (find x, find y, funct)
    | FMul (x, y, funct) -> FMul (find x, find y, funct)
    | FInv (x, funct)    -> FInv (find x, funct)
    | FSqrt (x, funct)   -> FSqrt (find x, funct)
    | FLoad (x, y')      -> FLoad (find x, find' y')
    | FStore (x, y, z')  -> FStore (find x, find y, find' z')
    | FLoadC (x, y')     -> FLoadC (find x, find' y')
    | FStoreC (x, y, z') -> FStoreC (find x, find y, find' z')
    | Send (x)           -> Send (find x)
    | ItoF (x)           -> ItoF (find x)
    | FtoI (x)           -> FtoI (find x)
    | Call (l, ys)       -> Call (l, List.map find ys)
    | IfEq (x, y')       -> IfEq (find x, find' y')
    | IfLt (x, y')       -> IfLt (find x, find' y')
    | IfFEq (x, y)       -> IfFEq (find x, find y)
    | IfFLt (x, y)       -> IfFLt (find x, find y)
    | JumpTo (x)         -> JumpTo (find x)
    | e                  -> e
  in
    (!re, { s_id = gensid (); exp = exp; use = use; def = def })


(* 関数func内で使用されうる変数を全て集める *)
let gather_variables_of func =
  G.fold
    (fun b ->
       List.fold_right
         (fun { use = use; def = def } ->
            S.union (S.union (S.of_list use) (S.of_list (List.map fst def)))))
    func.cfg S.empty

(*** begin of debug ***)
let string_of_funct funct =
  String.concat ", " (List.map (function FPUBit -> "FPU" | NegBit -> "Neg" | AbsBit -> "Abs") funct)

let string_of_stmt { s_id = s_id; exp = exp; use = use; def = def } =
  "" ^ s_id ^ ": " ^
  (if def = [] then
     ""
   else
     String.concat ", "
       (List.map
          (fun (x, t) -> x ^ " : " ^ Type.string_of t)
          def) ^
     " <- ") ^ 
  (match exp with
     | Nop -> "Nop"
     | Set i -> "Set(" ^ string_of_int i ^ ")"
     | SetLabel l -> "SetLabel(" ^ l ^ ")"
     | FSet f -> "FSet(" ^ string_of_float f ^ ")"
     | Mov x -> "Mov(" ^ x ^ ")"
     | Add (x', y) -> "Add(" ^ string_of_ii x' ^ " + " ^ y ^ ")"
     | Sub (x', y) -> "Sub(" ^ string_of_ii x' ^ " - " ^ y ^ ")"
     | ShiftR (x, i) -> "ShiftR(" ^ x ^ " >> " ^ string_of_int i ^ ")"
     | ShiftL (x, i) -> "ShiftL(" ^ x ^ " << " ^ string_of_int i ^ ")"
     | Load (x, y') -> "Load(" ^ x ^ " + " ^ string_of_ii y' ^ ")"
     | Store (x, y, z') -> "Store(M[" ^ y ^ " + " ^ string_of_ii z' ^ "] = " ^ x ^ ")"
     | LoadC (x, y') -> "LoadC(" ^ x ^ " + " ^ string_of_ii y' ^ ")"
     | StoreC (x, y, z') -> "StoreC(M[" ^ y ^ " + " ^ string_of_ii z' ^ "] = " ^ x ^ ")"
     | FMov (x, funct) -> "FMov(" ^ x ^ "; " ^ string_of_funct funct ^ ")"
     | FAdd (x, y, funct) -> "FAdd(" ^ x ^ " + " ^ y ^ "; " ^ string_of_funct funct ^ ")"
     | FMul (x, y, funct) -> "FMul(" ^ x ^ " * " ^ y ^ "; " ^ string_of_funct funct ^ ")"
     | FInv (x, funct) -> "FInv(1.0 /. " ^ x ^ "; " ^ string_of_funct funct ^ ")"
     | FSqrt (x, funct) -> "FSqrt(" ^ x ^ "; " ^ string_of_funct funct ^ ")"
     | FLoad (x, y') -> "FLoad(" ^ x ^ " + " ^ string_of_ii y' ^ ")"
     | FStore (x, y, z') -> "FStore(M[" ^ y ^ " + " ^ string_of_ii z' ^ "] = " ^ x ^ ")"
     | FLoadC (x, y') -> "FLoadC(" ^ x ^ " + " ^ string_of_ii y' ^ ")"
     | FStoreC (x, y, z') -> "FStoreC(M[" ^ y ^ " + " ^ string_of_ii z' ^ "] = " ^ x ^ ")"
     | Send x -> "Send(" ^ x ^ ")"
     | Recv -> "Recv"
     | ItoF x -> "ItoF(" ^ x ^ ")"
     | FtoI x -> "FtoI(" ^ x ^ ")"
     | IfEq (x, y') -> "IfEq(" ^ x ^ " = " ^ string_of_ii y' ^ ")"
     | IfLt (x, y') -> "IfLt(" ^ x ^ " < " ^ string_of_ii y' ^ ")"
     | IfFEq (x, y) -> "IfFEq(" ^ x ^ " = " ^ y ^ ")"
     | IfFLt (x, y) -> "IfFLt(" ^ x ^ " < " ^ y ^ ")"
     | Call (l, xs) -> "Call " ^ l ^ "(" ^ (String.concat ", " xs) ^ ")"
     | Entry -> "Entry"
     | Return -> "Return"
     | Jmp -> "Jmp"
     | JumpTo (x)         -> "JumpTo(" ^ x ^ ")")

let string_of_block b stmts =
  "" ^ b ^ ":\n" ^
  List.fold_left (fun res stmt -> res ^ "\n" ^ string_of_stmt stmt) "" stmts

let output_cfg_to oc graph =
  Printf.fprintf oc "digraph cfg {\n";
  Printf.fprintf oc "node [shape=box];\n";
  G.fold
    (fun b block () ->
       Printf.fprintf oc "%s [label=\"%s\"];\n" (Id.escape b) (String.escaped (string_of_block b block)))
    graph ();
  G.fold_edge
    (fun v w e_tag () ->
       let s_tag = match e_tag with Then -> "Then" | Else -> "Else" | _ -> "" in
       let style = "" in(*let style = match e_tag with Continue -> "style=dotted" | _ -> "" in *)
         Printf.fprintf oc "%s -> %s [label=\"%s\"%s];\n" (Id.escape v) (Id.escape w) (String.escaped s_tag) style) graph ();
  Printf.fprintf oc "}\n"
let output_cfg path graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    output_cfg_to oc graph;
    close_out oc
let output_prog path prog =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    M.iter (fun l { cfg = cfg } -> output_cfg_to oc cfg) prog.entries;
    close_out oc

let output_call_graph path graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
    Printf.fprintf oc "digraph call_graph {\n";
    F.fold
      (fun l _ () ->
         Printf.fprintf oc "%s [label=\"%s\"];\n" l (String.escaped l))
      graph ();
    F.fold_edge
      (fun v w e_tag () ->
         Printf.fprintf oc "%s -> %s;\n" v w) graph ();
    Printf.fprintf oc "}\n";
    close_out oc
(*** end of debug ***)


let reverse cfg = (* 処理しやすいように, 基本ブロック内の命令列を逆順にする *)
  G.map (fun v stmts -> List.rev stmts) cfg


let check_immediate_bits = function
  | Add (C (i), _) | Sub (C (i), _) | Load (_, C (i)) | Store (_, _, C (i))
  | FLoad (_, C (i)) | FStore (_, _, C (i)) -> assert (Asm.fit_immediate i)
  | IfEq (_, C (i)) | IfLt (_, C (i)) -> assert (Asm.fit_small_immediate i)
  | _ -> ()

(* 文に対して満たすべき性質を確認する. *)
let check_consistency_of_stmt stmt =
  (* lsはls'に含まれるか. *)
  let is_subset ls ls' = List.for_all (fun x -> List.mem x ls') ls in
  (* 2つのリストが集合として等しいか. *)
  let is_equal ls ls' = is_subset ls ls' && is_subset ls' ls in
    check_immediate_bits stmt.exp;
    match stmt with
      (* Entryは引数やグローバル変数を定義する. *)
      | { def = _; exp = Entry; use = [] } -> ()
      (* Callの場合. useは引数と等しい. *)
      | { exp = Call (l, xs); use = use } when is_equal xs use -> ()
      (* Returnは、出口生存であるもの全てを"使用"する. *)
      | { def = []; exp = Return; use = _ } -> ()
      (* Recvは引数レジスタの下位8bitに代入するので、自分自身も使用する.
       * しかし上位24bitが切り捨てられる場合には, 使用しない. *)
      | { def = [(x, t)]; exp = Recv; use = [y] } when t <> Type.Float && x = y -> ()
      | { def = [(_, t)]; exp = Recv; use = [] } when t <> Type.Float -> ()
      (* 整数を定義するもの *)
      | { def = [(_, t)]; exp = (Set _ | SetLabel _); use = [] } when t <> Type.Float -> ()
      | { def = [(_, t)];
          exp = Mov (x) | Add (C _, x) | Sub (C _, x) | ShiftL (x, _) | ShiftR (x, _) | Load (x, C _) | LoadC (x, C _) | FtoI (x);
          use = [y] }
          when t <> Type.Float && x = y -> ()
      | { def = [(_, t)];
          exp = Add (V (x), y) | Sub (V (x), y) | Load (x, V (y)) | LoadC (x, V (y));
          use = use }
          when t <> Type.Float && is_equal use [x; y] -> ()
      (* Floatを定義するもの *)
      | { def = [(_, Type.Float)]; exp = FSet _; use = [] } -> ()
      | { def = [(_, Type.Float)];
          exp = FMov (x, _) | FInv (x, _) | FSqrt (x, _) | FLoad (x, C _) | FLoadC (x, C _) | ItoF (x);
          use = [y] }
          when x = y -> ()
      | { def = [(_, Type.Float)];
          exp = FAdd (x, y, _) | FMul (x, y, _) | FLoad (x, V (y)) | FLoadC (x, V (y));
          use = use }
          when is_equal use [x; y] -> ()
      (* 何も定義しないもの *)
      | { def = []; exp = Jmp | Nop; use = [] } -> ()
      | { def = []; exp = Send (x) | IfEq (x, C _) | IfLt (x, C _) | JumpTo (x); use = [y] } when x = y -> ()
      | { def = [];
          exp = IfEq (x, V (y)) | IfLt (x, V (y)) | IfFEq (x, y) | IfFLt (x, y) | Store (x, y, C _) | FStore (x, y, C _) | StoreC (x, y, C _) | FStoreC (x, y, C _);
          use = use }
          when is_equal use [x; y] -> ()
      | { def = []; exp = Store (x, y, V (z)) | FStore (x, y, V (z)) | StoreC (x, y, V (z)) | FStoreC (x, y, V (z)); use = use }
          when is_equal use [x; y; z] -> ()
      (* 以上以外であれば間違っている. *)
      | _ -> Format.eprintf "A statement '%s' is not consistent@." (string_of_stmt stmt); assert false

(* CFGが満たすべき性質を確認する *)
let check_consistency_of_cfg cfg =
  (* 文IDに重複がないか確認する *)
  ignore
    (G.fold
       (fun b ->
          List.fold_right
            (fun ({ s_id = s_id } as stmt) env ->
               if S.mem s_id env then Format.eprintf "%s is duplicated in %s@." (string_of_stmt stmt) b;
               assert (not (S.mem s_id env));
               S.add s_id env))
       cfg S.empty);
  (* graph.mlが正常かどうか調べる. *)
  G.iter
    (fun b _ ->
       List.iter (fun b' -> assert (G.mem b' cfg)) (G.succ b cfg);
       List.iter (fun b' -> assert (G.mem b' cfg)) (G.pred b cfg))
    cfg;
  (* 各文について一貫性を調べる. *)
  G.iter (fun _ -> List.iter check_consistency_of_stmt) cfg;
  (* SetLabelが同じCFG内のブロックのラベルを示しているか, もしくはグローバル配列か. *)
  G.iter (fun _ -> List.iter (function { exp = SetLabel (l) } -> assert (G.mem l cfg || M.mem l !Global.all) | _ ->())) cfg;
  (* 各ブロックの分岐と, そのブロックから出る辺が一貫しているかどうか調べる. *)
  G.iter
    (fun v stmts ->
       (* 何もないブロックはないはず。最低でもJmpはあるはず. *)
       assert (stmts <> []);
       let ({ exp = branch } as branch_stmt, stmts) = partition_last stmts in
       let deg = G.out_degree_of v cfg in
         (* 基本ブロックの途中には分岐命令はないはず *)
         assert (List.for_all (fun { exp = exp } -> not (is_branch exp)) stmts);
         (* 基本ブロックの最後の命令は分岐であるはず *)
         if (not (is_branch branch)) then
           Format.eprintf "%s is not branch! @." (string_of_stmt branch_stmt);
         assert (is_branch branch);
         (try
            (match branch with
               | IfEq _ | IfLt _ | IfFEq _ | IfFLt _ ->
                   assert (deg = 2);
                   ignore (G.succ_of v Then cfg); (* Then, Elseに一致する辺のタグがあるか？なければNot_found *)
                   ignore (G.succ_of v Else cfg)
               | Jmp ->
                   assert (deg = 1);
                   ignore (G.succ_of v Other cfg)
               | Return ->
                   (* CFGの最後を意味するので、どこにも行かないはず. *)
                   assert (deg = 0);
               | JumpTo (x) -> ()
               | _ -> assert false)
          with
              Not_found -> assert false (* 辺のタグに一致するものがなかったとき発生する(G.succ_ofで) *)))
    cfg

(* プログラム全体に対して、一貫性を確認する. *)
let check_consistency prog =
  M.iter
    (fun _ func ->
       (* CFGについて一貫性を調べる. *)
       check_consistency_of_cfg func.cfg;
       (* v_entryはcfg内の頂点であるはず. *)
       assert (G.mem func.v_entry func.cfg))
    prog.entries;
  M.iter
    (fun _ func ->
       G.iter
         (fun _ ->
            List.iter
              (function
                 | { exp = Call (l, xs); def = def } ->
                     (* 関数の型が一致することを確認する. *)
                     assert (M.mem l prog.entries);
                     let func = M.find l prog.entries in
                       if List.length def <> List.length func.rets then Format.eprintf "%s <-> %s@." (String.concat ", " (List.map fst def)) (String.concat ", " (List.map fst func.rets));
                       assert (List.length def = List.length func.rets);
                       assert (List.length xs = List.length func.args)
                 | _ -> ()))
         func.cfg)
    prog.entries


(* 不要なJmpを除去する.
 * 使用されない関数とブロックを削除する.
 * 明らかに不要な命令を削除する. *)
let rec simplify prog =
  check_consistency prog;
  (* 到達可能な関数とブロックを計算する.
   * 関数名とブロック名の末尾にはId.counterが付加されているので一意的である. *)
  let rec dfs_func vis l =
    if S.mem l vis then vis
    else
      let vis = S.add l vis in
      let func = M.find l prog.entries in
        dfs_block func.cfg vis func.v_entry
  and dfs_block cfg vis v =
    if S.mem v vis then vis
    else
      let vis = S.add v vis in
      (* 制御フローによる遷移をたどる *)
      let vis = List.fold_left (dfs_block cfg) vis (G.succ v cfg) in
      (* Callによる遷移をたどる *)
      let stmts = G.find v cfg in
        List.fold_left
          (fun vis stmt ->
             match stmt.exp with
               | Call (l, _) -> dfs_func vis l
               | _ -> vis)
          vis stmts
  in
  let reachable = dfs_func S.empty Id.main_label in
  (* 到達できない関数を削除 *)
  let prog =
    { prog with
          call_graph =
            F.fold
              (fun l _ acc -> if S.mem l reachable then acc else F.remove_vertex l acc)
              prog.call_graph prog.call_graph;
          entries = M.filter (fun l _ -> S.mem l reachable) prog.entries; }
  in
  (* 到達できないブロックを削除 *)
  let prog =
    map_cfg
      (fun l cfg ->
         G.fold
           (fun v _ cfg -> if S.mem v reachable then cfg else G.remove_vertex v cfg)
           cfg cfg)
      prog
  in
  (* 関数呼び出しグラフを再構築 *)
  let prog =
    let call_graph =
      F.fold
        (fun l _ acc -> if S.mem l reachable then acc else F.remove_vertex l acc)
        prog.call_graph prog.call_graph
    in
    let call_graph = F.fold_edge (fun l r _ -> F.remove_edge l r) call_graph call_graph in
    let call_graph =
      M.fold
        (fun l func ->
           G.fold_stmt
             (fun stmt call_graph ->
                match stmt.exp with
                  | Call (r, _) ->
                      let cnt = if F.has_edge l r call_graph then F.tag_of_edge l r call_graph else 0 in
                        F.add_edge l r (cnt + 1) call_graph
                  | _ -> call_graph)
             func.cfg)
        prog.entries call_graph
    in
      { prog with call_graph = call_graph }
  in
  

(*
  (* 削除されたブロックを参照しているSetLabelを(擬似的に)削除.
   * この代入はDElimで削除されるはず.
   * TODO: 本当に削除されているか?
   * ここらへんは何かバグっている! *)
  let prog =
    map_cfg
      (fun _ cfg->
         G.map_stmt
           (function
              | { exp = SetLabel (l) } as stmt
                  when not (G.mem l cfg || M.mem l !Global.all) -> { stmt with exp = Set (0) (* dummy *) }
              | stmt -> stmt)
           cfg)
      prog
  in*)
  check_consistency prog;
  (* (1) Jmpだけのブロックを縮約.
   * (2) Jmp先のブロックの先行ブロックがただ一つであるようなJmpを縮約.
   * conv: 元のブロック -> 新しいブロック *)
  let remove_jmp push v (conv, cfg) =
    (* 縮約をするので、vはすでにCFGから削除されているかもしれない. *)
    if not (G.mem v cfg) then (conv, cfg)
    else
      match partition_last (G.find v cfg) with
        (* Jmpだけのブロックの場合. *)
        | ({ exp = Jmp }, []) ->
            (* Jmp先のブロックwを取り出す *)
            let w = G.succ_of v Other cfg in
              if v = w then (conv, cfg)
              else (
                push v;
                (* wは削除されるので, convに追加しておく. *)
                let conv = M.add w v conv in
                let cfg = G.contract v w (G.find w cfg) cfg in
                  (conv, cfg)
              )
        | ({ exp = Jmp }, stmts) ->
            let w = G.succ_of v Other cfg in
              if v = w || G.pred w cfg <> [v] then (conv, cfg)
              else (
                (* v <> wで, wの唯一の先行ブロックがvである場合 *)
                push v;
                let conv = M.add w v conv in
                let cfg = G.contract v w (stmts @ G.find w cfg) cfg in
                  (conv, cfg)
              )
        | _ -> (conv, cfg)
  in
  let prog =
    { prog with
          entries =
            M.map
              (fun func ->
                 (* Jmpを分岐として持つブロックについて、ワークリストに挿入する. *)
                 let que = G.filter (fun v stmts -> match partition_last stmts with ({ exp = Jmp }, _) -> true | _ -> false) func.cfg in
                 let (conv, cfg) = Common.do_worklist que (M.empty, func.cfg) remove_jmp in
                 (* CFGの辺以外でブロックを参照する場合は, それを変換する. *)
                 let convert x = if M.mem x conv then M.find x conv else x in
                 let cfg =
                   G.map_stmt
                     (function
                        | { exp = SetLabel (l) } as stmt ->
                            { stmt with exp = SetLabel (convert l) }
                        | stmt -> stmt)
                     cfg
                 in
                   { func with
                         cfg = cfg;
                         v_entry = convert func.v_entry; })
              prog.entries }
  in
  check_consistency prog;
  (* 明らかに無駄な命令を省く. *)
  let prog =
    map_cfg
      (fun l cfg ->
         G.map
           (fun _ ->
              List.filter
                (function
                   | { exp = Nop } -> false
                   (* コピー命令の移動元と移動先が同じ. *)
                   | { exp = Mov (x) | FMov (x, []); def = [(y, _)] }
                       when x = y -> false
                   | _ -> true))
           cfg)
      prog
  in
  let prog =
    map_cfg
      (fun _ ->
         G.map_stmt
           (function
              | { exp = Add (C (0), x) } as stmt -> { stmt with exp = Mov (x) }
              | stmt -> stmt))
      prog
  in
  check_consistency prog;
    prog



(* srcからdestへコピーする. destは型付き.
 * srcとdestに共通部分があったらシャッフルして移動する. *)
let move_list (dest : (Id.t * Type.t) list) (src : Id.t list) =
  let mvs =
    try
      List.combine src dest
    with Invalid_argument _ as e->
      Format.eprintf "move (%s) <- (%s) is not consistent.@."
        (String.concat ", " (List.map (fun (x, t) -> x ^ ":" ^ Type.string_of t) dest))
        (String.concat ", " src);
      raise e
  in
  let mvs =
    List.filter
      (function
         (* コピー元とコピー先が同じである場合は無視する. *)
         | (s, (d, _)) when d = s -> false
         (* Unit型はコピーしない *)
         | (_, (_, Type.Unit)) -> false
         | _ -> true)
      mvs
  in
  (* (s, (d, _))をグラフの辺だとみなして辺をトポロジカルソートする. *)
  let rec shuffle xys =
    match List.partition (fun (_, (y, _)) -> List.mem_assoc y xys) xys with
      | [], [] -> []
      | (x, (y, t)) :: xys, [] ->
          let sw = Id.genid "SWAP" in
            (* yをswに退避してから, xをyへコピーする. *)
            (y, (sw, t)) :: (x, (y, t)) ::
            shuffle (List.map (function (y', (z, t)) when y = y' -> (sw, (z, t)) | yz -> yz) xys)
      | xys, acyc -> acyc @ shuffle xys
  in
  let mvs = shuffle mvs in
    (* コピー命令の列に変換する. *)
    List.map
      (function
         | (s, (d, Type.Float)) -> gen_stmt [(d, Type.Float)] (FMov (s, []))
         | (s, (d, t)) -> gen_stmt [(d, t)] (Mov (s)))
      mvs


(* グローバル変数varを追加する.
 *
 * (1) メイン関数以外の全ての関数内で入口生存かつ出口生存となる.
 * 
 * (2) Call命令のuse, defに追加される.
 * ただし, is_constがtrueのときはdefには追加されない.
 * (すなわち, 関数内で不変な場合にis_const = trueとする.)
 * 
 * (3) 全ての関数引数と戻り値にグローバル変数が追加される.
 * ただし, is_constがtrueのときには戻り値には追加されない.
 *
 * restrict = trueの場合は,
 * (2), (3)について, その関数を呼び出すことによって使用, 定義されるときのみ追加される.
 *
 * 
 * (注)
 * 1. レジスタ割当てでグローバル変数と競合するような割当てはできないので,
 * 全ての関数内で入口/出口生存としておく. (1)の条件を弱めるのは難しい.
 *
 * 2. restrictは, 後でグローバル変数の使用や定義を追加しないときにのみ, trueにすること.
 *
 *)
let add_global_variable ?(is_const = false) ?(restrict = false) (var, typ) prog =
  let prog = { prog with global_variables = (var, typ) :: prog.global_variables } in

  (* 入口生存, 出口生存に追加. *)
  let prog =
    map_cfg
      (fun l cfg ->
         if l = Id.main_label then cfg
         else
           G.map_stmt
             (function
                | { exp = Entry; def = def } as stmt ->
                    { stmt with def = (var, typ) :: def }
                | { exp = Return; use = use } as stmt ->
                    { stmt with use = var :: use }
                | stmt -> stmt)
             cfg)
      prog
  in

  (* Call-Graphを逆向きにDFSする. *)
  let rec dfs vis l =
    if S.mem l vis then vis
    else
      let vis = S.add l vis in
      let vis = List.fold_left dfs vis (F.pred l prog.call_graph) in
        vis
  in
  (* このグローバル変数を使う関数と定義する関数を集める. *)
  let (use_env, def_env) =
    (* restrict = falseの場合は全ての関数で使用と定義が行われると考える. *)
    if not restrict then
      let all = F.fold (fun l _ -> S.add l) prog.call_graph S.empty in
        (all, all)
    else
      M.fold
        (fun l func ->
           G.fold_stmt
             (fun { exp = exp; use = use; def = def } (use_env, def_env) ->
                match exp with
                  (* Entry, Returnはダミーのuse, defを持つので無視する. *)
                  | Entry | Return -> (use_env, def_env)
                  | _ ->
                      let use_env = if List.mem var use then dfs use_env l else use_env in
                      let def_env = if List.mem_assoc var def then dfs def_env l else def_env in
                        (use_env, def_env))
             func.cfg)
        prog.entries (S.empty, S.empty)
  in
  (* is_const = trueであれば, 関数の返り値に追加しない. *)
  let def_env = if is_const then S.empty else def_env in
  (* メイン関数には引数や返り値を追加しない. *)
  let (use_env, def_env) = (S.remove Id.main_label use_env, S.remove Id.main_label def_env) in

         Format.eprintf "%s is used in %s (restrict = %s)@."
           var (String.concat ", " (S.to_list use_env)) (string_of_bool restrict);
         Format.eprintf "%s is defined in %s (restrict = %s)@."
           var (String.concat ", " (S.to_list def_env)) (string_of_bool restrict);

  let prog =
    map_cfg
      (fun l ->
         G.map_stmt
           (function
              | { exp = Call (l, args); use = use; def = def } as stmt ->
                  (* 関数引数, 返り値に追加する. *)
                  let stmt = if S.mem l use_env then { stmt with exp = Call (l, args @ [var]); use = use @ [var] } else stmt in
                  let stmt = if S.mem l def_env then { stmt with def = (var, typ) :: def } else stmt in
                    stmt
              | stmt -> stmt))
      prog
  in

  (* 関数定義にも追加する. *)
  let prog =
    { prog with
          entries =
            M.mapi
              (fun l func ->
                 let func = if S.mem l use_env then { func with args = func.args @ [var, typ] } else func in
                 let func = if S.mem l def_env then { func with rets = (var, typ) :: func.rets } else func in
                   func)
              prog.entries }
  in
    prog




(* 時間の計測と表示を行う *)

let env = ref M.empty

let start s =
  env := M.add s (Sys.time ()) !env

let stop s =
  assert (M.mem s !env);
  let time = Sys.time () -. M.find s !env in
    (* 微小時間であった場合は表示しない. *)
    if time >= 1.0e-2 then
      Format.eprintf "%s: %g sec@." s (Sys.time () -. M.find s !env)


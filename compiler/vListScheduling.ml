(* VLIWのためのリストスケジューリング *)

open Vliw

(* 最大レイテンシ *)
let max_latency = 6

(* 演算結果が使用可能になるまで、間に何clock必要か。 *)
let get_latency_of = function
  (* ALU *)
  | Set _ | SetLabel _ | Mov _ | Add _ | Sub _ | ShiftL _ | ShiftR _ -> 1
  (* FPU *)
  | FMov _ | FAdd _ | FMul _ | FInv _ | FSqrt _ -> 3
  (* load from SRAM *)
  | Load _ | FLoad _ -> 6
  (* load from cache memory *)
  | LoadC _ | FLoadC _ -> 1
  (* other *)
  | Recv -> 1
  | Send _ -> 0
  | Store _ | FStore _ -> 0
  | StoreC _ | FStoreC _ -> 0
  | Entry -> 0
  | Nop -> 0
  (* Callは疑似命令なので適当にしておく. *)
  | Call _ -> 0
  (* 分岐 *)
  | IfEq _ | IfLt _ | IfFEq _ | IfFLt _ | Return | Jmp | JumpTo _ -> 0

(* 遅延スロットの個数 *)
let delay_slot = 2

(* 遅延スロットをNopで埋める.
 * そもそも分岐がない場合は無視する. *)
let fill_delay_slot stmts =
  try
    let (stmts, branch, delay) = partition_block stmts in
      (match delay with
         | [] -> stmts @ [branch] @ [{ (gen_stmt [] Nop) with stall = 1 }]
         (* stmtのストールを増やす方法もあるが, こっちのほうが後続命令のストールが減少する. *)
         | [stmt] when stmt.stall = 0 -> stmts @ [branch] @ [stmt; gen_stmt [] Nop]
         (* 一番目の遅延スロットのストール数が1以上 or 2命令以上入っている場合は, すでに遅延スロットが埋まっている *)
         | [_] | [_; _] -> stmts @ [branch] @ delay
         (* 遅延スロットは高々2個であるはず *)
         | _ -> assert false)
  with Invalid_argument ("partition_block") -> stmts


(* 式の形に依存関係があるかを調べる. *)
let is_dependant exp exp' =
  let is_store = function
    | Store _ | FStore _ -> true
    | _ -> false
  in
  let is_load = function
    | Load _ | FLoad _ -> true
    | _ -> false
  in
  let is_store_c = function
    | StoreC _ | FStoreC _ -> true
    | _ -> false
  in
  let is_load_c = function
    | LoadC _ | FLoadC _ -> true
    | _ -> false
  in

  let is_io = function
    | Send _ | Recv -> true
    | _ -> false
  in
  (* いかなる命令とも入れ替えられないもの
   * 特に, Callは遅延スロットにいれてはいけないことに注意.
   * [XXX] Returnもとりあえず移動しないことに. *)
  let is_critical = function
    | Call _ | Entry | Return -> true
    | _ -> false
  in
    is_critical exp || is_critical exp' ||
    (is_io exp && is_io exp') ||
    (is_store exp && is_store exp') ||
    (is_store exp && is_load exp')  ||
    (is_load exp && is_store exp') ||
    (is_store_c exp && is_store_c exp') ||
    (is_store_c exp && is_load_c exp')  ||
    (is_load_c exp && is_store_c exp')

let is_dependant stmt stmt' =
  is_dependant stmt.left.exp stmt'.left.exp ||
  is_dependant stmt.left.exp stmt'.right.exp ||
  is_dependant stmt.right.exp stmt'.left.exp ||
  is_dependant stmt.right.exp stmt'.right.exp


(* half -> half'の順番で命令があるとき, RAWハザードが生ずるか *)
let is_half_raw half half' =
  List.exists (fun (x, _) -> List.mem x half'.use) half.def

let is_raw stmt stmt' =
  List.exists (fun (x, _) -> List.mem x (use_of stmt')) (def_of stmt)

let is_war stmt stmt' = is_raw stmt' stmt

let is_waw stmt stmt' =
  (* Recvと分岐はWAWハザードがあると考える.
   * すると同時に発行されうるが, Recvは遅延スロットに入らない. *)
  ((stmt.left.exp = Recv || stmt.right.exp = Recv) && is_branch stmt'.right.exp) ||
  List.exists (fun (x, _) -> List.mem_assoc x (def_of stmt)) (def_of stmt')

(* stmt -> stmt'という命令列を入れ替えられるか *)
let is_swapable stmt stmt' = not (is_raw stmt stmt' || is_war stmt stmt' || is_waw stmt stmt' || is_dependant stmt stmt')

                               (*
(* 後続命令stmt'のRAWハザードによるレイテンシを求める. *)
let latency_of_raw stmt stmt' =
  fold_half
    (fun half stall ->
       max stall
         (if List.exists (fun (x, _) -> List.mem x (use_of stmt')) half.def then
            get_latency_of half.exp
          else
            0))
    stmt 0
                                *)

(* stmt -> stmt'という順番のときに, stmt'がストールすべき数を求める. *)
let seq_latency_of stmt stmt' =
  let raw =
    fold_half
      (fun half stall ->
         max stall
           (if List.exists (fun (x, _) -> List.mem x (use_of stmt')) half.def then
              get_latency_of half.exp
            else
              0))
      stmt 0
  in
  let waw =
    fold_half
      (fun half acc ->
         fold_half
           (fun half' acc ->
              if List.exists (fun (x, _) -> List.mem_assoc x half'.def) half.def then
                max acc (get_latency_of half.exp - get_latency_of half'.exp)
              else
                acc)
           stmt' acc)
      stmt 0
  in
    max raw waw


(* ブロックbの前に発行され得る命令を求める.
 * stmt list listを返す.
 * 1クロック前に発行され得るstmtのリストが, 返り値の1番目に入る. *)
let rec stmts_before b cfg =
  (* 逆向きにDFSしながら, 先行命令に成り得る命令を集める
   * 発行されるクロックとリストでの位置が対応している. *)
  let rec dfs lim b =
    let rec combine = function
      | x :: xls, y :: yls ->
          (x @ y) :: combine (xls, yls)
      | xls, [] -> xls
      | [], yls -> yls
    in
      List.fold_left
        (fun acc b ->
           let rec take lim = function
             | _ when lim <= 0 ->
                 []
             | stmt :: stmts ->
                 [stmt] :: Common.replicate stmt.stall [] @ take (lim - (stmt.stall + 1)) stmts
             | [] ->
                 dfs lim b
           in
           let stmts = G.find b cfg in
             combine (acc, take lim (List.rev stmts)))
        [] (G.pred b cfg)
  in
    dfs max_latency b

(* 前に来る命令のリストがstmtssのとき, stmtのストールすべき回数を求める *)
let stall_of ?(call_latency = 0) stmtss stmt =
  let (_, stall) =
    List.fold_left
      (fun (i, stall) stmts ->
         (i + 1,
          List.fold_left
            (fun stall stmt' ->
               let stall' =
                 match stmt' with
                   (* Callは疑似命令なので特別に扱う. *)
                   | { left = { exp = Call _ } } -> call_latency
                   | _ -> seq_latency_of stmt' stmt
               in
                 max stall (stall' - i))
            stall stmts))
      (0, 0) stmtss
  in
    stall
  

      (*
(* ブロックの先頭のストール数を削除 *)
let elim_stall prog =
  map_cfg
    (fun _ ->
       G.map_stmt
         (fun stmt ->
            { stmt with stall = 0 }))
    prog
       *)

(* 制約を満たすようにストール数を増やす.
 * 入力のprogに含まれるストール数には, ストール数の下限を入れておけば良い.
 * TODO: 1つ目の遅延スロットのストール数が1以上になったら, 2つ目の遅延スロットはこのブロック内で発行できない. *)
let validate_stall_for_cfg cfg =
  let update b cfg =
    let rec loop slot = function
      | [] -> []
      | stmt :: stmts ->
          let stall = stall_of ~call_latency:max_latency slot stmt in
          (* この文のストール数は, 今計算したものと元のストール数のうち大きい方を採用する. *)
          let stmt = { stmt with stall = max stall stmt.stall } in
          let slot = [stmt] :: Common.replicate stmt.stall [] @ slot in
          let slot = Common.take max_latency slot in
            stmt :: loop slot stmts
    in
    let slot = stmts_before b cfg in
      loop slot (G.find b cfg)
  in
    (* quasi-topologicalの順序で実行していく.
     * 先行命令のストール数が多いほど, 注目している命令のストール数は小さくなるため. *)
    let (_, order) = G.topological_sort cfg in
      List.fold_left
        (fun cfg b -> G.update_block b (update b cfg) cfg)
        cfg order

let validate_stall prog =
  map_cfg (fun _ -> validate_stall_for_cfg) prog

exception Impossible

(* stmtとstmt'を1つにまとめる. *)
let combine_stmt stmt stmt' =
  (* Nop以外ならaccに加える *)
  let add half acc = if half.exp = Nop then acc else half :: acc in
    match
      fold_half add stmt (fold_half add stmt' [])
    with
      | [] -> stmt
      | [half] -> stmt_of_half half
      | [half; half'] ->
          (* 疑似命令の場合は同時に発行させない. *)
          let is_combinable = function Call _ | Entry -> false | _ -> true in
            if not (is_combinable half.exp && is_combinable half'.exp) then raise Impossible;
            (match (restriction_of half.exp, restriction_of half'.exp) with
               | (LeftOnly, LeftOnly) | (RightOnly, RightOnly) ->
                   raise Impossible
               | (LeftOnly, _) | (_, RightOnly) ->
                   { stall = 0; left = half; right = half' }
               | _ ->
                   { stall = 0; left = half'; right = half })
      | _ -> raise Impossible

(* 命令を1つにまとめられるか.
 * データ依存関係は検査しない. 式の形を見て制約に合っているかのみチェックする. *)
let is_combinable stmt stmt' =
  try
    ignore (combine_stmt stmt stmt');
    true
  with
      Impossible -> false


(* 依存グラフ *)
module D =
  Graph.Make
    (struct
       type vertex = Id.t
       type v_tag = stmt
       (* 辺の始点が発行された後, 何クロックで演算結果が利用可能になるか.
        * すなわち, 先行命令のレイテンシ.
        * ただし, -1のときは逆依存関係(WAR)を表すことにする. *)
       type e_tag = int
     end)

(* ready集合 *)
module PQ =
  Set.Make
    (struct
       (* int: 優先度の符号を反転させたもの.
        * すなわち小さい要素から最初に取り出すべき.
        * (こうしているのはS.iterが昇順であるから.) *)
       type t = int * Id.t
       let compare = compare
     end)

type data = {
  (* 依存グラフ *)
  graph : D.t;
  (* graphに対応する命令列 *)
  stmts : stmt list;
  (* 依存関係は解消されたが, オペランドが用意できるのを待っているhalf_stmtの集合 *)
  waiting : PQ.t;
  (* 今すぐに発行できるhalf_stmtの集合 *)
  ready : PQ.t;
  (* オペランドが用意できるクロックの下限 *)
  finclk : int M.t;
  (* D.vertex -> その命令の優先度 *)
  priority : int M.t;
  (* このブロック内で発行しなくてもよい命令のD.vertexの集合 *)
  ext : S.t;
  (* クロックの(もしあれば)上限 *)
  clk_limit : int option;
}

let rec output_graph path graph =
  let oc = open_out_gen [Open_creat; Open_trunc; Open_wronly; Open_text] 436 path in
  Printf.fprintf oc "digraph dep_graph {\n";
  Printf.fprintf oc "node [shape=box];\n";
  D.fold
    (fun b stmt () ->
       Printf.fprintf oc "%s [label=\"%s\"];\n" (Id.escape b) (String.escaped (string_of_stmt stmt)))
    graph ();
  D.fold_edge
    (fun v w e_tag () ->
       let style = "" in(*let style = match e_tag with Continue -> "style=dotted" | _ -> "" in *)
         Printf.fprintf oc "%s -> %s [label=\"%d\"%s];\n" (Id.escape v) (Id.escape w) e_tag style) graph ();
  Printf.fprintf oc "}\n";
  close_out oc


(* stmtのID
 * 分岐命令のIDがhalf.s_idと等しくなるように, 左側がNopであれば右側のs_idを使う. *)
let id_of = function
  | { left = { exp = Nop }; right = right } -> right.s_id
  | { left = left } -> left.s_id

(* waitingの元を作る *)
let waiting_node_of v data = (M.find v data.finclk, v)
let add_to_waiting v data = { data with waiting = PQ.add (waiting_node_of v data) data.waiting }

(* ready集合の元を作る. S.iterのために負号をつけていることに注意. *)
let ready_node_of v data = (- M.find v data.priority, v)

(* リストスケジューリングに必要なデータを, 命令列stmtsについて構築
 * stmtss: このブロックの先行ブロックになる文のリストのリスト
 * ext   : 発行しなくてもよいstmtのリスト. stmtsの部分集合でなければならない. *)
let build ?(stmtss = []) ?(ext = []) stmts =
  let (stmts, ext_set) =
    List.fold_right
      (fun stmt (stmts, ext_set) ->
         let expanded =
           match (is_half_raw stmt.left stmt.right, is_half_raw stmt.right stmt.left) with
             (* 文を分解できる場合は分解する.
              * 分岐は必ず分解できるので, 分岐のIDはs_idに等しくなる. *)
             | (false, _) -> [stmt_of_half stmt.left; stmt_of_half stmt.right]
             | (true, false) -> [stmt_of_half stmt.right; stmt_of_half stmt.left]
             (* 順序関係をつけると正しくなくなる場合はそのままにする.
              * 例えば, x <- y \ y <- x はxとyのスワップを意味するが, これは同時に発行しなければ正しくない. *)
             | (true, true) -> [stmt]
         in
         let ext_set =
           if List.mem stmt ext then
             List.fold_right (fun stmt -> S.add (id_of stmt)) expanded ext_set
           else
             ext_set
         in
           (expanded @ stmts, ext_set))
      stmts ([], S.empty)
  in
  (* nopのみの文は消去する. *)
  let stmts = List.filter (function { left = { exp = Nop }; right = { exp = Nop } } -> false | _ -> true) stmts in
  (* 依存グラフを構築
   * ブロックの先頭からみていって, 既に加えられた命令との依存関係を調べていく. *)
  let graph =
    List.fold_left
      (fun graph stmt ->
         let id = id_of stmt in
         let graph = D.add_vertex id stmt graph in
         let graph =
           D.fold
             (fun id' stmt' graph ->
                match () with
                  | _ when stmt = stmt' -> graph
                  | _ when is_raw stmt' stmt || is_waw stmt' stmt ->
                      D.add_edge id' id (seq_latency_of stmt' stmt) graph
                  | _ when is_dependant stmt' stmt ->
                      (* その他の依存関係の場合 *)
                      D.add_edge id' id 0 graph
                  | _ when is_war stmt' stmt ->
                      (* 逆依存関係しかない場合は(同時に発行でき得るので)レイテンシ = -1とする. *)
                      D.add_edge id' id (-1) graph
                  | _ -> graph)
             graph graph
         in
           graph)
      D.empty stmts
  in
  let init = D.fold (fun v _ -> M.add v 0) graph M.empty in
  (* グラフを逆順からDPして、出口までの最大の長さ（クリティカルパス）を求める *)
  let priority =
    D.topological
      (fun v w latency env -> 
         let p' = max (M.find v env + 1 + latency) (M.find w env) in
           M.add w p' env)
      (D.rev graph) init
  in
  (* 命令に依存する命令が多いほど優先度を高く設定する. *)
  let priority =
    M.mapi
      (fun v p ->
         D.expand (fun _ latency acc -> latency + 1 + acc) v graph 0 + p * 100)
      priority
  in
    (* [XXX] 総クロック数が増加したのでとりあえずやめる. 
  (* 分岐からの最長路を求める *)
  let branch_dist =
    D.topological
      (fun v w latency env -> 
         let p = M.find v env in
           if p = -1 then
             env
           else
             M.add w (max (p + 1 + latency) (M.find w env)) env)
      (D.rev graph)
      (M.mapi (fun v _ -> if is_branch (D.find v graph).right.exp then 0 else -1) init)
  in
  (* 分岐からパスがない場合は優先度に加算しない. パスがある場合は, (最長路 + 1) * 100を優先度に加算する. *)
  let priority = M.mapi (fun v x -> x + (M.find v branch_dist + 1) * 100) priority in
     *)
  (* このブロックで発行する必要のない命令は, 優先度を低く設定する. *)
  let priority = M.mapi (fun v x -> if S.mem v ext_set then x - 1000000 else x) priority in
  (* stmtssとのレイテンシを考えて発行可能なクロックを計算する. *)
  let finclk =
    D.fold
      (fun v stmt -> M.add v (stall_of stmtss stmt))
      graph M.empty
  in
  let data =
    {
      graph = graph;
      stmts = stmts;
      waiting = PQ.empty;
      ready = PQ.empty;
      finclk = finclk;
      priority = priority;
      ext = ext_set;
      clk_limit = None;
    }
  in
    (* waiting集合に, 次数0の頂点を追加. *)
    D.fold
      (fun v _ data ->
         if D.in_degree_of v graph = 0 then
           add_to_waiting v data
         else
           data)
      graph data


(* クロックをclkまで進める.
 * すなわち, 命令を(もしあれば)waitingからreadyへ移動させる. *)
let next_clk clk data =
  let rec loop data =
    if PQ.is_empty data.waiting then
      data
    else
      let (clk', v) as elt = PQ.min_elt data.waiting in
        (* vをreadyに移動させる *)
        if clk' <= clk then
          loop
            { data with
                  waiting = PQ.remove elt data.waiting;
                  ready = PQ.add (ready_node_of v data) data.ready }
        else
          data
  in
    loop data

(* 同時に発行できる命令を見つけたときに投げる例外 *)
exception Found of stmt

(* 命令vを発行する.
 * ただし, 同時発行できる可能性もあるので, まだクロックは進めない. *)
let issue clk v data =
  (* ready集合から取り除く. *)
  let data = { data with ready = PQ.remove (ready_node_of v data) data.ready } in
  (* 命令を発行したので, クロックを進める *)
  let clk' = clk + 1 in
  let data =
    D.expand
      (fun w latency data ->
         (* wはstmtの演算結果が利用可能になるまで( = clk' + latency )は発行できない. *)
         let data =
           { data with
                 finclk = M.add w (max (M.find w data.finclk) (clk' + latency)) data.finclk }
         in
         (* vにしか依存していなかった場合は, waiting集合に移動させる. *)
         let data =
           if D.pred w data.graph = [v] then
             add_to_waiting w data
           else
             data
         in
           data)
      v data.graph data
  in
  (* 今発行した命令stmtをグラフから削除 *)
  let data = { data with graph = D.remove_vertex v data.graph } in
  (* waitingが変更されたので(clk'ではなく)clkまで進める.
   * この時点で新たにreadyに追加されるものは, stmtと逆依存関係をもつ命令である.
   * (その場合はstmtと同時に発行してもよい.) *)
  let data = next_clk clk data in
    (clk', data)


(* 発行すべき命令を発行できなかった場合に生ずる例外 *)
exception FailedIssue

(* dataをスケジューリングする. ディレイスロットも埋める.
 * 返り値
 * 第一要素: スケジューリング後の命令列.
 * 第二要素: このブロックにスケジューリングされなかった命令のリスト *)
let schedule data =
  let remain_ref = ref [] in
  let rec loop clk data =
    match data.clk_limit, (PQ.is_empty data.ready, PQ.is_empty data.waiting) with
      | Some (limit), _ when limit <= clk ->
          D.iter (fun id _ -> if not (S.mem id data.ext) then raise FailedIssue) data.graph;
          remain_ref := D.fold (fun _ stmt acc -> stmt :: acc) data.graph [];
          []
      (* 全ての命令が発行し終わったとき. *)
      | _, (true, true) -> assert (D.is_empty data.graph); []
      (* クロックを進めてwaitingからreadyへ移動させる *)
      | _, (true, false) ->
          let (clk', _) = PQ.min_elt data.waiting in
          let data = next_clk clk' data in
          let stall = clk' - clk in
            assert (stall >= 0);
            (match loop clk' data with
               | stmt :: stmts ->
                   (* ストールしなければならない *)
                   { stmt with stall = stall } :: stmts
               | [] -> [])
      (* readyから命令を発行する *)
      | _, (false, _) ->
          let (clk', stmt, data') =
            let (_, v) = PQ.min_elt data.ready in
            let stmt = D.find v data.graph in
            let (clk', data) = issue clk v data in
            let (stmt, (clk'', data)) =
              try
                PQ.iter
                  (fun (_, v) ->
                     let stmt' = D.find v data.graph in
                       if is_combinable stmt stmt' then
                         raise (Found (stmt')))
                  data.ready;
                (* 同時に発行できる命令が見つからなかったとき. *)
                (stmt, (clk', data))
              with Found (stmt') ->
                (combine_stmt stmt stmt',
                 issue clk (id_of stmt') data)
            in
            (* 今のところは全ての命令は発行すると + 1クロックになるので, clk' = clk''なはず. *)
            let () = assert (clk' = clk'') in
            let clk' = max clk' clk'' in
            (* 命令を発行したのでクロックを進める.
             * 注意: issueではクロックは進まない. *)
            let data = next_clk clk' data in
              (clk', stmt, data)
          in
            if is_branch stmt.right.exp then
              try
                (* 分岐命令が発行されたので, あとdelay_slotクロックしかこのブロック内で発行できない. *)
                let data' = { data' with clk_limit = Some (clk' + delay_slot) } in
                  stmt :: loop clk' data'
              with FailedIssue ->
(*                let data_orig = data in*)
                (* 分岐命令を早く発行しすぎたとき *)
                let id = stmt.right.s_id in
                (* 分岐命令をready, waitingから取り除く. *)
                let data = { data with ready = PQ.remove (ready_node_of id data) data.ready } in
                let data = { data with waiting = PQ.remove (waiting_node_of id data) data.waiting } in
                (* 次に発行されるべき命令next_idに分岐idが依存していることにする. *)

                (* 遅延スロットでスピルが起きるなどして不可能になった. *)
                if not (not (PQ.is_empty data.ready) || not (PQ.is_empty data.waiting)) then raise Impossible;

                (*
                if not (not (PQ.is_empty data.ready) || not (PQ.is_empty data.waiting)) then (
                  output_graph "a.dot" data_orig.graph;
                  output_graph "b.dot" data.graph
                );
                if not (not (PQ.is_empty data.ready) || not (PQ.is_empty data.waiting)) then (
                  Format.eprintf "%s@."
                    (string_of_block "" data.stmts)
                );

                 *)
                let () = assert (not (PQ.is_empty data.ready) || not (PQ.is_empty data.waiting)) in
                let (_, next_id) = if not (PQ.is_empty data.ready) then PQ.min_elt data.ready else PQ.min_elt data.waiting in
                let data = { data with graph = D.add_edge next_id id 0 data.graph } in
                  (* もう一度, 分岐以外を発行しなおす *)
                  loop clk data
            else
              stmt :: loop clk' data'
  in
  let stmts = loop 0 data in
  let stmts = fill_delay_slot stmts in
    (stmts, !remain_ref)

(* stmtsを全てスケジューリングする. *)
let schedule_all ?(stmtss = []) stmts =
  let (stmts, remain) = schedule (build ~stmtss stmts) in
    assert (remain = []);
    stmts

(* stmtsのフェッチが終了するまでの総クロックを得る.
 * ただし, 分岐がある場合はその分岐が確定するまでのクロック数. *)
let total_clk_of stmts =
  let stmts =
    if List.exists (fun stmt -> is_branch stmt.right.exp) stmts then
      fill_delay_slot stmts
    else
      stmts
  in
    List.fold_left
      (fun acc stmt -> acc + stmt.stall + 1)
      0 stmts


(* 遅延スロットを消去する.
 * ブロック内にちょうど1つの分岐があることが前提条件. *)
let cut_off_delay_slot prog =
  map_cfg
    (fun _ cfg ->
       G.fold
         (fun b stmts cfg ->
            let (stmts, branch, delay) = partition_block stmts in
            (* 分岐の左スロットも遅延スロットだと考える *)
            let (branch, delay) =
              (stmt_of_half branch.right,
               stmt_of_half branch.left :: delay)
            in
              if List.for_all
                   (fun stmt -> is_swapable branch stmt)
                   delay
              then
                (* 分岐と依存関係がなければ, 単純に分岐の前に移動する. *)
                G.update_block b (stmts @ delay @ [branch]) cfg
              else
                let cfg = G.update_block b (stmts @ [branch]) cfg in
                  G.expand
                    (fun b' e_tag cfg ->
                       let (b'', cfg) = G.add_block (List.map copy_stmt (delay @ [gen_stmt [] Jmp])) cfg in
                       let cfg =
                         G.add_edge b'' b' Other
                           (G.add_edge b b'' e_tag
                              (G.remove_edge b b' cfg))
                       in
                         cfg)
                    b cfg cfg)
         cfg cfg)
    prog

(* 基本ブロック内だけでスケジューリングを行う *)
let f prog =
(*  (* 遅延スロットは埋めておかないとstmts_beforeの集合が大きくなってストール数を大きく見積もってしまう *)
  let prog = map_cfg (fun _ -> G.map (fun _ -> fill_delay_slot)) prog in*)
  Format.eprintf "# List Scheduling@.";
  Time.start "# List Scheduling";
  let prog =
    map_cfg
      (fun _ cfg ->
         G.fold
           (fun b stmts cfg ->
              try
                let stmts' = schedule_all ~stmtss:(stmts_before b cfg) stmts in
                  G.update_block b stmts' cfg
              with Impossible ->
                (* 基本ブロック内でスケジューリングできない場合は, 後続ブロックを新たに作って遅延スロットを分配 *)
                let (stmts, branch, delay) = partition_block stmts in
                let cfg = G.update_block b (stmts @ [branch]) cfg in
                  G.expand
                    (fun b' e_tag cfg ->
                       let (b'', cfg) = G.add_block (List.map copy_stmt (delay @ [gen_stmt [] Jmp])) cfg in
                       let cfg =
                         G.add_edge b'' b' Other
                           (G.add_edge b b'' e_tag
                              (G.remove_edge b b' cfg))
                       in
                         (try
                            VProfile.add_prob b b'' (VProfile.prob_of b b');
                            VProfile.add_prob b'' b' 1.0;
                            VProfile.add_count b'' (VProfile.count_of_edge b b')
                          with Not_found -> ());
                         cfg)
                    b cfg cfg)
           cfg cfg)
      prog
(*

         G.map
           (fun b stmts ->
              schedule_all ~stmtss:(stmts_before b cfg) stmts)
           cfg)
 *)
  in
  Time.stop "# List Scheduling";
  prog


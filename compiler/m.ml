(* customized version of Map *)

module M =
  Map.Make
    (struct
      type t = Id.t
      let compare = compare
    end)

include M

let add_list xys env = List.fold_left (fun env (x, y) -> add x y env) env xys
let add_list2 xs ys env = List.fold_left2 (fun env x y -> add x y env) env xs ys
let to_keys env = fold (fun k v acc -> k :: acc) env []

(* デフォルトのキーが[]であると考えてリストにyを追加 *)
let add_ls x y env = M.add x (y :: if M.mem x env then M.find x env else []) env

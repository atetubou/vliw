(* 分岐先の一方でしか使用されない変数の定義文を後ろへ移動する.
 *
 * 移動するときは生存情報を更新しなければならないことに注意.
 *
 * e.g.
 * b1:
 *   y <- 1
 *   x <- y
 *   if ... then goto b2 else goto b3
 *
 * b2:
 *   _ <- x
 *
 * b3:
 *   _ <- y
 *
 * x <- yをb2に移動した後は, yはb2, b3のどちらでも生存していることになるので,
 * y <- 1はb3に移動できない.
 * *)
open Block

let is_target = function
  | Set _ | SetLabel _ | FSet _ | Mov _ | FMov _ | Add _ | Sub _
  | ShiftL _ | ShiftR _ | FAdd _ | FMul _ | FInv _ | FSqrt _ | ItoF _ | FtoI _
  | Load _ | FLoad _ | LoadC _ | FLoadC _ -> true
  | _ -> false

let add_list = List.fold_right S.add

let g cfg =
  let liveness = Liveness.create cfg in
  let cfg =
    G.fold
      (fun b stmts cfg ->
         match partition_last stmts with
           | ({ exp = IfEq _ | IfLt _ | IfFEq _ | IfFLt _ }, _) ->
               let b_then = G.succ_of b Then cfg in
               let live_then = Liveness.live_in_of b_then liveness in
               let b_else = G.succ_of b Else cfg in
               let live_else = Liveness.live_in_of b_else liveness in
               let (stmts, stmts_then, _, stmts_else, _) =
                 List.fold_right
                   (fun stmt (followings, stmts_then, live_then, stmts_else, live_else) ->
                      match stmt with
                        | { def = [x, _] }
                            when is_target stmt.exp &&
                                 (* stmtの後続命令と入れ替えられる *)
                                 List.for_all
                                   (fun stmt' ->
                                      not (ListScheduling.is_hazardable ~branch_ok:true stmt stmt'))
                                   followings
                          ->
                            (* xがelse節で生存していない場合は, stmtはthen節に移動してよい *)
                            if not (S.mem x live_else) then
                              (followings,
                               stmt :: stmts_then, add_list stmt.use live_then,
                               stmts_else, live_else)
                            else if not (S.mem x live_then) then
                              (followings,
                               stmts_then, live_then,
                               stmt :: stmts_else, add_list stmt.use live_else)
                            else
                              (stmt :: followings, stmts_then, live_then, stmts_else, live_else)
                        | _ -> (stmt :: followings, stmts_then, live_then, stmts_else, live_else))
                   stmts ([], [], live_then, [], live_else)
               in
               let cfg =
                 if stmts_then = [] then cfg
                 else
                   let cfg = G.remove_edge b b_then cfg in
                   let (b_then', cfg) = G.add_block (stmts_then @ [gen_stmt [] Jmp]) cfg in
                   let cfg = G.add_edge b b_then' Then (G.add_edge b_then' b_then Other cfg) in
                     cfg
               in
               let cfg =
                 if stmts_else = [] then cfg
                 else
                   let cfg = G.remove_edge b b_else cfg in
                   let (b_else', cfg) = G.add_block (stmts_else @ [gen_stmt [] Jmp]) cfg in
                   let cfg = G.add_edge b b_else' Else (G.add_edge b_else' b_else Other cfg) in
                     cfg
               in
               let cfg = G.update_block b stmts cfg in
                 cfg
           | _ -> cfg)
      cfg cfg
  in
    cfg


let f prog =
  map_cfg (fun l cfg -> g cfg) prog


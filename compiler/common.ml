(* for lexer.mll and parser.mly *)
let parsing_file = ref ""
let accum_line = ref [0]
let files = ref []
let add_file file =
  let inchan = open_in file in
    accum_line := [0]; parsing_file := file; files := (file, inchan) :: !files; inchan
let finalize = List.iter (fun (file, inchan) -> close_in inchan) !files; files := []
let num2pos num =
    let ls = List.filter (fun x -> x <= num) !accum_line in
      (List.length ls, num - List.hd ls + 1)
type position = Position of string * int * int * int * int * int * int
let dummy_position = Position ("dummy", 0, 0, 0, 0, 0, 0)
let string_of_position (Position (file, spos, sline, scol, epos, eline, ecol)) =
  if sline <> eline then
    Printf.sprintf "file %s, line %d, column %d to line %d, column %d" file sline scol eline ecol
  else
    Printf.sprintf "file %s, line %d, column %d to %d" file sline scol ecol
let rec replace s c d = if String.contains s c then (s.[String.index s c] <- d; replace s c d) else s;;
let code_of_position (Position (file, spos, sline, scol, epos, eline, ecol)) = 
  let inchan = List.assoc file !files in
    seek_in inchan spos;
    let count = epos - spos in
    let buf = String.create count in
      ignore (really_input inchan buf 0 count); replace buf '\n' ' '

let rec indent n =
  if n > 0 then
    "  " ^ indent (n-1)
  else
    ""

let debug_flag = ref false
let ifdbg f = if !debug_flag then f else (fun x -> x)

let iter m f x =
  let rec sub n x =
    if n = m then x else
      let x' = f n x in
        if x = x' then x else
          sub (n+1) x'
  in
    sub 0 x

let verbose = ref false
let word = 1
let optimize = ref true

(* pであるもののうち前にあるものをとる *)
let rec take_while p = function
  | [] -> []
  | x :: ls when not (p x) -> []
  | x :: ls -> x :: take_while p ls

(* Haskellのspanに等しい. *)
let rec span p = function
  | [] -> ([], [])
  | x :: ls when p x ->
      let (ls, rs) = span p ls in
        (x :: ls, rs)
  | ls -> ([], ls)

let rec do_worklist que init f =
  let que = ref que in
  let rec loop acc =
    match !que with
      | [] -> acc
      | x :: ls ->
          que := ls;
          let push x = que := x :: !que in (* 破壊的なpushを行う *)
          let acc = f push x acc in
            loop acc
  in
    loop init

let rec last = function
  | [x] -> x
  | x :: ls -> last ls
  | [] -> assert false

(* リストからn個とる. とれない場合はとれたもののみ. *)
let rec take n = function
  | [] -> []
  | _ when n <= 0 -> []
  | x :: ls ->
      x :: take (n-1) ls

(* n個のxからなるリストを作る *)
let rec replicate n x =
  if n <= 0 then
    []
  else
    x :: replicate (n-1) x

(* fに関して比べる *)
let compare_on f b b' = compare (f b) (f b')

(* i番目のリストの要素を得る *)
let rec at i = function
  | x :: _ when i = 0 -> x
  | _ :: ls when i > 0 -> at (i-1) ls
  | _ -> assert false



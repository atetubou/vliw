library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.definst.all;


entity branch is
  port(
    clk : in std_logic;
    opcode : in std_logic_vector(4 downto 0);

    rs,rt : in std_logic_vector(31 downto 0);
    frs,frt : in std_logic_vector(31 downto 0);
    imm16 : in std_logic_vector(15 downto 0);
    imm6 : in std_logic_vector(4 downto 0);

    flag : out std_logic;
    pcout : out std_logic_vector(14 downto 0));

end branch;

architecture branch of branch is

begin  -- branch

  process(clk)
  begin
    if rising_edge(clk) then

      case opcode is
        when OPJR =>
          pcout <= rs(pcout'length - 1  downto 0);          
        when others =>
          pcout <= imm16(14 downto 0);
      end case;
      
      case opcode is
        when OPBEQ =>                -- beq
          if rs=rt then
            flag <= '1';
          else
            flag <= '0';
          end if;
          
        when OPBLT =>                -- blt
          if signed(rs) < signed(rt) then
            flag <= '1';
          else
            flag <= '0';
          end if;

        when  OPBEQI =>
          if signed(rs) = resize(signed(imm6),rs'length) then
            flag <= '1';
          else
            flag <= '0';
          end if;

        when OPBLTI =>
          if signed(rs) < resize(signed(imm6),rs'length) then
            flag <= '1';
          else
            flag <= '0';
          end if;

        when OPFBEQ =>              -- fbeq
          if (frs=frt) or (frs(30 downto 23) = x"00" and frt(30 downto 23) = x"00") then
            flag <= '1';
          else
            flag <= '0';
          end if;

        when OPFBLT =>
          
          if (frs(31)='1' and frt(31)='0') or
             (frs(31)='0' and frt(31)='0' and unsigned(frs(30 downto 0)) < unsigned(frt(30 downto 0))) or
             (frs(31)='1' and frt(31)='1' and unsigned(frs(30 downto 0)) > unsigned(frt(30 downto 0)))    
          then
            flag <= '1';
          else
            flag <= '0';
          end if;
          
        when OPJMP | OPJR =>
          
          flag <= '1';
            
        when others =>
          flag <= '0';
      end case;
    end if;
  end process;

end branch;

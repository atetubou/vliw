library ieee;
use ieee.std_logic_1164.all;


entity top is
  
end top;

architecture top of top is

  component cpu
    Port(MCLK1 : in STD_LOGIC;
         RS_TX : out STD_LOGIC;
         RS_RX : in STD_LOGIC;       
         ZD : inout STD_LOGIC_VECTOR(31 downto 0);
         ZA : out STD_LOGIC_VECTOR(19 downto 0);
         XE1 : out STD_LOGIC;
         E2A : out STD_LOGIC;
         XE3 : out STD_LOGIC;
         XGA : out STD_LOGIC;
         XZCKE : out STD_LOGIC;
         ADVA : out STD_LOGIC;
         XLBO : out STD_LOGIC;
         ZZA : out STD_LOGIC;
         XFT : out STD_LOGIC;
         XZBE : out STD_LOGIC_VECTOR(3 downto 0);
         ZCLKMA : out STD_LOGIC_VECTOR(1 downto 0);
         XWA : out STD_LOGIC);
  end component;

  --component simsram
  --port (
  --  ZD : inout STD_LOGIC_VECTOR(31 downto 0);
  --  dataout : out STD_LOGIC_VECTOR(31 downto 0);
  --  ZA : in STD_LOGIC_VECTOR(19 downto 0);
  --  XE1 : in STD_LOGIC;
  --  E2A : in STD_LOGIC;
  --  XE3 : in STD_LOGIC;
  --  XGA : in STD_LOGIC;
  --  XZCKE : in STD_LOGIC;
  --  ADVA : in STD_LOGIC;
  --  XLBO : in STD_LOGIC;
  --  ZZA : in STD_LOGIC;
  --  XFT : in STD_LOGIC;
  --  XZBE : in STD_LOGIC_VECTOR(3 downto 0);
  --  ZCLKMA : in STD_LOGIC_VECTOR(1 downto 0);
  --  XWA : in STD_LOGIC);
  --end component;

  signal mclk1,rs_tx,rs_rx,xe1,e2a,xe3,xga,xzcke,adva,xlbo,zza,xft,xwa : std_logic := '0';
  signal zd : std_logic_vector(31 downto 0) := (others => 'Z');
  signal za : std_logic_vector(19 downto 0) := (others => '0');
  signal XZBE : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
  signal ZCLKMA : STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
  signal sramout : std_logic_vector(31 downto 0) := (others => '0');

begin  -- top

  cpu_r : cpu port map(
    mclk1 => mclk1,
    rs_tx => rs_tx,
    rs_rx => rs_rx,
    zd => zd,
    za => za,
    xe1 => xe1,
    e2a => e2a,
    xe3 => xe3,
    xga => xga,
    xzcke => xzcke,
    adva => adva,
    xlbo => xlbo,
    zza => zza,
    xft => xft,
    xzbe => xzbe,
    zclkma => zclkma,
    xwa => xwa);

  rs_rx <= rs_tx;

  --simsram_r : simsram port map(
  --  zd => zd,
  --  dataout => sramout,
  --  za => za,
  --  xe1 => xe1,
  --  e2a => e2a,
  --  xe3 => xe3,
  --  xga => xga,
  --  xzcke => xzcke,
  --  adva => adva,
  --  xlbo => xlbo,
  --  zza => zza,
  --  xft => xft,
  --  xzbe => xzbe,
  --  zclkma => zclkma,
  --  xwa => xwa);


  clkgen : process
  begin
    mclk1 <= '0';
    wait for 7 ns;
    mclk1 <= '1';
    wait for 7 ns;
  end process;

end top;

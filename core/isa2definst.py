from sys import *


print """
library ieee;
use ieee.std_logic_1164.all;

package definst is
"""



isa = stdin.readlines()

flag = False

for line in isa:
    flag |= line.startswith("Type")

    line = line.split(',')
    
    if flag and (line[0][0] in "-IRSBC"):
        fmt = "  constant op%s : std_logic_vector := \"%s\";"
        print fmt%(line[3].strip(),line[1].strip())
    
print "\nend definst;"

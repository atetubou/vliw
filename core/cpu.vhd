library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.definst.all;

library work;
use work.inst.all;

entity cpu is
  Port(MCLK1 : in STD_LOGIC;
       RS_TX : out STD_LOGIC;
       RS_RX : in STD_LOGIC;       
       ZD : inout STD_LOGIC_VECTOR(31 downto 0);
       ZA : out STD_LOGIC_VECTOR(19 downto 0);
       XE1 : out STD_LOGIC;
       E2A : out STD_LOGIC;
       XE3 : out STD_LOGIC;
       XGA : out STD_LOGIC;
       XZCKE : out STD_LOGIC;
       ADVA : out STD_LOGIC;
       XLBO : out STD_LOGIC;
       ZZA : out STD_LOGIC;
       XFT : out STD_LOGIC;
       XZBE : out STD_LOGIC_VECTOR(3 downto 0);
       ZCLKMA : out STD_LOGIC_VECTOR(1 downto 0);
       XWA : out STD_LOGIC);  
end cpu;


architecture rtl of cpu is
  
  constant cachesize : integer := 12;
  COMPONENT blockcache
    PORT (
      clka : IN STD_LOGIC;
      wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      addra : IN STD_LOGIC_VECTOR(cachesize-1 DOWNTO 0);
      dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
  END COMPONENT;
  -- cache signal

  signal cachewrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal cacheiwe,cachefwe : std_logic := '0';
  signal ldaddr : std_logic_vector(cachesize - 1 downto 0) := (others => '0');
  
  signal cachedout,cachedin : std_logic_vector(31 downto 0) := (others => '0');
  signal cachewea : std_logic_vector(0 downto 0) := "0";
  
  COMPONENT instblock
    PORT (
      clka : IN STD_LOGIC;
      wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      addra : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
      dina : IN STD_LOGIC_VECTOR(62 DOWNTO 0);
      clkb : IN STD_LOGIC;
      addrb : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
      doutb : OUT STD_LOGIC_VECTOR(62 DOWNTO 0)
      );
  END COMPONENT;

  component io
    generic (
      wtime : std_logic_vector(15 downto 0) := x"1ADB");
    port(
      clk : in std_logic;
      opcode : in std_logic_vector(4 downto 0);
      din : in std_logic_vector(31 downto 0);
      rx : in std_logic;

      wrreg : in std_logic_vector(4 downto 0);

      dout : out std_logic_vector(31 downto 0);
      iowe : out std_logic;
      iooutreg : out std_logic_vector(4 downto 0);
      
      tx : out std_logic;
      polling : out std_logic);
  end component;

  signal polling : std_logic := '1';
  signal iowe : std_logic := '0';
  signal iowrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal iowrdata : std_logic_vector(31 downto 0) := (others => '0');

  component alu
    port(
      clk : in std_logic;
      
      op : in std_logic_vector(4 downto 0);
      rs,rt : in std_logic_vector(31 downto 0);
      imm16 : in std_logic_vector(15 downto 0);

      rsi,rdi : in std_logic_vector(4 downto 0);

      aluwe : out std_logic;
      aluoutreg : out std_logic_vector(4 downto 0);
      aluout : out std_logic_vector(31 downto 0));
  end component;

  -- alu1

  signal alu1we : std_logic := '0';
  signal alu1wrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal alu1wrdata : std_logic_vector(31 downto 0) := (others => '0');

  -- alu2

  signal alu2we : std_logic := '0';
  signal alu2wrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal alu2wrdata : std_logic_vector(31 downto 0) := (others => '0');

  component fpu
    port(
      clk : in std_logic;
      
      opcode : in std_logic_vector(4 downto 0);
      funct : in std_logic_vector(8 downto 0);
      frs,frt : in std_logic_vector(31 downto 0);

      fdi : in std_logic_vector(4 downto 0);

      fpuwe : out std_logic;
      fpuoutreg : out std_logic_vector(4 downto 0);
      fpuout : out std_logic_vector(31 downto 0));
    
  end component;

  -- fpu1

  signal fpu1we : std_logic := '0';
  signal fpu1wrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal fpu1wrdata : std_logic_vector(31 downto 0) := (others => '0');

  -- fpu2

  signal fpu2we : std_logic := '0';
  signal fpu2wrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal fpu2wrdata : std_logic_vector(31 downto 0) := (others => '0');  


  component ram
    port(
      clk : in std_logic;
      rs : in std_logic_vector(31 downto 0);
      frs : in std_logic_vector(31 downto 0);

      opcode : in std_logic_vector(4 downto 0);

      rsi : in std_logic_vector(4 downto 0);
      rdi : in std_logic_vector(4 downto 0);
      
      ramwrreg : out std_logic_vector(4 downto 0);
      memiwe : out std_logic;
      memfwe : out std_logic;

      dout : out std_logic_vector(31 downto 0);
      mdata : inout std_logic_vector(31 downto 0));
  end component;


  signal memiwe : std_logic := '0';
  signal memfwe : std_logic := '0';
  signal memwrreg : std_logic_vector(4 downto 0) := (others => '0');
  signal memwrdata : std_logic_vector(31 downto 0) := (others => '0');
  

  component branch
    port(
      clk : in std_logic;
      opcode : in std_logic_vector(4 downto 0);

      rs,rt : in std_logic_vector(31 downto 0);
      frs,frt : in std_logic_vector(31 downto 0);
      imm16 : in std_logic_vector(15 downto 0);
      imm6 : in std_logic_vector(4 downto 0);

      flag : out std_logic;
      pcout : out std_logic_vector(14 downto 0));
  end component;

  signal branchflag : std_logic := '0';
  signal branchpc : std_logic_vector(14 downto 0) := (others => '0');


  -- fetch signal


  signal pc_reg,next_pc : std_logic_vector(14 downto 0) := (others => '0');

  -- decode signal
  constant regbit : integer := 5;
  type regarray is array (0 to (2**regbit) - 1) of std_logic_vector(31 downto 0);
  signal ireg1 : regarray := (others => x"00000000");
  --signal ireg2 : regarray := (others => x"00000000");
  --signal useireg : std_logic_vector(63 downto 0) := (others => '0');
  
  signal freg1 : regarray := (others => x"00000000");
  --signal freg2 : regarray := (others => x"00000000");
  --signal usefreg : std_logic_vector(63 downto 0) := (others => '0');

  signal stallflag : std_logic := '0';
  signal decodestallcount : std_logic_vector(2 downto 0) := (others => '0');
  signal decodeinst1 : std_logic_vector(29 downto 0) := (others => '0');
  signal decodeinst2 : std_logic_vector(29 downto 0) := (others => '0');  

  signal rsi1,rti1,rdi1 : std_logic_vector(4 downto 0) := (others => '0');
  signal rsi2,rti2,rdi2 : std_logic_vector(4 downto 0) := (others => '0');

  signal iv161,iv162 : std_logic_vector(15 downto 0) := (others => '0');
  
  signal rs1,rt1,rs2,rt2 : std_logic_vector(31 downto 0) := (others => '0');
  signal frs1,frt1,frs2,frt2 : std_logic_vector(31 downto 0) := (others => '0');

  signal opcode1,opcode2 : std_logic_vector(4 downto 0) := (others => '0');
  signal funct1,funct2 : std_logic_vector(8 downto 0) := (others => '0');  

  -- top 

  signal clk,txwa,dxwa : std_logic := '0';
  signal dzd,dzd2,dzd3 : std_logic_vector(31 downto 0) := (others => '0');
  signal dtmpaddr : std_logic_vector(18 downto 0) := (others => '0');
  signal tmpaddr : std_logic_vector(18 downto 0) := (others => '0');
  
  COMPONENT clock_gen
    PORT(
      CLKIN_IN : IN std_logic;          
      CLKFX_OUT : OUT std_logic;
      CLKIN_IBUFG_OUT : OUT std_logic;
      CLK0_OUT : OUT std_logic
      );
  END COMPONENT;
  signal douta,dina : std_logic_vector(62 downto 0) := (others => '0');
  signal instwea : std_logic_vector(0 downto 0) := "0";
  signal instaddra : std_logic_vector(14 downto 0) := (others => '0');
  signal instdina : std_logic_vector(62 downto 0) := (others => '0');


  
  
  --forwarding signal
  signal forwardfpu1we : std_logic := '0';
  signal forwardfpu1data : std_logic_vector(31 downto 0) := (others => '0');
  signal forwardfpu1wrreg : std_logic_vector(4 downto 0) := (others => '0');
begin  -- rtl

  
  your_instance_name : instblock
    PORT MAP (
      clka => clk,
      wea => instwea,
      addra => instaddra,
      dina => instdina,
      clkb => clk,
      addrb => next_pc,
      doutb => douta
      );
--
--	your_instance_name : instblock
--	  PORT MAP (
--		 clka => clk,
--		 addra => next_pc,
--		 douta => douta
--	  );
--	  
  clock_r : clock_gen port map(
    clkin_in => mclk1,
    clkfx_out => clk,
    clkin_ibufg_out => open,
    clk0_out => open);
  --clk <= MCLK1;
  
  
  ZCLKMA(0) <= clk;
  ZCLKMA(1) <= clk;
  XE1 <= '0';
  E2A <= '1';
  XE3 <= '0';
  XGA <= '0';
  XZCKE <= '0';
  ADVA <= '0';
  XLBO <= '1';
  ZZA <= '0';
  --XFT <= '0';                           -- flow through
  XFT <= '1';                           -- pipe line
  XZBE <= "0000";
  

  -- fetch

  process(clk)
  begin
    if rising_edge(clk) then
      pc_reg <= next_pc;

      --fetchinst1 <= inst1mem(to_integer(unsigned(next_pc)));
      --fetchinst2 <= inst2mem(to_integer(unsigned(next_pc)));

      --fetchstallcount <= stallmem(to_integer(unsigned(next_pc)));
      
    end if;
  end process;

  next_pc <=
    branchpc when branchflag = '1' else
    pc_reg when stallflag = '1' else
    std_logic_vector(unsigned(pc_reg) + 1);



  -- decode

  process(clk)
  begin
    if rising_edge(clk) then

      if stallflag = '0' then
        --decodeinst1 <= fetchinst1;
        --decodeinst2 <= fetchinst2;
        --decodestallcount <= fetchstallcount;
        
        decodeinst1 <= douta(62 downto 33);
        decodeinst2 <= douta(32 downto 3);
        decodestallcount <= douta(2 downto 0);
        --decodestallcount <= "011";
      elsif decodestallcount /= "000" then
        decodestallcount <= std_logic_vector(unsigned(decodestallcount) - 1);
      end if;

      dzd <= zd;
      dzd2 <= dzd;
      dzd3 <= dzd2;

      if alu1we='1' then
        ireg1(to_integer(unsigned(alu1wrreg))) <= alu1wrdata;
--        useireg(to_integer(unsigned(alu1wrreg))) <= '1';
      elsif iowe='1' then
        ireg1(to_integer(unsigned(iowrreg))) <= iowrdata;
--        useireg(to_integer(unsigned(iowrreg))) <= '1';
      elsif cacheiwe='1' then
        ireg1(to_integer(unsigned(cachewrreg))) <= cachedout;
      end if;

      if alu2we='1' then
        ireg1(to_integer(unsigned(alu2wrreg))) <= alu2wrdata;
--        useireg(to_integer(unsigned(alu2wrreg))) <= '1';
      end if;

      if memiwe='1' then
        ireg1(to_integer(unsigned(memwrreg))) <= dzd3;
--        useireg(to_integer(unsigned(memwrreg))) <= '1';
      end if;
      
      --forwardfpu1we <= fpu2we;
      --forwardfpu1data <= fpu2wrdata;
      --forwardfpu1wrreg <= fpu2wrreg;

      if fpu2we='1' then
        freg1(to_integer(unsigned(fpu2wrreg))) <= fpu2wrdata;
        --usefreg(to_integer(unsigned(fpu1wrreg))) <= '1';
      end if;
        
      if memfwe='1' then
        freg1(to_integer(unsigned(memwrreg))) <= dzd3;
--        usefreg(to_integer(unsigned(memwrreg))) <= '1';
      end if;

      if cachefwe='1' then
        freg1(to_integer(unsigned(cachewrreg))) <= cachedout;
      end if;

--      if fpu1we='1' then
--        freg1(to_integer(unsigned(fpu1wrreg))) <= fpu1wrdata;
----        usefreg(to_integer(unsigned(fpu2wrreg))) <= '1';
--      end if;
--
--      if memfwe='1' then
--        freg1(to_integer(unsigned(memwrreg))) <= zd;
----        usefreg(to_integer(unsigned(memwrreg))) <= '1';
--      end if;
      dtmpaddr <= tmpaddr;
      ZA(19) <= '0';
      ZA(18 downto 0) <= tmpaddr;
      dxwa <= txwa;
      xwa <= txwa;
    end if;
  end process;


  stallflag <=
    '1' when decodestallcount /= "000" or
    (decodeinst1(29 downto 25) = OPRECV and polling = '1') else
    '0';

  opcode1 <= decodeinst1(29 downto 25) when stallflag = '0' else
             OPNOP;


  rsi1 <= decodeinst1(24 downto 20);
  rti1 <= decodeinst1(19 downto 15);
  rdi1 <= decodeinst1(13 downto 9);
  funct1 <= decodeinst1(8 downto 0);
  iv161 <= decodeinst1(15 downto 0);
  
  opcode2 <= decodeinst2(29 downto 25) when stallflag = '0' else
             OPNOP;
  
  rsi2 <= decodeinst2(24 downto 20);
  rti2 <= decodeinst2(19 downto 15);
  rdi2 <= decodeinst2(13 downto 9);
  funct2 <= decodeinst2(8 downto 0);
  iv162 <= decodeinst2(15 downto 0);


  rs1 <= ireg1(to_integer(unsigned(rsi1)));
  --when useireg(to_integer(unsigned(rsi1))) = '1' else
  --ireg2(to_integer(unsigned(rsi1)));

  rt1 <= ireg1(to_integer(unsigned(rti1)));
  --when useireg(to_integer(unsigned(rti1))) = '1' else
  --ireg2(to_integer(unsigned(rti1)));

  rs2 <= ireg1(to_integer(unsigned(rsi2)));
  --when useireg(to_integer(unsigned(rsi2))) = '1' else
  --ireg2(to_integer(unsigned(rsi2)));

  rt2 <= ireg1(to_integer(unsigned(rti2)));
  --when useireg(to_integer(unsigned(rti2))) = '1' else
  --ireg2(to_integer(unsigned(rti2)));


  frs1 <= --forwardfpu1data when forwardfpu1we='1' and forwardfpu1wrreg=rsi1 else
          --fpu1wrdata when fpu1we='1' and fpu1wrreg=rsi1 else
          freg1(to_integer(unsigned(rsi1)));
  --when usefreg(to_integer(unsigned(rsi1))) = '1' else
  --freg2(to_integer(unsigned(rsi1)));

  frt1 <= --forwardfpu1data when forwardfpu1we='1' and forwardfpu1wrreg=rti1 else
          --fpu1wrdata when fpu1we='1' and fpu1wrreg=rti1 else
          freg1(to_integer(unsigned(rti1)));
  --when usefreg(to_integer(unsigned(rti1))) = '1' else
  --freg2(to_integer(unsigned(rti1)));

  frs2 <= --forwardfpu1data when forwardfpu1we='1' and forwardfpu1wrreg=rsi2 else
          --fpu1wrdata when fpu1we='1' and fpu1wrreg=rsi2 else
          freg1(to_integer(unsigned(rsi2)));
  --when usefreg(to_integer(unsigned(rsi2))) = '1' else
  --freg2(to_integer(unsigned(rsi2)));

  frt2 <= --forwardfpu1data when forwardfpu1we='1' and forwardfpu1wrreg=rti2 else
          --fpu1wrdata when fpu1we='1' and fpu1wrreg=rti2 else
          freg1(to_integer(unsigned(rti2)));
  --when usefreg(to_integer(unsigned(rti2))) = '1' else
  --freg2(to_integer(unsigned(rti2)));

  tmpaddr <= std_logic_vector(signed(rt2) + signed(iv162(14 downto 0)));
  txwa <= '0' when opcode2 = OPSTOREI or opcode2 = OPFSTOREI else
          '1';


  -- program loader

  process(clk)
  begin
    if rising_edge(clk) then
      case opcode1 is
        when OPLOADINST =>
          instdina(62 downto 31) <= rs1;
          instdina(30 downto 0) <= instdina(62 downto 32);
        when others => null;
      end case;

      case opcode1 is
        when OPWRITEINST =>
          instwea(0) <= '1';
        when others =>
          instwea(0) <= '0';
      end case;
      
      instaddra <= rs1(14 downto 0);
    end if;
  end process;


  -- cache process
  
  blockccache_r : blockcache
    PORT MAP (
      clka => clk,
      wea => cachewea,
      addra => ldaddr,
      dina => cachedin,
      douta => cachedout
      );
      
  cachewea(0) <= '1' when opcode1 = OPCASTI or opcode1 = OPCAFSTI else
                 '0';
  cachedin <= rs1 when opcode1 = OPCASTI else
              frs1;
  ldaddr <=
    std_logic_vector(signed(rt1)+signed(rs1)) when opcode1 = OPCALDR else
    std_logic_vector(signed(rt1)+signed(iv161(14 downto 0)));
  
  process(clk)
  begin
    if rising_edge(clk) then
      case opcode1 is
        when OPCALDI | OPCALDR =>
          cacheiwe <= '1';
          cachefwe <= '0';
        when OPCAFLDI =>
          cacheiwe <= '0';
          cachefwe <= '1';
        when others =>
          cacheiwe <= '0';
          cachefwe <= '0';
      end case;

      case opcode1 is
        when OPCALDR =>
          cachewrreg <= rdi1;
        when others =>
          cachewrreg <= rsi1;
      end case;
      --cachewrreg <= rsi2;
      
    end if;
  end process;


  alu1 : alu port map(
    clk => clk,
    op => opcode1,
    rs => rs1,
    rt => rt1,
    imm16 => iv161,

    rsi => rsi1,
    rdi => rdi1,
    aluwe => alu1we,
    aluoutreg => alu1wrreg,
    aluout => alu1wrdata
    );

  alu2 : alu port map(
    clk => clk,
    op => opcode2,
    rs => rs2,
    rt => rt2,
    imm16 => iv162,

    rsi => rsi2,
    rdi => rdi2,
    aluwe => alu2we,
    aluoutreg => alu2wrreg,
    aluout => alu2wrdata      
    );


  --fpu1 : fpu port map(
  --  clk => clk,
  --  opcode => opcode1,
  --  funct => funct1,
  --  frs => frs1,
  --  frt => frt1,
  --  fdi => rdi1,

  --  fpuwe => fpu1we,
  --  fpuoutreg => fpu1wrreg,
  --  fpuout => fpu1wrdata
  --  );

  fpu2 : fpu port map(
    clk => clk,
    opcode => opcode2,
    funct => funct2,
    frs => frs2,
    frt => frt2,
    fdi => rdi2,

    fpuwe => fpu2we,
    fpuoutreg => fpu2wrreg,
    fpuout => fpu2wrdata
    );

  io_r : io
    generic map (
      --wtime => x"0D6D") -- 9600 33.33MHz
      --wtime => x"1ADB") -- 9600 66.66MHz
      --wtime => x"008F") -- 460800 66.66MHz
      --wtime => x"0074") -- 57600 66.66MHz
      --wtime => x"0048") -- 921600 66.66MHz
      --wtime => x"0021") -- 2000000 66.66MHz    
      --wtime => x"0607") -- 57600 88.88MHz
      --wtime => x"00C1") -- 460800 88.88MHz
      --wtime => x"00D9") -- 460800 99.99MHz
      --wtime => x"00E5") -- 460800 66.66 * 19 / 12 MHz
      --wtime => x"00F1") -- 460800 111.1MHz
      --wtime => x"00FD")	  -- 460800 7/4 119.988MHz
      --wtime => x"0104")	  -- 460800 9/5 119.988MHz
      --wtime => x"0109") -- 460800 122.21MHz
      --wtime => x"010E") -- 460800 28/15 124.4324MHz      
      --wtime => x"0113") -- 460800 19/10 126.654MHz
      wtime => x"0121") -- 460800 133.33MHz
      --wtime => x"012B") -- 460800 31/15 137.764MHz
      --wtime => x"012C") -- 460800 29/14 138.08MHz
      --wtime => x"0139") -- 460800 144.43MHz
      --wtime => x"000A")
    port map(
      clk => clk,
      opcode => opcode1,
      din => rs1,
      rx => rs_rx,

      wrreg => rdi1,
      dout => iowrdata,
      iowe => iowe,
      iooutreg => iowrreg,

      tx => rs_tx,
      polling => polling
      );

  ram_r : ram port map(
    clk => clk,
    rs => rs2,
    frs => frs2,
    opcode => opcode2,
    rsi => rsi2,
    rdi => rdi2,

    ramwrreg => memwrreg,
    memiwe => memiwe,
    memfwe => memfwe,
    dout => memwrdata,
    mdata => zd
    );

  branch_r : branch port map (
    clk => clk,
    opcode => opcode2,
    rs => rs2,
    rt => rt2,
    frs => frs2,
    frt => frt2,
    imm16 => iv162,
    imm6 => rti2,
    flag => branchflag,
    pcout => branchpc
    );
  
end rtl;

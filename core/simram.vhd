library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.definst.all;

entity ram is
  port(
    clk : in std_logic;
    rs : in std_logic_vector(31 downto 0);
    frs : in std_logic_vector(31 downto 0);
    memaddr : in std_logic_vector(19 downto 0);

    opcode : in std_logic_vector(4 downto 0);

    rsi : in std_logic_vector(5 downto 0);
    rdi : in std_logic_vector(5 downto 0);
    
    ramwrreg : out std_logic_vector(5 downto 0);
    memiwe : out std_logic;
    memfwe : out std_logic;

    dout : out std_logic_vector(31 downto 0);
    mdata : inout std_logic_vector(31 downto 0));
  
end ram;

architecture ram of ram is

  signal ddout,ddata,fdata : std_logic_vector(31 downto 0) := (others => '0');
  signal ddopcode,dopcode : std_logic_vector(4 downto 0) := (others => '0');
  signal ddwrreg,dwrreg : std_logic_vector(6 downto 0) := (others => '0');
  signal daddr : std_logic_vector(19 downto 0) := (others => '0');

  type RAM is array (0 to 1023) of std_logic_vector(31 downto 0);
  signal mem : RAM := (others => x"00000000");

begin  -- ram

  process(clk)
  begin

    if rising_edge(clk) then
      ddata <= rs;
      fdata <= frs;
      daddr <= memaddr;
      dopcode <= opcode;
      ddopcode <= dopcode;

      if opcode=OPSTOREI then
        mem(to_integer(unsigned(memaddr(9 downto 0)))) <= rs;
      elsif opcode=OPFSTOREI then
        mem(to_integer(unsigned(memaddr(9 downto 0)))) <= frs;
      end if;

      if opcode=OPLOADI or opcode=OPLOADR then
        memiwe <= '1';
        memfwe <= '0';
      elsif opcode=OPFLOADI or opcode=OPFLOADR then
        memfwe <= '1';
        memiwe <= '0';
      else
        memfwe <= '0';
        memiwe <= '0';
      end if;

      dout <= mem(to_integer(unsigned(memaddr(9 downto 0))));
      --dout <= ddout;

      if opcode=OPLOADI or opcode=OPFLOADI then
        ramwrreg <= rsi;
      else
        ramwrreg <= rdi;
      end if;

      ddwrreg <= dwrreg;
      --ramwrreg <= ddwrreg;
    end if;
    
    
  end process;
end ram;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.definst.all;

entity ram is
  port(
    clk : in std_logic;
    rs : in std_logic_vector(31 downto 0);
    frs : in std_logic_vector(31 downto 0);

    opcode : in std_logic_vector(4 downto 0);

    rsi : in std_logic_vector(4 downto 0);
    rdi : in std_logic_vector(4 downto 0);
    
    ramwrreg : out std_logic_vector(4 downto 0);
    memiwe : out std_logic;
    memfwe : out std_logic;

    dout : out std_logic_vector(31 downto 0);
    mdata : inout std_logic_vector(31 downto 0));
  
end ram;

architecture ram of ram is
  
  signal datasignal,datasignal2,datasignal3 : std_logic_vector(31 downto 0) := (others => '0');
  signal dopcode,dopcode2,dopcode3,dopcode4,dopcode5 : std_logic_vector(4 downto 0) := (others => '0');
  signal dwrreg,dwrreg2,dwrreg3,dwrreg4,dwrreg5 : std_logic_vector(4 downto 0) := (others => '0');
  signal memen,memen2,memen3 : std_logic := '0';
begin  -- ram

  process(clk)
  begin
    if rising_edge(clk) then

      dopcode <= opcode;
      dopcode2 <= dopcode;
      dopcode3 <= dopcode2;
      dopcode4 <= dopcode3;
      dopcode5 <= dopcode4;

      if opcode=OPSTOREI then
        datasignal <= rs;
        memen <= '1';
      elsif opcode=OPFSTOREI then
        datasignal <= frs;
        memen <= '1';
      else
        memen <= '0';
      end if;
		
      datasignal2 <= datasignal;
      if memen2='1' then
        mdata <= datasignal2;
      else
        mdata <= (others => 'Z');
      end if;
      datasignal3 <= datasignal2;
      memen2 <= memen;
      memen3 <= memen2;
      
      if dopcode5=OPLOADI then
        memiwe <= '1';
        memfwe <= '0';
      elsif dopcode5=OPFLOADI then
        memiwe <= '0';
        memfwe <= '1';
      else
        memfwe <= '0';
        memiwe <= '0';
      end if;
      
      dwrreg <= rsi;
      dwrreg2 <= dwrreg;
      dwrreg3 <= dwrreg2;
      dwrreg4 <= dwrreg3;
      dwrreg5 <= dwrreg4;
      
      ramwrreg <= dwrreg5;
    end if;
  end process;

  --mdata <= datasignal3 when memen3 = '1' else
  --         (others => 'Z');
  
end ram;

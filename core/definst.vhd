
library ieee;
use ieee.std_logic_1164.all;

package definst is

  constant opnop : std_logic_vector := "00000";
  constant oploadi : std_logic_vector := "00001";
  constant opstorei : std_logic_vector := "00010";
  constant opfloadi : std_logic_vector := "00011";
  constant opfstorei : std_logic_vector := "00100";
  constant opcaldi : std_logic_vector := "01111";
  constant opcasti : std_logic_vector := "00110";
  constant opcafldi : std_logic_vector := "10110";
  constant opcafsti : std_logic_vector := "10111";
  constant opaddi : std_logic_vector := "00101";
  constant opshll : std_logic_vector := "00111";
  constant opshra : std_logic_vector := "10101";
  constant opcaldr : std_logic_vector := "01000";
  constant opcafldr : std_logic_vector := "01010";
  constant opadd : std_logic_vector := "01011";
  constant opsub : std_logic_vector := "01100";
  constant opfadd : std_logic_vector := "01101";
  constant opfmul : std_logic_vector := "01101";
  constant opfinv : std_logic_vector := "01101";
  constant opfsqrt : std_logic_vector := "01101";
  constant opfmv : std_logic_vector := "01101";
  constant oprecv : std_logic_vector := "10000";
  constant opsend : std_logic_vector := "10001";
  constant oploadinst : std_logic_vector := "01001";
  constant opwriteinst : std_logic_vector := "01110";
  constant opsetlo : std_logic_vector := "10010";
  constant opsetlabel : std_logic_vector := "10010";
  constant opbeq : std_logic_vector := "11000";
  constant opblt : std_logic_vector := "11001";
  constant opfbeq : std_logic_vector := "11010";
  constant opfblt : std_logic_vector := "11011";
  constant opjmp : std_logic_vector := "11100";
  constant opjr : std_logic_vector := "11101";
  constant opbeqi : std_logic_vector := "11110";
  constant opblti : std_logic_vector := "11111";

end definst;

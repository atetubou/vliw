library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


library work;
use work.definst.all;


entity io is
  generic (
    wtime : std_logic_vector(15 downto 0) := x"1ADB");
  
  port(
    clk : in std_logic;
    opcode : in std_logic_vector(4 downto 0);
    din : in std_logic_vector(31 downto 0);
    rx : in std_logic;

    wrreg : in std_logic_vector(4 downto 0);

    dout : out std_logic_vector(31 downto 0);
    iowe : out std_logic;
    iooutreg : out std_logic_vector(4 downto 0);
    
    tx : out std_logic;
    polling : out std_logic);
end io;

architecture uart of io is

  COMPONENT iooutbuff
    PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
  END COMPONENT;
  signal outdin,outdout : std_logic_vector(7 downto 0) := (others => '0');
  signal outwr_en,outrd_en,outempty,sending_ok : std_logic := '0';
  
  signal senddata : std_logic_vector(8 downto 0) := "111111111";
  signal sendstate : std_logic_vector(3 downto 0) := "0000";
  signal presendstate : std_logic_vector(2 downto 0) := "000";
  signal sendcountdown : std_logic_vector(15 downto 0):= (others => '0');


  signal recvstate : std_logic_vector(3 downto 0) := "0000";
  signal numzero : std_logic_vector(15 downto 0) := (others => '0');
  signal recvdata : std_logic_vector(7 downto 0) := (others => '0');
  signal recvcountdown : std_logic_vector(15 downto 0) := (others => '0');
  signal count : std_logic_vector(3 downto 0) := (others => '0');
  
  COMPONENT ioinbuff
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
  END COMPONENT;
  signal indin,indout : std_logic_vector(7 downto 0) := (others => '0');
  signal inwr_en,inrd_en,inempty : std_logic := '0';
begin

  iooutbuff_r : iooutbuff
    PORT MAP (
      clk => clk,
      rst => '0',
      din => outdin,
      wr_en => outwr_en,
      rd_en => outrd_en,
      dout => outdout,
      full => open,
      empty => outempty
    );

  sendrs232c : process(clk)
  begin
    if rising_edge(clk) then
    
      if sending_ok='0' then
        case presendstate is
          when "000" =>
            outrd_en <= not outempty;
            if outempty='0' then
              presendstate <= "001";
            end if;
          when "001" =>
            sending_ok <= '1';
            outrd_en <= '0';
            presendstate <= "000";
          when others => null;
        end case;
      else
        case sendstate is
          when "0000" =>
            senddata <= outdout & "0";
            sendstate <= "0001";
            sendcountdown <= wtime;
          when others =>
            if sendcountdown=x"0000" then
              senddata <= "1" & senddata(8 downto 1);
              sendcountdown <= wtime;
              if sendstate="1011" then
                sendstate <= "0000";
                sending_ok <= '0';
              else
                sendstate <= std_logic_vector(unsigned(sendstate)+1);
              end if;
            else
              sendcountdown <= std_logic_vector(unsigned(sendcountdown)-1);
            end if;
        end case;
      end if;
      
    end if;
  end process;
  tx <= senddata(0);

  sendinst : process(clk)
  begin
    if rising_edge(clk) then
      outdin <= din(7 downto 0);
      if opcode=OPSEND then
        outwr_en <= '1';
      else
        outwr_en <= '0';
      end if;
    end if;
  end process;
  
  
  
  ioinbuff_r : ioinbuff
	  PORT MAP (
		 clk => clk,
		 rst => '0',
		 din => indin,
		 wr_en => inwr_en,
		 rd_en => inrd_en,
		 dout => indout,
		 full => open,
		 empty => inempty
	  );  
	  
  recvrs232c : process(clk)
  begin
    if rising_edge(clk) then

      if recvstate="1001" and recvcountdown=x"0000" then
        inwr_en <= '1';
      else
        inwr_en <= '0';
      end if;
      
      case recvstate is
        when "0000" =>
          if unsigned(numzero) > unsigned(wtime(15 downto 1)) + unsigned(wtime(15 downto 2)) then
            recvstate <= "0001";
            numzero <= (others => '0');
            recvcountdown <= wtime;
          elsif rx='0' then
            numzero <= std_logic_vector(unsigned(numzero)+1);
          end if;
          
        when others =>

          if recvcountdown=x"0000" then
            if recvstate="1001" then
              indin <= recvdata;
              recvstate <= "0000";
            else
              recvstate <= std_logic_vector(unsigned(recvstate) + 1);
            end if;

            if unsigned(numzero) > unsigned(wtime(15 downto 1)) then
              recvdata <= "0" & recvdata(7 downto 1);
            else
              recvdata <= "1" & recvdata(7 downto 1);
            end if;
            
            numzero <= (others => '0');
            recvcountdown <= wtime;
          else
            recvcountdown <= std_logic_vector(unsigned(recvcountdown) - 1);
            
            if rx='0' then
              numzero <= std_logic_vector(unsigned(numzero) + 1);
            end if;
            
          end if;
          
      end case;
    end if;
  end process;  
  

  inrd_en <=  '1' when opcode=OPRECV else
              '0';
              
  recvinst : process(clk)
  begin
    if rising_edge(clk) then
      iooutreg <= wrreg;
      
      if opcode=OPRECV then
        iowe <= '1';
      else
        iowe <= '0';
      end if;

      count <= std_logic_vector(unsigned(count) + 1);
      if count = "0000" then
        polling <= inempty;
      else
        polling <= '1';
      end if;
    end if;
  end process;
  dout <= x"000000" & indout;
end uart;

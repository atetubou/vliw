library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.definst.all;


entity alu is
  port(
    clk : in std_logic;
    
    op : in std_logic_vector(4 downto 0);
    rs,rt : in std_logic_vector(31 downto 0);
    imm16 : in std_logic_vector(15 downto 0);

    rsi,rdi : in std_logic_vector(4 downto 0);

    aluwe : out std_logic;
    aluoutreg : out std_logic_vector(4 downto 0);
    aluout : out std_logic_vector(31 downto 0));

end alu;


architecture alu of alu is

begin  -- alu

  process(clk)
  begin
    if rising_edge(clk) then
      case op is
        when OPADD | OPSUB =>
          aluwe <= '1';
          aluoutreg <= rdi;
        when OPADDI | OPSHLL | OPSHRA | OPSETLO =>
          aluwe <= '1';
          aluoutreg <= rsi;
        when others =>
          aluwe <= '0';
      end case;
      
      case op is
        when  OPADD =>                 -- add
          aluout <=
            std_logic_vector(unsigned(rs) + unsigned(rt));
          
        when OPSUB =>                  -- sub
          aluout <= 
            std_logic_vector(unsigned(rs) - unsigned(rt));
          
        when OPADDI =>              -- addi
          aluout <=
            std_logic_vector(signed(imm16(14 downto 0)) + signed(rt));
          
        when OPSETLO =>              -- setlo
          if imm16(15)='1' then
            aluout <= x"ffff" & imm16;
          else
            aluout <= x"0000" & imm16;
          end if;

        when OPSHLL =>              -- shll
          aluout <=
            std_logic_vector(shift_left(signed(rt),to_integer(unsigned(imm16(4 downto 0)))));

        when OPSHRA =>              -- shra
          aluout <=
            std_logic_vector(shift_right(signed(rt),to_integer(unsigned(imm16(4 downto 0)))));

        when others =>
              null;
      end case;
      
    end if;
  end process;
  
end alu;

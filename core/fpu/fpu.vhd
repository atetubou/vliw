library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.definst.all;


entity fpu is
  port(
    clk : in std_logic;
    
    opcode : in std_logic_vector(4 downto 0);
    funct : in std_logic_vector(8 downto 0);
    frs,frt : in std_logic_vector(31 downto 0);

    fdi : in std_logic_vector(4 downto 0);

    fpuwe : out std_logic;
    fpuoutreg : out std_logic_vector(4 downto 0);
    fpuout : out std_logic_vector(31 downto 0));
  
end fpu;

architecture fpu of fpu is

  component fadd
    port (
      clk : in std_logic;
      ina : in std_logic_vector(31 downto 0);
      inb : in std_logic_vector(31 downto 0);    
      res : out std_logic_vector(31 downto 0));
  end component;

  component fmul_vel
    port ( fa : in std_logic_vector(31 downto 0);
           fb : in std_logic_vector(31 downto 0);
           clk : in std_logic;
           result  : out std_logic_vector(31 downto 0));
  end component;


  component finv
    port ( op : in std_logic_vector(31 downto 0);
           clk : in std_logic;
           result  : out std_logic_vector(31 downto 0));
  end component;

  component sqrtf
    port (
      op : in std_logic_vector(31 downto 0);
      clk : in std_logic;
      result : out std_logic_vector(31 downto 0));
  end component;

  signal faddout,fmulout,finvout,fsqrtout : std_logic_vector(31 downto 0) := (others => '0');
  signal dfrs,dfrt : std_logic_vector(31 downto 0) := (others => '0');

  signal dwrreg1,dwrreg2 : std_logic_vector(4 downto 0) := (others => '0');
  signal dopcode1,dopcode2 : std_logic_vector(4 downto 0) := (others => '0');
  signal dfunct1,dfunct2,dfunct3 : std_logic_vector(8 downto 0) := (others => '0');

  signal tmpout : std_logic_vector(31 downto 0) := (others => '0');
  signal dfpucode1,dfpucode2,dfpucode3,fpucode : std_logic_vector(2 downto 0) := (others => '0');

  constant FUNCTFADD  : std_logic_vector(2 downto 0) := "000";
  constant FUNCTFMUL  : std_logic_vector(2 downto 0) := "001";
  constant FUNCTFINV  : std_logic_vector(2 downto 0) := "010";
  constant FUNCTFSQRT : std_logic_vector(2 downto 0) := "011";
  constant FUNCTFMV   : std_logic_vector(2 downto 0) := "100";
  
begin  -- fpu

  fadd_r : fadd port map (
    clk => clk,
    ina => dfrs,
    inb => dfrt,
    res => faddout);

  fpucode <= funct(8 downto 6);


  fmull_r : fmul_vel port map(
    clk => clk,
    fa => dfrs,
    fb => dfrt,
    result => fmulout);

  finv_r : finv port map (
    clk    => clk,
    op     => dfrt,
    result => finvout);

  fsqrt_f : sqrtf port map (
    clk    => clk,
    op     => dfrt,
    result => fsqrtout);

  
  process(clk)
  begin
    if rising_edge(clk) then
	 
		if fpucode = FUNCTFMV then
			dfrs <= (others => '0');
		else
			dfrs <= frs;
		end if;
		
		dfrt(30 downto 0) <= frt(30 downto 0);
		dfrt(31) <= frt(31) xor funct(0);
		
      dwrreg1 <= fdi;
		dwrreg2 <= dwrreg1;
      dopcode1 <= opcode;

      dfunct1 <= funct;
      dfunct2 <= dfunct1;
		dfunct3 <= dfunct2;

      dfpucode1 <= fpucode;
      dfpucode2 <= dfpucode1;
		dfpucode3 <= dfpucode2;

      case dopcode2 is
        when OPFADD =>
          fpuwe <= '1';
        when others =>
          fpuwe <= '0';
      end case;

      dopcode2 <= dopcode1;
      fpuoutreg <= dwrreg2;
    end if;
  end process;

  tmpout <=
    finvout when dfpucode3 = FUNCTFINV else
    fsqrtout when dfpucode3 = FUNCTFSQRT else
    fmulout when dfpucode3 = FUNCTFMUL else
    faddout;

  fpuout(30 downto 0) <= tmpout(30 downto 0);
  fpuout(31) <= (tmpout(31) and not dfunct3(1)) xor dfunct3(2);

end fpu;

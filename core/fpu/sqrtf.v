
module sqrtf(op,clk,result);

input [31:0]op;
input clk;
output[31:0]result;

wire [35:0]param;
reg sign;
reg [22:0] p;
reg [26:0] q;
reg [8:0] exp;
reg [31:0] res;

sqrtf_table st(op[23:14],param);
assign result = res;

always @(posedge clk)
begin
    /*stage 1*/
    sign <= op[31];
    p <= param[35:13];
    q <= param[12:0]*op[13:0]+{{15{1'b0}},1'b1,{11{1'b0}}};

    exp <= {1'b0,op[30:23]}-127;
    /*stage 2*/
    res[31] <= sign;
    res[30:23] <= exp[8:1]+127;
    res[22:0] <= p + q[26:13];
end

endmodule

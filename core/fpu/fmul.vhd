library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity fmul is
        port ( fa : in std_logic_vector(31 downto 0);
               fb : in std_logic_vector(31 downto 0);
               clk : in std_logic;
               result  : out std_logic_vector(31 downto 0));

end fmul;

architecture float_mul of fmul is
    signal hh: std_logic_vector(25 downto 0);
    signal hl: std_logic_vector(23 downto 0);
    signal lh: std_logic_vector(23 downto 0);
    signal frac: std_logic_vector(25 downto 0);
    signal exp1 : std_logic_vector(8 downto 0);
    signal exp21 : std_logic_vector(7 downto 0);
    signal exp22 : std_logic_vector(7 downto 0);
    signal zero: std_logic;
    signal uf:std_logic;
    signal sign1: std_logic;
    signal sign2: std_logic;
begin
    fmul_body: process(clk)
      variable frac: std_logic_vector(25 downto 0);      
    begin
        if rising_edge(clk) then
            --stage 1
            hh <=
              std_logic_vector(unsigned('1'&fa(22 downto 11))*unsigned('1'&fb(22 downto 11)));
            hl <=
              std_logic_vector(unsigned('1'&fa(22 downto 11))*unsigned(fb(10 downto 0)));
            lh <=
              std_logic_vector(unsigned('1'&fb(22 downto 11))*unsigned(fa(10 downto 0)));
            exp1 <=
              std_logic_vector(signed('0'&fa(30 downto 23)) + signed('0'&fb(30 downto 23))+ 129);
            sign1 <= fa(31) xor fb(31);
            zero <=
              (fa(30) or fa(29) or fa(28) or fa(27) or fa(26) or fa(25) or fa(24) or fa(23)) and
              (fb(30) or fb(29) or fb(28) or fb(27) or fb(26) or fb(25) or fb(24) or fb(23));
            --stage2
            sign2 <= sign1;
            frac :=
              std_logic_vector(unsigned(hh)+unsigned(hl(23 downto 11))+unsigned(lh(23 downto 11))+2);
            exp21 <=
              std_logic_vector(unsigned(exp1(7 downto 0))+1);
            exp22 <= exp1(7 downto 0);
            uf <= exp1(8) and  zero;
            --stage3
            if (exp1(8) and zero)='0' or exp1(7 downto 0)=x"00" then 
                result(31) <= '0';
                result(30 downto 23) <= (others =>'0');
                result(22 downto 0) <= (others=>'0');
            elsif frac(25)='1' then
                result(30 downto 23) <=
                  std_logic_vector(unsigned(exp1(7 downto 0))+1);
                result(22 downto 0) <= frac(24 downto 2);
                result(31) <= sign1;
            else
                result(30 downto 23) <= exp1(7 downto 0);
                result(22 downto 0) <= frac(23 downto 1) ;
                result(31) <= sign1;
            end if;
        end if;
    end process;
end float_mul;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.finv_table.all;

entity finv is
        port ( op : in std_logic_vector(31 downto 0);
               clk : in std_logic;
               result  : out std_logic_vector(31 downto 0));

end finv;

architecture float_inv of finv is
    signal sign: std_logic;
    signal p : std_logic_vector(22 downto 0);
    signal q : std_logic_vector(25 downto 0);
    signal exp : std_logic_vector(7 downto 0);
begin
    fmul_body: process(clk)
    begin
        if rising_edge(clk) then
            --stage1
            sign <= op(31);
            q <=
              std_logic_vector(unsigned(get_q(op(22 downto 13)))*
                               unsigned(op(12 downto 0)));
            p <= get_p(op(22 downto 13));
            exp <= std_logic_vector(253-unsigned(op(30 downto 23)));
            sign <= op(31);

            --stage2
            result(31) <= sign;
            result(30 downto 23) <= exp;
            result(22 downto 0) <=
              std_logic_vector(unsigned(p)-unsigned(q(25 downto 12)));
        end if;
    end process;
end float_inv;

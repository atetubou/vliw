
module fmul_vel(
    fa,
    fb,
    clk,
    result
);


input [31:0]fa,fb;
input clk;
output [31:0]result;

wire [25:0]frac;

reg [25:0]hh;
reg [23:0]hl,lh;
reg [8:0]exp;
reg [8:0]expi;
reg sign;
reg [31:0] res;

assign result = res;
assign frac = hh + hl[23:11] + lh[23:11] + 2;

always @(posedge clk)
begin
    if(fa[30:23]==0 || fb[30:23]==0) begin
        exp <= {8{1'b0}};
        hh <= {25{1'b0}};
    end
    else begin
        exp <= fa[30:23] + fb[30:23] + 129;
        expi <= fa[30:23] + fb[30:23] + 130;
        sign <= fa[31]^fb[31];
        hh <= {1'b1,fa[22:11]}*{1'b1,fb[22:11]};
        lh <= {1'b1,fa[22:11]}*fb[10:0];
        hl <= {1'b1,fb[22:11]}*fa[10:0];
    end
    if(frac[25])
    begin
        if(expi[7:0]==0) begin
            res<= {32{1'b0}};
        end
        else if(expi[8])
        begin
            res[31] <= sign;
            res[30:23] <= expi[7:0];
            res[22:0] <= frac[24:2];
        end
        else begin
            res<= {32{1'b0}};
        end
    end
    else
    begin
        if(exp[7:0]==0) begin
            res<= {32{1'b0}};
        end
        else if(exp[8])
        begin
            res[31] <= sign;
            res[30:23] <= exp[7:0];
            res[22:0] <= frac[23:1];
        end
        else
        begin
            res<= {32{1'b0}};
        end
    end
end

endmodule

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity zlc is
  port(
    input : in std_logic_vector(28 downto 0);
    count : out std_logic_vector(4 downto 0);
    output : out std_logic_vector(28 downto 0));
end zlc;

architecture zlc of zlc is
  signal sh16,sh8,sh4,sh2,sh1 : std_logic_vector(28 downto 0);  
  signal tmp2 : std_logic_vector(4 downto 0);

  --signal zero2 : std_logic_vector(14 downto 0);
  --signal zero4 : std_logic_vector(7 downto 0);
  --signal zero8 : std_logic_vector(3 downto 0);
  --signal zero16 : std_logic_vector(1 downto 0);
begin  -- zlc

  --zero2(0) <= not input(0) or not input(1);
  --zero2(1) <= not input(2) or not input(3);
  --zero2(2) <= not input(4) or not input(5);
  --zero2(3) <= not input(6) or not input(7);
  --zero2(4) <= not input(8) or not input(9);
  --zero2(5) <= not input(10) or not input(11);
  --zero2(6) <= not input(12) or not input(13);
  --zero2(7) <= not input(14) or not input(15);
  --zero2(8) <= not input(16) or not input(17);
  --zero2(9) <= not input(18) or not input(19);
  --zero2(10) <= not input(20) or not input(21);
  --zero2(11) <= not input(22) or not input(23);
  --zero2(12) <= not input(24) or not input(25);
  --zero2(13) <= not input(26) or not input(27);
  --zero2(14) <= not input(28);

  --zero4(0) <= zero2(0) or zero2(1);
  --zero4(1) <= zero2(2) or zero2(3);
  --zero4(2) <= zero2(4) or zero2(5);
  --zero4(3) <= zero2(6) or zero2(7);
  --zero4(4) <= zero2(8) or zero2(9);
  --zero4(5) <= zero2(10) or zero2(11);
  --zero4(6) <= zero2(12) or zero2(13);
  --zero4(7) <= zero2(14);

  --zero8(0) <= zero4(0) or zero4(1);
  --zero8(1) <= zero4(2) or zero4(3);
  --zero8(2) <= zero4(4) or zero4(5);
  --zero8(3) <= zero4(6) or zero4(7);

  --zero16(0) <= zero8(0) or zero8(1);
  --zero16(1) <= zero8(2) or zero8(3);



  --sh16 <=
  --  input(12 downto 0) & "0000000000000000" when tmp2(4)='1' else
  --  input;

  --sh8 <=
  --  sh16(20 downto 0) & "00000000" when tmp2(3)='1' else
  --  sh16;
  
  --sh4 <=
  --  sh8(24 downto 0) & "0000" when tmp2(2)='1' else
  --  sh8;

  --sh2 <=
  --  sh4(26 downto 0) & "00" when tmp2(1)='1' else
  --  sh4;

  --output <=
  --  sh2(27 downto 0) & "0" when tmp2(0)='1' else
  --  sh2;

  output <=
    std_logic_vector(shift_left(unsigned(input),to_integer(unsigned(tmp2))));
  
  tmp2(4 downto 0) <=
    "00000" when input(28)='1' else
    "00001" when input(27)='1' else
    "00010" when input(26)='1' else
    "00011" when input(25)='1' else
    "00100" when input(24)='1' else
    "00101" when input(23)='1' else
    "00110" when input(22)='1' else
    "00111" when input(21)='1' else
    "01000" when input(20)='1' else
    "01001" when input(19)='1' else
    "01010" when input(18)='1' else
    "01011" when input(17)='1' else
    "01100" when input(16)='1' else
    "01101" when input(15)='1' else
    "01110" when input(14)='1' else
    "01111" when input(13)='1' else
    "10000" when input(12)='1' else
    "10001" when input(11)='1' else
    "10010" when input(10)='1' else
    "10011" when input(9)='1' else
    "10100" when input(8)='1' else
    "10101" when input(7)='1' else
    "10110" when input(6)='1' else
    "10111" when input(5)='1' else
    "11000" when input(4)='1' else
    "11001" when input(3)='1' else
    "11010" when input(2)='1' else
    "11011" when input(1)='1' else
    "11100" when input(0)='1' else
    "00000";
  count <= tmp2;
end zlc;

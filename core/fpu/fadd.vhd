library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fadd is
  port (
    clk : in std_logic;
    ina : in std_logic_vector(31 downto 0);
    inb : in std_logic_vector(31 downto 0);    
    res : out std_logic_vector(31 downto 0));
end fadd;

architecture fadd of fadd is
  component zlc
  port(
    input : in std_logic_vector(28 downto 0);
    count : out std_logic_vector(4 downto 0);
    output : out std_logic_vector(28 downto 0));
  end component;
  signal fa,fa2,fb,fb2,restmp : std_logic_vector(31 downto 0) := (others => '0');
  signal bige,smalle,bige2,smalle2,notbige2,notbige21,marumeshifte : std_logic_vector(7 downto 0);
  signal bigs,smalls,bigs2,smalls2 : std_logic;
  signal bigm,smallm,bigm2 : std_logic_vector(26 downto 0);
  signal shiftsmallm,shiftsmallm2 : std_logic_vector(26 downto 0);
  signal expdiff,expdiff2 : std_logic_vector(7 downto 0) := (others => '0');


  signal ketaotiadd1 : std_logic_vector(26 downto 0);
  signal ketaotiadd2 : std_logic_vector(28 downto 0);
  signal ketaotiadd3 : std_logic_vector(28 downto 0);
  signal ketaoticount : std_logic_vector(4 downto 0);

  signal nototiadd21,nototiadd1,nototiadd11,nototiadd2,marume,marumeshift : std_logic_vector(27 downto 0);

  signal nototires,ketaotires : std_logic_vector(31 downto 0);

  signal aisbig : std_logic;
begin  -- fadd

  zlc_ketaoti : zlc port map(
    input => ketaotiadd2,
    count => ketaoticount,
    output => ketaotiadd3);

  process(clk)
  begin
    if rising_edge(clk) then
      if ina(30 downto 0) > inb(30 downto 0) then
        fa <= ina;
        fb <= inb;
      else
        fa <= inb;
        fb <= ina;
      end if;

  

      fa2 <= fa;
      fb2 <= fb;
      bigs2 <= bigs;
      smalls2 <= smalls;
      bige2 <= bige;
      smalle2 <= smalle;
      expdiff2 <= expdiff;
      ketaotiadd2 <= ketaotiadd1 & "00";

      shiftsmallm2 <= shiftsmallm;
      bigm2 <= bigm;
      
      nototiadd2 <= nototiadd21;
		notbige2 <= notbige21;
    end if;
  end process;
  


  
  bigs <= fa(31);
  bige <= fa(30 downto 23);
  bigm <= "1"&fa(22 downto 0)&"000";

  smalls <= fb(31);
  smalle <= fb(30 downto 23);
  smallm <= "1"&fb(22 downto 0)&"000";  




  expdiff <=
    std_logic_vector(unsigned(bige) - unsigned(smalle));

  ketaotiadd1 <=
    std_logic_vector(unsigned(bigm) - unsigned(smallm))
    when expdiff=x"00" else
    std_logic_vector(unsigned(bigm) - unsigned("0"&smallm(26 downto 1)));
  
  -- ketaotinasi

  
  shiftsmallm(26 downto 1) <=
    std_logic_vector(shift_right(unsigned(smallm(26 downto 1)),to_integer(unsigned(expdiff))));
  
  shiftsmallm(0) <=                     -- sticky
    '1' when 26<unsigned(expdiff) else
    '1' when shift_left(unsigned(smallm),to_integer(26-unsigned(expdiff))) /= 0 else
    '0';
	 
  nototiadd11 <=
    std_logic_vector(unsigned("0"&bigm) - unsigned("0"&shiftsmallm)) when bigs /= smalls else
    std_logic_vector(unsigned("0"&bigm) + unsigned("0"&shiftsmallm));


  notbige21 <=
    std_logic_vector(unsigned(bige) + 1) when nototiadd11(27)='1' else
    bige when nototiadd11(26)='1' else
    std_logic_vector(unsigned(bige) - 1);

  nototiadd21 <=
    "0" & nototiadd11(27 downto 2) & (nototiadd11(1 downto 1) or nototiadd11(0 downto 0)) when nototiadd11(27)='1' else
    nototiadd11 when nototiadd11(26)='1' else
    nototiadd11(26 downto 0) & "0";


  -----------------------------------------------------------------------------
  -- clk no sikiri
  -----------------------------------------------------------------------------
  
  ketaotires(31) <= bigs2;
  ketaotires(30 downto 23) <=
    x"00" when fa2(30 downto 0) = fb2(30 downto 0) else
    std_logic_vector(unsigned(bige2) - unsigned(ketaoticount));
  ketaotires(22 downto 0) <=
    ketaotiadd3(27 downto 5) when
    ketaotiadd3(4)='0' or
    (ketaotiadd3(4 downto 0)= "10000" and ketaotiadd3(5)='0') else
    std_logic_vector(unsigned(ketaotiadd3(27 downto 5)) + 1);




  -- ketaotinasi


  marume <=
    std_logic_vector(unsigned(nototiadd2) + 8)
    when nototiadd2(2)='1' and (nototiadd2(0)='1' or nototiadd2(1)='1' or nototiadd2(3)='1') else
    nototiadd2;

  marumeshift <=
    "0" & marume(27 downto 1) when marume(27)='1' else
    marume;

  marumeshifte <=
    std_logic_vector(unsigned(notbige2) + 1) when marume(27)='1' else
    notbige2;

  

  nototires <=  bigs2 & marumeshifte & marumeshift(25 downto 3);


  -- kekka mux

  res <=
    x"00000000" when bige2=x"00" else
    fa2 when smalle2=x"00" else
    ketaotires when bigs2 /= smalls2 and (expdiff2=x"00" or expdiff2=x"01") else
    nototires;

--  res <=
--    x"00000000" when restmp(30 downto 23)=x"00" else
--    restmp;

end fadd;

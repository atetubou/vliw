(* for min-rt *)

(* begining of miniMLRuntime.ml *)
let rec fequal a b = a = b
let rec fless a b = (a < b)
let rec fispos a = (a > 0.0)
let rec fisneg a = (a < 0.0)
let rec fiszero a = (a = 0.0)
let rec fneg a = -. a
let rec fhalf a = a *. 0.5
let rec fsqr a = a *. a

let rec xor x y = if x then not y else y

let rec atan x = 
  let res = 0.0 in
  let res = res *. x +. 0.0296672 in
  let res = res *. x -. 0.139238 in
  let res = res *. x -. 0.0174956 in
  let res = res *. x +. 0.928238 in
  let res = res *. x -. 0.0149898 in
    res

let rec sin_sub x =
  let res = 0.0 in
  let res = res *. x +. -1.68924e-06 in
  let res = res *. x +. 4.81445e-05 in
  let res = res *. x +. -0.000428513 in
  let res = res *. x +. 0.000511988 in
  let res = res *. x +. 0.00808715 in
  let res = res *. x +. -0.000913705 in
  let res = res *. x +. -0.165562 in
  let res = res *. x +. 0.000333488 in
  let res = res *. x +. 0.99947 in
  let res = res *. x +. -2.8811e-05 in
    res

let rec sin x = 
  let pi2 = 3.14159265358979323 *. 2.0 in
  if x < pi2 then sin_sub x
  else
    let pi4 = pi2 *. 2.0 in
      if x < pi4 then sin_sub (x -. pi2)
      else sin_sub (x -. pi4)

let rec cos x =
  let xx = x *. x in
    1.0 +. xx *.(-0.499293 +. xx *. 0.0394696)

let rec tan x =
  let res = 0.0 in
  let res = res *. x +. 0.471859 in
  let res = res *. x +. -0.065516 in
  let res = res *. x +. 0.123416 in
  let res = res *. x +. 0.987333 in
  let res = res *. x +. 0.000200355 in
    res

let rec floor x =
  let magic = 8388608.0 in
  if x < 0.0 then (
      let y = -. x in
          let z = y +. magic -. magic in
            if z < y then
            -. (z +. magic +. 1.0 -. magic)
            else
            -. z
  ) else (
    let y = x +. magic -. magic in
      if x < y then
        y -. 1.0
      else
        y
  )

      (*
let rec floor x =
  let magic = 8388608.0 in
  if x < 0.0 then (
      let y = -. x in
        if magic < y then
        -. (y +. magic +. 1.0 -. magic)
        else
          let z = y +. magic -. magic in
            if z < y then
            -. (z +. magic +. 1.0 -. magic)
            else
            -. z
  ) else (
    if magic < x then
      x -. 1.0
    else
      let y = x +. magic -. magic in
        if x < y then
          y -. 1.0
        else
          y
  )
       *)

let rec ceil x = -. floor (-. x)

(* オブジェクトの個数 *)
let n_objects = Array.create 1 0

(* オブジェクトのデータを入れるベクトル（最大60個）*)
let objects = Array.allocate 60
(*
let objects = 
  let dummy = Array.create 0 0.0 in
  Array.create 60 (0, 0, 0, 0, dummy, dummy, false, dummy, dummy, dummy, dummy) in
 *)

(* Screen の中心座標 *)
let screen = Array.create 3 0.0
(* 視点の座標 *)
let viewpoint = Array.create 3 0.0
(* 光源方向ベクトル (単位ベクトル) *)
let light = Array.create 3 0.0
(* 鏡面ハイライト強度 (標準=255) *)
let beam = Array.create 1 255.0
(* AND ネットワークを保持 *)
let and_net_inner = Array.create 1 (-1)
let and_net = Array.create 50 and_net_inner
(*let and_net = Array.create 50 (Array.create 1 (-1)) in*)
(* OR ネットワークを保持 *)
let or_net_inner_inner = Array.create 1 (-1)
let or_net_inner = Array.create 1 (or_net_inner_inner)
let or_net = Array.create 1 or_net_inner
(*let or_net = Array.create 1 (Array.create 1 (and_net.(0))) in*)

(* 以下、交差判定ルーチンの返り値格納用 *)
(* solver の交点 の t の値 *)
let solver_dist = Array.create 1 0.0
(* 交点の直方体表面での方向 *)
let intsec_rectside = Array.create 1 0
(* 発見した交点の最小の t *)
let tmin = Array.create 1 (1000000000.0)
(* 交点の座標 *)
let intersection_point = Array.create 3 0.0
(* 衝突したオブジェクト番号 *)
let intersected_object_id = Array.create 1 0
(* 法線ベクトル *)
let nvector = Array.create 3 0.0
(* 交点の色 *)
let texture_color = Array.create 3 0.0

(* 計算中の間接受光強度を保持 *)
let diffuse_ray = Array.create 3 0.0
(* スクリーン上の点の明るさ *)
let rgb = Array.create 3 0.0

(* 画像サイズ *)
let image_size = Array.create 2 0
(* 画像の中心 = 画像サイズの半分 *)
let image_center = Array.create 2 0
(* 3次元上のピクセル間隔 *)
let scan_pitch = Array.create 1 0.0

(* judge_intersectionに与える光線始点 *)
let startp = Array.create 3 0.0
(* judge_intersection_fastに与える光線始点 *)
let startp_fast = Array.create 3 0.0

(* 画面上のx,y,z軸の3次元空間上の方向 *)
let screenx_dir = Array.create 3 0.0
let screeny_dir = Array.create 3 0.0
let screenz_dir = Array.create 3 0.0

(* 直接光追跡で使う光方向ベクトル *)
let ptrace_dirvec  = Array.create 3 0.0

(* 間接光サンプリングに使う方向ベクトル *)
let dirvecs = Array.allocate 5
(*
let dirvecs = 
  let dummyf = Array.create 0 0.0 in
  let dummyff = Array.create 0 dummyf in
  let dummy_vs = Array.create 0 (dummyf, dummyff) in
  Array.create 5 dummy_vs in
 *)

(* 光源光の前処理済み方向ベクトル *)
let light_dirvec =
  let v3 = Array.create 3 0.0 in
  let consts = Array.allocate 60 in
  (v3, consts)
  (*
let light_dirvec =
  let dummyf2 = Array.create 0 0.0 in
  let v3 = Array.create 3 0.0 in
  let consts = Array.create 60 dummyf2 in
  (v3, consts) in
   *)

(* 鏡平面の反射情報 *)
let reflections = Array.allocate 3
  (*
let reflections =
  let dummyf3 = Array.create 0 0.0 in
  let dummyff3 = Array.create 0 dummyf3 in
  let dummydv = (dummyf3, dummyff3) in
  Array.create 180 (0, dummydv, 0.0) in
   *)

(* reflectionsの有効な要素数 *) 

let n_reflections = Array.create 1 0

(* end of global.ml *)
  
(* this is not needed *)
let rec mul_sub a b res =
  if b = 0 then res
  else (
      if b lsl 31 = 0 then
        (mul_sub (a lsl 1) (b lsr 1) res)
      else
        (mul_sub (a lsl 1) (b lsr 1) (res + a))
  )

let rec mul a b =
  if b < 0 then 
    mul_sub (-a) (-b) 0
  else
    mul_sub a b 0

let rec div_binary_search a b left right =
  let mid = (left + right) lsr 1 in
  let x = mid * b in
    if right - left <= 1 then
      left
    else
      if x < a then
        div_binary_search a b mid right
      else if x = a then
             mid
           else
             div_binary_search a b left mid


let rec print_int x =
  (*
  if x < 0 then
    (print_char 45; print_int (-x))
  else
   *)
    (* 1000000000の位を表示 *)
    let tx = div_binary_search x 1000000000 0 3 in
    let dx = tx * 1000000000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then false
      else (print_char (48 + tx); true) in
    (* 100000000の位を表示 *)
    let tx = div_binary_search x 100000000 0 10 in
    let dx = tx * 100000000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in

        (* 10000000の位を表示 *)
    let tx = div_binary_search x 10000000 0 10 in
    let dx = tx * 10000000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in

          (* 1000000の位を表示 *)
    let tx = div_binary_search x 1000000 0 10 in
    let dx = tx * 1000000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in

          (* 100000の位を表示 *)
    let tx = div_binary_search x 100000 0 10 in
    let dx = tx * 100000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in

          (* 10000の位を表示 *)
    let tx = div_binary_search x 10000 0 10 in
    let dx = tx * 10000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in

          (* 1000の位を表示 *)
    let tx = div_binary_search x 1000 0 10 in
    let dx = tx * 1000 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in
          (* 100の位を表示 *)
    let tx = div_binary_search x 100 0 10 in
    let dx = tx * 100 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in
          (* 10の位を表示 *)
    let tx = div_binary_search x 10 0 10 in
    let dx = tx * 10 in
    let x = x - dx in
    let flg = 
      if tx <= 0 then
        (if flg then
           (print_char (48 + tx); true)
         else
           false)
      else
        (print_char (48 + tx); true) in
          (* 1の位を表示 *)
          print_char (48 + x)

let rec print_newline () = print_char 10

let rec char_of_int x = x

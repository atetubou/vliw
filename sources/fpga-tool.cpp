#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/time.h>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <bitset>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
using namespace std;


/* serial port */
const char ttyUSB[] = "/dev/ttyUSB0";


#ifndef B460800 /* for Mac OS X */
#define B460800 460800
#endif
#define handle_error(x) if ((x) == -1) fprintf(stderr, "FATAL: line %d: %s\n", __LINE__, strerror(errno)), exit(1)

void sleep(double t) {
	struct timespec ts = { /*.tv_sec = */static_cast<time_t>(t), /*.tv_nsec = */static_cast<long>((t - floor(t)) * 1e9) };
	struct timespec rem;
	while(nanosleep(&ts, &rem) == -1 && errno == EINTR) ts = rem;
}
double tv2double(const struct timeval &tv) { return (double)tv.tv_sec + (double)tv.tv_usec * 1e-6; }
struct timeval double2tv(double t) { return (struct timeval) { /*.tv_sec =*/ static_cast<time_t>(t), /*.tv_usec =*/ static_cast<suseconds_t>((t-floor(t)) * 1e6) }; }
double get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv2double(tv);
}

speed_t get_baud_rate(int baud_rate) {
	if (baud_rate < 14400) return B9600;
	else if (baud_rate < 28800) return B19200;
	else if (baud_rate < 48000) return B38400;
	else if (baud_rate < 86400) return B57600;
	else if (baud_rate < 122800) return B115200;
	else if (baud_rate < 345600) return B230400;
	else return B460800;
}
int g_baud_rate = -1;
void print_baud_rate(speed_t baud_rate) {
	const string name = "Baud Rate";
	switch(baud_rate){
		case B0      : cerr << "Baud Rate: " <<  (g_baud_rate = 0)      << endl   ; break ; 
		case B9600   : cerr << "Baud Rate: " <<  (g_baud_rate = 9600)   << endl   ; break ; 
		case B19200  : cerr << "Baud Rate: " <<  (g_baud_rate = 19200)  << endl   ; break ; 
		case B38400  : cerr << "Baud Rate: " <<  (g_baud_rate = 38400)  << endl   ; break ; 
		case B57600  : cerr << "Baud Rate: " <<  (g_baud_rate = 57600)  << endl   ; break ; 
		case B115200 : cerr << "Baud Rate: " <<  (g_baud_rate = 115200) << endl   ; break ; 
		case B230400 : cerr << "Baud Rate: " <<  (g_baud_rate = 230400) << endl   ; break ; 
		case B460800 : cerr << "Baud Rate: " <<  (g_baud_rate = 460800) << endl   ; break ; 
		default      : cerr << "Baud Rate: " <<  "Unknown"              << endl   ; break ; 
	}
}
void set_term(int fd, int baud_rate) {
	speed_t baud_speed;
	if (baud_rate == -1) {
		struct termios default_term;
		handle_error(tcgetattr(fd, &default_term));
		baud_speed = cfgetispeed(&default_term);
	} else {
		baud_speed = get_baud_rate(baud_rate);
	}
	print_baud_rate(baud_speed);

	struct termios term = {};
	term.c_cflag = baud_speed | CS8 | CLOCAL | CREAD;
	term.c_iflag = IGNPAR;
	term.c_oflag = term.c_lflag = 0;
	term.c_cc[VTIME] = 0;
	term.c_cc[VMIN] = 1;

	handle_error(tcflush(fd, TCIFLUSH));
	handle_error(tcsetattr(fd, TCSANOW, &term));
}
int open_serial_port(int baud) {
	int fd;
	if ((fd = open(ttyUSB, O_RDWR | O_NOCTTY)) == -1) {
		if (errno == ENOENT) cerr << "open: Failed to open serial port '" << ttyUSB << "'" << endl, exit(1);
		handle_error(-1);
	}
	set_term(fd, baud);
	return fd;
}
void close_serial_port(int fd) {
	handle_error(fd);
}
int write_all(int fd, istream &ins) {
	int len = 0;
	for(char c; ins.get(c); ) {
		ssize_t res;
		handle_error(res = write(fd, &c, sizeof(c)));
		len += (int)res;
	}
	return len;
}

bool select_for_one(int fd, double timeout) {
	const int nfds = fd + 1;
	fd_set fdseti;
	FD_ZERO(&fdseti); FD_SET(fd, &fdseti);
	struct timeval tv = double2tv(timeout);
	int res;
	handle_error(res = select(nfds, &fdseti, NULL, NULL, &tv));
	if (res == 0) return false; /* timeout */
	return true;
}

bool recv_all(int fd, char *buf, ssize_t buf_size, double timeout) {
	double start_time = get_time();
	ssize_t recved = 0;
  double nexttime = 0;
	for(;;) {
		double rem = timeout - (get_time() - start_time);
		if (rem <= 0 || !select_for_one(fd, rem)) break; /* timeout */

		ssize_t res;
		handle_error(res = read(fd, buf + recved, buf_size - recved));
		if (res == 0) cerr << "Serial port has closed." << endl, exit(1);
		if (recved == 0) {
			cerr << "first: " << buf[0] << " " << buf[1] << endl;
		}
		recved += res;
    double ctime = get_time() - start_time;
		if(ctime>nexttime){
      cerr << "recved " << recved << ' ' << ctime << endl;
      cerr << "expected time : " << buf_size*ctime/(recved+1) << endl;
      nexttime += 0.5;
    }
		if (recved >= buf_size)
			return true;
	}
	return false;
}


int main(int argc, char *argv[]) {
	/* parsing arguments */
	int baud = 460800;
	int fd = open_serial_port(baud);
	assert(argc == 3);

	{
		string source = argv[1];
		cerr << "Source File: " << source << endl;
		ifstream srcs(source.c_str(), ios::in | ios::binary);
		if (!srcs) cerr << "No such file" << endl, exit(1);

		cerr << "Write source file" << endl;
		write_all(fd, srcs);
	}


	while(select_for_one(fd, 5)) {
		cerr << ".";
		cerr.flush();
		char buf[4096];
		ssize_t res;
		handle_error(res = read(fd, buf, sizeof(buf)));
		if (res == 0) cerr << "Serial port has been closed." << endl, exit(1);
	}
	cerr << endl;

	cerr << "Maybe writing has been successfully done!" << endl;
  
	double start = get_time();
	{
		string input = argv[2];
		cerr << "Input File: " << input << endl;
		ifstream ins(input.c_str(), ios::in | ios::binary);
		if (!ins) cerr << "No such file" << endl, exit(1);

		cerr << "Send Input File" << endl;
    start = get_time();
		write_all(fd, ins);
	}

	cerr << "Sended!" << endl;
	const int SIZE = 49167;
	char buf[SIZE];

	if (!recv_all(fd, buf, SIZE, 45))
		cerr << "TIMEOUT!" << endl;

	cerr << "FINISHED." << endl;
	cerr << "elapsed time = " << get_time() - start << endl;

	const string res = "a.ppm";
	cerr << "Result File: " << res << endl;

	ofstream outs(res.c_str(), ios::out | ios::trunc);
	if (!outs) cerr << "Can't open " << res << endl, exit(1);

	for(int i=0; i<SIZE; i++)
		outs.put(buf[i]);
}


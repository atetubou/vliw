#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <set>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <cstring>


#ifdef EXACT_FPU
#include "fpu.h"
#define FMUL(a,b) fmul_sim(a,b)
#define FINV(a) finv_sim(a)
#define FSQRT(a) sqrtf_sim(a)
#else
#define FMUL(a,b) (a*b)
#define FINV(a) (1./a)
#define FSQRT(a) (sqrt(a))
#endif

#define CHECK_BLOCK

using namespace std;

#define pb(a) push_back(a)
#define mp(a,b) make_pair(a,b)
#define rep(i,n) for(int i=0;i<(int)(n);++i)
#define FOR(it,a) for(__typeof((a).begin()) it=(a).begin();it!=(a).end();++it)
#define SZ(a) (int)((a).size())


#define OPNOP         (0b00000)
#define OPLOADI       (0b00001)
#define OPSTOREI      (0b00010)
#define OPFLOADI      (0b00011)
#define OPFSTOREI     (0b00100)
#define OPADDI        (0b00101)
#define OPSHLL        (0b00111)

#define OPCALOADR       (0b01000)
#define OPCAFLOADR      (0b01010)
#define OPADD         (0b01011)
#define OPSUB         (0b01100)

#define OPFPU         (0b01101)
#define OPFADD        (0b01101)
#define OPFMUL        (0b01101)
#define OPFINV        (0b01101)
#define OPFSQRT       (0b01101)
#define OPFMV         (0b01101)

#define FUNCTFADD     (0b000)
#define FUNCTFMUL     (0b001)
#define FUNCTFINV     (0b010)
#define FUNCTFSQRT    (0b011)
#define FUNCTFMV      (0b100)

#define FPUBIT        (0b001)
#define ABSBIT        (0b010)
#define NEGBIT        (0b100)

//#define OPITOF        (0b01110)
//#define OPFTOI        (0b01111)
#define OPRECV        (0b10000)
#define OPSEND        (0b10001)
#define OPSETLO       (0b10010)
#define OPSETLABEL    (0b10010)
#define OPFSETHI      (0b10011)
#define OPFSETLO      (0b10100)
#define OPSHRA        (0b10101)


#define OPLOADINST    (0b01001)
#define OPWRITEINST   (0b01110)

#define OPCACHELOAD   (0b01111)
#define OPCACHESTORE  (0b00110)
#define OPFCACHELOAD  (0b10110)
#define OPFCACHESTORE (0b10111)

/*
#define OPNBEQ         (0b01001)
#define OPNBLT         (0b01110)
#define OPNFBEQ        (0b01111)
#define OPNFBLT        (0b00110)
#define OPNBEQI        (0b10110)
#define OPNBLTI        (0b10111)
*/

#define OPBEQ         (0b11000)
#define OPBLT         (0b11001)
#define OPFBEQ        (0b11010)
#define OPFBLT        (0b11011)
#define OPJMP         (0b11100)
#define OPJR          (0b11101)
#define OPBEQI        (0b11110)
#define OPBLTI        (0b11111)



typedef pair<unsigned,unsigned> PU;
typedef long long ll;
vector<string> assembly;
map<string,unsigned> label2pc;

vector<PU> instructions;
vector<int> stallnum;


map<string,unsigned> iinst,rinst,binst,cinst,sinst;
map<string,unsigned> op2funct;

int hazard[1<<5];

void init(){
  hazard[OPLOADI] =  hazard[OPFLOADI] = 3;
  hazard[OPCALOADR] = hazard[OPCAFLOADR] = 1;
  hazard[OPFPU] = 2;
  hazard[OPADDI] = hazard[OPSHLL] = hazard[OPSHRA] =
    hazard[OPADD] = hazard[OPSUB] =
    hazard[OPCACHELOAD] = hazard[OPFCACHELOAD] =
    hazard[OPRECV] = hazard[OPSETLO] = hazard[OPSETLABEL] = 1;

  op2funct["fadd"] =0b000000000;
  op2funct["fmul"] =0b001000000;
  op2funct["finv"] =0b010000000;
  op2funct["fsqrt"]=0b011000000;
  op2funct["fmv"]  =0b100000000;


  iinst["loadi"]   =OPLOADI;
  iinst["storei"]  =OPSTOREI;
  iinst["floadi"]  =OPFLOADI;
  iinst["fstorei"] =OPFSTOREI;
  iinst["addi"]    =OPADDI;
  iinst["shll"]    =OPSHLL;
  iinst["shra"]    =OPSHRA;
  iinst["caldi"]   =OPCACHELOAD;
  iinst["casti"]   =OPCACHESTORE;
  iinst["cafldi"]  =OPFCACHELOAD;
  iinst["cafsti"]  =OPFCACHESTORE;
  FOR(it,iinst) it->second <<= 27;

  rinst["caldr"]   =OPCALOADR;
  rinst["cafldr"]  =OPCAFLOADR;
  rinst["add"]     =OPADD;
  rinst["sub"]     =OPSUB;
  rinst["fadd"]    =OPFADD;
  rinst["fmul"]    =OPFMUL;
  rinst["finv"]    =OPFINV;
  rinst["fsqrt"]   =OPFSQRT;
  rinst["fmv"]     =OPFMV;
  rinst["recv"]    =OPRECV;
  rinst["send"]    =OPSEND;
  rinst["loadinst"] = OPLOADINST;
  rinst["writeinst"] = OPWRITEINST;
  FOR(it,rinst) it->second <<= 27;

  sinst["setlo"]   =OPSETLO;
  sinst["setlabel"]=OPSETLABEL;
  sinst["fsethi"]  =OPFSETHI;
  sinst["fsetlo"]  =OPFSETLO;
  FOR(it,sinst) it->second <<= 27;

  binst["beq"]     =OPBEQ;
  binst["blt"]     =OPBLT;
  binst["fbeq"]    =OPFBEQ;
  binst["fblt"]    =OPFBLT;
  
  // binst["bneq"]     =OPNBEQ;
  // binst["bnlt"]     =OPNBLT;
  // binst["fbneq"]    =OPNFBEQ;
  // binst["fbnlt"]    =OPNFBLT;
  
  binst["jmp"]     =OPJMP;
  binst["jr"]      =OPJR;
  FOR(it,binst) it->second <<= 27;
  
  cinst["beqi"]    =OPBEQI;
  cinst["blti"]    =OPBLTI;
  // cinst["bneqi"]    =OPNBEQI;
  // cinst["bnlti"]    =OPNBLTI;
  FOR(it,cinst) it->second <<= 27;
}

unsigned getint(string a){
  int ret = 0;
  FOR(it,a) if(isdigit(*it)) ret=ret*10+*it-'0';
  if(a.find('-')!=string::npos) return -ret;
  return ret;
}

unsigned convertinst(string inst){
  FOR(it,inst) if(*it==',') *it=' ';
  stringstream ss(inst);
  string op,rs,rt,rd,iv,addr,opt1,opt2,opt3;
  ss >> op;
  if(op == "nop") return 0;

  if(iinst.count(op)){
    ss >> rs >> rt >> iv;
    return iinst[op]|(getint(rs)<<21)|(getint(rt)<<15)|(getint(iv)&0x7fff);
  }

  if(rinst.count(op)){
    if(op=="caldr" || op=="cafldr"){
      string iv;
      ss >> rs >> rt >> rd >> iv;
      return
        rinst[op]|(getint(rs)<<21)|(getint(rt)<<15)|(getint(rd)<<9)|(getint(iv)&0x1ff);
    }
    ss >> rs >> rt >> rd >> opt1 >> opt2 >> opt3;
    unsigned funct(0);
    if(opt1=="fpu" || opt2 == "fpu" || opt3 == "fpu") funct |= 1<<0;
    if(opt1=="abs" || opt2 == "abs" || opt3 == "abs") funct |= 1<<1;
    if(opt1=="neg" || opt2 == "neg" || opt3 == "neg") funct |= 1<<2;

    return
      rinst[op]|
      (getint(rs)<<21)|(getint(rt)<<15)|(getint(rd)<<9)|funct|op2funct[op];
  }

  if(binst.count(op)){
    ss >> rs >> rt >> iv;
    if(op == "beq" || op == "blt" ||
       op == "fbeq" || op == "fblt" ||
       op == "jmp"){
      if(!label2pc.count(iv)){
        cerr << "not found label " << iv << endl;
        exit(1);
      }

      return binst[op]|(getint(rs)<<21)|(getint(rt)<<15)|label2pc[iv];
    }

    return
      binst[op]|(getint(rs)<<21)|(getint(rt)<<15)|getint(iv);
  }

  if(cinst.count(op)){
    ss >> rs >> iv >> addr;
    if(!label2pc.count(addr)){
      cerr << "not found label " << addr << endl;
      exit(1);
    }

    return
      cinst[op]|(getint(rs)<<21)|((getint(iv)&0x3f)<<15)|label2pc[addr];
  }

  if(sinst.count(op)){
    ss >> rs >> iv;
    if(op=="setlabel"){
      if(label2pc.count(iv))
        return sinst[op]|(getint(rs)<<21)|label2pc[iv];
      cerr << "not found label " << iv << endl;
      exit(1);
    }
    return
      sinst[op]|(getint(rs)<<21)|(getint(iv)&0xffff);
  }

  cerr << "undefined inst" << inst << endl;
  exit(1);
}

bool checkfail(unsigned inst1,unsigned inst2){
#ifdef FASTSIM
  return false;
#endif  
  unsigned op1=(inst1>>27)&0x1f;
  unsigned op2=(inst2>>27)&0x1f;
  if(op1 == OPBEQ ||
     op1 == OPBLT ||
     op1 == OPFBEQ ||
     op1 == OPFBLT ||
     op1 == OPJMP ||
     op1 == OPJR ||
     op1 == OPBEQI ||
     op1 == OPBLTI ||
     op1 == OPLOADI ||
     op1 == OPSTOREI ||
     op1 == OPFLOADI ||
     op1 == OPFSTOREI){
    cerr << "cant memoly/branch op in left" << endl;
    return true;
  }

  if(op2 == OPCACHELOAD ||
     op2 == OPCACHESTORE ||
     op2 == OPFCACHELOAD ||
     op2 == OPFCACHESTORE ||
     op2 == OPCALOADR ||
     op2 == OPCAFLOADR ||
     op2 == OPRECV ||
     op2 == OPSEND){
    cerr << "cant cache/io op in right" << endl;
    return true;
  }

  return false;
}

vector<int> pc2assembly;

void assemble(){

  int pc = 0;

  FOR(it,assembly){
    int idx = it->find('#');
    string line = *it;
    if(idx != string::npos) line = line.substr(0,idx);
    stringstream ss(line);
    if(!(ss >> line)) continue;
    if(line.find(':') != string::npos){
      label2pc[line.substr(0,line.find(':'))] = pc;
      continue;
    }
    ++pc;
  }

  FOR(it,assembly){
    //cerr << "assemble FOR " << *it << endl;
    int idx = it->find('#');
    string line = *it;
    if(idx != string::npos) line = line.substr(0,idx);

    if(line.find(':') != string::npos) continue;
    int notspace = 0;
    FOR(lit,line) notspace += !isspace(*lit);
    if(!notspace) continue;

    string line1,line2;
    idx = line.find('\\');
    if(idx == string::npos){
      cerr << "only single instruction " << *it << endl;
      exit(1);
    }

    line1 = line.substr(0,idx);
    line2 = line.substr(idx+1);
    string line3;
    idx = line2.find('\\');
    if(idx == string::npos) line3 = "4";
    else {
      line3 = line2.substr(idx+1);
      line2 = line2.substr(0,idx);
    }
    int inst1 = convertinst(line1);
    int inst2 = convertinst(line2);
    if(checkfail(inst1,inst2)){
      cerr << " error near " << it - assembly.begin() << ' ' << *it << endl;
      exit(1);
    }
    instructions.pb(mp(inst1,inst2));
    stallnum.pb(getint(line3));
    /*
    fprintf(stderr,"inst push %08x %08x\n",inst1,inst2);
    fflush(stderr);
    */

    pc2assembly.pb(it-assembly.begin());
  }
}


void setuse(int opcode,int rs,int rt,int* use1,int* use2,int fpucode){
  const int fr = 0x100;
  switch(opcode){
  case OPLOADI:
  case OPFLOADI:
  case OPCACHELOAD:
  case OPFCACHELOAD:
    use1[0] = rt;
    break;
  case OPFPU:
  case OPFBEQ:
  case OPFBLT:
    if(opcode != OPFPU ||
       (fpucode != FUNCTFMV &&
        fpucode != FUNCTFINV &&
        fpucode != FUNCTFSQRT)) use1[0] = rs|fr;
    use2[0] = rt|fr;
    break;
  case OPADDI:
  case OPSHLL:
  case OPSHRA:
    use1[0] = rt;
    break;
  case OPBEQI:
  case OPBLTI:
  case OPSEND:
  case OPJR:
    use1[0] = rs;
    break;
  case OPCALOADR:
  case OPSTOREI:
  case OPCACHESTORE:
  case OPCAFLOADR:
  case OPADD:
  case OPSUB:
  case OPBEQ:
  case OPBLT:
    use1[0] = rs;
    use2[0] = rt;
    break;
  case OPFSTOREI:
  case OPFCACHESTORE:
    use1[0] = rs|fr;
    use2[1] = rt;
    break;
  }
}

typedef union{
  int u;
  float f;
}IF;

IF reg[64],freg[64];
const int sramsize = 1<<19;
IF memory[sramsize];
const int cachesize = 1<<12;
IF softwarecache[cachesize];
vector<ll> lineexnum;
vector<ll> linestall;
vector<ll> branchnum;
ll totalclock;
ll totalstall;

int max_addr = 0;
int max_cache_addr = 0;
//#define cutoff(x) ((x)&((1<<20)-1))
int cutoff(int x) {
	int res = x & ((sramsize)-1);
	max_addr = max(max_addr, res);
	return res;
}

//#define cachecutoff(x) ((x)&((cachesize)-1))
int cachecutoff(int x) {
	int res = x & ((cachesize)-1);
	max_cache_addr = max(max_cache_addr, res);
	return res;
}


void execute(){
  int pc = 0;

  ll instnum[1<<5] = {};
  ll fpunum[1<<3] = {};
  lineexnum=vector<ll>(SZ(assembly));
  linestall=vector<ll>(SZ(assembly));
  branchnum=vector<ll>(SZ(assembly));
  int beff = -1,bef = -1;
  int befff = -1;

#ifdef CHECKSTALL
  int r44=-1,r43=-1,r42=-1,r41=-1,r40=-1;  
  int r33=-1,r32=-1,r31=-1,r30=-1;
  int r22=-1,r21=-1,r20=-1;
  int r11=-1,r10=-1;
  int l44=-1,l43=-1,l42=-1,l41=-1,l40=-1;
  int l33=-1,l32=-1,l31=-1,l30=-1;
  int l22=-1,l21=-1,l20=-1;
  int l11=-1,l10=-1;
#endif

  totalstall = 0;
  totalclock = 0;
  ll doubleinst = 0;
  ll lim = 500000000;

  while(true){
    int cpc = pc;
    if(befff != -1){
      cpc = befff;
      befff = -1;
    }
    pc = cpc + 1;
    if(cpc<0 || cpc>=SZ(pc2assembly)){
      cerr << "pc error " << cpc << endl;
      exit(1);
    }
    int idx = pc2assembly[cpc];
    ++lineexnum[idx];

    string& ass = assembly[idx];
    PU & cinst = instructions[cpc];
    doubleinst += !!cinst.first && !!cinst.second;
    unsigned instruction[]={cinst.first,cinst.second};
    int stall = stallnum[cpc]+1;
    linestall[idx] += stall;
    totalclock += stall;
    totalstall += stall - 1;

    if(totalclock > lim){
      cerr << totalclock << " clock simulated" << endl;
      lim += 500000000;
    }

    if(stall == 1){

      if(befff == -1) befff = beff;
      beff = bef;
      bef = -1;

    }else{

      if(befff != -1) beff=bef=-1;
      else if(beff != -1){
        befff = beff;
        beff = bef = -1;
      }else if(bef != -1){
        befff = bef;
        beff = bef = -1;
      }else befff=beff=bef=-1;

    }

#ifdef CHECKSTALL
    if(stall == 5){
      l40 = l41 = l42 = l43 = l44 = -1;
      l30 = l31 = l32 = l33 = -1;
      l20 = l21 = l22 = -1;
      r40 = r41 = r42 = r43 = r44 = -1;      
      r30 = r31 = r32 = r33 = -1;
      r20 = r21 = r22 = -1;
      l11 = l10 = -1;      
      r11 = r10 = -1;      

    }else if(stall == 4){

      l40 = l44;
      l41 = l42 = l43 = l44 = -1;
      l30 = l31 = l32 = l33 = -1;
      l20 = l21 = l22 = -1;
      r40 = r44;
      r41 = r42 = r43 = r44 = -1;
      r30 = r31 = r32 = r33 = -1;
      r20 = r21 = r22 = -1;
      l11 = l10 = -1;      
      r11 = r10 = -1;      

    }else if(stall == 3){

      l40 = l43;
      l41 = l44;
      l42 = l43 = l44 = -1;
      l30 = l33;
      l31 = l32 = l33 = -1;
      l20 = l21 = l22 = -1;
      r40 = r43;
      r41 = r44;
      r42 = r43 = r44 = -1;
      r30 = r33;
      r31 = r32 = r33 = -1;
      r20 = r21 = r22 = -1;
      l11 = l10 = -1;      
      r11 = r10 = -1;      

    }else if(stall == 2){
      l40 = l42;
      l41 = l43;
      l42 = l44;
      l43 = l44 = -1;

      l30 = l32;
      l31 = l33;
      l32 = l33 = -1;

      l20 = l22;
      l21 = l22 = -1;

      r40 = r42;
      r41 = r43;
      r42 = r44;
      r43 = r44 = -1;

      r30 = r32;
      r31 = r33;
      r32 = r33 = -1;

      r20 = r22;
      r21 = r22 = -1;

      l11 = l10 = -1;      
      r11 = r10 = -1;      

    }else if(stall == 1){
      l40 = l41;
      l41 = l42;
      l42 = l43;
      l43 = l44;
      l44 = -1;

      l30 = l31;
      l31 = l32;
      l32 = l33;
      l33 = -1;

      l20 = l21;
      l21 = l22;
      l22 = -1;

      l10 = l11;
      l11 = -1;

      r40 = r41;
      r41 = r42;
      r42 = r43;
      r43 = r44;
      r44 = -1;
      
      r30 = r31;
      r31 = r32;
      r32 = r33;
      r33 = -1;

      r20 = r21;
      r21 = r22;
      r22 = -1;

      r10 = r11;
      r11 = -1;

    }
#endif

    IF rd[2];
    int rwe[2] = {-1,-1};
    int frwe[2] = {-1,-1};

#ifdef CHECKSTALL
    int use1[2] = {-2,-2};
    int use2[2] = {-2,-2};
#endif

    rep(i,2){
      unsigned& cinst = instruction[i];
      unsigned opcode = (cinst >> 27)&0x1f;
      if(opcode == OPNOP) continue;

      unsigned rsi = (cinst >> 21) & 0x3f;
      unsigned rti = (cinst >> 15) & 0x3f;
      unsigned rdi = (cinst >>  9) & 0x3f;

#ifdef CHECKSTALL
      unsigned funct = cinst & 0x1ff;
      unsigned fpucode = (funct >> 6) & 0x7;
      setuse(opcode,rsi,rti,use1+i,use2+i,fpucode);
#endif

      if(opcode == OPCALOADR || opcode == OPADD ||
         opcode == OPSUB || opcode == OPRECV)
        rwe[i] = rdi;
      else if(opcode == OPSETLO || opcode == OPLOADI ||
              opcode == OPADDI || opcode == OPSHLL || opcode == OPSHRA ||
	      opcode == OPCACHELOAD)
        rwe[i] = rsi;
      else if(opcode == OPCAFLOADR ||
              opcode == OPFPU)
        frwe[i] = rdi;
      else if(opcode == OPFSETLO || opcode == OPFSETHI || opcode == OPFLOADI ||
	      opcode == OPFCACHELOAD)
        frwe[i] = rsi;
    }

    int wr1 = rwe[0];
    int wr2 = rwe[1];


    if(frwe[0] != -1) wr1 = frwe[0] | 0x100;
    if(frwe[1] != -1) wr2 = frwe[1] | 0x100;

#ifdef CHECKSTALL
    if(hazard[cinst.first>>27] == 4) l44 = wr1;
    else if(hazard[cinst.first>>27] == 3) l33 = wr1;
    else if(hazard[cinst.first>>27] == 2) l22 = wr1;
    else if(hazard[cinst.first>>27] == 1) l11 = wr1;
    
    if(hazard[cinst.second>>27] == 4) r44 = wr1;
    else if(hazard[cinst.second>>27] == 3) r33 = wr2;
    else if(hazard[cinst.second>>27] == 2) r22 = wr2;
    else if(hazard[cinst.second>>27] == 1) r11 = wr2;


    if(use1[0] == r43 || use1[0] == r42 || use1[0] == r41 || use1[0] == r40 ||
       use1[0] == r32 || use1[0] == r31 || use1[0] == r30 ||
       use1[0] == r21 || use1[0] == r20 ||
       use1[1] == r43 || use1[1] == r42 || use1[1] == r41 || use1[1] == r40 ||
       use1[1] == r32 || use1[1] == r31 || use1[1] == r30 ||
       use1[1] == r21 || use1[1] == r20 ||
       use1[1] == r43 || use1[1] == r42 || use1[1] == r41 || use1[1] == r40 ||
       use2[0] == r32 || use2[0] == r31 || use2[0] == r30 ||
       use2[0] == r21 || use2[0] == r20 ||
       use2[1] == r43 || use2[1] == r42 || use2[1] == r41 || use2[1] == r40 ||
       use2[1] == r32 || use2[1] == r31 || use2[1] == r30 ||
       use2[1] == r21 || use2[1] == r20 ||
       use1[0] == l43 || use1[0] == l42 || use1[0] == l41 || use1[0] == l40 ||
       use1[0] == l32 || use1[0] == l31 || use1[0] == l30 ||
       use1[0] == l21 || use1[0] == l20 ||
       use1[1] == l43 || use1[1] == l42 || use1[1] == l41 || use1[1] == l40 ||
       use1[1] == l32 || use1[1] == l31 || use1[1] == l30 ||
       use1[1] == l21 || use1[1] == l20 ||
       use2[0] == l43 || use2[0] == l42 || use2[0] == l41 || use2[0] == l40 ||
       use2[0] == l32 || use2[0] == l31 || use2[0] == l30 ||
       use2[0] == l21 || use2[0] == l20 ||
       use2[1] == l43 || use2[1] == l42 || use2[1] == l41 || use2[1] == l40 ||
       use2[1] == l32 || use2[1] == l31 || use2[1] == l30 ||
       use2[1] == l21 || use2[1] == l20){
      fprintf(stderr,"%d %d %d %d\n",r43,r42,r41,r40);      
      fprintf(stderr,"%d %d %d\n",r32,r31,r30);
      fprintf(stderr,"%d %d\n",r21,r20);
      fprintf(stderr,"%d\n",r10);
      fprintf(stderr,"%d %d %d %d\n",l43,l42,l41,l40);
      fprintf(stderr,"%d %d %d\n",l32,l31,l30);
      fprintf(stderr,"%d %d\n",l21,l20);
      fprintf(stderr,"%d\n",l10);
      cerr << use1[0] << ' ' << use1[1] << ' ' << use2[0] << ' ' << use2[1] << endl;
      cerr << "stall num check failure in line " << idx << endl;
      cerr << ass << endl;
      exit(1);
    }
#endif


    rep(i,2){
      unsigned& cinst = instruction[i];
      unsigned opcode = (cinst >> 27)&0x1f;
      if(opcode == OPNOP) continue;
      ++instnum[opcode];

      unsigned rsi = (cinst >> 21) & 0x3f;
      unsigned rti = (cinst >> 15) & 0x3f;
      unsigned rdi = (cinst >>  9) & 0x3f;
      int iv9 = cinst & 0x1ff;
      int iv15 = cinst & 0x7fff;
      int iv16 = cinst & 0xffff;

      if(iv9 & 0x200) iv9 |= (-1) ^ 0xff;
      if(iv15 & 0x4000) iv15 |= (-1) ^ 0x7fff;
      if(iv16 & 0x8000) iv16 |= (-1) ^ 0x7fff;
      int iv6 = rti;
      if(iv6 & 0x20) iv6 |= (-1) ^ 0x1f;
      unsigned addr = cinst & 0x7fff;
      unsigned funct = cinst & 0x1ff;
      unsigned fpucode = (funct >> 6) & 0x7;

      IF rs = reg[rsi],rt = reg[rti];
      IF frs = freg[rsi],frt = freg[rti];

      if(opcode == OPFLOADI){
        rd[i] = memory[cutoff(rt.u + iv15)];
      }else if(opcode == OPCACHELOAD){
        rd[i] = softwarecache[cachecutoff(rt.u + iv15)];
      }else if(opcode == OPFCACHELOAD){
        rd[i] = softwarecache[cachecutoff(rt.u + iv15)];
      }else if(opcode == OPFPU){
        ++fpunum[fpucode];
        if(funct & FPUBIT) frt.f = - frt.f;

        if(fpucode == FUNCTFADD){
          rd[i].f = frs.f + frt.f;
        }else if(fpucode == FUNCTFMUL){
          rd[i].f = FMUL(frs.f,frt.f);
        }else if(fpucode == FUNCTFINV){
          rd[i].f = FINV(frt.f);
        }else if(fpucode == FUNCTFSQRT){
          rd[i].f = FSQRT(frt.f);
        }else if(fpucode == FUNCTFMV){
          rd[i].f = frt.f;
        }else{
          cerr << "unknown fpucode" << endl;
          exit(1);
        }

        if(funct & ABSBIT) rd[i].f = abs(rd[i].f);
        if(funct & NEGBIT) rd[i].f = - rd[i].f;
      }else if(opcode == OPLOADI){
        rd[i] = memory[cutoff(rt.u + iv15)];
      }else if(opcode == OPBEQI){
        if(rs.u == iv6) {
          bef = addr;
#ifdef CHECK_BLOCK
          ++branchnum[idx];
#endif
        }
//       }else if(opcode == OPNBEQI){
//         if(rs.u != iv6) {
//           bef = addr;
// #ifdef CHECK_BLOCK
//           ++branchnum[idx];
// #endif
//         }
        
      }else if(opcode == OPJMP){
        if(pc == addr+1)goto OUTPUT;
        bef = addr;
#ifdef CHECK_BLOCK
        ++branchnum[idx];
#endif        
      }else if(opcode == OPADDI){
        rd[i].u = iv15 + rt.u;
      }else if(opcode == OPCALOADR){
        rd[i] = softwarecache[cachecutoff(rs.u + rt.u + iv9)];
      }else if(opcode == OPFBLT){
        if(frs.f < frt.f){
#ifdef CHECK_BLOCK
          ++branchnum[idx];
#endif          
          bef = addr;
        }
//       }else if(opcode == OPNFBLT){
//         if(frs.f >= frt.f){
// #ifdef CHECK_BLOCK
//           ++branchnum[idx];
// #endif          
//           bef = addr;
//         }        
      }else if(opcode == OPSHLL){
        rd[i].u = rt.u << iv15;
      }else if(opcode == OPCACHESTORE){
        softwarecache[cachecutoff(rt.u + iv15)] = rs;
      }else if(opcode == OPFCACHESTORE){
        softwarecache[cachecutoff(rt.u + iv15)] = frs;
      }else if(opcode == OPSTOREI){
        memory[cutoff(rt.u + iv15)] = rs;
      }else if(opcode == OPFSTOREI){
        memory[cutoff(rt.u + iv15)] = frs;
      }else if(opcode == OPCAFLOADR){
        rd[i] = softwarecache[cachecutoff(rs.u + rt.u + iv9)];
      }else if(opcode == OPADD){
        rd[i].u = rs.u + rt.u;
      }else if(opcode == OPSUB){
        rd[i].u = rs.u - rt.u;
      }else if(opcode == OPRECV){
        int tmp = getchar();
        if(tmp == EOF){
          cerr << "End of file " << endl;
          goto OUTPUT;
        }
        rd[i].u = tmp & 0xff;
      }else if(opcode == OPSEND){
        putchar(rs.u&0xff);
      }else if(opcode == OPSETLO){
        rd[i].u = iv16;
      }else if(opcode == OPFSETLO){
        rd[i].u = (frs.u&0xffff0000) | iv16;
      }else if(opcode == OPFSETHI){
        rd[i].u = (frs.u&0xffff) | (iv16 << 16);
      }else if(opcode == OPBEQ){
        if(rs.u == rt.u){
#ifdef CHECK_BLOCK
          ++branchnum[idx];
#endif          
          bef = addr;
        }
//       }else if(opcode == OPNBEQ){
//         if(rs.u != rt.u){
// #ifdef CHECK_BLOCK
//           ++branchnum[idx];
// #endif          
//           bef = addr;
//         }
      }else if(opcode == OPBLT){
        if(int(rs.u) < int(rt.u)){
#ifdef CHECK_BLOCK
          ++branchnum[idx];
#endif          
          bef = addr;
        }
//       }else if(opcode == OPNBLT){
//         if(int(rs.u) >= int(rt.u)){
// #ifdef CHECK_BLOCK
//           ++branchnum[idx];
// #endif          
//           bef = addr;
//         }
      }else if(opcode == OPFBEQ){
        if(frs.f == frt.f){
#ifdef CHECK_BLOCK
          ++branchnum[idx];
#endif          
          bef = addr;
        }
//       }else if(opcode == OPNFBEQ){
//         if(frs.f != frt.f){
// #ifdef CHECK_BLOCK
//           ++branchnum[idx];
// #endif          
//           bef = addr;
//         }
      }else if(opcode == OPJR){
        bef = rs.u;
      }else if(opcode == OPBLTI){
        if((int)(rs.u) < iv6){
#ifdef CHECK_BLOCK
          ++branchnum[idx];
#endif          
          bef = addr;
        }
//       }else if(opcode == OPNBLTI){
//         if((int)(rs.u) >= iv6){
// #ifdef CHECK_BLOCK
//           ++branchnum[idx];
// #endif          
//           bef = addr;
//         }
        
      }else if(opcode == OPSHRA){
        rd[i].u = ((int)rt.u) >> iv15;
      }else{
        cerr << "unknown opcode " << pc << ' ' << idx << ' ' << opcode << endl;
        exit(1);
      }
    }

    if(rwe[0] != -1) reg[rwe[0]] = rd[0];
    if(rwe[1] != -1) reg[rwe[1]] = rd[1];

    if(frwe[0] != -1) freg[frwe[0]] = rd[0];
    if(frwe[1] != -1) freg[frwe[1]] = rd[1];
  }

 OUTPUT:;

  ll total = 0;

  cerr << endl;
  cerr << "iinstruction num" << endl;
  FOR(it,iinst){
    total += instnum[it->second >> 27];
    fprintf(stderr,"%8s %10lld\n",it->first.c_str(),instnum[it->second >> 27]);
  }
  cerr << endl;

  cerr << "rinstruction num" << endl;
  FOR(it,rinst){
    if(it->second >> 27 == OPFADD && it->first != "fadd") continue;
    else if(it->second >> 27 == OPFADD){
      string fpuname[] = {"fadd","fmul","finv","fsqrt","fmv"};

      rep(i,5){
        total += fpunum[i];
        fprintf(stderr,"%8s %10lld\n",fpuname[i].c_str(),fpunum[i]);
      }
      continue;
    }
    total += instnum[it->second >> 27];
    fprintf(stderr,"%8s %10lld\n",it->first.c_str(),instnum[it->second >> 27]);
  }
  cerr << endl;

  cerr << "binstruction num" << endl;
  FOR(it,binst){
    total += instnum[it->second >> 27];
    fprintf(stderr,"%8s %10lld\n",it->first.c_str(),instnum[it->second >> 27]);
  }
  cerr << endl;

  cerr << "cinstruction num" << endl;
  FOR(it,cinst){
    total += instnum[it->second >> 27];
    fprintf(stderr,"%8s %10lld\n",it->first.c_str(),instnum[it->second >> 27]);
  }
  cerr << endl;

  cerr << "sinstruction num" << endl;
  FOR(it,sinst){
    if(it->first == "setlabel") continue;
    total += instnum[it->second >> 27];
    fprintf(stderr,"%8s %10lld\n",it->first.c_str(),instnum[it->second >> 27]);
  }
  cerr << endl;



  cerr << "total inst  : " << total << endl;
  cerr << "total stall : " << totalstall << endl;
  cerr << "total clock : " << totalclock  << endl << endl;
  cerr << "IPC: " << total*1./totalclock << endl;
  cerr << "CPI: " << totalclock*1./total << endl;

  cerr << endl;
  
  for(int i=6;i<15;++i)
    cerr << "expected time in " << (11.11 * i) << " MHz : " << totalclock/(11.11 * i * 1000000) << " sec " << endl;
  cerr << endl;
  cerr << "for 10 sec execution " << totalclock/10000000.0 << " MHz clock need" << endl;

  cerr << "not contain nop instruction " << doubleinst << endl;
  cerr << endl;
  cerr << "if scalar architecture ... " << endl;
  totalclock += doubleinst;
  cerr << "total clock : " << totalclock  << endl << endl;
  cerr << "IPC: " << total*1./totalclock << endl;
  cerr << "CPI: " << totalclock*1./total << endl;

  cerr << endl;
  
  for(int i=6;i<15;++i)
    cerr << "expected time in " << (11.11 * i) << " MHz : " << totalclock/(11.11 * i * 1000000) << " sec " << endl;
  cerr << endl;
  cerr << "for 10 sec execution " << totalclock/10000000.0 << " MHz clock need" << endl;

  cerr << "maximum used address of SRAM : " << max_addr << endl;
  cerr << "maximum used address of CACHE: " << max_cache_addr << endl;
}

void coeout(ofstream& fout,bool vliw){
  fout << "MEMORY_INITIALIZATION_RADIX=2;" << endl;
  fout << "MEMORY_INITIALIZATION_VECTOR=" << endl;
  rep(i,SZ(instructions)){
    if(vliw){
      unsigned long long fi,se;
      fi=instructions[i].first;
      se=instructions[i].second;
      unsigned left=0,right=(stallnum[i]<<1)&0xf;
      left =   fi     &0xf8000000;
      left |= (fi<<1 )&0x07c00000;
      left |= (fi<<2 )&0x003ffffc;
      left |= (se>>30)&0x00000003;

      right |= (se<<2 )& 0xe0000000;
      right |= (se<<3 )& 0x1f000000;
      right |= (se<<4 )& 0x00fffff0;
      
      rep(j,32){
        fout << ((left>>(31-j))&1);
      }
      rep(j,31){
        fout << ((right>>(31-j))&1);
      }
      
    }else rep(j,32) fout << (((instructions[i].first|instructions[i].second)>>(31-j))&1);
    /*//rep(j,3) fout << ((stallnum[i]>>(2-j))&1);*/
    if(i+1<SZ(instructions)) fout << ',' << endl;
    else fout << ';' << endl;
  }
}

void int32out(ofstream& fout,unsigned a){
  fout << char((a>>24)&0xff);
  fout << char((a>>16)&0xff);
  fout << char((a>> 8)&0xff);
  fout << char((a>> 0)&0xff);
}

void binout(ofstream& fout,bool vliw){
  int n = SZ(instructions);
  int32out(fout,n);
  rep(i,n){
    unsigned long long fi,se;
    fi=instructions[i].first;
    se=instructions[i].second;
    unsigned left=0,right=(stallnum[i]<<1)&0xf;    
    left =   fi     &0xf8000000;
    left |= (fi<<1 )&0x07c00000;
    left |= (fi<<2 )&0x003ffffc;
    left |= (se>>30)&0x00000003;

    right |= (se<<2 )& 0xe0000000;
    right |= (se<<3 )& 0x1f000000;
    right |= (se<<4 )& 0x00fffff0;
    
    if(vliw){
      int32out(fout,right);
      int32out(fout,left);      
      //int32out(fout,fi);
      //int32out(fout,se);
    }else int32out(fout,instructions[i].second|instructions[i].first);
    //stallnum[i] = 3;
    //fout << char(stallnum[i]&0xff);
  }
}

void assembleout(ofstream& fout){
  fout << "library ieee;" << endl;
  fout << "use ieee.std_logic_1164.all;" << endl;
  fout << "package inst is" << endl;
  fout << "  type ramtype is array(" << SZ(instructions) 
       << " downto 0) of std_logic_vector(31 downto 0);" << endl;
  
  fout << "  constant inst1mem : ramtype := (" << endl;
  rep(i,SZ(instructions)){
    char out[100];
    sprintf(out,"  %6d => x\"%08X\",",i,instructions[i].first);
    fout << out << endl;
  }
  fout << "  others => x\"00000000\");" << endl << endl;

  fout << "  constant inst2mem : ramtype := (" << endl;
  rep(i,SZ(instructions)){
    char out[100];
    sprintf(out,"  %6d => x\"%08X\",",i,instructions[i].second);
    fout << out << endl;
  }
  fout << "  others => x\"00000000\");" << endl << endl;
  

  fout << "  type stalltype is array(" << SZ(instructions) 
       << " downto 0) of std_logic_vector(2 downto 0);" << endl << endl;


  fout << "  constant stallmem : stalltype := (" << endl;
  rep(i,SZ(instructions)){
    char out[100];
    int v = stallnum[i];
    sprintf(out,"  %6d => \"%d%d%d\",",i,v/4,v/2&1,v&1);
    fout << out << endl;
  }
  fout << "  others => \"000\");" << endl;
  
  fout << "end inst;" << endl;
}

void blockout(ofstream& fout){
  set<string> branchinst;
  branchinst.insert("beqi");
  branchinst.insert("blti");
  branchinst.insert("beq");
  branchinst.insert("blt");    
  branchinst.insert("fbeq");
  branchinst.insert("fblt");
  branchinst.insert("jmp");
  branchinst.insert("jr");

  branchinst.insert("bneqi");
  branchinst.insert("bnlti");
  branchinst.insert("bneq");
  branchinst.insert("bnlt");    
  branchinst.insert("fbneq");
  branchinst.insert("fbnlt");


  int cidx = 0;
  while(cidx < SZ(assembly) && assembly[cidx].find(":") == string::npos) ++cidx;

  while(cidx < SZ(assembly)){
    ll blockexnum = lineexnum[cidx+1];
    fout << assembly[cidx].substr(0,SZ(assembly[cidx])-1) << ' ' << blockexnum << ' ';
    vector<pair<string,ll> > outp;
    ++cidx;
    
    while(cidx < SZ(assembly)){
      if(assembly[cidx].find(":") != string::npos)
        break;
      if(assembly[cidx].find(" jr ") != string::npos) blockexnum = 0;
      bool ok = false;
      stringstream ss(assembly[cidx]);
      string word;
      bool branchinstapp = false;
      while(ss >> word){
        if(branchinstapp && label2pc.count(word)){
          outp.pb(mp(word,branchnum[cidx]));
          blockexnum -= branchnum[cidx];
          break;
        }
        branchinstapp |= branchinst.count(word);
      }
      ++cidx;
    }
    
    if(cidx<SZ(assembly))
      outp.pb(mp(assembly[cidx].substr(0,SZ(assembly[cidx])-1),blockexnum));
    fout << SZ(outp);
    FOR(it,outp) fout << ' ' << it->first << ' ' << it->second;
    fout << endl;
  }
}

int main(int argc,char **argv){
  if(argc < 2){
    cerr << " file name needed" << endl;
    return 1;
  }

  ifstream fin(argv[1]);
  if(!fin){
    cerr << " not found " << argv[1] << endl;
    return 1;
  }

  string str;
  while(getline(fin,str)) assembly.pb(str);

  init();
  assemble();
  cerr << "assembly complete " << SZ(instructions) << " word * 2 instructions" << endl;

  const char* coesuf=".coe";
  if(argc>2 && string(argv[2]).find(coesuf) == strlen(argv[2])-strlen(coesuf)){
    ofstream fout(argv[2]);
    if(!fout){
      cerr << "cant open " << argv[2] << endl;
      exit(1);
    }

    coeout(fout,argc<4);
    return 0;
  }
  
  const char* binsuf=".bin";
  if(argc>2 && string(argv[2]).find(binsuf) == strlen(argv[2])-strlen(binsuf)){
    ofstream fout(argv[2]);
    if(!fout){
      cerr << "cant open " << argv[2] << endl;
      exit(1);
    }
    binout(fout,argc<4);
    return 0;
  }  
  
  const char* vhdsuf=".vhd";
  if(argc>2 && string(argv[2]).find(vhdsuf) == strlen(argv[2])-strlen(vhdsuf)){
    ofstream fout(argv[2]);
    if(!fout){
      cerr << "cant open " << argv[2] << endl;
      exit(1);
    }    
    assembleout(fout);
    return 0;
  }
  execute();

  if(argc>2 && string(argv[2]).find(".block") != string::npos) {
    ofstream fout(argv[2]);
    if(!fout){
      cerr << "cant open " << argv[2] << endl;
      exit(1);
    }        
    blockout(fout);
    return 0;
  }
  
  if(argc>2){
    ofstream fout(argv[2]);
    if(!fout){
      cerr << "cant open " << argv[2] << endl;
      exit(1);
    }
    ll printedclock = 0;
    int bef = -1;
    rep(i,SZ(assembly)){
      bool flag = false;
      const int wid = 5;
      for(int j=max(0,i-wid);j<min(SZ(assembly),i+wid+1);++j)
        flag |= lineexnum[j] >=1000000;
      
      if(flag){
        if(i != bef+1) fout << endl;
        fout << setw(5) << right << i << ' ' << assembly[i] << ' ' << setw(8) << right << linestall[i] << ' ' << linestall[i]*1.0/totalstall << endl;
        bef = i;        
        printedclock += linestall[i];
      }
    }
    fout << "printed part clock " << printedclock << endl;
    fout << endl;
    rep(i,SZ(assembly)){
      bool flag = true;
      const int wid = 5;
      if(flag){
        if(i != bef+1) fout << endl;
        fout << setw(5) << right << i << ' ' << assembly[i] << ' ' << setw(8) << right << linestall[i] << ' ' << linestall[i]*1.0/totalstall << endl;
        bef = i;
        printedclock += linestall[i];
      }
    }
  }
}

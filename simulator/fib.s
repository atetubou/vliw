main:
        setlo r3 11 \ jmp 0 0 fib \ 0
        setlabel r4 aftercallfib \ nop \ 1
fib:
        addi r5,r3,-2 \ blti r3 2 ret \ 0
        addi r2 r1 0 \ casti r3 r1 0 \ 0
        nop \ casti r4 r1 1 \ 0
        addi r3 r3 -1\ jmp 0 0 fib \ 0
        nop \ addi r1 r1 3 \ 0
        setlabel r4 fib1 \ casti r5 r2 2 \ 0
fib1:
        nop \ caldi r3 r1 -1 \ 0
        nop \ jmp 0 0 fib \ 0
        setlabel r4 fib2 \ casti r6 r1 -3 \ 1
fib2:
        nop \ caldi r5 r1 -2 \ 0
        nop \ caldi r4 r1 -3 \ 0
        addi r1 r1 -3 \ jr r5 0 0 \ 0
        add r6 r4 r6 \ nop \ 0
        nop \ nop \ 0
ret:
        addi r6 r3 0 \ jr r4 0 0 \ 0
        nop \ nop \ 1
aftercallfib:
        send r6 0 0 \ nop \ 0
halt:
        nop \  jmp 0 0 halt \ 0
        nop \ nop \ 1
   
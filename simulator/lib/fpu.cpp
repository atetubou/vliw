#include "fpu.h"
#include "finv_table.h"
#include "sqrt_table.h"

static int mask(fi_t op, unsigned int from, unsigned int to)
{
    unsigned int range = to - from + 1;
    op.i &= ((1 << range)-1) << from;
    return (op.i >> from);
}

static int mask(unsigned int op, unsigned int from, unsigned int to)
{
    fi_t tmp;
    tmp.i = op;
    return mask(tmp,from,to);
}

static int mask(int op, unsigned int from, unsigned int to)
{
    fi_t tmp;
    tmp.i = op;
    return mask(tmp,from,to);
}

static int mask(float op, unsigned int from, unsigned int to)
{
    fi_t tmp;
    tmp.f = op;
    return mask(tmp,from,to);
}

float fmul_sim(float fa,float fb)
{
    fi_t res,a,b;
    a.f = fa;
    b.f = fb;
    res.i = ((a.i >> 31)^(b.i >> 31)) << 31;
    int hh = (mask(a,11,22)+(1 << 12))*(mask(b,11,22)+(1 << 12));
    int hl = (mask(a,11,22)+(1 << 12))*(mask(b,0,10));
    int lh = (mask(b,11,22)+(1 << 12))*(mask(a,0,10));
    int exp = mask(a,23,30) + mask(b,23,30) + 129;
    unsigned long long frac = hh + (hl >> 11) + (lh >> 11) + 2;
    if(frac&(1 << 25)) exp++;
    if(mask(a,23,30)==0||mask(b,23,30)==0){
        res.i = 0;
    }
    else if(!(exp&(1 << 8))){
        res.i = 0;
    }
    else if(mask(exp,0,7)==0){
        res.i = 0;
    }
    else if(frac&(1<<25)){
        res.i += mask((int)frac,2,24);
        res.i += (exp&((1 << 8)-1)) << 23;
    }
    else{
        res.i += mask((int)frac,1,23);
        res.i += (exp&((1 << 8)-1)) << 23;
    }
    return res.f;

}

static int finvman(int i)
{
    return p[i >> 13] - ((((long long)q[i >> 13] ) * (i - ((i >> 13) << 13)))>>12)+ (1 << 23);
}

float finv_sim(float a)
{
    fi_t res;
    unsigned int exp = 253 - mask(a,23,30);
    res.i = (mask(a,31,31) << 31);
    res.i += exp << 23;
    res.i += mask(finvman(mask(a,0,22)),0,22);
    if(mask(a,23,30)==0||mask(res.i,23,30)==0) res.i = 0;
    return res.f;
}

static int sqrtman(int i)
{
    return sqrt_p[i >> 14] + ((((long long)sqrt_q[i >> 14] ) * mask(i,0,13)+(1 << 11))>>13);
}

float sqrtf_sim(float x)
{
    fi_t res,tar;
    tar.f = x;
    unsigned int exp = mask((((unsigned int)mask(tar,23,30))-127) >> 1,0,7);
    int frac = mask(sqrtman(mask(tar,0,23)),0,22);
    
    res.i = (mask((exp+127),0,7) << 23)+frac;
    if(mask(x,23,30)==0||mask(res.i,23,30)==0) res.i = 0;
    return res.f;
}

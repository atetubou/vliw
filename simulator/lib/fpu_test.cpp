#include "fpu.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <math.h>

using namespace std;


static int mask(fi_t op, unsigned int from, unsigned int to)
{
    unsigned int range = to - from + 1;
    op.i &= ((1 << range)-1) << from;
    return (op.i >> from);
}
static void print_fi(fi_t x)
{
    for(int i=31;i>=0;i--){
        cout << mask(x,i,i);
        if(i==31||i==23) cout << ' ';
    }
    cout << endl;
}

static float randf()
{
    unsigned int a = (((int)rand()) >> 2)%(1<<11);
    unsigned int b = (((int)rand()) >> 2)%(1<<12);
    unsigned int c;
    unsigned int d = (((int)rand()) >> 2)%(2);
    do{
         c= (((int)rand()) >> 2)%(1<<8);
    }while(c <= 0 || c >= 255);
    fi_t res;
    res.i = (d << 31)|(c << 23)|(b << 11)|a;
    return res.f;
}

int main(int argc, char **argv)
{
    fi_t a,b,c;
    srand(time(NULL));
    int num;
    if(argc < 2) num = 100;
    else num = atoi(argv[1]);
    for(int i=0;i<num;i++){
        a.f = randf();
        if(a.f < 0) a.f *= -1;
        b.f = sqrt(a.f);
        c.f = sqrtf_sim(a.f);
        if(abs((int)b.i -(int)c.i) > 4 ){
            print_fi(a);
            print_fi(b);
            print_fi(c);
            cout << "error" << endl;
        }
    }
    return 0;
}

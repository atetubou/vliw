#ifndef FPU_H
#define FPU_H

#ifdef __cplusplus
extern "C" {
#endif

typedef union {
  float f;
  unsigned int i;
} fi_t;

float fmul_sim(float a, float b);
float finv_sim(float a);
float sqrtf_sim(float a);
#ifdef __cplusplus
}
#endif

#endif
